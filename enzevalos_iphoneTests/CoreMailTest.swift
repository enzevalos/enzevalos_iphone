//
//  CoreMailTest.swift
//  enzevalos_iphoneTests
//
//  Created by Oliver Wiese on 30.09.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import XCTest

/**
 TODO:
 Test attachments
 Test keys stuff. Update keys etc.
 */

@testable import enzevalos_iphone
class CoreMailTest: XCTestCase {

    var provider = PersistentDataProvider()
    let path = "Testfolder"
    
    
    private func deleteAll() {
        provider = PersistentDataProvider()
        let deleteExpectation = expectation(description: "Delete all data!")
        provider.deleteAll(completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing addresses! \(error)")
            }
            deleteExpectation.fulfill()
        })
        wait(for: [deleteExpectation], timeout: TimeInterval(20))
        provider.reset()
        let frc = provider.fetchedMailResultsController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 0)
        provider = PersistentDataProvider()
    }
    
    override func setUpWithError() throws {
        deleteAll()
    }

    override func tearDownWithError() throws {
        deleteAll()
    }
    
    /**
     We test if two mails from the same sender is added to the sender as inFromMails.
     */
    func testImportMail(){
        let addr = "vic@example.com"
        let addr1 = AddressProperties(email: addr)
        let folder = FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: "Testfolder", parent: nil, children: nil, flags: 0)
        
        let importExpectation = expectation(description: "Import new msg!")

        
        let m1 = MailProperties(messageID: "1", uid: UInt64(arc4random()), subject: "MSG1", date: Date(), flags: 0, from: addr1, folder: folder, signatureState: 0, encryptionState: 0, attachedPublicKeys: [], attachedSecretKeys: [])
        let m2 = MailProperties(messageID: "2", uid: UInt64(arc4random()), subject: "MSG2", date: Date(), flags: 0, from: addr1, folder: folder, signatureState: 0, encryptionState: 0)

        provider.importNewData(from: [m1,m2], completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing messages! \(error)")
            }
            importExpectation.fulfill()})
        wait(for: [importExpectation], timeout: TimeInterval(20))
        self.provider.reset()
        try? provider.fetchedMailResultsController.performFetch()
        // Problem  Data sind fault...
        if let mails = provider.fetchedMailResultsController.fetchedObjects {
            for m in mails {
                XCTAssertEqual(m.fromAddress?.email ?? "", addr, "Wrong email from address: \(m.fromAddress?.email ?? "-1")")
            }
        }
        else {
            XCTFail("No mails...")
        }
        
        let frc = provider.fetchedAddressResultController
        if let objects = frc.fetchedObjects {
            for obj in objects {
                XCTAssertEqual(obj.inFromField?.count, 2, "Missing mails.. \(String(describing: obj.inFromField?.count))")
            }
        } else {
            XCTFail("No addresses...")
        }
    }
    
    func testDuplicateMails() {
        let addr1 = AddressProperties(email: "vic@example.com")
        let folder = FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: "Testfolder", parent: nil, children: nil, flags: 0)

        let m1 = MailProperties(messageID: "1", uid: UInt64(arc4random()), subject: "MSG1", date: Date(), flags: 0, from: addr1, folder: folder, signatureState: 0, encryptionState: 0)
        let m2 = MailProperties(messageID: "1", uid: UInt64(arc4random()), subject: "Second MSG1", date: Date(), flags: 0, from: addr1, folder: folder, signatureState: 0, encryptionState: 0)
        
        provider.reset()
        let frc = provider.fetchedMailResultsController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 0, "Too much messages! \(frc.fetchedObjects?.count ?? 0)")
        let importExpectation = expectation(description: "Import new data!")
        provider.importNewData(from: [m1,m2], completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing messages! \(error)")
            }
            importExpectation.fulfill()
        })
        wait(for: [importExpectation], timeout: TimeInterval(20))
        provider.reset()
        do {
            try frc.performFetch()
        } catch {
            XCTFail("Could not fetch! \(error)")
        }
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 1, "Missing message! \(frc.fetchedObjects?.count ?? 0)")
        if let obj = frc.fetchedObjects?.first {
            XCTAssert(obj.subject == "Second MSG1",  "Wired subject: \(obj.subject)")// || subject == "MSG1", "Wired subject: \(subject)")
        } else {
            XCTFail("No messages!")
        }
    }
    
    func testDuplicateMails2() {
        let addr1 = AddressProperties(email: "vic@example.com")
        let folder = FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: "Testfolder", parent: nil, children: nil, flags: 0)

        let m1 = MailProperties(messageID: "1", uid: UInt64(arc4random()), subject: "MSG1", date: Date(), flags: 0, from: addr1, folder: folder, signatureState: 0, encryptionState: 0)
        let m2 = MailProperties(messageID: "1", uid: UInt64(arc4random()), subject: "Second MSG1", date: Date(), flags: 0, from: addr1, folder: folder, signatureState: 0, encryptionState: 0)
        
        provider.reset()
        let frc = provider.fetchedMailResultsController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 0, "Too much messages! \(frc.fetchedObjects?.count ?? 0)")
        var importExpectation = expectation(description: "Import new data!")
        provider.importNewData(from: [m1], completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing messages! \(error)")
            }
            importExpectation.fulfill()
        })
        wait(for: [importExpectation], timeout: TimeInterval(20))
        
        importExpectation = expectation(description: "Import new data!")
        provider.importNewData(from: [m2], completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing messages! \(error)")
            }
            importExpectation.fulfill()
        })
        wait(for: [importExpectation], timeout: TimeInterval(20))
        provider.reset()
        do {
            try frc.performFetch()
        } catch {
            XCTFail("Could not fetch! \(error)")
        }
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 1, "Missing message! \(frc.fetchedObjects?.count ?? 0)")
        if let obj = frc.fetchedObjects?.first {
            XCTAssertEqual(obj.subject, "Second MSG1", "We updated the message...")
        } else {
            XCTFail("No messages!")
        }
        
    }


    /**
     Tests to import multiple Mails in one Fetch
     */
    func testImportMails() {
        let n = 1000
        
        let addr1 = AddressProperties(email: "vic@example.com")
        let addr2 = AddressProperties(email: "alex@example.com")
        let addrs = [addr1, addr2]
        let folder = FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: "Testfolder", parent: nil, children: nil, flags: 0)

        
        var msgs = [MailProperties]()
        for i in 1...n {
            let a = addrs[i%addrs.count]
            let m = MailProperties(messageID: "\(i)", uid: UInt64(arc4random()), subject: "MSG\(i)", date: Date(), flags: 0, from: a, folder: folder, signatureState: 0, encryptionState: 0)
            msgs.append(m)
        }
        
        provider.reset()
        let frc = provider.fetchedMailResultsController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 0, "Too much messages! \(frc.fetchedObjects?.count ?? 0)")
        let importExpectation = expectation(description: "Import new data!")
        provider.importNewData(from: msgs, completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing messages! \(error)")
            }
            importExpectation.fulfill()
        })
        wait(for: [importExpectation], timeout: TimeInterval(20))
        provider.reset()
        do {
            try frc.performFetch()
        } catch {
            XCTFail("Could not fetch! \(error)")
        }
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, n, "Missing message! \(frc.fetchedObjects?.count ?? 0)")
        if let folders = provider.fetchedFolderResultsController.fetchedObjects {
            XCTAssert(folders.count == 1, "We have only one folder! folders: \(folders.count)")
            if let f = folders.first {
                XCTAssertEqual(f.path, "Testfolder")
                XCTAssertEqual(f.mailsInFolder?.count, n)
            }
        } else{
            XCTFail("No folders!")
        }
    
    }
    
    
    private func generateMails(n: Int = 100, m: Int = 10) -> Int {
        var k = 1
        
        let addr1 = AddressProperties(email: "vic@example.com")
        let addr2 = AddressProperties(email: "alex@example.com")
        let addrs = [addr1, addr2]
        let folder = FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: "Testfolder", parent: nil, children: nil, flags: 0)
        var expectations = [XCTestExpectation]()
        
        var res = 0
        for _ in 1...m {
            var msgs = [MailProperties]()
            for i in 1...n {
                let a = addrs[i%addrs.count]
                let msg = MailProperties(messageID: "\(k)", uid: UInt64(arc4random()), subject: "MSG\(k)", date: Date(), flags: 0, from: a, folder: folder, signatureState: 0, encryptionState: 0)
                k = k + 1
                res = res + 1
                msgs.append(msg)
            }
            
           
            let importExpectation = expectation(description: "Import new data!")
            expectations.append(importExpectation)
            provider.importNewData(from: msgs, completionHandler: {error in
                if let error = error {
                    XCTFail("Error while importing messages! \(error)")
                }
                importExpectation.fulfill()
            })
        }
        wait(for: expectations, timeout: TimeInterval(60))
        
        provider.reset()
        let frc = provider.fetchedMailResultsController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, res, "Too much messages! \(frc.fetchedObjects?.count ?? 0)")
        do {
            try frc.performFetch()
        } catch {
            XCTFail("Could not fetch! \(error)")
        }
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, res, "Missing message! \(frc.fetchedObjects?.count ?? 0)")
        return res
    }
    
    func testImportMoreMails() {
        let n = generateMails()
        
        if let folders = provider.fetchedFolderResultsController.fetchedObjects {
            XCTAssert(folders.count == 1, "We have only one folder! folders: \(folders.count)")
            if let f = folders.first {
                XCTAssertEqual(f.path, "Testfolder")
                XCTAssertEqual(f.mailsInFolder?.count, n)
            }
            else{
               XCTFail("No folders!")
           }
        } else{
            XCTFail("No folders!")
        }
    }
    
    func testFetchFolder() {
        var n = generateMails()
        let genMails = n
        // CONSIDER FETCHLIMIT!
        if n > PersistentDataProvider.FETCHLIMIT {
            n = PersistentDataProvider.FETCHLIMIT
        }
        let frc = provider.generateFetchedMailsInFolderResultsController(folderpath: path)
        try? frc.performFetch()
        if let mails = frc.fetchedObjects {
            var i = 1
            for m in mails {
                XCTAssertEqual(m.messageID, "\(i)")
                i = i + 1
                XCTAssertEqual(m.inFolder?.path, "Testfolder")
            }
            XCTAssertEqual(mails.count, n)
        }
        else {
            XCTFail("No mails...")
        }
        let frc2 = provider.generateFetchedFolderResultsController(folderpath: path)
        try? frc2.performFetch()
        if let folder = frc2.fetchedObjects?.first {
            XCTAssertEqual(folder.mailsInFolder?.count ?? -1, genMails)
        }
    }
    
    func testMoveMail() {
        let n = generateMails(n: 100, m: 1)
        let folderPath2 = "Testfolder2"
        let frc = provider.generateFetchedMailsInFolderResultsController(folderpath: path)
        try? frc.performFetch()
        if let mails = frc.fetchedObjects {
            XCTAssertEqual(mails.count, n)
            if let mail = mails.first {
                provider.moveMails(with: [UInt64(mail.uID)], from: path, to: folderPath2)
                try? frc.performFetch()
                XCTAssertEqual(frc.fetchedObjects?.count ?? 0,( n - 1))
                let frc2 = provider.generateFetchedMailsInFolderResultsController(folderpath: folderPath2)
                XCTAssertEqual(frc2.fetchedObjects?.count ?? 0, 1)
            }
        }
        else {
            XCTFail("No mails...")
        }
        
    }
}
