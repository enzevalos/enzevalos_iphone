//
//  ToggleableTag.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 22.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

/// a tag on which the user can tap to (de-)activate it
/// mostly used for filters 
struct ToggleableTag<UI:View>: View {
    
    @Binding var isActive : Bool
    
    var content : ()->UI
    
    var body: some View {
        Button(action: {isActive.toggle()}){
            content()
                .foregroundColor(isActive ? (Color(UIColor.systemBackground)) : .accentColor )
                .padding(.horizontal).padding(.vertical, 7)
        }.background(
            Capsule()
                .fill(isActive ? Color.accentColor : Color.gray)
                .opacity(isActive ? 0.9 : 0.1)
        )
    }
}

struct ToggleableTag_Previews: PreviewProvider {
    static var previews: some View {
        StatefulPreviewWrapper(true){
            ToggleableTag(isActive:$0){Text("pdf")}
        }
    }
}
