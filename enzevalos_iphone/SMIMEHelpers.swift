//
//  SMIMEHelpers.swift
//  enzevalos_iphone
//
//  Created by lazarog98 on 19.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation



class SMIMEError : Error {
    enum ErrorType {
        case fingerPrint
        case encryption
        case decryption
        case verification
        case signing
        case other
        case missingKey
        case noData
    }
    
    func errorArrayToString() -> [String] {
        var strArr: [String] = []
        
        for error in self.errorArray ?? [] {
            strArr.append(getErrorString(errCode: error))
        }
        
        return strArr
    }
    
    let message: String?
    let errorArray: [UInt]?
    let type: ErrorType
    
    init(message: String?, errorArray: [UInt]?, type: ErrorType) {
        self.message = message
        self.errorArray = errorArray
        self.type = type
    }
}


/**
 Returns a fingerprint hash of a certificate given a PEM
 
 - parameters:
    - pem: cerificate's PEM as a string
 
 - returns:a tuple of the form (fingerprint, nil, array of errors)
 */
func getFingerprintFromPem (pem: String) -> (String?, [String]?, [UInt]?) {
    let res = get_fingerprint_from_pem(pem, 0);
    defer {
        deallocateResult(res: res)
    }
    
    let result = res?.pointee;
    
    let fpStr: String? = result?.extractField(field: ResultAttribute.output)
    let errArr: [UInt]? = result?.extractField(field: ResultAttribute.errors)
    
    return (fpStr, nil, errArr)
}

 /**
 Exrtracts a private key from an ecnrypted PEM
  
  - parameters:
    - pem: key's PEM as a string
    - passwd: password to decrypt the PEM
  - returns:(<decrypted PEM>, <array of errors>)
  */
 func getPKeyFromEncryptedPem(pem: String, passwd: String) -> (String?, [UInt]?) {
     let res = get_pkey_from_enc_pem(pem, passwd)
     defer {
         deallocateResult(res: res)
     }
     
     let result = res?.pointee
     
     let pKey: String? = result?.extractField(field: ResultAttribute.output)
     let errArr: [UInt]? = result?.extractField(field: ResultAttribute.errors)
     
    return (Key: pKey, Error: errArr)
 }

/**
 Encrypts a private key's PEM with a password (encryption key)
 
 - parameters:
   - pem: key's PEM as a string
   - passwd: password for the encryption
 - returns:(<encrypted PEM>, <array of errors>)
 */
 func getEncryptedPemFromPKey(pem: String, passwd: String) -> (String?, [UInt]?) {
     let res = get_enc_pem_from_pkey(pem, passwd)
     defer {
         deallocateResult(res: res)
     }
     
     let result = res?.pointee
     
     let pKey: String? = result?.extractField(field: ResultAttribute.output)
     let errArr: [UInt]? = result?.extractField(field: ResultAttribute.errors)
     
     return (pKey, errArr)
 }

/**
 Returns the complete string of an OpenSSL error. Includes things such as reason, function that caused it and so on
 
 - parameters;
    - errCode: the error code
 */
func getErrorString(errCode: UInt) -> String {
    let cStr = get_err_string(errCode);
    defer {
        cStr?.deallocate()
    }
    
    if (cStr != nil) {
        let str = String( cString: cStr!)
        return str;
    }
    return "Invalid error code!"
}

/**
 Returns the reason part of the string of an OpenSSL error.
 
 - parameters;
    - errCode: the error code
 */
func getErrorReasonString(errCode: UInt) -> String {
    let cStr = get_err_reason_string(errCode);
    defer {
        cStr?.deallocate()
    }
    
    if (cStr != nil) {
        let str = String( cString: cStr!)
        return str;
    }
    return "Invalid error code!"
}

/**
 Returns the list of all e-mails from an array of certificates. Each email has only one instance in the array.
 
 - parameters:
    - certs: an array of PEMs
 
 - returns:an array of emails
 */
func getAllEmailsFromPEMs(certs: [String]) -> [String] {
    var certObjects = [Certificate]()
    for pem in certs {
        if let cert = try? Certificate(pem: pem) {
            certObjects.append(cert)
        }
    }
    
    var addrs : [String:Bool] = [:]
    for c in certObjects
    {
        for e in c.eMails!
        {
            addrs[e] = true
        }
    }
    
    return Array(addrs.keys)
}

func deallocateResult(res: UnsafeMutablePointer<result>?) {
    deallocate_result(res)
}

func deallocateCertInfo(certInfo: UnsafeMutablePointer<cert_info>?) {
    deallocate_cert_info(certInfo)
}

enum ResultAttribute {
    case certificates
    case errors
    case output
    case fingerprints
}

/**
 A structure returned by the C crypto functions
 
 This is not intended to be used outside of the SMIME library for letterbox. Look at the encryption/decryption functions for example usages
 */
extension result {
    /**
    Extracts an array of certs in PEM of the respective function from the result object
    Deallocates the "errors" part of the result object
    Only use in combination with_extractOutput and _extractErrors
    */
    private func extractStringArray(attrType: ResultAttribute) -> [String]? {
        let arr: UnsafeMutablePointer<UnsafeMutablePointer<Int8>?>?
        switch (attrType) {
        case .certificates:
            arr = self.certs
        case .fingerprints:
            arr = self.fingerprints
        default:
            // TODO: Exceptions
            return nil
        }
        var strArr: [String]? = nil
        
        if arr != nil {
            strArr = []
            let size = self.num_certs
            for i in 0..<size {
                let str = String(cString: arr![Int(i)]!)
                strArr?.append(str)
            }
        }
        
        return strArr
    }

    /**
    Extracts an array of errors of the respective function from the result object
    Deallocates the "errors" part of the result object
    Only use in combination with_extractOutput
    */
    private func extractErrors() -> [UInt]? {
        let arr = self.errors
        var errArr: [UInt]? = nil
        
        if arr != nil {
            errArr = []
            let size = self.num_errors
            for i in 0..<size {
                errArr?.append(arr![Int(i)])
            }
        }
        
        return errArr
    }
    
    /**
    Extracts the output of the respective function from the result object
    Deallocates the "res" part of the result object
    Only use in combination with_extractErrors
     */
    private func extractOutput() -> String? {
         let cStr = self.res
         
         var swiftStr: String? = nil
         
         if cStr != nil {
             swiftStr = String(cString: cStr!)
         }
         
         return swiftStr
    }
        
    /**
     A generic function that reads a field and casts it to the type of the variable that stores the result. The variable must therefore have the correct type. Look at encrypWithPem() for example usage
     
     - parameters:
        - field: enum, specifiying the field to extract
     */
    func extractField<T>(field: ResultAttribute) -> T? {
        switch field {
        case .certificates:
            fallthrough
        case .fingerprints:
            let res = self.extractStringArray(attrType: field)
            if res is T? {
                return res as! T?
            }
            break
        case .errors:
            let res = self.extractErrors()
            if res is T? {
                return res as! T?
            }
        case .output:
            let res = self.extractOutput()
            if res is T? {
                return res as! T?
            }
        }
        
        // TODO: exception
        return nil
    }
}

extension String {
    func extractPattern(pattern: String) -> [String]{
        let range = NSRange(location: 0, length: self.utf16.count)
        let regex = try! NSRegularExpression(pattern: pattern)
        let check = regex.matches(in: self, options: [], range: range)
        var foundLinks:[String] = []
        for match in check{
            guard let range = Range(match.range, in: self) else {continue}
            foundLinks.append(String(self[range]))
        }
        return (foundLinks)
    }
}
