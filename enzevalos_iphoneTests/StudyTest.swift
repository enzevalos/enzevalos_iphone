//
//  StudyTest.swift
//  enzevalos_iphoneTests
//
//  Created by Oliver Wiese on 29.01.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import XCTest

@testable import enzevalos_iphone
class StudyTest: XCTestCase {
    override func setUp() {
        super.setUp()
        self.reset()
    }
    
    override func tearDown() {
        super.tearDown()
        self.reset()
    }
    
    
    private func reset() {
        for p in StudySettings.parameters {
            _ = p.reset()
        }
    }
    
    func testMultipleSecurityIndicator() {
        for _ in 0...100 {
            testSecurityIndicator()
        }
    }
    
    func testSecurityIndicator(){
        testOneParameter(parameter: SecurityIndicator.self)
        let secIndicator: SecurityIndicator = SecurityIndicator.load() as! SecurityIndicator
        XCTAssertEqual(secIndicator.rawValue, StudySettings.securityIndicator.rawValue)
        testDistribution(parameter: SecurityIndicator.self)
    }
    
    func testOneParameter(parameter: StudyParameterProtocol.Type) {
        StudySettings.parameters = [parameter]
        
        for para in StudySettings.allAvailableParameters {
            if para.self != parameter.self {
                XCTAssertEqual(para.defaultValue.value, para.load().value)
                if para == Inviation.self {
                    XCTAssertEqual(para.defaultValue.value, StudySettings.invitationsmode.value)
                }
            }
        }
    }
    
    func testSecurityIndicatorAndWarning() {
        testMultipleIndicators(parameters: [SecurityIndicator.self, Warning.self])
        let secIndicator: SecurityIndicator = SecurityIndicator.load() as! SecurityIndicator
        XCTAssertEqual(secIndicator.rawValue, StudySettings.securityIndicator.rawValue)
        
    }
    
    func testSecurityIndicatorAndInvitation() {
        testMultipleIndicators(parameters: [SecurityIndicator.self, Inviation.self])
        let secIndicator: SecurityIndicator = SecurityIndicator.load() as! SecurityIndicator
        XCTAssertEqual(secIndicator.rawValue, StudySettings.securityIndicator.rawValue)
        let invMode: Inviation = Inviation.load() as! Inviation
        XCTAssertEqual(invMode.value, StudySettings.invitationsmode.value)
    }
    
    func testMultipleIndicators(parameters: [StudyParameterProtocol.Type]) {
        StudySettings.parameters = parameters
        for para in StudySettings.allAvailableParameters {
            var isPara = false
            for x in parameters {
                if para.keyName == x.keyName {
                    isPara = true
                }
            }
            if !isPara {
                XCTAssertEqual(para.defaultValue.value, para.load().value)
                if para == Inviation.self {
                    XCTAssertEqual(para.defaultValue.value, StudySettings.invitationsmode.value)
                }
            }
        }
    }
    
    func testDistribution(parameter: StudyParameterProtocol.Type) {
        var values: [Int] = Array(repeating: 0, count: Int(parameter.numberOfTreatments))
        let n = 100
        let mean = Double(n) / Double(parameter.numberOfTreatments)
        let threshold = mean * 0.2
        
        self.reset()
        for _ in 0...n {
            self.testOneParameter(parameter: parameter)
            let v = parameter.load().value
            values[v] = values[v] + 1
            self.reset()
        }
        for v in values {
            XCTAssertLessThan(v, Int(mean + threshold), "n: \(n) | values: \(values)")
            XCTAssertGreaterThan(v, Int(mean - threshold))
        }
    }
}
