//
//  general-helpers.c
//  enzevalos_iphone
//
//  Created by lazarog98 on 18.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//


#include "generic-helpers.h"

void bio_to_str(BIO *bio_in, char **out) {
    char * tmp = NULL, *tmp2 = NULL;
    long size = BIO_get_mem_data(bio_in, &tmp);
    tmp2 = malloc(size+1);
    tmp2[size]=0; // To Nullterminate the string
    memcpy(tmp2, tmp, size);
    *out = tmp2;
}

int get_array_size(array_with_length *arr) {
    return arr->size;
}

char *get_err_string(unsigned long err) {
    char * error_permanent = malloc(256);
    ERR_error_string_n(err, error_permanent, 256);
    
    return error_permanent;
}

char *get_err_reason_string(unsigned long err) {
    const char * reason_str = ERR_reason_error_string(err);
    char * error_permanent = (char *) malloc(strlen(reason_str)+1);
    error_permanent[strlen(reason_str)]=0; // To Nullterminate the string
    memcpy(error_permanent,reason_str,strlen(reason_str));
    
    return error_permanent;
}

char ** init_str_arr(int num)
{
    char ** arr = malloc(num*sizeof(char*));
    return arr;
}

char ** add_str_to_arr(const char *str, char **arr, int i)
{
    unsigned long len = strlen(str);
    char *newstr = malloc(len+1);
    memcpy(newstr, str, len);
    newstr[len]=0;
    arr[i]=newstr;
    return arr;
}

void deallocate_str_arr(char **arr, int len)
{
    for (int i=0; i < len; i++){
        free(arr[i]);
    }
    free(arr);
}

char *bin_to_hex(unsigned char *bin, int len)
{
    unsigned char buf[3];
    char *res = malloc(2*len+1);
    for (int i =0;i<len;i++)
    {
        sprintf((char*) buf, "%02x", bin[i]);
        res[i*2]=buf[0];
        res[i*2+1]=buf[1];
    }
    res[len*2]=0;
    return res;
}

result *new_result()
{
    result *res = malloc(sizeof(result));
    res->certs = NULL;
    res->errors = NULL;
    res->num_certs =0;
    res->num_errors =0;
    res->certs = NULL;
    res->fingerprints = NULL;
    return res;
    
}

void deallocate_result(result *res) {
    if (res->num_certs > 0) {
        if (res->certs != NULL) deallocate_str_arr(res->certs, res->num_certs);
        if (res->fingerprints != NULL) deallocate_str_arr(res->fingerprints, res->num_certs);
    }
    if (res->num_errors > 0) {
        free(res->errors);
    }
    if (res->res != NULL) {
        free(res->res);
    }
    free(res);
}

