//
//  AttachmentRecord.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 10.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

extension AttachmentRecord {
    static let entityName = "AttachmentRecord"

    func update(property: AttachmentProperties) {
        self.type = property.type
        self.name = property.name
        self.mimeType = property.mimeType
        self.contentID = property.contentID
        self.data = property.data
    }
}

extension AttachmentRecord: DisplayAttachment {
    var myName: String {
        return name ?? "NoName!"
    }
    
    var myData: Data {
        return data ?? Data()
    }
    
    
}
