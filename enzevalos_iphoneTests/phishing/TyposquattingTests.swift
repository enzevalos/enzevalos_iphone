//
//  TyposquattingTests.swift
//  enzevalos_iphoneTests
//
//  Created by Lauren Elden on 30.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import XCTest

@testable import enzevalos_iphone

class TyposquattingTests: XCTestCase {
    
    let typosquatting = Typosquatting()
    
    // Setup for testcases
    let validRD = ["fu-berlin.de", "google.de", "nljbjkjk.de", "w3schools.com", "w2schools.com", "w8schools.com", "w33schools.com", "w22schools.com", "w99schools.eu.com", "3schools.com"]
    
    let validSLD = ["fu-berlin", "google", "nljbjkjk", "w3schools", "w2schools", "w8schools", "w33schools", "w22schools", "w99schools.eu", "3schools"]
    
    let validURLs = ["www.fu-berlin.de", "www.google.de", "nljbjkjk.com", "www.w3schools.com", "www.w2schools.com", "www.w8schools.de", "www.w33schools.com", "www.w22schools.com", "www.w99schools.eu.com", "www.3schools.com"]

    let testMailText = "https://git.imp.fu-berlin.de/enzevalos/enzevalos_iphone/issues/240http:// www.google.de http://nljbjkjk.de https://www.w3schools.com https://www.w2schools.com https://www.w8schools.com  https://www.w33schools.com https://www.w22schools.com https://www.w99schools.eu.com https://git.imp.fu-ber_lin.de/enzevalos/enzevalos_iphone/issues/240 http://www.google-.de http://nljbj#kjk.de https://www.3schools.com https://www.w2sc..hools.com https://www.w8sch?ools.com https://www.w33sc_hools.com  https://www.w22schooIs.com  https://www.W99schools$.com"
    

    func testIsValidRD() {
        let pattern = typosquatting.isValidRD(mailBody: testMailText)
        XCTAssertNotNil(pattern)
        XCTAssertEqual(pattern[0], validRD[0])
        XCTAssertEqual(pattern[1], validRD[1])
        XCTAssertEqual(pattern[2], validRD[2])
        XCTAssertEqual(pattern[3], validRD[3])
        XCTAssertEqual(pattern[4], validRD[4])
        XCTAssertEqual(pattern[5], validRD[5])
        XCTAssertEqual(pattern[6], validRD[6])
        XCTAssertEqual(pattern[7], validRD[7])
        XCTAssertEqual(pattern[8], validRD[8])
        XCTAssertEqual(pattern[9], validRD[9])
    }
    
    func testCompareDomainWithDomians() {
        let domain = "w2schools"
        XCTAssertFalse(typosquatting.compareDomainWithDomians(secondLvlDomain: domain, domains: validSLD, allowedEditDistance: 4))
    }
    
    func testGetDomainEditDistance() {
        let domain = "w2schools"
        print(typosquatting.getDomainEditDistanceAsDic(domain: domain, domains: validSLD))
    }
    
    func testCompareDomainWithDomiansDic() {
        let domain = "w2schools"
        print(typosquatting.compareDomainWithDomiansAsDic(secondLvlDomain: domain, domains: validSLD, allowedEditDistance: 4))
    }
    
    func testGetSLDEditdistance() {
        let url = "www.w2schools.com"
        print(typosquatting.getSLDEditdistanceAsDic(url: url, domains: validSLD))
    }
    
    func testGetURLEditdistanceAsDic() {
        let url = "www.w2schools.com"
        print(typosquatting.getURLEditdistanceAsDic(url: url, urls: validURLs))
    }
    
    func testCompareURLWithSLDListAsDic() {
        let url = "www.w2schools.com"
        print(typosquatting.compareURLWithSLDListAsDic(url: url, domains: validSLD, allowedEditDistance: 4))
    }
    
    func testCompareURLWithSLDList() {
        let url = "www.google.com"
        let domains = ["googIe", "google", "gogle", "gooGle", "gle", "glllll"]
        XCTAssertFalse(typosquatting.compareURLWithSLDList(url: url, domains: domains))
    }
    
    func testCompareURLsAsDic() {
        let url = "www.google.com"
        print(typosquatting.compareURLsAsDic(url: url, urls: validURLs, allowedEditDistance: 4))
    }
    
    func testCompareURLs() {
        let url = "www.w2schools.com"
        let (bool, _) = typosquatting.compareURLs(myUrl: url, urls: validURLs)
        XCTAssertFalse(bool)
    }
}
