//
//  FeedbackButtonHelper.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 18.10.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation

enum ViewName: String {
    case Inbox, Read, Compose, Key, Contact, Folder, Management, Information, SecretKeyTable, Paste, KeyTable, Import, Export1, Export2
}

class FeedbackButtonHelper {
    
    static let Name = NSLocalizedString("Feedback.Name", comment: "Feedback")
    
    static var SpaceButton: UIBarButtonItem {
        get {
            return UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        }
    }
    
    static func PrefilledMail(view: ViewName) -> PreMailData {
        let receiver = SUPPORT_MAIL_ADR
        let subject = String(format: NSLocalizedString("Feedback.Mail.Subject", comment: "Feedback"), view.rawValue)
        let body = String(format: NSLocalizedString("Feedback.Mail.Body", comment: ""), view.rawValue)
        return PreMailData(body: body, subject: subject, to: [receiver], cc: [], bcc: [])
    }
}
