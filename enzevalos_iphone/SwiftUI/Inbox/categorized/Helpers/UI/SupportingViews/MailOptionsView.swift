//
//  MailOptionsView.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 08.02.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

///the mechanics for the extension at the bottom
fileprivate struct MailOptionsWrapper<Content:View> : View {
    let mail : eMail
    let extraOptions : [Button<Label<Image,Text>>]
    let content: ()->Content
    
    @State var showPossibleCategories : Bool = false
    
    var body: some View {
        ZStack{
            NavigationLink(destination: dst, isActive: $opens) { EmptyView() }
            content()
                .contextMenu{menu}
        }
    }
    
    @ViewBuilder private var menu : some View {
        Button(action: {
            mail.isRead.toggle()
        }){
            Label(NSLocalizedString("Mark as unread", comment: ""), systemImage: "envelope.badge")
        }
        Button(action: {
            setDst(ReadMainView(model: ReadModel(mail:mail)))
            open()
        }){
            Text(NSLocalizedString("Info", comment: ""))
            Image(systemName: "info.circle.fill")
        }
        
        Divider()
        
        if (!mail.isSingle){
            Button(action: {
                newMail(to: mail.ccs)
            }){
                Text(NSLocalizedString("Reply all", comment: ""))
                Image(systemName: "arrowshape.turn.up.left.2.fill")
            }
        }
        Button(action: {
            newMail(to: [mail.fromAddress])
        }){
            Text(NSLocalizedString("Reply", comment: ""))
            Image(systemName: "arrowshape.turn.up.left.fill")
        }
        Button(action: {
            newMail(to: [])
        }){
            Text(NSLocalizedString("Forward", comment: ""))
            Image(systemName: "arrowshape.turn.up.right.fill")
        }
        
        Divider()
        
        Button(action: {
            showPossibleCategories.toggle()
            //TODO: sadly this instantly collapses (not only this but every press on a contextMenu makes it collapse)
        }){
            Text(NSLocalizedString("Open in", comment: ""))
            Image(systemName: showPossibleCategories ? "chevron.up" : "chevron.down")
        }
        
        if showPossibleCategories {
            ForEach(CategoryViewModelProvider.s.viewModelList.array){ vm in
                if vm.filter(mail) >= .canOpen && !vm.isActive {
                    Button(action: {
                        vm.isActive = true
                        //TODO: open this specific mail or at least highlight it
                    }){
                        Text(vm.name)
                        vm.constData.icon
                    }
                }
            }
        }
        
        
    }
    
    func open() { opens = true }
    @State  private var opens   : Bool = false
    @State private var dst     : AnyView = AnyView()
    func  newMail(to receivers : [AddressRecord?]){
        let receivers = receivers.compactMap{$0}.map{$0.email}
        setDst(
            ComposeView(
                preData: PreMailData(to: receivers)
            )
        )
        open()
    }
    private func setDst<V:View>(_ dst : V) { self.dst = AnyView(dst) }
    
}



// MARK: - PREVIEW
struct ContentView: View {
    @State var showsAlert = false
    var body: some View {
        VStack {
            Text("Man stelle sich vor das hier ist eine eMail")
        }
        //.mailOptions(mail: mail)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

// MARK: - extensions
extension View {
    func mailOptions(mail: eMail, extraOptions: [Button<Label<Image,Text>>] = [] ) -> some View {
        MailOptionsWrapper(mail: mail, extraOptions: extraOptions , content:{self})
    }
}

extension View {
    ///if the user holds a mail some mail options pop up (like reply, info etc.)
    func capsuledMailOptions(mail: eMail, extraOptions: [Button<Label<Image,Text>>] = [] ) -> some View {
        self.padding(10)
            .background(Capsule().fill(Color.gray.opacity(0.0001)))
            .mailOptions(mail: mail, extraOptions: extraOptions)
            .padding(-10)
    }
}
