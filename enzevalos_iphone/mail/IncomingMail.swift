//
//  IncomingMail.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 06.03.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation

enum PGPPart {
    case MESSAGE
    case PUBLICKEY
    case SIGNATURE
    case SECRETKEY
    case SIGNEDMESSAGE
    
    var start: String {
        get {
            return pgpBlock(start: true)
        }
    }
    
    var end: String {
        get {
            return pgpBlock(start: false)
        }
    }
    
    private func pgpBlock(start: Bool) -> String{
        var type = "END"
        if start {
            type = "BEGIN"
        }
        switch self {
        case .MESSAGE:
            return "-----\(type) PGP MESSAGE-----"
        case .PUBLICKEY:
            return "-----\(type) PGP PUBLIC KEY BLOCK-----"
        case .SIGNATURE:
            return "-----\(type) PGP SIGNATURE-----"
        case .SECRETKEY:
            return "-----\(type) PGP PRIVATE KEY BLOCK-----"
        case .SIGNEDMESSAGE:
            return "-----\(type) PGP SIGNED MESSAGE-----"

        }
    }
    
    func findPGPPartInString(content: String) -> [String]{
        var parts = [String]()
        let startString = self.start
        let endString = self.end
        if content.contains(startString) {
            if let start = content.range(of: startString) {
                var end = content.range(of: endString+"\n")
                if end == nil {
                    end = content.range(of: endString)
                }
                if end == nil && self == .SIGNEDMESSAGE {
                    end = content.range(of: "-----END PGP")
                }
                if let end = end {
                    let s = start.lowerBound
                    let e1 = start.upperBound
                    
                    let s2 = end.lowerBound
                    let e = end.upperBound
                    var partString: String
                    var part = content[s..<e]
                    if self == .PUBLICKEY || self == .SECRETKEY {
                        // Remove whitespaces in data
                        partString = String(content[s..<e1]).appending(String(content[e1..<s2]).replacingOccurrences(of: " ", with: "")).appending(  String(content[s2..<e]))
                    } else {
                        if self == .SIGNEDMESSAGE {
                            part = content[s...]
                        }
                        partString = String(part)
                    }
                    
                    partString = partString.replacingOccurrences(of: "\r\n", with: "\n")
                    partString = partString.replacingOccurrences(of: "\n", with: "\r\n")
                    partString = partString.trimmingCharacters(in: .whitespaces)
                    // We do not consider parts of previous mails.
                    if !part.contains(">"){
                        parts.append(partString)
                    }
                    
                }
            }
        }
        return parts
    }
}

enum PgpMIMEType {
    case SIGNATURE
    case ENCRYPTED
    case OCTET
    case KEYS
    
    static let allValues = [PgpMIMEType.ENCRYPTED, PgpMIMEType.KEYS, PgpMIMEType.OCTET, PgpMIMEType.SIGNATURE]
    
    var name: String {
        get{
            switch self {
            case .SIGNATURE:
                return "application/pgp-signature"
            case .ENCRYPTED:
                return "application/pgp-encrypted"
            case .OCTET:
                return "application/octet-stream"
            case .KEYS:
                return "application/pgp-keys"
            }
        }
    }
    
    func isTypeOf(attachment: MCOAttachment) -> Bool {
        return attachment.mimeType == self.name
    }
    
    static func findType(attachment: MCOAttachment) -> PgpMIMEType? {
        return findType(name: attachment.mimeType)
    }
    
    static func findType(name: String) -> PgpMIMEType? {
        for type in PgpMIMEType.allValues {
            if type.name == name {
                return type
            }
        }
        return nil
    }
}

class IncomingMail {
    private let rawData: Data
    private var msgParser: MCOMessageParser
    private let firstParser: MCOMessageParser
    // Header fields
    private let flags: MCOMessageFlag
    private let uID: UInt64
    private let folderPath: String
    private var moveToFolderPath: String?
    
    private var rec: [MCOAddress] = []
    private var cc: [MCOAddress] = []
    private var bcc: [MCOAddress] = []
    private var from: MCOAddress?
    private var fromKeyIds: [String] {
        get{
            var keyIds: [String] = []
            keyIds.append(contentsOf: newAutocrypPublicKeys)
            keyIds.append(contentsOf: newPublicKeys)
            if let fromAdr = from?.mailbox{
                if let adr = PersistentDataProvider.dataProvider.generateFetchedAddressResultsController(address: fromAdr).fetchedObjects?.first {
                    for id in adr.keyIDs {
                        keyIds.append(id)
                    }
                }
            }
            return keyIds
        }
    }
    private var attachedSignature: Data? = nil
    private var subject: String = ""
    private var date: Date = Date()
    private var autocrypt: Autocrypt? = nil
    private var references: [String] = []
    private var boundary: String?
    var msgID: String = ""
    private var userAgent: String = ""
    private var mimeVersion: String = ""
    private var received: String = ""
    private var contentType: String = ""
    private var contentTransferEncoding: String = ""
    private var contentDisposition: String = ""
    private var dkimSignature: String = ""
    private var inReplyTo: String = ""
    private var xMailer: String = ""
    
    
    // Body info
    private var secretKey: String? = nil
    private var html: String = ""
    private var lineArray: [String] = []
    var cryptoObj: CryptoObject? = nil
    private var body: String = ""
    private var encryptedBody: String?
    private var secretKeys: [String] = []
    private var relatedSecrectKey: SecretKeyRecord? = nil
    private var newPublicKeys: [String] = []
    private var newAutocrypPublicKeys: [String] = []
    private var newSecretKeyIDs: [String]? = []
    
    private var readableAttachments = Set<TempAttachment>()
    private var useAttachments = Set<TempAttachment>()
 

    
    // Crypto info
    private var storeEncrypted = false
    var cryptoData: Data {
        get {
            if let data = body.data(using: String.Encoding.utf8, allowLossyConversion: true) {
                return data
            }
            return msgParser.data()
        }
    }
    
    var decryptionKeyIDs: [String] {
        get {
            let secretkeys = PersistentDataProvider.dataProvider.fetchedSecretKeyResultsController.fetchedObjects ?? []
            var decIds = [String]()
            for sk in secretkeys {
                if let id = sk.fingerprint {
                    decIds.append(id)
                }
            }
            return decIds
        }
    }
    
    var signatureKeyIDs: [String] {
        get {
            return fromKeyIds
        }
    }
    
    var signatureAddr: String? {
        get {
            if let adr = from?.mailbox {
                return adr
            }
            return nil
        }
    }
    
    var hasSecretKeys: Bool {
        get {
            return !secretKeys.isEmpty
        }
    }
    
    init(rawData: Data, uID: UInt64, folderPath: String, flags: MCOMessageFlag){
        self.rawData = rawData
        self.uID = uID
        self.folderPath = folderPath
        self.flags = flags
        self.firstParser = MCOMessageParser(data: rawData)
        self.msgParser = firstParser
        self.parseHeader()
        if subject == "Autocrypt Setup Message" {
           body = "Not supported yet. Coming soon."
        } else {
            self.parseBody()
        }
    }
    
    func export() -> MailProperties {
        let to = rec.map{$0.export()}
        let ccProperties = cc.map{$0.export()}
        let bccProperties = bcc.map{$0.export()}
        
        let folder = FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: self.folderPath, flags: 0)
        let from = self.from?.export() ?? nil
        let sigState = self.cryptoObj?.signatureState.rawValue ?? 0
        var sigKey: PublicKeyProperties? = nil
        if let cryptoObj = cryptoObj, let keyId = cryptoObj.signKey {
            let addr = cryptoObj.signedAdrs
            sigKey = PublicKeyProperties(fingerprint: keyId, cryptoProtocol: cryptoObj.encType, origin: .AutocryptHeader, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: nil, usedAddresses: addr.map({AddressProperties(email: $0)})) //TODO FIXXXX!!!!!
        }
        let encState = self.cryptoObj?.encryptionState.rawValue ?? 0
        
        var lastsignedDate: Date? = nil
        if let sigState = self.cryptoObj?.signatureState, sigState == .ValidSignature{
            lastsignedDate = self.date
        }
        var usedAddr = [AddressProperties]()
        if let from = from {
            usedAddr.append(from)
        }
        
        var autocryptPK = [PublicKeyProperties]()
        for fpr in newAutocrypPublicKeys {
            var signed: Date? =  nil
            if let sigendKeys = self.cryptoObj?.signedKeys, sigendKeys.contains(fpr) {
                signed = lastsignedDate
            }
            let pk = PublicKeyProperties(fingerprint: fpr, cryptoProtocol: .PGP, origin: .AutocryptHeader, preferEncryption: autocrypt?.prefer_encryption, lastSeenInAutocryptHeader: date, lastSeenSignedMail: signed, secretKeyProperty: nil, usedAddresses: usedAddr)
            autocryptPK.append(pk)
        }
        
        var attPK = [PublicKeyProperties]()
        for fpr in newPublicKeys {
            let pk = PublicKeyProperties(fingerprint: fpr, cryptoProtocol: .PGP, origin: .MailAttachment, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: nil, usedAddresses: usedAddr)
            attPK.append(pk)
        }
        
        
        var attachments = [AttachmentProperties]()
        for att in readableAttachments{
            // TODO add missing fields
            let property = AttachmentProperties(type: 0, name: att.name, mimeType: att.mimeType.rawValue, contentID: "", data: att.data)
            attachments.append(property)
        }
        let f = Int16(self.flags.rawValue)
        
        var m = MailProperties(messageID: self.msgID, uid: uID, subject: subject, date: date, flags: f, from: from, to: to, cc: ccProperties, bcc: bccProperties, folder: folder, body: body, signatureState: sigState, encryptionState: encState, signatureKey: sigKey, decryptionKey: nil, autocryptHeaderKey: autocryptPK, attachedPublicKeys: attPK, attachedSecretKeys: [])
            // TODO: FIX KEYS
        m.to = to
        m.cc = ccProperties
        m.bcc = bccProperties
        m.attachments = attachments
        return m
    }
    
    private func handleSMIMEMail(smimeObj: CryptoObject) {
        // We do not allow nested SMIME/PGP stuff...
        guard self.cryptoObj == nil else {
            readableAttachments = parseUserReadableAttachments(parser: msgParser)
            plainTextBody()
            return
        }
        self.cryptoObj = smimeObj
        if smimeObj.signatureState == .ValidSignature, let signatureKeyFingerprint = smimeObj.signKey, let sender =  self.from?.mailbox {
            // Valid signature -> add signature key to sender
            let property = PublicKeyProperties(fingerprint: signatureKeyFingerprint, cryptoProtocol: .SMIME, origin: .MailAttachment, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: self.date, secretKeyProperty: nil, usedAddresses: [AddressProperties(email: sender)])
            PersistentDataProvider.dataProvider.importNewData(from: [property], completionHandler: {(error) in print(error ?? "")})
        }
        msgParser = MCOMessageParser(data: smimeObj.decryptedData)
        
        readableAttachments = parseUserReadableAttachments(parser: msgParser)
        plainTextBody()
    }
    
    private func parseBody() {
        var isEncrypted = false
        var isSigned = false
        var encData: Data?
        var publicKeyRaw: [String] = []
        var secretKeyRaw: [String] = []
        var signaturesRaw: [String] = []
        let pgp = SwiftPGP()
        
        if let cryptoObject = cryptoObj, cryptoObject.encryptionState == .ValidedEncryptedWithCurrentKey || cryptoObject.encryptionState == .ValidEncryptedWithOldKey {
            readableAttachments = parseUserReadableAttachments(parser: msgParser, sentEncrypted: true)
            useAttachments = readableAttachments.filter({$0.mimeType == .travelUse})
        }
        
        readableAttachments = parseUserReadableAttachments(parser: msgParser)

        if let cryptoObj = extractEncSMIMEState() {
            handleSMIMEMail(smimeObj: cryptoObj)
            return
        } else if let cryptoObj = extractSignedSMIMEState() {
            handleSMIMEMail(smimeObj: cryptoObj)
            return
        } else {
            // TODO: Refactor... do pgp stuff somewhere else...
            for a in msgParser.attachments() {
                if let attachment = a as? MCOAttachment {
                    if let pgpType = PgpMIMEType.findType(attachment: attachment) {
                        if pgpType == PgpMIMEType.ENCRYPTED {
                            isEncrypted = true
                        }
                        else if pgpType == PgpMIMEType.OCTET {
                            encData = attachment.data
                        }
                        else if pgpType == PgpMIMEType.SIGNATURE {
                            isSigned = true
                            signaturesRaw.append(contentsOf: IncomingMail.extractPGPSignature(attachment: attachment))
                        }
                    }
                publicKeyRaw.append(contentsOf: IncomingMail.findPublicKeys(attachment: attachment))
                secretKeyRaw.append(contentsOf: IncomingMail.findSecretKeys(attachment: attachment))
                }
            }
            body = msgParser.plainTextBodyRenderingAndStripWhitespace(false)
                    
            
            publicKeyRaw.append(contentsOf:IncomingMail.extractPublicKeys(text: body))
            secretKeyRaw.append(contentsOf:IncomingMail.extractSecretKeys(text: body))
            signaturesRaw.append(contentsOf: IncomingMail.extractPGPSignature(text: body))
            if !signaturesRaw.isEmpty {
                isSigned = true
            }
            if let encData = encData, isEncrypted {
                // We drop plaintext keys.
                publicKeyRaw = []
                secretKeyRaw = []
                msgParser = MCOMessageParser(data: encData)
                prepareDecryption()
                let cryptoObject = pgp.decrypt(mail: self)
                encryptedBody = cryptoObject.chiperString
                mergeCryptoObject(newCryptoObj: cryptoObject)
                msgParser = MCOMessageParser(data: cryptoObject.decryptedData)
                return parseBody()
            }
            else if isSigned {
                var inlineSigned = false
                var signedStrings = extractSignedParts(text: msgParser.plainTextBodyRendering())
                if signedStrings.isEmpty {
                    signedStrings = IncomingMail.extractSignedMessage(text: body)
                    inlineSigned = true
                }
                
                var text: String?
                var signedObject: CryptoObject?
                if signedStrings.count == 1 {
                    text = signedStrings.first
                }
                else if signedStrings.count > 1 {
                    text = signedStrings.joined(separator: "\r\n")
                }
                if let signedData = text?.data(using: .utf8) { // No Signed Data!
                    // First Signature as main attachedSignature for mail -- not optimal
                    if let sig = signaturesRaw.first {
                        self.attachedSignature = try? Armor.readArmored(sig)
                    }
                    for sig in signaturesRaw {
                        if let signature = try? Armor.readArmored(sig), let adr = from?.mailbox {
                            for id in fromKeyIds {
                                if inlineSigned {
                                    signedObject = pgp.verify(data: signedData, attachedSignature: nil, verifyId: id, fromAdr: adr)
                                }
                                else {
                                    signedObject = pgp.verify(data: signedData, attachedSignature: signature, verifyId: id, fromAdr: adr)
                                }
                                if let ob = signedObject, ob.signatureState == .ValidSignature {
                                    break
                                }
                            }
                            if let signedObject = signedObject {
                                mergeCryptoObject(newCryptoObj: signedObject)
                                if signedObject.signatureState != .NoSignature {
                                    if !inlineSigned || (inlineSigned && signedObject.signatureState == .ValidSignature) {
                                        msgParser = MCOMessageParser(data: signedData)
                                        return parseBody()
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                plainTextBody()
                let chiphers = IncomingMail.extractInlinePGP(text: body)
                // We consider only the first encrypted part! TODO: improve
                if let text = chiphers.first, let data = text.data(using: .utf8) {
                    msgParser = MCOMessageParser(data: data)
                    let cryptoObject = pgp.decrypt(mail: self)
                    encryptedBody = cryptoObject.chiperString
                    mergeCryptoObject(newCryptoObj: cryptoObject)
                    msgParser = MCOMessageParser(data: cryptoObject.decryptedData)
                    return parseBody()
                }
            }
            newPublicKeys = pgp.importKeys(keys: publicKeyRaw)
            secretKeys.append(contentsOf: secretKeyRaw)
        }
    }
    
    
    private func mergeCryptoObject(newCryptoObj: CryptoObject) {
        if let oldCrypto = cryptoObj {
            var sigState = oldCrypto.signatureState
            var encState = oldCrypto.encryptionState
            var signKey = oldCrypto.signKey
            var signedAddr = oldCrypto.signedAdrs
            
            if newCryptoObj.signatureState.rawValue > sigState.rawValue {
                // TODO: What about dropped information? Users are confused?
                sigState = newCryptoObj.signatureState
                signKey = newCryptoObj.signKey
                signedAddr = newCryptoObj.signedAdrs
            }
            if newCryptoObj.encryptionState.rawValue > encState.rawValue {
                encState = newCryptoObj.encryptionState
            }
            cryptoObj = CryptoObject(chiphertext: oldCrypto.chiphertext, plaintext: newCryptoObj.plaintext, decryptedData: newCryptoObj.decryptedData, sigState: sigState, encState: encState, signKey: signKey, encType: newCryptoObj.encType, signedAdrs: signedAddr)
        }
        else {
            cryptoObj = newCryptoObj
        }
        
    }
    
    private func prepareDecryption() {
        if let data = msgParser.data(), let text = String(data: data, encoding: .utf8) {
            body = text
        }
        else {
            html = msgParser.plainTextRendering()
            lineArray = html.components(separatedBy: "\n")
            lineArray.removeFirst(4)
            body = lineArray.joined(separator: "\n")
            body = body.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            body.append("\n")
        }
    }
    
    private func plainTextBody() {
        var text = ""
        if let main = msgParser.mainPart(), main.mimeType == "text/plain", let attachment = main as? MCOAttachment, let body = attachment.decodedString() {
            let pattern = ["Content-Type: \\w*\\/?\\w*;", "charset=\\\"\\S*\\\"", "Content-Transfer-Encoding:.?\\S*\r\n","Content-Disposition:.?\\w*\r\n"]
            text = body
            for p in pattern {
                text = text.replacingOccurrences(of: p, with: "", options: .regularExpression)
            }
        }
        if text == "", let body = msgParser.plainTextBodyRenderingAndStripWhitespace(false) {
            text = body
        }
        body = text
        var c = body.first
        while (c != nil && CharacterSet.whitespacesAndNewlines.contains((c?.unicodeScalars.first)!)) {
            body.removeFirst()
            c = body.first
        }
        c = body.last
        while (c != nil && CharacterSet.whitespacesAndNewlines.contains((c?.unicodeScalars.first)!)) {
            body.removeLast()
            c = body.last
        }
    }
    
    private func parseHeader() {
        if let header = msgParser.header {
            if let refs = header.references {
                for ref in refs{
                    if let string = ref as? String {
                        references.append(string)
                    }
                }
            }
            if let tos = header.to {
                for r in tos{
                    rec.append(r as! MCOAddress)
                }
            }
            if let ccs = header.cc {
                for r in ccs {
                    cc.append(r as! MCOAddress)
                }
            }
            if let bccs = header.bcc {
                for a in bccs {
                    bcc.append(a as! MCOAddress)
                }
            }
            
            if let _ = header.extraHeaderValue(forName: Autocrypt.AUTOCRYPTHEADER){
                let autocrypt = Autocrypt(header: header)
                self.autocrypt = autocrypt
                let pgp = SwiftPGP()
                let keys = try? pgp.importKeys(key: autocrypt.key, pw: nil, isSecretKey: false, autocrypt: true)
                if let keys = keys {
                    newAutocrypPublicKeys = keys
                }
            }
            
            if let f = header.from {
                from = f
            }
            if let sub = header.subject {
                subject = sub
            }
            if let d = header.date {
                date = d
            }
            if let id = header.messageID {
                msgID = id
            }
            if let agent = header.userAgent {
                userAgent = agent
            }
            if let mime = header.extraHeaderValue(forName: HeaderExtractionValues.MIMEVersion.rawValue) {
               mimeVersion = mime
            }
            if let received_h =  header.extraHeaderValue(forName: HeaderExtractionValues.Received.rawValue) {
                received = received_h
            }
            if let content = header.extraHeaderValue(forName: HeaderExtractionValues.ContentType.rawValue){
                contentType = content
            }
            if let contentTransEncode = header.extraHeaderValue(forName: HeaderExtractionValues.ContentTransferEncoding.rawValue){
                contentTransferEncoding = contentTransEncode
            }
            if let contentDispos =  header.extraHeaderValue(forName: HeaderExtractionValues.ContentDisposition.rawValue) {
                contentDisposition = contentDispos
            }
            if let dkimSig =  header.extraHeaderValue(forName: HeaderExtractionValues.DKIMSignature.rawValue){
                dkimSignature = dkimSig
            }
            if let inReplyToo = header.extraHeaderValue(forName: HeaderExtractionValues.InReplyTo.rawValue){
                inReplyTo = inReplyToo
            }
            if let xMail = header.extraHeaderValue(forName: HeaderExtractionValues.XMailer.rawValue){
                xMailer = xMail
            }
            boundary = findBoundary()
        }
    }
    
    private func parseUserReadableAttachments(parser: MCOMessageParser, sentEncrypted: Bool = false) -> Set<TempAttachment> {
        var attachments = Set<TempAttachment>()
        for at in parser.attachments() {
            if let attachment = at as? MCOAttachment {
                if let mimetype = MIMETYPE.findMIMETYPE(attachment: attachment), attachment.data.count > 0 {
                    var name = "untitled"
                    if let n = attachment.filename {
                        name = n
                    }
                    attachments.insert(TempAttachment(name: name, data: attachment.data, mimeType: mimetype))
               }
            }
        }
        return attachments
    }

    
    private func extractSignedParts(text: String) -> [String] {
        var parts: [String] = []
        var boundary = IncomingMail.findBoundary(text: text)
        if boundary == nil {
            boundary = self.boundary
        }
        guard text.contains("Content-Type: multipart/signed") && boundary != nil  else {
            return parts
        }

        parts = text.components(separatedBy: boundary!)
        // If text contains a header -> we drop it
        if let header = parts.first, IncomingMail.findBoundary(text: header) != nil {
            parts = Array(parts.dropFirst())
        }
        parts = Array(parts.filter({(s: String ) -> Bool in
            // A messages, where a signed part is signed again, are strange and verification can fail -> What about a response etc?
            if s.contains(PGPPart.SIGNATURE.start) {
                return false
            }
            return true
        }))
        
        parts = Array(parts.map({(s: String) -> String in
            // Convert strings according to RFC3156. See: https://tools.ietf.org/html/rfc3156 page 6
            var newString = s.replacingOccurrences(of: "\r\n", with: "\n")
            newString = newString.replacingOccurrences(of: "\n", with: "\r\n")
            // Sometimes we lose tabs...
            newString = newString.replacingOccurrences(of: "    charset", with: "\tcharset")
            //Remove -- if last symbol
            if newString.hasSuffix("--") {
                newString = String(newString.dropLast(2))
            }
            else if newString.hasSuffix("--\r\n") {
                newString = String(newString.dropLast(4))
            }
            if newString.hasPrefix("\r\n") {
                newString = String(newString.dropFirst())
            }
            if newString.hasSuffix("\r\n") {
                newString = String(newString.dropLast())
            }
            newString = newString.trimmingCharacters(in: .whitespaces)
            return newString
        }))
        
        parts = Array(parts.filter({(s: String ) -> Bool in
            if s.isEmpty {
                return false
            }
            return true
        }))
        return parts
    }
    
    
    
    private func findBoundary() -> String? {
        if let rawString = String.init(data: rawData, encoding:.utf8) {
            return IncomingMail.findBoundary(text: rawString)
        }
        return nil
    }
    
    func extractEncSMIMEState() -> CryptoObject? {
        for att in msgParser.attachments() {
            if let att = att as? MCOAttachment, att.mimeType ==  "application/pkcs7-mime", let email = from?.mailbox {
                // We have a signature!
                let smime = SMIME()               
                // Add content-Type to our string...
                if let cryptoObj = try? smime.decrypt(data: msgParser.data(), fromAddr: email, isMailNew: true){
                    //TODO check email, isMailNew etc....
                    return cryptoObj
                }
            }
        }
        return nil
    }
    
    
    func extractSignedSMIMEState() -> CryptoObject? {
        // TODO: smime.importCA(certs: [ca])
        for att in msgParser.attachments() {
            if let att = att as? MCOAttachment, att.mimeType ==  "application/pkcs7-signature" {
                // We have a signature!
                let smime = SMIME()
                // Now check the all mime stuff....
                if let boundary = boundary, let text = String(data: msgParser.data(), encoding: .utf8), let email = from?.mailbox {
                    let parts = text.components(separatedBy: boundary)
                    var string = ""
                    var first = true
                    for p in parts {
                        if first {
                            first = false
                        } else {
                            string += boundary + p
                        }
                    }
                    // Add content-Type to our string...
                    string = "MIME-Version: 1.0\r\nContent-Type: multipart/signed; boundary=\""+boundary.dropFirst(2)+"\"\r\n\r\n" + string + "\r\n"
                    cryptoObj = smime.verify(data: nil, string: string, email: email, isMailNew: true)
                    //TODO check email, isMailNew, etc....
                    return cryptoObj
                }
            }
        }
        return nil
    }
    
    func findSignedDataAndSignature(data: Data) -> (signedTexts: [String], signature: [String]) {
        var sig: [String] = []
        if let parser = MCOMessageParser.init(data: data) {
            for part in parser.attachments() {
                if let attachment = part as? MCOAttachment {
                    let data = IncomingMail.extractPGPSignature(attachment: attachment)
                    sig.append(contentsOf: data)
                    // TODO: What about more signatures?
                }
            }
            return (extractSignedParts(text: parser.plainTextBodyRendering()), sig)
        }
        return ([], sig)
    }
    
    private static func importPublicKeys(attachment: MCOAttachment) -> [String] {
        var newKey = [String]()
        let pgp = SwiftPGP()
        if let content = attachment.decodedString() {
          newKey = pgp.importKeys(keys: PGPPart.PUBLICKEY.findPGPPartInString(content: content))
        }
        else if PgpMIMEType.KEYS.isTypeOf(attachment: attachment) {
            if let keyIds = try? pgp.importKeys(data: attachment.data, pw: nil, secret: false) {
                newKey.append(contentsOf: keyIds)
            }
        }
        return newKey
    }
    
    private static func findKeys(attachment: MCOAttachment, type: PGPPart) -> [String]{
        var keys: [String] = []
        // Maybe String(data: attachment.data, encoding: .utf8) ?
        if let content = attachment.decodedString() {
            keys.append(contentsOf: type.findPGPPartInString(content: content))
        }
        else if let content = attachment.decodedString() {
            keys.append(contentsOf: type.findPGPPartInString(content: content))
        }
        return keys
    }
    
    private static func findSecretKeys(attachment: MCOAttachment) -> [String] {
        return findKeys(attachment: attachment, type: PGPPart.SECRETKEY)
    }
    
    private static func findPublicKeys(attachment: MCOAttachment) -> [String] {
       return findKeys(attachment: attachment, type: PGPPart.PUBLICKEY)
    }
    
    private static func extractPublicKeys(text: String) -> [String] {
        return PGPPart.PUBLICKEY.findPGPPartInString(content: text)
    }
    
    private static func extractSecretKeys(text: String) -> [String] {
        return PGPPart.SECRETKEY.findPGPPartInString(content: text)
    }
    
    private static func extractSignedMessage(text: String) -> [String] {
        return PGPPart.SIGNEDMESSAGE.findPGPPartInString(content: text)
    }
    
    private static func extractInlinePGP(text: String) -> [String] {
       return PGPPart.MESSAGE.findPGPPartInString(content: text)
    }
    
    private static func extractPGPSignature(attachment: MCOAttachment) -> [String] {
        if let content = attachment.decodedString() {
            return PGPPart.SIGNATURE.findPGPPartInString(content: content)
        }
        else if let content = String(data: attachment.data, encoding: .ascii){
            return PGPPart.SIGNATURE.findPGPPartInString(content: content)
        }
        return []
    }
    
    private static func extractPGPSignature(text: String) -> [String] {
        return PGPPart.SIGNATURE.findPGPPartInString(content: text)
    }

    
        
    private static func findBoundary(text: String) -> String? {
        let boundaries = matches(for: "boundary=\".*\"", in: text)
        if let bound = boundaries.first {
            let splitted = bound.split(separator: "\"")
            if splitted.count == 2 {
                // Be definition: add -- to boundary declaration...
                return "--"+String(splitted[1]) as String
            }
        }
        return nil
    }
    private static func matches(for regex: String, in text: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text, range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch {
            return []
        }
    }
}
