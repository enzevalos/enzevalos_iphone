//
//  MailTest.swift
//  
//
//  Created by Oliver Wiese on 31.10.18.
//

import XCTest

/*
 
 //TODO: Fix test cases, account settings int to enum!
 Test cases:
 
 parse incoming mails:
 MUA = {Letterbox, AppleMail, iOSMail, Thunderbird (+ Enigmail) [DONE], K9 (+ OKC)(, WebMail)}
 MUA x EncState x SigState (x Attachment)
 
 parse pgp mails:
    * inline pgp DONE
    * mime pgp DONE
 
 parse special mails:
    * public key import (attachment, inline)
    * secret key import (attachment, inline)

 
 parse mail compontens:
    * header (to, cc, bcc, subject, date etc.) DONE
    * body DONE
    * attachments
 
What about errors and special cases?
    * mixed encState/sigState in mail
    * html mail
    * attachments
    * remote content
    * java script
 
 create Mails: -> Export as eml (in Message builder)?
    * EncState x SigState -> is correct?
    * mixed receivers (plain, enc) -> Text if matching is correct DONE
    * attach public key
    * export secret key
 
 TODOS:
 What about input validation e.g. addr: b@example
 */

@testable import enzevalos_iphone
class MailTest: XCTestCase {
    
    let dataprovider = PersistentDataProvider.dataProvider
    let mailHandler = LetterboxModel.instance.mailHandler
    let pgp = SwiftPGP()
    let userAdr = "bob@enzevalos.de"
    let userName = "bob"
    var user: MCOAddress =  MCOAddress.init(mailbox: "bob@enzevalos.de")
    var userKeyID: String = ""
    
    
    static let body = """
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dapibus id diam ac volutpat. Sed quis cursus ante. Vestibulum eget gravida felis. Nullam accumsan diam quis sem ornare lacinia. Aenean risus risus, maximus quis faucibus et, maximus at nunc. Duis pharetra augue libero, et congue diam varius eget. Nullam efficitur ex purus, non accumsan tellus laoreet hendrerit. Suspendisse gravida interdum eros, eu venenatis ante suscipit nec. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent pellentesque cursus sem, non ornare nunc commodo vel. Praesent sed magna at ligula ultricies sagittis malesuada non est. Nam maximus varius mauris. Etiam dignissim congue ligula eu porta. Nunc rutrum nisl id mauris efficitur ultrices. Maecenas sit amet velit ac mauris consequat sagittis at et lorem.
    """
    
    
    var testCA = """
-----BEGIN CERTIFICATE-----
MIIC5TCCAc0CCH6WoapleQq4MA0GCSqGSIb3DQEBCwUAMDUxCzAJBgNVBAYTAkRF
MRMwEQYDVQQKDApBIE1BSUxURVNUMREwDwYDVQQDDAhteVRlc3RDQTAeFw0yMDAz
MDUxMzM1MTFaFw00MDAyMjkxMzM0NTlaMDUxCzAJBgNVBAYTAkRFMRMwEQYDVQQK
DApBIE1BSUxURVNUMREwDwYDVQQDDAhteVRlc3RDQTCCASIwDQYJKoZIhvcNAQEB
BQADggEPADCCAQoCggEBAKI5WXFBwGEnvAaFCFf9LhTO+w9uD09bt40rG1x2vTdF
xIO6aHsLuHbXEAfFgXzcF5ifdKlFwkIs8YV3R1Eqe7Ehoi393js9idqx5zefgHw4
Q/e6XbvAFWc0BwHNCL9EzwJzKZoQi2HxxWg/wMpu7urWQRxtYpxsp/Hr/oJaADBI
j/H/brx5Hybwy38T0IhD2FFRHFjmlJmnvawrzKk76/bMLg2TNBUIj/dNPdnfTMEk
RiMJJQ4tP4FPH0WoOoPrQNA2ijKN+nf/QyRzx8dEcOaTtsjhXDxsZmRooyuCL08x
GO/mXF9b6Mz5sCUjYetAfyhdytO2VzrMe5/oQYrjSJUCAwEAATANBgkqhkiG9w0B
AQsFAAOCAQEAmtn5vDCC3PCx9xBCA4R2YaQ34wjoCkVAwLzAmHDoXIo9kbEaARhu
/ikNDCtNd394HRjWZlVunWmyzKba7cZ53OY2q6lLwAyFFRC3+RF7MaA4OIiGF5Cx
t+NcrmzKm4cHJvi+DUxOqskl16fwycspIqokru3+dv3tqfZ8kxIOe3/hILLWYCbR
jRqChUmvLzwDtP8bFJ5COad91Cfx9DmHSnoPkhdg0f+2x+0eajjEx8fRCppHOSPK
5O2AvjjS4akSRqVwlqFUqgDvO1PRhLw31i14EbkO3q1OC/4t00HJnszqzbwGWTrf
n1O3czuVl7rPXrJn0A/MVI2ReKOQeIAYMg==
-----END CERTIFICATE-----
"""
    
    private func deleteAll() {
        let provider = PersistentDataProvider()
        let deleteExpectation = expectation(description: "Delete all data!")
        provider.deleteAll(completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing addresses! \(error)")
            }
            deleteExpectation.fulfill()
        })
        wait(for: [deleteExpectation], timeout: TimeInterval(30))
        provider.reset()
        let frc = provider.fetchedMailResultsController
        XCTAssertEqual(frc.fetchedObjects?.count ?? -1, 0)
    }
    
    override func setUp() {
        super.setUp()
        
        pgp.resetKeychains()
        deleteAll()
        (user, userKeyID) = owner()
    }
    
    override func tearDown() {
        super.tearDown()
        pgp.resetKeychains()
        deleteAll()
    }

    private func storeAndTestMail(mails: [IncomingMail], testFunc: (_ mail: MailRecord) -> ()) {
        let importExpectation = expectation(description: "Import new email!")
        dataprovider.importNewData(from: mails.map({$0.export()}), completionHandler: { error in
            if let error = error {
                XCTFail("Error while importing  an email! \(error)")
            }
            importExpectation.fulfill()
        })
        wait(for: [importExpectation], timeout: TimeInterval(20))
        dataprovider.reset()
        try? dataprovider.fetchedMailResultsController.performFetch()
        if let data = dataprovider.fetchedMailResultsController.fetchedObjects {
            //TODO XCTAssertEqual(data.count, mails.count + oldmails)
            for mail in data {
                testFunc(mail)
            }
        } else {
            XCTFail("No stored mails...")
        }
    }
    
    func testSimpleMailCreation() {
        // Init
        let tos = ["to1@example.com", "to2@example.com"]
        let ccs = ["cc1@example.com"]
        let bccs = ["bcc1@example.com"]
        let subject = "subject"
        let body = "This is the body"
        let outMail = OutgoingMail(toAddresses: tos, ccAddresses: ccs, bccAddresses: bccs, subject: subject, textContent: body, htmlContent: nil)
        if let data = outMail.plainData {
            // Test parsing!
            let incMail = IncomingMail(rawData: data, uID: 0, folderPath: "INBOX", flags: MCOMessageFlag.init(rawValue: 0))
            storeAndTestMail(mails: [incMail], testFunc: {mail in
                XCTAssertTrue(MailTest.compareAdrs(adrs1: tos, adrs2: mail.tos))
                XCTAssertTrue(MailTest.compareAdrs(adrs1: ccs, adrs2: mail.ccs))
                XCTAssertTrue(mail.bccs.count == 1)
                XCTAssertEqual(subject, mail.subject)
                XCTAssertEqual(body, mail.body)
                XCTAssertEqual(mail.encryptionState, .NoEncryption)
                XCTAssertEqual(mail.signatureState, .NoSignature)
            })
            
        }
        else {
            XCTFail()
        }
    }
    
    
    func testFormatMail() {
        // Init
        let tos = ["to1@example.com", "to2@example.com"]
        let ccs = ["cc1@example.com"]
        let bccs = ["bcc1@example.com"]
        let subject = "subject"
        var body = """
        Another at:
        
        https://www.fu-berlin.de
        
        Found that one because I was submitting the same URL from a phishing email.
        
        See: mi.fu-berlin.de
        For host name format of Azure static web sites.
        https certs will be valid – Microsoft signed.
        """
        body = body.replacingOccurrences(of: "\n", with: "\r\n")
        let outMail = OutgoingMail(toAddresses: tos, ccAddresses: ccs, bccAddresses: bccs, subject: subject, textContent: body, htmlContent: nil)
        if let data = outMail.plainData {
            // Test parsing!
            let incMail = IncomingMail(rawData: data, uID: 0, folderPath: "INBOX", flags: MCOMessageFlag.init(rawValue: 0))
            storeAndTestMail(mails: [incMail], testFunc: {mail in
                XCTAssertEqual(subject, mail.subject)
                XCTAssertEqual(body, mail.body)
            })
        } else {
            XCTFail()
        }
    }
    
    func testSecureMailCreation() {
        let encAdr = "enc@example.com"
        let subject = "subject"
        let body = "body"
        _ = createPGPUser(adr: encAdr, name: encAdr)
        let outMail = OutgoingMail(toAddresses: [userAdr], ccAddresses: [], bccAddresses: [], subject: subject, textContent: body, htmlContent: nil)
        if let data = outMail.pgpData{
            let incMail = IncomingMail(rawData: data, uID: 1, folderPath: "INBOX", flags: MCOMessageFlag.init(rawValue: 0))
            let b = incMail.export().body
            print(b)
            storeAndTestMail(mails: [incMail], testFunc: {mail in
                XCTAssertEqual(body, mail.body)
                XCTAssertEqual(mail.encryptionState, .ValidedEncryptedWithCurrentKey)
                XCTAssertEqual(mail.signatureState, .ValidSignature)
            })
        }
        else {
            XCTFail()
        }
    }
    
    func testMixedMailCreation() {
        let encAdr = "enc@example.com"
        let plainAdr = "plain@example.com"
        let subject = "subject"
        let body = "body"
        _ = createPGPUser(adr: encAdr, name: encAdr)
        let outMail = OutgoingMail(toAddresses: [plainAdr, userAdr], ccAddresses: [], bccAddresses: [], subject: subject, textContent: body, htmlContent: nil)
        if let data = outMail.pgpData {
            let incMail = IncomingMail(rawData: data, uID: 2, folderPath: "INBOX", flags: MCOMessageFlag.init(rawValue: 0))
            storeAndTestMail(mails: [incMail], testFunc: {mail in
                XCTAssertEqual(body, mail.body)
                XCTAssertTrue(MailTest.compareAdrs(adrs1: [userAdr, plainAdr], adrs2: mail.tos))
                XCTAssertEqual(mail.encryptionState, .ValidedEncryptedWithCurrentKey)
                XCTAssertEqual(mail.signatureState, .ValidSignature)
            })
        }
        else {
            XCTFail()
        }
        if let data = outMail.plainData {
            let incMail = IncomingMail(rawData: data, uID: 3, folderPath: "INBOX", flags: MCOMessageFlag.init(rawValue: 0))
            incMail.msgID = "dasdasd0990asd"
            storeAndTestMail(mails: [incMail], testFunc: { mail in
                if mail.messageID == "dasdasd0990asd" {
                    XCTAssertEqual(body, mail.body)
                    XCTAssertTrue(MailTest.compareAdrs(adrs1: [plainAdr, userAdr], adrs2: mail.tos))
                    XCTAssertEqual(mail.encryptionState, .NoEncryption)
                    XCTAssertEqual(mail.signatureState, .NoSignature)
                }
            })
        }
        else {
            XCTFail()
        }
    }
    
    func testImportSecretKeyWithOutPWNoEnc(){
        receiveImportKeyMail(file: "BobWithoutPW", keyID: "6F60DF88E48ECBDA", pw: nil, encMail: false)
    }
    
    func testImportSecretKeyWithOutPWNoEncAttached(){
        receiveImportKeyMail(file: "BobWithoutPW", keyID: "6F60DF88E48ECBDA", pw: nil, encMail: false, attached: true)
    }
    
    func testImportSecretKeyWithOutPWEnc(){
        receiveImportKeyMail(file: "BobWithoutPW", keyID: "6F60DF88E48ECBDA", pw: nil, encMail: true)
    }
    
    func testImportSecretKeyWithPWNoEnc(){
        receiveImportKeyMail(file: "BobPWTEST1234", keyID: "15E26A88438FC2DF", pw: "TEST1234", encMail: false)
    }
    
    func testImportSecretKeyWithPWEnc(){
        receiveImportKeyMail(file: "BobPWTEST1234", keyID: "15E26A88438FC2DF", pw: "TEST1234", encMail: true)
    }
    
    func receiveImportKeyMail(file: String, keyID: String, pw: String?, encMail: Bool, attached: Bool = false){
        let bundle = Bundle(for: type(of: self))
        if let url = bundle.url(forResource: file, withExtension: "asc"), let keyData = try? Data(contentsOf: url), let body = String(data: keyData, encoding: .utf8) {
            var mail = OutgoingMail(toAddresses: [userAdr], ccAddresses: [], bccAddresses: [], subject: "New secret key", textContent: body, htmlContent: nil)
            if attached, let attachment = MCOAttachment(data: keyData, filename: "secretKey.asc") {
                attachment.mimeType = ""
                attachment.charset = "UTF-8"
                mail = OutgoingMail(toAddresses: [userAdr], ccAddresses: [], bccAddresses: [], subject: "New attached secret key", textContent: MailTest.body, htmlContent: nil, textparts: 0, sendEncryptedIfPossible: false, attachments: [attachment])
            }
            var data = mail.plainData
            if encMail {
                data = mail.pgpData
            }
            if let data = data {
                let incMail = IncomingMail(rawData: data, uID: 0, folderPath: "Inbox", flags: MCOMessageFlag(rawValue: 0))
                XCTAssertTrue(incMail.hasSecretKeys, "No secret keys in IncommingMail")
                storeAndTestMail(mails: [incMail], testFunc: { mail in
                    XCTAssertTrue(mail.attachedSecretKey != nil, "No secret key in PersitentMail")
                    // When parsing a mail, we do not import the key...
                    //TODO let newSK = datahandler.findSecretKey(keyID: keyID)
                    //TODO XCTAssertNil(newSK, "New secret key should not exist")
                    
                    // ... only when calling the processSecretKey we import the key.
                    // ProcessSecretKey should require a user interaction, e.g. entering the password.
                   /* if let imported = try? storedMail.processSecretKey(pw: pw) {
                        XCTAssertTrue(imported, "Couldn't import key")
                     if let newSK = datahandler.findSecretKey(keyID: keyID) {
                            XCTAssertEqual(newSK.keyID, keyID)
                        }else {
                            XCTFail("New secret key should exist")
                        }
                    }
                    else {
                        XCTFail("Could not import secret key")
                    }*/
                })
            }
            else {
                XCTFail("No outgoing mail data")
            }
        }
        else {
            XCTFail("No key")
        }
    }
    
    func testThunderbirdPlainMail() {
        testMailAliceToBob(name: "plainThunderbird", encState: EncryptionState.NoEncryption, sigState: SignatureState.NoSignature)
    }
    func testThunderbirdSecureMail(){
        testSecureMail(name: "enc+signedThunderbird")
    }
    func testThunderBirdSecureInlineMail() {
        testSecureMail(name: "enc+signedInlineThunderbird")
    }
    func testThunderbirdEncMail(){
        testMailAliceToBob(name: "encThunderbird", encState: EncryptionState.ValidedEncryptedWithCurrentKey, sigState: SignatureState.NoSignature)
    }
    func testThunderbirdEncInlineMail(){
        testMailAliceToBob(name: "encInlineThunderbird", encState: EncryptionState.ValidedEncryptedWithCurrentKey, sigState: SignatureState.NoSignature)
    }
    func testThunderbirdSigedInlineMail() {
        testMailAliceToBob(name: "signedInlineThunderbird", encState: EncryptionState.NoEncryption, sigState: SignatureState.ValidSignature)
    }
    func testThunderbirdSigedMail() {
        testMailAliceToBob(name: "signedThunderbird", encState: EncryptionState.NoEncryption, sigState: SignatureState.ValidSignature)
    }
    
    func testMacPlainMail() {
        testMailAliceToBob(name: "PlainMailFromMac", encState: EncryptionState.NoEncryption, sigState: SignatureState.NoSignature)
    }
    func testMacSecureMail(){
        testSecureMail(name: "signedEncMailFromApple", accountVersion: 1)
    }
    func testMacEncMail(){
        testMailAliceToBob(name: "EncMailFromMac", encState: EncryptionState.ValidedEncryptedWithCurrentKey, sigState: SignatureState.NoSignature)
    }
    func testMacSigedMail() {
        testMailAliceToBob(name: "SignedMailFromMac", encState: EncryptionState.NoEncryption, sigState: SignatureState.ValidSignature)
    }
    
     func testK9SigedInlineMail() {
        testMailAliceToBob(name: "signinlineK9", encState: EncryptionState.NoEncryption, sigState: SignatureState.ValidSignature, accountVersion : 3)
    }
    func testK9SigedMail() {
        testMailAliceToBob(name: "signK9", encState: EncryptionState.NoEncryption, sigState: SignatureState.ValidSignature, accountVersion :  3)
    }
    func testK9SecureMail(){
        testSecureMail(name: "signencK9", accountVersion: 3)
    }
    func testK9SecureInlineMail() {
        testSecureMail(name: "signencinlineK9", accountVersion:  3)
    }
    
    func testSecureMail(name: String, accountVersion: Int  = 0) {
        testMailAliceToBob(name: name, encState: .ValidedEncryptedWithCurrentKey, sigState: .ValidSignature, accountVersion: accountVersion)
    }
    
    func testSMIMEMailFromMac(){
        let ca = """
        -----BEGIN CERTIFICATE-----
        MIIDwzCCAqugAwIBAgIBATANBgkqhkiG9w0BAQsFADCBgjELMAkGA1UEBhMCREUx
        KzApBgNVBAoMIlQtU3lzdGVtcyBFbnRlcnByaXNlIFNlcnZpY2VzIEdtYkgxHzAd
        BgNVBAsMFlQtU3lzdGVtcyBUcnVzdCBDZW50ZXIxJTAjBgNVBAMMHFQtVGVsZVNl
        YyBHbG9iYWxSb290IENsYXNzIDIwHhcNMDgxMDAxMTA0MDE0WhcNMzMxMDAxMjM1
        OTU5WjCBgjELMAkGA1UEBhMCREUxKzApBgNVBAoMIlQtU3lzdGVtcyBFbnRlcnBy
        aXNlIFNlcnZpY2VzIEdtYkgxHzAdBgNVBAsMFlQtU3lzdGVtcyBUcnVzdCBDZW50
        ZXIxJTAjBgNVBAMMHFQtVGVsZVNlYyBHbG9iYWxSb290IENsYXNzIDIwggEiMA0G
        CSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCqX9obX+hzkeXaXPSi5kfl82hVYAUd
        AqSzm1nzHoqvNK38DcLZSBnuaY/JIPwhqgcZ7bBcrGXHX+0CfHt8LRvWurmAwhiC
        FoT6ZrAIxlQjgeTNuUk/9k9uN0goOA/FvudocP05l03Sx5iRUKrERLMjfTlH6VJi
        1hKTXrcxlkIF+3anHqP1wvzpesVsqXFP6st4vGCvx9702cu+fjOlbpSD8DT6Iavq
        jnKgP6TeMFvvhk1qlVtDRKgQFRzlAVfFmPHmBiiRqiDFt1MmUUOyCxGVWOHAD3bZ
        wI18gfNycJ5v/hqO2V81xrJvNHy+SE/iWjnX2J14np+GPgNeGYtEotXHAgMBAAGj
        QjBAMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgEGMB0GA1UdDgQWBBS/
        WSA2AHmgoCJrjNXyYdK4LMuCSjANBgkqhkiG9w0BAQsFAAOCAQEAMQOiYQsfdOhy
        NsZt+U2e+iKo4YFWz827n+qrkRk4r6p8FU3ztqONpfSO9kSpp+ghla0+AGIWiPAC
        uvxhI+YzmzB6azZie60EI4RYZeLbK4rnJVM3YlNfvNoBYimipidx5joifsFvHZVw
        IEoHNN/q/xWA5brXethbdXwFeilHfkCoMRN3zUA7tFFHei4R40cR3p1m0IvVVGb6
        g1XqfMIpiRvpb7PO4gWEyS8+eIVibslfwXhjdFjASBgMmTnrpMwatXlajRWc2BQN
        9noHV8cigwUtPJslJj0Ys6lDfMjIq2SPDqO/nBudMNva0Bkuqjzx+zOAduTNrRlP
        BSeOE6Fuwg==
        -----END CERTIFICATE-----

        """
        let _ = SMIME().importCA(certs: [ca, testCA])
        let mailData = MailTest.loadMail(name: "signedSMIMEfromMac")
        let incMail = IncomingMail(rawData: mailData, uID: 4, folderPath: "INBOX", flags: MCOMessageFlag.init(rawValue: 0))
        storeAndTestMail(mails: [incMail], testFunc: {mail in
            print("\(mail.subject) \(mail.encryptionState) \(mail.signatureState) keyid: \(String(describing: mail.signatureKeyID))")
            XCTAssertEqual(mail.encryptionState, .NoEncryption)
            XCTAssertEqual(mail.signatureState, .ValidSignature)
        })
    }
    
    func testMailAliceToBob(name: String, encState: EncryptionState? = nil, sigState: SignatureState? = nil, accountVersion : Int = 0) {
        testMailAliceToBob(pkExists: true, name: name, encState: encState, sigState: sigState,  accountVersion : accountVersion)
        /* TODO if accountVersion == 0  {
            tearDown()
            setUp()
            testMailAliceToBob(pkExists: false, name: name, encState: encState, sigState: sigState, accountVersion : accountVersion)
        }*/
    }
    
    func testMailAliceToBob(pkExists: Bool, name: String, encState: EncryptionState? = nil, sigState: SignatureState? = nil, accountVersion : Int = 0) {
        let mailData = MailTest.loadMail(name: name )
        var (alice, _) = addAliceAndBob(addAlice: pkExists)
        if accountVersion == 1 {
            (alice, _) = addAliceAndBobLetterbox(addAlice: pkExists)
        }
        if accountVersion == 3 
        {
          (alice, _) = addAliceAndBobV3(addAlice:pkExists)
        }
        let incMail = IncomingMail(rawData: mailData, uID: 4, folderPath: "INBOX", flags: MCOMessageFlag.init(rawValue: 0))
        storeAndTestMail(mails: [incMail], testFunc: {mail in
            print("\(mail.subject) \(mail.encryptionState) \(mail.signatureState) keyid: \(String(describing: mail.signatureKeyID))")
            if let sig = sigState, sig == .ValidSignature{
                XCTAssertEqual(mail.signatureKey?.fingerprint, alice)
            }
            if let encState = encState {
                XCTAssertEqual(mail.encryptionState, encState)
            }
            if let sigState = sigState {
                XCTAssertEqual(mail.signatureState, sigState)
            }
            XCTAssertEqual(mail.body.removeNewLines(), MailTest.body.removeNewLines())
        })
        
    }
    
    func addAliceAndBob(addAlice: Bool) -> (alice: String, bob: String){
        let aliceKeyId = importKey(file: "alicePublic", isSecretKey: false)
        var imports = [DataPropertyProtocol]()
        if addAlice {
            let pk = PublicKeyProperties(fingerprint: aliceKeyId, cryptoProtocol: .PGP, origin: .FileTransfer, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: nil, usedAddresses: [AddressProperties(email: "alice@enzevalos.de")])
            imports.append(pk)
        }
        let bobKeyId = importKey(file: "bobSecret", isSecretKey: true)
        var sk = SecretKeyProperties(fingerprint: bobKeyId, cryptoProtocol: .PGP, usedAddresses: [AddressProperties(email: "bob@enzevalos.de")])
        sk.preferedKey = true
        imports.append(sk)
        let importExceptation = expectation(description: "import")
        dataprovider.importNewData(from: imports, completionHandler: { error in
            importExceptation.fulfill()
        })
        wait(for: [importExceptation], timeout: 30)
        UserManager.storeUserValue("bob@enzevalos.de" as AnyObject, attribute: Attribute.userAddr)
        UserManager.storeUserValue(bobKeyId as AnyObject, attribute: Attribute.prefSecretKeyID)
        return (aliceKeyId, bobKeyId)
    }
    
    func addAliceAndBobLetterbox(addAlice: Bool) -> (alice: String, bob: String){
        var imports = [DataPropertyProtocol]()
        let aliceKeyId = importKey(file: "Alice Letterbox (439EE43C) – Public", isSecretKey: false)
        if addAlice {
            let pk = PublicKeyProperties(fingerprint: aliceKeyId, cryptoProtocol: .PGP, origin: .FileTransfer, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: nil, usedAddresses: [AddressProperties(email: "alice@letterbox-app.org")])
            imports.append(pk)
        }
        let bobKeyId = importKey(file: "Bob Letterbox (0B6CD0A0) – Secret", isSecretKey: true)
        let sk = SecretKeyProperties(fingerprint: bobKeyId, cryptoProtocol: .PGP, usedAddresses: [AddressProperties(email: "bob@letterbox-app.org")])
        imports.append(sk)
        let importExceptation = expectation(description: "import")
        dataprovider.importNewData(from: imports, completionHandler: { error in
            importExceptation.fulfill()
        })
        wait(for: [importExceptation], timeout: 30)
        UserManager.storeUserValue("bob@letterbox-app.org" as AnyObject, attribute: Attribute.userAddr)
        UserManager.storeUserValue(bobKeyId as AnyObject, attribute: Attribute.prefSecretKeyID)
        return (aliceKeyId, bobKeyId)
    }
 
    func addAliceAndBobV3(addAlice: Bool) -> (alice: String, bob: String){
        var imports = [DataPropertyProtocol]()
        let aliceKeyId = importKey(file: "Alice.v3.pub", isSecretKey: false)
        if addAlice {
            let pk = PublicKeyProperties(fingerprint: aliceKeyId, cryptoProtocol: .PGP, origin: .FileTransfer, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: nil, usedAddresses: [AddressProperties(email: "alice@letterbox-app.org")])
            imports.append(pk)
        }
        let bobKeyId = importKey(file: "Bob Letterbox (0B6CD0A0) – Secret", isSecretKey: true)
        let sk = SecretKeyProperties(fingerprint: bobKeyId, cryptoProtocol: .PGP, usedAddresses: [AddressProperties(email: "bob@letterbox-app.org")])
        imports.append(sk)
        let importExceptation = expectation(description: "import")
        dataprovider.importNewData(from: imports, completionHandler: { error in
            importExceptation.fulfill()
        })
        wait(for: [importExceptation], timeout: 30)
        UserManager.storeUserValue("bob@letterbox-app.org" as AnyObject, attribute: Attribute.userAddr)
        UserManager.storeUserValue(bobKeyId as AnyObject, attribute: Attribute.prefSecretKeyID)
        return (aliceKeyId, bobKeyId)
    }  
    
    static func compareAdrs(adrs1: [String], adrs2: [AddressRecord]) -> Bool{
        for adr in adrs1 {
            var found = false
            for adr2 in adrs2 {
                if adr == adr2.email{
                    found = true
                }
            }
            if !found {
                return false
            }
        }
        
        for adr in adrs2 {
            var found = false
            for adr2 in adrs1 {
                if adr.email == adr2 {
                    found = true
                }
            }
            if !found {
                return false
            }
        }
        return true
        
    }
    
    func importKey(file: String, isSecretKey: Bool) -> String{
        let bundle = Bundle(for: type(of: self))
        do {
            let keyData = try Data(contentsOf: bundle.url(forResource: file, withExtension: "asc")!)
            let ids = try pgp.importKeys(data: keyData, pw: nil, secret: isSecretKey)
            if ids.count > 0 {
                return ids.first!
            }
        } catch {
            XCTFail()
        }
        XCTFail()
        return ""
    }
    
    static func loadMail(name: String) -> Data {
        let bundle = Bundle(for: self)
        do {
            let mail = try Data(contentsOf: bundle.url(forResource: name, withExtension: "eml")!)
            return mail
        } catch {
            XCTFail()
        }
        return Data(base64Encoded: "")!
    }
    
    func createUser(adr: String = String.random().lowercased(), name: String = String.random()) -> MCOAddress {
        return MCOAddress.init(displayName: name, mailbox: adr.lowercased())
    }
    
    func createPGPUser(adr: String = String.random().lowercased(), name: String = String.random()) -> (MCOAddress, String) {

        let user = createUser(adr: adr, name: name)
        let id = pgp.generateKey(adr: user.mailbox)
        let createExpectation = expectation(description: "Create public key record!")
        let property = PublicKeyProperties(fingerprint: id, cryptoProtocol: .PGP, origin: .FileTransfer, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: nil, usedAddresses: [AddressProperties(email: user.mailbox)])
        dataprovider.importNewData(from: [property], completionHandler: {error in
            if let error = error {
                XCTFail("Error while creating a new key: \(error)")
            }
            createExpectation.fulfill()
        })
        wait(for: [createExpectation], timeout: TimeInterval(30))
        return (user, id)
    }
    
    func owner() -> (MCOAddress, String) {
        let createExpectation = expectation(description: "Create key!")
        Logger.logging = false
        let user = createUser(adr: userAdr, name: userName)
        let userid = dataprovider.createNewSecretKey(addr: userAdr, completionHandler: {error in
            if let error = error {
                XCTFail("Error while creating a new key: \(error)")
            }
            createExpectation.fulfill()
        })
        wait(for: [createExpectation], timeout: TimeInterval(30))
        UserManager.storeUserValue(userAdr as AnyObject, attribute: Attribute.userAddr)
        UserManager.storeUserValue(userid as AnyObject, attribute: Attribute.prefSecretKeyID)
        return (user, userid)
    }
    
}
