//
//  InboxView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 16.10.20.
//  Modified by Chris Offner & Claire Bräuer in March 2021.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import SwiftUI

struct InboxView: View {    
    var folderPath: String
    var folderName: String
    @State private var updating = false
    @State private var composeMail = false
    @State private var goToFolders = false
    @ObservedObject var refreshModel: InboxRefreshModel
    
    init(folderPath: String, folderName: String) {
        self.folderPath = folderPath
        self.folderName = folderName
        self.refreshModel = InboxRefreshModel(folderPath: folderPath)
    }
    
    var body: some View {
        mailListView
            .onAppear(perform: refreshModel.updateMails)
            .sheet(isPresented: $composeMail) { ComposeView() }
            .navigationBarItems(trailing: keyManagementButton)
            .toolbar {
                ToolbarItem(placement: .status) {
                    Button(action: refreshModel.updateMails) {
                        Text(refreshModel.lastUpdate)
                    }
                }
                
                ToolbarItemGroup(placement: .bottomBar) {
                    Spacer()
                    composeButton
                }
            }
    }
    
    
    private var mailListView: some View {
        MailListView(folderPath: folderPath, folderName: folderName, refreshModel: refreshModel)
            .environment(\.managedObjectContext,
                         PersistentDataProvider
                            .dataProvider
                            .persistentContainer
                            .viewContext)
    }
    
    private var composeButton: some View {
        Button {
            composeMail = true
        } label: {
            Image(systemName: "square.and.pencil").imageScale(.large)
        }
    }
    
    private var keyManagementButton: some View {
        NavigationLink(destination: KeyManagementOverview()) {
            Image(systemName: "key")
                .imageScale(.large)
                .rotationEffect(Angle(degrees: 45))
        }
    }
    
    private var folderButton: some View {
        Button {
            goToFolders = true
        } label:  {
            Image(systemName: "tray.2").imageScale(.large)
        }
        .background(NavigationLink(destination: FolderListView()
                                    .environment(\.managedObjectContext,
                                                 PersistentDataProvider
                                                    .dataProvider
                                                    .persistentContainer
                                                    .viewContext),
                                   isActive: $goToFolders) {})
    }
}

// Preview
struct InboxView_Previews: PreviewProvider {
    static var previews: some View {
        return InboxView(folderPath: "INBOX", folderName: "INBOX")
            .environment(\.managedObjectContext,
                         PersistentDataProvider
                            .proxyPersistentDataProvider
                            .persistentContainer
                            .viewContext)
    }
}
