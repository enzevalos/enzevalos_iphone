//
//  SwiftUIOnboardingUITests.swift
//  enzevalos_iphoneUITests
//
//  Created by Oliver Wiese on 20.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import XCTest

class SwiftUIOnboardingUITests: XCTestCase {
    let timeoutShort: TimeInterval = 10
    var app: XCUIApplication!
    let accounts = loadAccounts()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        app = XCUIApplication()
        app.launch()
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        XCTAssertGreaterThan(accounts.count, 0)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testHappyPath1() {
        guard let user = accounts["1"] else {
            XCTFail("No account with id 1")
            return
        }
        guard user.adr != "FIX ME" else {
           XCTFail("Wrong account file loaded!")
           return
        }
        print(user.adr)
        print(user.pw)
        testHappyPath(user: user)
    }
    
    func testHappyPath2() {
           guard let user = accounts["2"] else {
               XCTFail("No account with id 2")
               return
           }
           guard user.adr != "FIX ME" else {
              XCTFail("Wrong account file loaded!")
              return
           }
           print(user.adr)
           print(user.pw)
           testHappyPath(user: user)
    }
    
    func testHappyPath3() {
           guard let user = accounts["3"] else {
               XCTFail("No account with id 3")
               return
           }
           guard user.adr != "FIX ME" else {
              XCTFail("Wrong account file loaded!")
              return
           }
           print(user.adr)
           print(user.pw)
           testHappyPath(user: user)
    }
    
    func testHappyPath4() {
           guard let user = accounts["4"] else {
               XCTFail("No account with id 4")
               return
           }
           guard user.adr != "FIX ME" else {
              XCTFail("Wrong account file loaded!")
              return
           }
           print(user.adr)
           print(user.pw)
           testHappyPath(user: user)
    }
    
    func testHappyPath5() {
           guard let user = accounts["5"] else {
               XCTFail("No account with id 5")
               return
           }
           guard user.adr != "FIX ME" else {
              XCTFail("Wrong account file loaded!")
              return
           }
           print(user.adr)
           print(user.pw)
           testHappyPath(user: user)
       }
    
    func testTesds(){
        
        app.buttons["Login"].tap()
        
        let mailAddressTextField = app.scrollViews.otherElements.textFields["Mail address"]
        mailAddressTextField.tap()
        mailAddressTextField.tap()
        mailAddressTextField.tap()
        
        let cutStaticText = app/*@START_MENU_TOKEN@*/.staticTexts["Cut"]/*[[".menus",".menuItems[\"Cut\"].staticTexts[\"Cut\"]",".staticTexts[\"Cut\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/
        cutStaticText.tap()
        mailAddressTextField.tap()
        
        app/*@START_MENU_TOKEN@*/.menuItems["Paste"]/*[[".menus.menuItems[\"Paste\"]",".menuItems[\"Paste\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        mailAddressTextField.tap()
        app/*@START_MENU_TOKEN@*/.staticTexts["Select All"]/*[[".menus",".menuItems[\"Select All\"].staticTexts[\"Select All\"]",".staticTexts[\"Select All\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
        cutStaticText.tap()
       
        
    }
    
    func testHappyPath(user: (adr: String, pw: String, hostIMAP: String, portIMAP: String, conTypeIMAP: String, hostSMTP: String, portSMTP: String, conTypeSMTP: String)){
        app.buttons["Login"].tap()
        app.scrollViews.otherElements.buttons["eye.slash"].tap()

        let elementsQuery = app.scrollViews.otherElements
        let mailAddressTextField = elementsQuery.textFields["Mail address"]
        let passwordSecureTextField = elementsQuery.textFields["Password"]
        input(text: user.adr, view: mailAddressTextField)
        input(text: user.pw, view: passwordSecureTextField)
        
        app.scrollViews.otherElements.buttons["Next"].tap()
        let exists = elementsQuery/*@START_MENU_TOKEN@*/.buttons["OnboardActionButtonAccessibilityIdentifier"]/*[[".buttons[\"Got it!\"]",".buttons[\"OnboardActionButtonAccessibilityIdentifier\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.waitForExistence(timeout: timeoutShort)
        if !exists {
            XCTFail("Could not login with user: \(user.adr)")
        }
    }
    
    private func input(text: String, view: XCUIElement){
        view.tap()
        app.typeText(text)
        var i = 0
        while text != view.value as! String {
            view.tap()
            view.clear()
            app.typeText(text)
            i = i + 1
            if i > 100 {
                XCTFail("Could not write: \(text) in \(view.description)")
                return
            }
            
        }
    }
    
    func testDetailLogin1() {
        guard let user = accounts["1"] else {
            XCTFail("No account with id 1")
            return
        }
        guard user.adr != "FIX ME" else {
           XCTFail("Wrong account file loaded!")
           return
        }
        print(user.adr)
        print(user.pw)
        testDetailLogin(user: user)
    }
    
    func testDetailLogin2() {
        guard let user = accounts["2"] else {
            XCTFail("No account with id 2")
            return
        }
        guard user.adr != "FIX ME" else {
           XCTFail("Wrong account file loaded!")
           return
        }
        print(user.adr)
        print(user.pw)
        testDetailLogin(user: user)
    }
    
    func testDetailLogin3() {
        guard let user = accounts["3"] else {
            XCTFail("No account with id 3")
            return
        }
        guard user.adr != "FIX ME" else {
           XCTFail("Wrong account file loaded!")
           return
        }
        print(user.adr)
        print(user.pw)
        testDetailLogin(user: user)
    }
    
    func testDetailLogin4() {
        guard let user = accounts["4"] else {
            XCTFail("No account with id 4")
            return
        }
        guard user.adr != "FIX ME" else {
           XCTFail("Wrong account file loaded!")
           return
        }
        print(user.adr)
        print(user.pw)
        testDetailLogin(user: user)
    }
    
    func testDetailLogin5() {
        guard let user = accounts["5"] else {
            XCTFail("No account with id 5")
            return
        }
        guard user.adr != "FIX ME" else {
           XCTFail("Wrong account file loaded!")
           return
        }
        print(user.adr)
        print(user.pw)
        testDetailLogin(user: user)
    }
    
    func testDetailLogin(user: (adr: String, pw: String, hostIMAP: String, portIMAP: String, conTypeIMAP: String, hostSMTP: String, portSMTP: String, conTypeSMTP: String)){
        app.buttons["Login"].tap()

        let scrollViewsQuery = app.scrollViews
        let elementsQuery = scrollViewsQuery.otherElements


        elementsQuery.buttons["eye.slash"].tap()
        let mailAddrField = elementsQuery.textFields["Mail address"]
        let pwField = elementsQuery.textFields["Password"]
        input(text: user.adr, view: mailAddrField)
        input(text: user.pw, view: pwField)
            
    
        elementsQuery.buttons["Advanced options"].tap()
        let userField = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").children(matching: .textField).matching(identifier: "Mail address").element(boundBy: 1)
        input(text: user.adr, view: userField)
        
        let imapServerField = elementsQuery.textFields["e.g. imap.web.de"]
        let imapPortField = elementsQuery.textFields["e.g. 883"]
        let imapEncTypeField = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").children(matching: .segmentedControl).element(boundBy: 0)
        input(text: user.hostIMAP, view: imapServerField)
        input(text: user.portIMAP, view: imapPortField)
        imapEncTypeField.buttons[user.conTypeIMAP].tap()
        
        
        let smtpServerField = elementsQuery.textFields["e.g. smtp.web.de"]
        let smtpPortField = elementsQuery.textFields["e.g. 559"]
        let smtpEncTypeField = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").children(matching: .segmentedControl).element(boundBy: 1)
        input(text: user.hostSMTP, view: smtpServerField)
        input(text: user.portSMTP, view: smtpPortField)
        smtpEncTypeField.buttons[user.conTypeSMTP].tap()
        
        scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").element.swipeUp()
        elementsQuery.buttons["Next"].tap()
        let exists = elementsQuery/*@START_MENU_TOKEN@*/.buttons["OnboardActionButtonAccessibilityIdentifier"]/*[[".buttons[\"Got it!\"]",".buttons[\"OnboardActionButtonAccessibilityIdentifier\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.waitForExistence(timeout: timeoutShort)
        if !exists {
            XCTFail("Could not login with user: \(user.adr)")
        }
    }

    func testWrongPassword(){
        guard let user = accounts["1"] else {
            XCTFail("No account with id 1")
            return
        }
        guard user.adr != "FIX ME" else {
           XCTFail("Wrong account file loaded!")
           return
        }
        app.buttons["Login"].tap()
        app.scrollViews.otherElements.buttons["eye.slash"].tap()

        let elementsQuery = app.scrollViews.otherElements
        let mailAddressTextField = elementsQuery.textFields["Mail address"]
        let passwordSecureTextField = elementsQuery.textFields["Password"]
        let next =  app.scrollViews.otherElements.buttons["Next"]
        
        input(text: user.adr, view: mailAddressTextField)
        input(text: "dasdasdasftzgb", view: passwordSecureTextField)
        next.tap()
        
        var exists = elementsQuery.staticTexts["COULDN'T CONNECT TO SERVER. \n YOUR ACCOUNT NAME OR PASSWORD IS WRONG. PLEASE, CHECK THEM."].waitForExistence(timeout: timeoutShort)
        if exists {
            XCTFail("No error but wrong password for user: \(user.adr)")
        }
        input(text: user.pw, view: passwordSecureTextField)
        next.tap()
        exists = elementsQuery/*@START_MENU_TOKEN@*/.buttons["OnboardActionButtonAccessibilityIdentifier"]/*[[".buttons[\"Got it!\"]",".buttons[\"OnboardActionButtonAccessibilityIdentifier\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.waitForExistence(timeout: timeoutShort)
        if !exists {
           XCTFail("Could not login with user: \(user.adr)")
        }
    }
    
    func testWrongPort() {
        guard let user = accounts["1"] else {
            XCTFail("No account with id 1")
            return
        }
        guard user.adr != "FIX ME" else {
           XCTFail("Wrong account file loaded!")
           return
        }
        
        app.buttons["Login"].tap()

        let scrollViewsQuery = app.scrollViews
        let elementsQuery = scrollViewsQuery.otherElements


        elementsQuery.buttons["eye.slash"].tap()
        let mailAddrField = elementsQuery.textFields["Mail address"]
        let pwField = elementsQuery.textFields["Password"]
        input(text: user.adr, view: mailAddrField)
        input(text: user.pw, view: pwField)
            
    
        elementsQuery.buttons["Advanced options"].tap()
        let userField = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").children(matching: .textField).matching(identifier: "Mail address").element(boundBy: 1)
        input(text: user.adr, view: userField)
        
        let imapServerField = elementsQuery.textFields["e.g. imap.web.de"]
        let imapPortField = elementsQuery.textFields["e.g. 883"]
        let imapEncTypeField = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").children(matching: .segmentedControl).element(boundBy: 0)
        input(text: user.hostIMAP, view: imapServerField)
        input(text: "111", view: imapPortField)
        imapEncTypeField.buttons[user.conTypeIMAP].tap()
        
        
        let smtpServerField = elementsQuery.textFields["e.g. smtp.web.de"]
        let smtpPortField = elementsQuery.textFields["e.g. 559"]
        let smtpEncTypeField = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").children(matching: .segmentedControl).element(boundBy: 1)
        input(text: user.hostSMTP, view: smtpServerField)
        input(text: user.portSMTP, view: smtpPortField)
        smtpEncTypeField.buttons[user.conTypeSMTP].tap()
        
        scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").element.swipeUp()
        elementsQuery.buttons["Next"].tap()
        let exists = elementsQuery.staticTexts["Couldn't connect to server.\n Please check your IMAP server configuration."].waitForExistence(timeout: timeoutShort)
        if exists {
            XCTFail("No error but wrong port for user: \(user.adr)")
        }
    }
    
    func testWrongEncType1() {
        guard let user = accounts["1"] else {
            XCTFail("No account with id 1")
            return
        }
        guard user.adr != "FIX ME" else {
           XCTFail("Wrong account file loaded!")
           return
        }
        
        app.buttons["Login"].tap()

        let scrollViewsQuery = app.scrollViews
        let elementsQuery = scrollViewsQuery.otherElements


        elementsQuery.buttons["eye.slash"].tap()
        let mailAddrField = elementsQuery.textFields["Mail address"]
        let pwField = elementsQuery.textFields["Password"]
        input(text: user.adr, view: mailAddrField)
        input(text: user.pw, view: pwField)
            
    
        elementsQuery.buttons["Advanced options"].tap()
        let userField = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").children(matching: .textField).matching(identifier: "Mail address").element(boundBy: 1)
        input(text: user.adr, view: userField)
        
        let imapServerField = elementsQuery.textFields["e.g. imap.web.de"]
        let imapPortField = elementsQuery.textFields["e.g. 883"]
        let imapEncTypeField = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").children(matching: .segmentedControl).element(boundBy: 0)
        input(text: user.hostIMAP, view: imapServerField)
        input(text: user.portIMAP, view: imapPortField)
        imapEncTypeField.buttons[user.conTypeIMAP].tap()
        
        
        let smtpServerField = elementsQuery.textFields["e.g. smtp.web.de"]
        let smtpPortField = elementsQuery.textFields["e.g. 559"]
        let smtpEncTypeField = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").children(matching: .segmentedControl).element(boundBy: 1)
        input(text: user.hostSMTP, view: smtpServerField)
        input(text: user.portSMTP, view: smtpPortField)
        smtpEncTypeField.buttons["Plaintext"].tap()
        
        scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").element.swipeUp()
        elementsQuery.buttons["Next"].tap()
        let exists = elementsQuery.staticTexts["Couldn't connect to server.\n Please check your IMAP server configuration."].waitForExistence(timeout: timeoutShort)
        if exists {
            XCTFail("No error but wrong port for user: \(user.adr)")
        }
    }
    
    func testWrongEncType2() {
        guard let user = accounts["1"] else {
            XCTFail("No account with id 1")
            return
        }
        guard user.adr != "FIX ME" else {
           XCTFail("Wrong account file loaded!")
           return
        }
        
        app.buttons["Login"].tap()

        let scrollViewsQuery = app.scrollViews
        let elementsQuery = scrollViewsQuery.otherElements


        elementsQuery.buttons["eye.slash"].tap()
        let mailAddrField = elementsQuery.textFields["Mail address"]
        let pwField = elementsQuery.textFields["Password"]
        input(text: user.adr, view: mailAddrField)
        input(text: user.pw, view: pwField)
            
    
        elementsQuery.buttons["Advanced options"].tap()
        let userField = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").children(matching: .textField).matching(identifier: "Mail address").element(boundBy: 1)
        input(text: user.adr, view: userField)
        
        let imapServerField = elementsQuery.textFields["e.g. imap.web.de"]
        let imapPortField = elementsQuery.textFields["e.g. 883"]
        let imapEncTypeField = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").children(matching: .segmentedControl).element(boundBy: 0)
        input(text: user.hostIMAP, view: imapServerField)
        input(text: user.portIMAP, view: imapPortField)
        imapEncTypeField.buttons[user.conTypeIMAP].tap()
        
        
        let smtpServerField = elementsQuery.textFields["e.g. smtp.web.de"]
        let smtpPortField = elementsQuery.textFields["e.g. 559"]
        let smtpEncTypeField = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").children(matching: .segmentedControl).element(boundBy: 1)
        input(text: user.hostSMTP, view: smtpServerField)
        input(text: user.portSMTP, view: smtpPortField)
        smtpEncTypeField.buttons["StartTLS"].tap()
        
        scrollViewsQuery.otherElements.containing(.staticText, identifier:"Welcome").element.swipeUp()
        elementsQuery.buttons["Next"].tap()
        let exists = elementsQuery.staticTexts["Couldn't connect to server.\n Please check your IMAP server configuration."].waitForExistence(timeout: timeoutShort)
        if exists {
            XCTFail("No error but wrong port for user: \(user.adr)")
        }
    }
    
    private static func loadAccounts() -> [String:(adr: String, pw: String, hostIMAP: String, portIMAP: String, conTypeIMAP: String, hostSMTP: String, portSMTP: String, conTypeSMTP: String)] {
        let bundle = Bundle(for: self)
        var newAccounts = [String:(adr: String, pw: String, hostIMAP: String, portIMAP: String, conTypeIMAP: String, hostSMTP: String, portSMTP: String, conTypeSMTP: String)]()
        guard let url = bundle.url(forResource: "accounts", withExtension: "json"), let data = try? Data(contentsOf: url),  let jsonDic = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)  else {
            XCTFail("No file!")
            return [:]
        }
        if let dic = jsonDic as? Dictionary<String, Any>{
            if let accounts = dic["accounts"], let array = accounts as? Array<Any> {
                for elem in array {
                    if let account = elem as? Dictionary<String, String> {
                        if let id = account["id"], let username = account["username"] , let pw = account["password"], let hostIMAP = account["hostIMAP"], let portIMAP = account["portIMAP"], let conTypeIMAP = account["conTypeIMAP"], let hostSMTP = account["hostSMTP"], let portSMTP = account["portSMTP"], let conTypeSMTP = account["conTypeSMTP"]{
                            newAccounts[id] = (username, pw, hostIMAP, portIMAP, conTypeIMAP, hostSMTP, portSMTP, conTypeSMTP)
                        }
                    }
                }
            }
            
        }
        XCTAssertGreaterThan(newAccounts.count, 0)
        return newAccounts
    }
}
