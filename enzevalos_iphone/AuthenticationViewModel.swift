//
//  AuthenticationViewModel.swift
//  enzevalos_iphone
//
//  Created by Cezary Pilaszewicz on 02.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation
import Combine

class AuthenticationViewModel : ObservableObject {

    @Published var errorMessage: String?
    @Published var isDetailedAuthentication: Bool = false
    @Published var showProgressSpinner: Bool = false
    
    let model: AuthenticationModel
    let letterboxModel: LetterboxModel
    var cancellable: AnyCancellable?
    
    static let encryptionOptions: [(name: String, value: Int)] = [(name: "Plaintext", value: MCOConnectionType.clear.rawValue), (name: "StartTLS", value:  MCOConnectionType.startTLS.rawValue), (name: "TLS/SSL", value: MCOConnectionType.TLS.rawValue)]
        
    init(authenticationModel: AuthenticationModel) {
        self.model = authenticationModel
        self.letterboxModel = LetterboxModel.instance
    }

    func validate(_ login: String, _ password: String) -> Void {
        let mailAccount = MailAccount(emailAddress: login, password: password)
        
        showProgressSpinner = true
        cancellable = model.checkConfig(mailAccount: mailAccount, extendedValidation: false).sink { promise in
            switch promise {
            case AuthenticationModel.AuthenticationResult.Success :
                self.authenticationSucceed()
            case AuthenticationModel.AuthenticationResult.Error(let value) :
                self.authenticationFailed(error: value)
            case AuthenticationModel.AuthenticationResult.Timeout :
                self.timeoutNotification()
            }
            self.showProgressSpinner = false
        }
    }
    
    func detailValidation(_ login: String, _ password: String, _ username: String, _ imapServer: String, _ imapPort: String, _      imapEncryption: Int,  _ smtpServer: String, _ smtpPort: String, _ smtpEncryption: Int) -> Void {
        let mailAccount = MailAccount(emailAddress: login, password: password, username: username, imapServer: imapServer, imapPort: Int(imapPort)!, imapEncryption: imapEncryption, smtpServer: smtpServer, smtpPort: Int(smtpPort)!, smtpEncryption: smtpEncryption)
        
        showProgressSpinner = true
        cancellable = model.checkConfig(mailAccount: mailAccount, extendedValidation: true).sink { promise in
            switch promise {
            case AuthenticationModel.AuthenticationResult.Success :
                self.authenticationSucceed()
            case AuthenticationModel.AuthenticationResult.Error(let value) :
                self.authenticationFailed(error: value)
            case AuthenticationModel.AuthenticationResult.Timeout :
                self.timeoutNotification()
            }
            self.showProgressSpinner = false
        }
    }
    
    func startGoogleOauth() {
        return
        /*TODO guard let vc = AppDelegate.getAppDelegate().window?.rootViewController else {
            print("No view controller!")
            return
        }
 
        //TODO Logger.log(onboardingState: .GoogleLogIn, duration: 0)
        
        EmailHelper.singleton().doEmailLoginIfRequired(onVC: vc, completionBlock: {
            guard let userEmail = EmailHelper.singleton().authorization?.userEmail, EmailHelper.singleton().authorization?.canAuthorize() ?? false else {
                print("Google authetication failed")
                return
            }
            let googleImapPort = 993
            let googleSmtpPort = 587
            
            let mailAccount = MailAccount(emailAddress: userEmail.lowercased(), password: "", username: userEmail.lowercased(), imapServer: "imap.gmail.com", imapPort: googleImapPort, imapEncryption: AuthenticationViewModel.encryptionOptions[2].value, smtpServer: "smtp.gmail.com", smtpPort: googleSmtpPort, smtpEncryption: AuthenticationViewModel.encryptionOptions[1].value, authType: MCOAuthType.xoAuth2.rawValue)

            self.showProgressSpinner = true
            self.cancellable = self.model.checkConfig(mailAccount: mailAccount, extendedValidation: true).sink { promise in
                switch promise {
                case AuthenticationModel.AuthenticationResult.Success :
                    self.authenticationSucceed()
                case AuthenticationModel.AuthenticationResult.Error(let value) :
                    self.authenticationFailed(error: value)
                case AuthenticationModel.AuthenticationResult.Timeout :
                    self.timeoutNotification()
                }
                self.showProgressSpinner = false
            }
        })
 */
    }
    
    func authenticationSucceed() {
        letterboxModel.afterOnboarding()        
    }
    
    func authenticationFailed(error: MailServerConnectionError) {
        errorMessage = NSLocalizedString(error.localizedUIBodyString, comment: "")
    }

    func timeoutNotification() {
        errorMessage = NSLocalizedString(MailServerConnectionError.TimeoutError.localizedUIBodyString, comment: "")
    }
}

