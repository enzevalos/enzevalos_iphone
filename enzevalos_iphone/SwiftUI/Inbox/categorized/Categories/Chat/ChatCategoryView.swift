//
//  ChatCategoryView.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 23.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

/// how the home of the chat category looks (with all the contacts in a list)
struct ChatCategoryView: View {
    
    init(model : CategoryViewModel) {
        baseModel  = model
        chatModel = ChatCategoryVM(model)
    }
    
    var baseModel : CategoryViewModel
    @ObservedObject var chatModel : ChatCategoryVM
    
    var body: some View {
            VStack{
                HStack{Spacer()}//make it use full width
                if let size = baseModel.size {
                    let optimalAmount = min(Int(size)/70 , chatModel.singleChats.count)
                    //Preview
                    ForEach(0 ..< optimalAmount,id:\.self){i in if let chat = chatModel.singleChats[safe: i]{
                        Chat_PeopleRow(chat: chat)
                    }}
                    
                }else{
                    //FullScreen
                    ScrollView{
                        ForEach(chatModel.singleChats, id: \.self){chat in
                            Chat_PeopleRow(chat: chat)
                        }
                    }
                }
            }
        .frame(height:baseModel.size).clipped()
    }
}
