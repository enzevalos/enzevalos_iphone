//
//  PasswordView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 01.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

struct PasswordView: View {
    let label = NSLocalizedString("Password", comment: "")
    @Binding var password: String
    @State var securePasswordInput = true
    
    var body: some View {
        HStack {
        if securePasswordInput {
            SecureField(label, text: $password)
                .padding(EdgeInsets(top: 8, leading:10, bottom: 8, trailing: 0))
                .background(Color.white)
                .foregroundColor(Color.black)
                .clipShape(RoundedRectangle(cornerRadius: 8))
                .shadow(radius: 8)
                .keyboardType(.default)
                .autocapitalization(.none)
                .disableAutocorrection(true)

        } else {
            TextField(label, text: $password)
            .padding(EdgeInsets(top: 8, leading:10, bottom: 8, trailing: 0))
            .background(Color.white)
            .foregroundColor(Color.black)
            .clipShape(RoundedRectangle(cornerRadius: 8))
            .shadow(radius: 8)
            .keyboardType(.default)
            .autocapitalization(.none)
            .disableAutocorrection(true)

        }
        Button(action: {
            self.securePasswordInput.toggle()
            }){
                if securePasswordInput {
                    Image(systemName: "eye.slash")
                } else {
                    Image(systemName: "eye")
                }
            }.foregroundColor(Color.primary)
                .background(Color.white)
                .padding([.bottom] , 8)
                .offset(x: -40, y: 4)
        }
        .padding()
    }
}


struct PasswordView_Previews: PreviewProvider {
    @State static var s = ""
    static var previews: some View {
        PasswordView(password: $s)
    }
}
