//
//  MailRowView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 27.10.20.
//  Modified by Chris Offner & Claire Bräuer in March 2021.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct MailRowView <M: DisplayMail>: View {
    let mail: M
    
    var body: some View {
        HStack {
            avatar
            
            VStack(alignment: .leading) {
                HStack {
                    // Sender name
                    Text(mail.sender.name)
                        .fontWeight(mail.isRead ? .regular : .medium)
                        .lineLimit(1)
                    
                    Spacer()
                    
                    // Arrival time
                    Text(mail.date.timeAgoText())
                        .font(.caption)
                }
                
                HStack {
                    // Subject
                    Text(mail.subject)
                        .font(.caption)
                        .lineLimit(2)
                    
                    Spacer()
                    
                    // Attachment indicator if relevant
                    if !mail.displayAttachments.isEmpty {
                        Image(systemName: "paperclip")
                            .font(.caption)
                            .foregroundColor(.secondary)
                    }
                }
            }
            .foregroundColor(mail.isRead ? .secondary : .primary)
        }
        .padding(4)
        .frame(height: 65)
    }
    
    private var avatar: some View {
        mail.sender.avatar
            .resizable()
            .aspectRatio(contentMode: .fit)
            .shadow(radius: 2)
            .opacity(mail.isRead ? 0.45 : 1)
    }
}

struct MailRow_Previews: PreviewProvider {
    static var previews: some View {
        MailRowView(mail: ProxyData.PlainMail)
            .previewLayout(.sizeThatFits)
    }
}
