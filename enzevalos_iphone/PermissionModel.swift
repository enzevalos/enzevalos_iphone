//
//  PermissionModel+.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 19.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Contacts

enum PermissionState {
    case CONTACTPERMISSION, NOTIFICATIONPERMISSION, CONTACTDENIED
}

class PermissionModel: ObservableObject {
    @Published var currentState: PermissionState = .CONTACTPERMISSION
    private var contactStore = CNContactStore()
    
    init() {
        currentState = .CONTACTPERMISSION
        self.requestForAccess(self.contactCheck)
    }
    
    func requestForAccess(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        //requesting for authorization
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        switch authorizationStatus {
        case .authorized:
            completionHandler(true)
            
        case .notDetermined:
            self.contactStore.requestAccess(for: CNEntityType.contacts) { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                } else {
                    if authorizationStatus == CNAuthorizationStatus.denied { }
                    completionHandler(false)
                }
            }
            
        default:
            completionHandler(false)
        }
    }
    
    func contactCheck(_ accessGranted: Bool) {
        if accessGranted {
            requestForNotifications()
        } else {
            DispatchQueue.main.async { self.currentState = .CONTACTDENIED }
            do { sleep(3) }
            self.requestForNotifications()
        }
    }
    
    func requestForNotifications(){
        DispatchQueue.main.async {
            self.currentState = .NOTIFICATIONPERMISSION
        }
        UNUserNotificationCenter.current().requestAuthorization(options: [ .badge], completionHandler: {didAllow, error in
            DispatchQueue.main.async {
                LetterboxModel.instance.afterPermissions()
            }
        })
    }
    
    func showMessage(_ message: String, completion: (() -> Void)?) {
        let alertController = UIAlertController(title: "Letterbox", message: message, preferredStyle: UIAlertController.Style.alert)
        
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (action: UIAlertAction) -> Void in
            if let cb = completion { cb() }
        }
        alertController.addAction(dismissAction)
    }
}

