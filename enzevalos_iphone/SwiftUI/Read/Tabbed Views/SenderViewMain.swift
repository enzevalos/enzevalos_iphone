//
//  SenderViewMain.swift
//  enzevalos_iphone
//
//  Created by fatimaaa96 on 12.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import CoreLocation

struct SenderViewMain <M: DisplayMail>: View {
    // SEE: PersonNameComponents
    
    @EnvironmentObject var model: ReadModel<M>
    
    @State var selectedLandmark: Landmark? = nil
    @State var showingLandmarkDetails = false
    
    var body: some View {
        ScrollView {
            sender
                .padding(.top, 110/2-20)
                .padding()
                .offset(y: (-110/2 + 20))
            icon
            SmallContactListView(contacts: model.mail.tos, title: NSLocalizedString("To", comment: "To"))
            Divider()
            SmallContactListView(contacts: model.mail.ccs, title: NSLocalizedString("Cc", comment: "CC"))
            Divider()
            SmallContactListView(contacts: model.mail.bccs, title: NSLocalizedString("Bcc", comment: "BCC"))
        }
    }
    
    let Landmarks = [
        Landmark(name: "Berlin", domain: "exampledomain.de", location: .init(latitude: 52.520008, longitude: 13.404954)),
        Landmark(name: "New York", domain: "secondexampledomain.de", location: .init(latitude: 40.730610, longitude: -73.935242)),
        Landmark(name: "Sydney", domain: "thirdexampledomain.de", location: .init(latitude: -33.865143, longitude: 151.209900))
    ]
    
    private func selectNextLandmark() {
        let landmarks = Landmarks//  mail.routingStops
        /// This method identifies and moves to the next selected landmark
        if let selectedLandmark = selectedLandmark, let currentIndex = landmarks.firstIndex(where: { $0 == selectedLandmark }), currentIndex + 1 < landmarks.endIndex {
            self.selectedLandmark = landmarks[currentIndex + 1]
        } else {
            selectedLandmark = landmarks.first
        }
    }
    
    private func selectPrevLandmark() {
        let landmarks = Landmarks //mail.routingStops
        /// This method identifies and moves to the prevoius selected landmark
        if let selectedLandmark = selectedLandmark, let currentIndex = landmarks.firstIndex(where: { $0 == selectedLandmark }), currentIndex + -1 >= 0 {
            self.selectedLandmark = landmarks[currentIndex - 1]
        } else {
            selectedLandmark = landmarks.last
        }
    }
    
    private var icon: some View {
        NavigationLink(destination: ContactView(contact: model.mail.sender)) {
            model.mail.sender.avatar
                .resizable()
                .frame(width: 130, height: 110)
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.white, lineWidth: 4))
                .shadow(radius: 10)
                .offset(y: -110/2)
                .padding(.bottom, -110/2)
        }
    }
    
    
    private var sender: some View {
        VStack (alignment: .leading, spacing: 10) {
            Text(NSLocalizedString("Sender", comment: "Sender"))
                .font(.headline)
                .padding(.bottom, 10)
            NavigationLink(destination: ContactView(contact: model.mail.sender)) {
                HStack {
                    VStack (alignment: .leading) {
                        Text(model.mail.sender.name)
                            .font(.headline)
                        Text(model.mail.sender.email)
                        .foregroundColor(.secondary)
                    }
                }
                Spacer()
                Image(systemName: "chevron.right")
            }
            
            HStack{
                Text(String(format: NSLocalizedString("ReadView.Sender.Previous", comment: "100 previous received mails"), model.mail.sender.previousMails))
                Spacer()
                Text(String(format: NSLocalizedString("ReadView.Sender.Responses", comment: "5 previous sent mails"), model.mail.sender.previousResponses))
                // TODO: Add last mail date
                // TODO: Go to mails?
            }
        }
        .padding(10)
    }
    
    private var map: some View {
        ZStack {
            ZStack {
                MapView(landmarks: Landmarks,//mail.routingStops,
                        selectedLandmark: $selectedLandmark,  showingLandmarkDetails: $showingLandmarkDetails)
                    .frame(height: 300)
                HStack {
                    Button(action: {
                        /// this button displays the previous selected landmark
                        self.selectPrevLandmark()
                    }) {
                        Text("Button.Prev")
                            .foregroundColor(.black)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(6)
                            .shadow(radius: 3)
                            .padding(.top, 150)
                    }
                    
                    Spacer()
                    Button(action: {
                        /// this button displays the next selected landmark
                        self.selectNextLandmark()
                    }) {
                        Text("Button.Next")
                            .foregroundColor(.black)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(6)
                            .shadow(radius: 3)
                            .padding(.top, 150)
                    }
                }
            } .alert(isPresented: $showingLandmarkDetails) {
                /// alert displays the landmark details and gets trigered when the user taps the information button of a landmark
                Alert(title: Text("Domain for this location"), message: Text(selectedLandmark?.domain ?? "Missing place information"), dismissButton: .default(Text("OK")) )
            }
        }
    }
    
    private func goToContact(contact: M.C) {
       // Tbd...
    }
}

struct SenderView_Previews: PreviewProvider {
    static var model = ReadModel(mail: ProxyData.SecureMail)
    static var previews: some View {
        model.currentTab = ReadPart.Header.value
        let sim = Simulators<ReadMainView<ProxyMail>>()
        return sim.previews(view: ReadMainView(model: model))
    }
}
