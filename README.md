## Coding Style

Please try to adhere to the [Github Swift Style Guide](https://github.com/github/swift-style-guide).

(If you want it super specific, there is also the very deep [Ray Wenderlich Guide](https://github.com/raywenderlich/swift-style-guide))

You can use the [Swimat Plugin](https://github.com/Jintin/Swimat) to do parts of that automatically.

## Documentation

Please document your code using [markup](https://developer.apple.com/library/archive/documentation/Xcode/Reference/xcode_markup_formatting_ref/index.html#//apple_ref/doc/uid/TP40016497-CH2-SW1)