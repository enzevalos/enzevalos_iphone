//
//  OutgoingMail.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 27.10.18.
//  Copyright © 2018 fu-berlin. All rights reserved.
//

import Foundation

class OutgoingMail {
    static var OutgoingFolder = FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: "OutgoingMails", parent: nil, children: nil, flags: Int16(MCOIMAPFolderFlag.sentMail.rawValue))
    
    private var encAddresses: [MCOAddress] = []
    var encReceivers: [MCOAddress] {
        get {
            return encAddresses
        }
    }
    
    private var plainAddresses: [MCOAddress] = []
    var plainReceivers: [MCOAddress] {
        get {
            return plainAddresses
        }
    }
    
    private let toAddresses: [MCOAddress]
    private let ccAddresses: [MCOAddress]
    private let bccAddresses: [MCOAddress]
    var username = UserManager.loadUserValue(Attribute.accountname) as? String ?? ""
    var useraddress = (UserManager.loadUserValue(Attribute.userAddr) as? String ?? "")
    var userDisplayName = UserManager.loadUserValue(Attribute.userDisplayName) as? String
    
    var sender: MCOAddress {
        get {
            if let name = userDisplayName, name.lowercased() != useraddress {
                return MCOAddress.init(displayName: name, mailbox: useraddress)
            } else {
                return MCOAddress.init(displayName: useraddress, mailbox: useraddress)
            }
        }
    }
    private var secretKeyID: String {
        get {
            return PersistentDataProvider.prefKeyID
        }
    }
    private let subject: String
    private var textparts: Int
    private let textContent: String?
    private var htmlContent: String? = nil
    private var attachments: [MCOAttachment] = []
    private var attachedSignature: Data?
    var pgpData: Data? {
        get {
           return createPGPEncData()
        }
    }
    
    var plainData: Data? {
        get {
            guard plainAddresses.count > 0  else {
                return nil
            }
            if loggingMail {
                return nil
            }
            return createPlainData()
        }
    }
    private var cryptoObject: CryptoObject? = nil
    
    private var sendEncryptedIfPossible: Bool
    var loggingMail = false
    var warningReact = false
    var inviteMail = false
    var onlySelfEnc = false
    var isPending = false
    var autocrypt = true
    
    private var isDraft = false
    
    fileprivate var exportSecretKey = false
    fileprivate var keyID: String?
    fileprivate var keyData: String?
    
    private var informUser = false

    var imapFlag: MCOMessageFlag {
        get {
            if isDraft {
                return MCOMessageFlag.draft
            }
            else if isPending {
                return MCOMessageFlag.submitPending
            }
            else {
                return MCOMessageFlag.mdnSent
            }
        }
    }
    
    var signedKeyId: String? {
        if createPGPEncData() != nil, let cryptoObj = cryptoObject {
            return cryptoObj.signKey
        }
        return nil
    }
    
    private var mail: MailRecord?
    
    init(toAddresses: [String],
         ccAddresses: [String],
         bccAddresses: [String],
         subject: String,
         textContent: String,
         htmlContent: String?,
         textparts: Int = 0,
         sendEncryptedIfPossible: Bool = true,
         attachments: [MCOAttachment] = []) {
        self.toAddresses = OutgoingMail.mapToMCOAddresses(addr: toAddresses)
        self.ccAddresses = OutgoingMail.mapToMCOAddresses(addr: ccAddresses)
        self.bccAddresses = OutgoingMail.mapToMCOAddresses(addr: bccAddresses)
        self.subject = subject
        self.textContent = textContent
        self.htmlContent = htmlContent
        self.textparts = textparts
        self.sendEncryptedIfPossible = sendEncryptedIfPossible
        self.attachments = attachments
        self.orderReceivers()
        if let attachment = attachments.first {
            let signature = OutgoingMail.extractPGPSignature(attachment: attachment)
            if let signatureData =  signature.first?.data(using: .utf8) {
                self.attachedSignature = signatureData
            }
        }
    }
    
    init(mail: MailRecord){
        toAddresses = OutgoingMail.mapToMCOAddresses(addr: mail.tos)
        ccAddresses = OutgoingMail.mapToMCOAddresses(addr: mail.ccs)
        bccAddresses = OutgoingMail.mapToMCOAddresses(addr: mail.bccs)
        subject = mail.subject
        textContent = mail.body
        htmlContent = "" //.decryptedBody //TODO FIX HERE
        textparts = 0 //TODO FIX HERE
        //sendEncryptedIfPossible = mail.isEncrypted //TODO FIX HERE
        sendEncryptedIfPossible = mail.encryptionState != .UnableToDecrypt
        self.orderReceivers()
        self.mail = mail
        //self.loggingMail = mail.isCorrectlySigned //TODO FIX HERE
        //TODO ??? self.attachedSignature = mail.attachedSignature
    }
    
    func store() {
        // Problem: Was ist die MessageID?
        let body = "" + (htmlContent ?? "") + (textContent ?? "")
        let bcc = bccAddresses.map({AddressProperties(email: $0.mailbox, name: $0.displayName)})
        let cc = ccAddresses.map({AddressProperties(email: $0.mailbox, name: $0.displayName)})
        let attachmentProperties = self.attachments.map({AttachmentProperties(type: 0, name: $0.filename ?? "", mimeType: $0.mimeType, contentID: $0.contentID, data: $0.data)})
        let to = self.toAddresses.map({AddressProperties(email: $0.mailbox, name: $0.displayName)})
        let sigState = self.cryptoObject?.signatureState.rawValue ?? SignatureState.NoSignature.rawValue
        let encState = self.cryptoObject?.encryptionState.rawValue ?? EncryptionState.NoEncryption.rawValue
        let uuid = UUID()
        let from = AddressProperties(email: self.sender.mailbox, name: self.sender.displayName)
        let m = MailProperties(messageID: uuid.uuidString, uid: UInt64(arc4random()), subject: subject, date: Date(), flags: 0, from: from, to: to, cc: cc , bcc: bcc, folder: OutgoingMail.OutgoingFolder, body: body, attachments: attachmentProperties, signatureState: sigState, encryptionState: encState, signatureKey: nil, decryptionKey: nil, autocryptHeaderKey: [], attachedPublicKeys: [], attachedSecretKeys: [])
        LetterboxModel.instance.dataProvider.importNewData(from: [m], completionHandler: { _ in })
        // TODO Fix crypto stuff (keys etc.)
    }
    
    func addTextAttachment(text: String) {
        let attachment = MCOAttachment.init(text: text)!
        attachments.append(attachment)
    }
    
    func addAttachment(filename: String, mimeType: String, charset: String?, data: Data) {
        if let attachment = MCOAttachment.init(contentsOfFile: filename){
            attachment.mimeType = mimeType
            if let charset = charset {
                attachment.setContentTypeParameterValue(charset, forName: "charset")
            }
            attachment.setContentTypeParameterValue(filename, forName: "name")
            attachment.filename = filename
            attachment.data = data
            attachments.append(attachment)
        }
    }
    
    func send(informUser: Bool = false) {
        self.informUser = informUser
        if let mail = mail {
            mail.delete()
        }
        LetterboxModel.instance.mailHandler.sendSMTP(mail: self, callback: { error in
            if error != nil {
                self.store()
            }
            if informUser {
                self.sendCallback(error: error)
            }
        })
    }
    
    func sendCallback(error: MailServerConnectionError?){
        if let error = error {
            if error == .NoInternetconnection || error == .ConnectionError {
                self.store()
                return
            }
        }
        else {
           // TODO DataHandler.handler.startToSendMore()
        }
        
    }
    
    func logMail() -> Bool {
        guard Logger.logging && !loggingMail else {
            return false
        }
        // We don't log the logging mail
        if loggingMail {
            return false
        }
        // TODO let fromLogging: MailAddress = DataHandler.handler.getMailAddress(useraddr, temporary: false) as! Mail_Address
        let toLogging: [AddressRecord] = []
        let ccLogging: [AddressRecord] = []
        let bccLogging: [AddressRecord] = []
        let secureAddrsInString = encAddresses.map { $0.mailbox }
        var secureAddresses: [AddressRecord] = []
        for addr in toLogging {
            for sec in secureAddrsInString {
                if addr.email == sec {
                    secureAddresses.append(addr)
                }
            }
        }
        for addr in ccLogging {
            for sec in secureAddrsInString {
                if addr.email == sec {
                    secureAddresses.append(addr)
                }
            }
        }
        for addr in bccLogging {
            for sec in secureAddrsInString {
                if addr.email == sec {
                    secureAddresses.append(addr)
                }
            }
        }
        /* TODO: Consider logging...
        var inviteMailContent: String? = nil
        if inviteMail {
            inviteMailContent = textparts.description
        }
        if let co = cryptoObject {
            if isDraft {
               //TODO  Logger.log(createDraft: toLogging, cc: ccLogging, bcc: bccLogging, subject: subject, bodyLength: textContent?.count ?? 0, isEncrypted: isEnc, isSigned: isSig, myKeyID: keyID ?? "")
            }
            else {
               //TODO  Logger.log(sent: fromLogging, to: toLogging, cc: ccLogging, bcc: bccLogging, subject: subject, bodyLength: co.chiperString?.count ?? 0, isEncrypted: isEnc, decryptedBodyLength: ("\n" + (co.plaintext ?? "")).count, decryptedWithOldPrivateKey: false, isSigned: isSig, isCorrectlySigned: isCorSig, signingKeyID: co.signKey ?? "", myKeyID: sk?.keyID ?? "", secureAddresses: secureAddresses, encryptedForKeyIDs: OutgoingMail.addKeys(adrs: pgpAddresses), inviteMailContent: inviteMailContent, invitationMail: inviteMail)
            }
        } //TODO: double logging? crpyto mail and plain mail?
        else {
            if isDraft {
                //TODO Logger.log(createDraft: toLogging, cc: ccLogging, bcc: bccLogging, subject: subject, bodyLength: textContent?.count ?? 0, isEncrypted: false, isSigned: false, myKeyID: "")
            }
            else {
                var textContentCount = textContent?.count ?? 0
                if textContentCount != 0 {
                    textContentCount += 1 //for \n
                }
                // TODO Logger.log(sent: fromLogging, to: toLogging, cc: ccLogging, bcc: bccLogging, subject: subject, bodyLength: textContentCount, isEncrypted: false, decryptedBodyLength: textContentCount , decryptedWithOldPrivateKey: false, isSigned: false, isCorrectlySigned: false, signingKeyID: "", myKeyID: "", secureAddresses: [], encryptedForKeyIDs: [], inviteMailContent: inviteMailContent, invitationMail: inviteMail)
            }
        }*/
        return true
    }
    
    private static func extractPGPSignature(attachment: MCOAttachment) -> [String] {
        if let content = attachment.decodedString() {
            return PGPPart.SIGNATURE.findPGPPartInString(content: content)
        }
        else if let content = String(data: attachment.data, encoding: .ascii){
            return PGPPart.SIGNATURE.findPGPPartInString(content: content)
        }
        return []
    }
    
    private func createPlainData() -> Data {
        let data = createSignedSMIMEData()
        if let data = data {
            return data
        }
        let builder = createBuilder()
        if let html = htmlContent {
            builder.htmlBody = html
        } else {
            builder.textBody = textContent // Maybe add both?!
        }
        for att in attachments {
            builder.addAttachment(att)
        }
        return builder.data()
    }
    
    private func receiverArrayToString(receivers: [Any]?) -> String? {
        guard let receivers = receivers else {
            return nil
        }
        var output = ""
        for x in receivers {
            if let receiver = x as? MCOAddress {
                if output == "" {
                    output = receiver.rfc822String()
                } else {
                    output = output + ", " + receiver.rfc822String()
                }
            }
        }
        if output == "" {
            return nil
        }
        return output
    }
    
    private func makeCustomHeaderString(builder: MCOMessageBuilder) -> String {
        let header = builder.header
        var headerString = ""
        if let from = header?.from.rfc822String() {
            headerString += "From: " + from + "\r\n"
        }
        if let tos = receiverArrayToString(receivers: header?.to) {
            headerString += "To: " + tos + "\r\n"
        }
        if let ccs = receiverArrayToString(receivers: header?.cc) {
            headerString += "CC: " + ccs + "\r\n"
        }
        if let bccs = receiverArrayToString(receivers: header?.bcc) {
            headerString += "BCC: " + bccs + "\r\n"
        }
        headerString += "X-Mailer: Letterbox \r\n"
        if let header = Autocrypt.customAutocryptHeader {
            headerString += header
        }
        if let msgID = header?.messageID {
            headerString += "Message-ID: " + msgID + "\r\n"

        }
        if let subject = header?.subject {
            headerString += "Subject: " + subject + "\r\n"
        }
        return headerString
    }
    
    func createSignedSMIMEData() -> Data? {
        let builder = createBuilder()
        guard let signObj = try? SMIME().sign(plainData: builder.dataForEncryption(), myEmail: builder.header.from.mailbox), let content = signObj.chiperString else {
            return nil
            }
        // add signed content
        let mail = makeCustomHeaderString(builder: builder) + content
        return mail.data(using: .utf8)
    }
    
    func createEncSMIMEData() -> Data? {
        let builder = createBuilder()
        let smime = SMIME()
        let ids = OutgoingMail.addKeys(adrs: encAddresses, cryptoScheme: .SMIME)
        // SMIME
        guard let encObj = try? smime.encrypt(plainData: builder.dataForEncryption(), ids: ids, ownAddr: builder.header.from.mailbox, encryptForMyId: true), let content = encObj.chiperString else {
            return nil
        }
        // add signed content
        let mail = makeCustomHeaderString(builder: builder) + content
        return mail.data(using: .utf8)
    }
    
    private func createPGPEncData() -> Data? {
        
        let encMailBuilder = createBuilder()
        let pgp = SwiftPGP()
        var missingOwnPublic = false
        var pgpKeyIds: [String] = []
        pgpKeyIds.append(secretKeyID)
      
        if !onlySelfEnc && encAddresses.count > 0{
            pgpKeyIds.append(contentsOf: OutgoingMail.addKeys(adrs: encAddresses, cryptoScheme: .PGP))
        }
        // We add our public key if one uses pgp but may not have our public key
        if let keys = PersistentDataProvider.dataProvider.generateFetchedPublicKeysResultController(keyIds: pgpKeyIds)?.fetchedObjects {
            for key in keys {
                if !key.receivedMyPublicKey {
                    missingOwnPublic = true
                    // TODO Update received public key = True
                }
            }
        }
        if missingOwnPublic {
            if let myPK = pgp.exportKey(id: secretKeyID, isSecretkey: false, autocrypt: false), let data = myPK.data(using: .utf8) {
                addAttachment(filename: "\(secretKeyID).asc", mimeType: "application/pgp-keys", charset: nil, data: data)
            }
        }
        let plainData = encMailBuilder.dataForEncryption()
        cryptoObject = pgp.encrypt(plaindata: plainData!, ids: pgpKeyIds, myId: secretKeyID)
        if let encData = cryptoObject?.chiphertext {
            return encMailBuilder.openPGPEncryptedMessageData(withEncryptedData: encData)
        }
        return nil
    }
    
    private func orderReceivers(){
        
        if !self.sendEncryptedIfPossible {
            plainAddresses.append(contentsOf: toAddresses)
            plainAddresses.append(contentsOf: ccAddresses)
            plainAddresses.append(contentsOf: bccAddresses)
            return
        }
    
        var addresses = [String]()
        addresses.append(contentsOf: toAddresses.map({$0.mailbox}))
        addresses.append(contentsOf: ccAddresses.map({$0.mailbox}))
        addresses.append(contentsOf: bccAddresses.map({$0.mailbox}))
        
        if let keys = LetterboxModel.instance.dataProvider.generateFetchedAddresesWithKeyResultsController(addresses: addresses).fetchedObjects {
            let addrsWithKeys = keys.map({$0.email})
            encAddresses.append(contentsOf: toAddresses.filter({ addrsWithKeys.contains($0.mailbox) }))
            encAddresses.append(contentsOf: ccAddresses.filter({ addrsWithKeys.contains($0.mailbox) }))
            encAddresses.append(contentsOf: bccAddresses.filter({ addrsWithKeys.contains($0.mailbox) }))
            plainAddresses.append(contentsOf: toAddresses.filter({ !addrsWithKeys.contains($0.mailbox) }))
            plainAddresses.append(contentsOf: ccAddresses.filter({ !addrsWithKeys.contains($0.mailbox) }))
            plainAddresses.append(contentsOf: bccAddresses.filter({ !addrsWithKeys.contains($0.mailbox) }))
        }
    }
    
    
    private func createBuilder() -> MCOMessageBuilder {
        let builder = MCOMessageBuilder()
        builder.header.to = toAddresses
        builder.header.cc = ccAddresses
        builder.header.bcc = bccAddresses
        builder.header.from = sender
        builder.header.subject = subject
       // builder.header.setExtraHeaderValue("1.0", forName: "MIME-Version")
        builder.header.setExtraHeaderValue("Letterbox", forName: "X-Mailer")
        if autocrypt {
            Autocrypt.addAutocryptHeader(builder)
        }
        if let text = textContent {
            builder.textBody = "\n\n"+text
        }
        if let html = htmlContent {
            builder.htmlBody = html
        }
        
        for attachment in attachments {
            builder.addAttachment(attachment)
        }
        
        if exportSecretKey {
            if let keyID = keyID, let keyData = keyData {
                Autocrypt.createAutocryptKeyExport(builder: builder, keyID: keyID, key: keyData)
            }
        }
        return builder
    }
    
    private static func mapToMCOAddresses(addr: [String]) -> [MCOAddress] {
        return addr.map({(value: String) -> MCOAddress in
            return MCOAddress.init(mailbox: value)
        })
    }
    
    private static func mapToMCOAddresses(addr: [AddressRecord]) -> [MCOAddress]{
        return addr.map{$0.mcoAddress}
    }
    
    
    
    private static func addKeys(adrs: [MCOAddress], cryptoScheme: CryptoScheme) -> [String] {
        var ids = [String]()
        if let records = PersistentDataProvider.dataProvider.generateFetchedAddresesWithKeyResultsController(addresses: adrs.map({$0.mailbox})).fetchedObjects {
            for adr in records {
                if let prim = adr.primaryKey, prim.type == cryptoScheme, let id = prim.fingerprint {
                    ids.append(id)
                }
            }
        }
        return ids
    }
    
    static func createDraft(toAddresses: [String], ccAddresses: [String], bccAddresses: [String], subject: String, textContent: String, htmlContent: String?) -> OutgoingMail{
        let mail = OutgoingMail(toAddresses: toAddresses, ccAddresses: ccAddresses, bccAddresses: bccAddresses, subject: subject, textContent: textContent, htmlContent: htmlContent)
        mail.isDraft = true
        return mail
    }
    static func createInvitationMail(toAddresses: [String], ccAddresses: [String], bccAddresses: [String], subject: String, textContent: String, htmlContent: String?) -> OutgoingMail{
        let mail = OutgoingMail(toAddresses: toAddresses, ccAddresses: ccAddresses, bccAddresses: bccAddresses, subject: subject, textContent: textContent, htmlContent: htmlContent)
        mail.inviteMail = true
        return mail
    }

    static func createSecretKeyExportMail(keyID: String, keyData: String) -> OutgoingMail{
        let useraddr = (UserManager.loadUserValue(Attribute.userAddr) as! String)
        let mail = OutgoingMail(toAddresses: [useraddr], ccAddresses: [], bccAddresses: [], subject: "Autocrypt Setup Message", textContent: "", htmlContent: nil)
        mail.plainAddresses = mail.encAddresses
        mail.encAddresses = []
        mail.exportSecretKey = true
        mail.keyData = keyData
        mail.keyID = keyID
        return mail
    }
    static func createLoggingMail(addr: String, textcontent: String) -> OutgoingMail{
            let mail = OutgoingMail(toAddresses: [addr], ccAddresses: [], bccAddresses: [], subject: "[Letterbox] Log", textContent: textcontent, htmlContent: nil)
            mail.loggingMail = true
            return mail
    }
}
