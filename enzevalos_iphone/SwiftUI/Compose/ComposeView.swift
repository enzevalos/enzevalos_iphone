//
//  ComposeView.swift
//  enzevalos_iphone
//
//  Created by Chris Offner & Claire Bräuer on 03.03.21.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import SwiftUI

/// A view used to compose and send an email.
struct ComposeView: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var model: ComposeModel
    // properties regarding the RecipientFields
    @State private var cc = ""
    // properties regarding attachments
    // used to control the floating action button
    @State private var showAttachments = false
    
    /// - Parameter preData: Data of email to reply to or forward.
    init(preData: PreMailData? = nil) {
        model = ComposeModel(preData: preData)
    }
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                // Top bar with Cancel and Send button
                ComposeViewHeader(model: model)
                ScrollView{
                Divider()
                // "To" recipients
                RecipientField(model: model.recipientsModel.toModel,
                               composeViewHeight: geometry.size.height)
                
                Divider()
                
                // "Cc/Bcc" recipients
                CcAndBccFields(model: model.recipientsModel,
                               composeViewHeight: geometry.size.height)
                
                // Subject
                HStack {
                    Text("Subject")
                        .foregroundColor(Color(UIColor.tertiaryLabel))
                    TextField("", text: $model.subject)
                        .autocapitalization(.none)
                }
                
                Divider()
                
                // Email body with attachment floating action button
                ZStack(alignment: Alignment(horizontal: .trailing, vertical: .top)) {
                                
                    TextEditor(text: $model.body)

                    // floating action button
                    Button (
                        action: {showAttachments.toggle()},
                        label: {
                            Image(systemName: "paperclip")
                        }
                    )
                        .frame(alignment: .topLeading)
                }
                
                // optional upload attachments section
                if showAttachments {
                    VStack {
                        Divider()
                        AddAttachmentsView(model: model)
                    }
                    .transition(.move(edge: .bottom))
                }
            }
            }
            // show this alert when user sends the mail without attachments,
            // but mentions them in the mail
            .alert(isPresented: $model.showAttachmentAlert) {
                AttachmentAlert
            }
            .padding()
            .animation(.easeInOut)
            .onDisappear{
                // when the ComposeView is closed, we make sure to
                // clean the Documents Directory from all the remaining
                // copies of attachment files
                model.removeAttachmentCopiesFromDocumentsDirectory()
            }
        }
    }
    
    /// an alert which is shown when the user mentions attachments in the mail, but sends it without attaching anything
    var AttachmentAlert: Alert {
        Alert(
            title: Text("Attachment.Alert.Title"),
            message: Text("Attachment.Alert.Text"),
            primaryButton: .destructive(Text("Attachment.Alert.PrimaryButton").foregroundColor(Color.blue)) {
                // send the mail anyway
                model.sendMail()
                // remove the attachments from the Documents Directory
                // to keep the DD clean
                model.removeAttachmentCopiesFromDocumentsDirectory()
                presentationMode.wrappedValue.dismiss()
            },
            secondaryButton: .destructive(Text("Attachment.Alert.SecondaryButton").foregroundColor(Color.blue)) {
                // quit sending, enable further editing
            }
        )
    }
    
    /// a func that deletes all the copies of the attachment files in the Documents Directory to keep it clean
    func removeAttachmentCopiesFromDocumentsDirectory() {
        for current in model.attachments {
            current.removeFileFromDocumentsDirectory()
        }
    }
}

/// A view in which recipients get added or removed.
struct RecipientField: View {
    @ObservedObject var model: RecipientFieldModel
    @State private var showList = false
    @State private var indexOfSelected: Int?
    var composeViewHeight: CGFloat
    
    var body: some View {
        VStack {
            HStack {
                // Recipient text field
                Text(model.type.rawValue)
                    .foregroundColor(Color(UIColor.tertiaryLabel))
                
                // Selected recipients as blue capsules,
                // followed by TextField for new recipients.
                ScrollView(.horizontal) {
                    HStack(spacing: 3) {
                        CommittedRecipients
                        NewRecipientTextField
                    }
                }
                
                // Toggles contact list
                if model.type != .ccBcc {
                    Button(action: { showList.toggle() }) {
                        Image(systemName: !showList ? "plus.circle" : "chevron.up.circle")
                    }
                }
            }
            .frame(height: 20)
            
            // Contact list
            if showList || !model.suggestions.isEmpty {
                Divider()
                RecipientListView()
                    .environmentObject(model)
                    .frame(height: composeViewHeight * 0.55)
            }
        }
    }
    
    /// A view that lists all recipients that have been committed to this RecipientField.
    var CommittedRecipients: some View {
        ForEach(model.selectedContacts) { (recipient: AddressRecord) in
            let isSelected = model.selectedContacts.firstIndex(of: recipient) == indexOfSelected
            
            RecipientCapsule(recipient: recipient,
                             isSelected: isSelected,
                             model: model,
                             indexOfSelected: $indexOfSelected)
                .onTapGesture {
                    indexOfSelected = isSelected ? nil : model.selectedContacts
                        .firstIndex(of: recipient)
                }
        }
        .padding(2)
    }
    
    /// A TextField into which new recipients can be entered.
    var NewRecipientTextField: some View {
        TextField("", text: $model.text) { isEditing in
            if model.type != .to,
               let parent = model.parentRecipientModel {
                parent.isEditingCcOrBcc = isEditing
            }
            
        } onCommit: {
            model.commitText()
            // TODO: Fix bug on first Cc or Bcc recipient commit
            // For some reason, model.selectedContacts.count stays 0
            // after the first committed recipient, leading to the Cc
            // and Bcc fields getting collapsed again despite the
            // first recipient clearly getting rendered as a blue
            // capsule in the ForEach loop above. 🤔🤔🤔
        }
        .frame(width: 200)              // TODO: Stretch dynamically over available horizontal space
        .autocapitalization(.none)      //       .frame(maxWidth: .infinity) somehow doesn't work
        .keyboardType(.emailAddress)
    }
}

/// A view that shows either a single "Cc/Bcc" field or separate "Cc" and "Bcc" fields.
struct CcAndBccFields: View {
    @ObservedObject var model: RecipientsModel
    var composeViewHeight: CGFloat
    
    var body: some View {
        VStack {
            RecipientField(model: model.ccModel,
                           composeViewHeight: composeViewHeight)
            
            Divider()
            
            if model.showBccField {
                RecipientField(model: model.bccModel,
                               composeViewHeight: composeViewHeight)
                Divider()
            }
        }
    }
}

/// A view that shows a recipient committed to a recipient field.
struct RecipientCapsule: View {
    var recipient: AddressRecord
    var isSelected: Bool
    @ObservedObject var model: RecipientFieldModel
    @Binding var indexOfSelected: Int?
    
    var body: some View {
        ZStack {
            Capsule()
                .stroke(isSelected ? Color.accentColor : .secondary,
                        lineWidth: isSelected ? 2 : 1)
                
            HStack {
                Text(name)
                    .fixedSize(horizontal: true, vertical: false)
                
                // Remove button shown if recipient capsule is selected
                if isSelected {
                    Button(action: removeRecipient) {
                        Image(systemName: "xmark")
                    } // Transition: Slide in from left, slide out to left
                    .transition(.asymmetric(
                                    insertion: AnyTransition
                                        .opacity.combined(with: .slide),
                                    removal: AnyTransition
                                        .opacity.combined(with: .move(edge: .leading)))
                    )
                }
            }
            .padding(EdgeInsets(top: 2, leading: 8, bottom: 2, trailing: 8))
            .foregroundColor(isSelected ? .accentColor : .secondary)
        }
    }
    
    // TODO: Use actual displayname once it's implemented.
    // Currently just uses first part of email before "@".
    var name: String {
        recipient.displayname ?? recipient.email.components(separatedBy: "@")[0]
    }
    
    /// Removes recipient from selected contacts of respective RecipientFieldModel.
    func removeRecipient() {
        if let index = model.selectedContacts.firstIndex(of: recipient) {
            model.deselectContact(at: index)
            indexOfSelected = nil
        }
    }
}

/// Canvas Preview
struct ComposeView_Previews: PreviewProvider {
    static var previews: some View {
        ComposeView()
    }
}
