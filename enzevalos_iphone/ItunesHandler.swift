//
//  ItunesKeyHandling.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 08.04.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation

class FileBrowserHandler {
    static let fileBrowserHandler = FileBrowserHandler()

    
    static func simpleImportSecretKey() {
        var sks = fileBrowserHandler.storedKeys.filter{$0.isSecret}
        // filter known secret keys
        if let knownKeyIDs = PersistentDataProvider.dataProvider.fetchedSecretKeyResultsController.fetchedObjects?.map({$0.fingerprint}) {
            sks = sks.filter({!knownKeyIDs.contains($0.keyID)})
        }
        // Import only keys with our mail addr.
        sks = sks.filter{
            if let useradr = UserManager.loadUserValue(Attribute.userAddr) as? String {
                return $0.mailAddresses.contains(useradr)
            }
            return false
        }
        // No alert
        /*
        if let sk = sks.first {
            let alert = MinimalImportUI(secretKey: sk, controller: con)
            alert.importSecretKeyDialog(first: true)
        }
        */
    }
    
    func extractSecretKeys(withKnownKeys: Bool) -> [TempKey] {
        var newSecretKeys = storedKeys.filter{$0.isSecret}
        if !withKnownKeys, let keys = PersistentDataProvider.dataProvider.fetchedSecretKeyResultsController.fetchedObjects {
            var knownIDs = keys.map{$0.keyID}
            knownIDs = knownIDs.filter{return !($0 == "")}
            newSecretKeys = newSecretKeys.filter{!knownIDs.contains($0.keyID)}
        }
        return newSecretKeys
    }
    
    func extractPublicKeys(withKnownKeys: Bool) -> [TempKey] {
        var newPublicKeys = storedKeys.filter{!$0.isSecret}
        if !withKnownKeys, let keys = PersistentDataProvider.dataProvider.fetchedPublicKeyResultsController.fetchedObjects {
            var knownIDs = keys.map{$0.keyID}
            knownIDs = knownIDs.filter{return !($0 == "")}
            newPublicKeys = newPublicKeys.filter{!knownIDs.contains($0.keyID)}
        }
        return newPublicKeys
    }
    
    var hasStoredKeys: Bool {
        get {
            return !keyURL.isEmpty
        }
    }
    var storedKeys: [TempKey] {
        get {
            var keys: [TempKey] = []
            for url in keyURL {
                if let file = try? FileHandle(forReadingFrom: url) {
                    let data = file.readDataToEndOfFile()
                    var creationDate = Date()
                    if let attrs = try? fileManager.attributesOfItem(atPath: url.path) as NSDictionary, let date = attrs.fileCreationDate() {
                        creationDate = date
                    }
                    keys.append(contentsOf: pgp.readKeys(data: data,importDate: creationDate))
                }
            }
            return keys
        }
    }
    
    private let fileManager: FileManager
    private let pgp = SwiftPGP()
    private let smime = SMIME()
    
    private var keyURL: [URL] {
        get {
            var keys: [URL] = []
            let dirs = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
            for url in dirs {
                if let files = try? fileManager.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: .skipsHiddenFiles) {
                    let pgpKeyFiles = files.filter{ $0.pathExtension == "asc"}
                    keys.append(contentsOf: pgpKeyFiles)
                    let smimeCertsFiles = files.filter{Certificate.isCertificateFileEnding(ending: $0.pathExtension)} 
                    keys.append(contentsOf: smimeCertsFiles)
                }
            }
            return keys
        }
    }

    init() {
        fileManager = FileManager.default
    }
    
    func importPublicKeys() -> [String]{
        let publicKeys = extractPublicKeys(withKnownKeys: true)
        var keyIds = pgp.store(tempKeys: publicKeys)
        
        // Import SMIME keys in keychain.
        var certs = [String]()
        for key in publicKeys {
            if key.type == .SMIME, let cert = key.smimeCert?.pem {
                certs.append(cert)
            }
        }
        keyIds.append(contentsOf: smime.importCerts(certs: certs))
        
        // Add keyIds in Database
        let properties = publicKeys.map{PublicKeyProperties(fingerprint: $0.keyID, cryptoProtocol: $0.type, origin: .FileTransfer, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: nil, usedAddresses: $0.mailAddresses.map{AddressProperties(email: $0)})}
        PersistentDataProvider.dataProvider.importNewData(from: properties, completionHandler: {error in print("Import public keys: \(String(describing: error))")})
        
        return keyIds
    }
}
