//
//  ContactMailListView.swift
//  enzevalos_iphone
//
//  Created by Amir Rajabi and Goekhan Guemues on 25.03.21
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI
import CoreData

/// A view that shows up to three emails received from or sent to this contact depending on the folderType.
struct ContactMailListView: View {
    public var folderpath: String
    public var folderName: String
    public var emailAddresses: [String]
    public var fetchRequest: FetchRequest<MailRecord>
    public var emails: FetchedResults<MailRecord>{fetchRequest.wrappedValue}
    
    
    init(folderpath: String, folderName: String, emailAddresses: [String]) {
        fetchRequest = MailRecord.mailsInFolderFetchRequest(folderpath: folderpath)
        self.folderpath = folderpath
        self.folderName = folderName
        self.emailAddresses = emailAddresses
    }
    
    
    var body: some View {
        ZStack {
            newRoundedRectangle()
            VStack {
                // Creates a filtered array of the array emails
                let filteredEmails = self.emails.filter(filterEmailList)
                
                // Lists up to three emails of the contact from the filtered emails
                ForEach (0..<min(3,filteredEmails.count)) { record in
                    NavigationLink(
                        destination: ReadMainView(model: ReadModel(mail: filteredEmails[record]))) {
                        SimpleMailRowView(mail: filteredEmails[record])
                    }
                    if record < min(2, filteredEmails.count - 1) {
                        Divider()
                    }
                }
                
                // If the list contains more than three emails, a button is displayed
                if filteredEmails.count > 3{
                    HStack {
                        Spacer()
                        NavigationLink(
                            destination: MoreContactMailView(folderpath: folderpath, folderName: folderName, emailAddresses: emailAddresses)) {
                            Text(NSLocalizedString("Contact.Button.ShowMore", comment: "Contact.Button.ShowMore"))
                        }
                    }.padding(.top, -10)
                }
                
            }
            .padding(.trailing)
            .padding(.vertical, 9)
            
        }
        .padding(.horizontal,5)
    }
    
    /// Depending on the folder type of the email, a check is made as to whether the contact is the sender or recipient of the email.
    /// - Parameter email: the email that is being checked
    func filterEmailList(email: MailRecord) -> Bool {
        
        // TODO: Checking the foldertype doesn't list (all) the emails.
        // It seems that the set flags in foldertype are buged right now.
        // For now it checks the folderName.
        if folderName == "Inbox"{
            
            if (emailAddresses.contains(email.sender.email)) {
                return true
            }
        }
        else{
            
            var counter = 0
            
            while counter < email.addresses.count {
                if (emailAddresses.contains(email.addresses[counter].email)) {
                    return true
                }
                counter += 1}
            
            return false
        }
        
        return false
    }
    
}

/// A view to list all emails received from or sent to the contact, depending on the clicked more button in ContactMailListView.
struct MoreContactMailView: View {
    var folderpath: String
    var folderName: String
    var emailAddresses: [String]
    
    public var fetchRequest: FetchRequest<MailRecord>
    public var emails: FetchedResults<MailRecord>{fetchRequest.wrappedValue}
    
    var receivedHeader: LocalizedStringKey = "Contact.Received.Header"
    var sentHeader: LocalizedStringKey = "Contact.Sent.Header"
    
    
    init(folderpath: String, folderName: String, emailAddresses: [String]) {
        fetchRequest = MailRecord.mailsInFolderFetchRequest(folderpath: folderpath)
        self.folderpath = folderpath
        self.folderName = folderName
        self.emailAddresses = emailAddresses
    }
    
    
    var body: some View {
        
        // Creates an instance of ContactMailListView to use the filteredEmailList function
        let functionCall = ContactMailListView(folderpath: folderpath, folderName: folderName, emailAddresses: emailAddresses)
        let filteredEmails = self.emails.filter(functionCall.filterEmailList)
        
        // The header is set depending on the folder name of the emails retrieved
        let header = (folderName == "Sent") ? sentHeader : receivedHeader
        HStack{
            Text(header)
                .font(.title)
                .fontWeight(.semibold)
            Spacer()
        }.padding(.leading, 21)
        .padding(.top, 5)
        
            List (filteredEmails, id: \.self){ record in
                NavigationLink(
                    destination: ReadMainView(model: ReadModel(mail: record))) {
                    MailRowView(mail: record, activateOnTap: false)
                }
            }
    }
}



struct ContactMailListView_Previews: PreviewProvider {
    static var previews: some View {
        ContactMailListView(folderpath: "INBOX", folderName: "INBOX", emailAddresses: [])
            .environment(\.managedObjectContext, PersistentDataProvider.proxyPersistentDataProvider.persistentContainer.viewContext)
    }
}
