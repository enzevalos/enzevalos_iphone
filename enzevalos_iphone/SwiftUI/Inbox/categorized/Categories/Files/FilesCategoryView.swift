//
//  FilesCategoryView.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 22.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

struct FilesCategoryView : View{
    
    init(model : CategoryViewModel) {
        baseModel  = model
        filesModel = FilesCategoryVM(model)
    }
    
    var baseModel : CategoryViewModel
    @ObservedObject var filesModel : FilesCategoryVM
    
    var body: some View{
        VStack{
            // TODO : there is nothing displayed for some reason currently
            TypePicker(selectedTypes: $filesModel.filteredFileTypes).padding(.horizontal, -20).padding(.bottom , 5)
            FilesPreviewer(height: baseModel.size, filesCategoryVM: filesModel).padding(.horizontal, -20)
        }
    }
}




struct FilesCategory_Previews: PreviewProvider {
    static var previews: some View {
        FilesCategory
    }
}

