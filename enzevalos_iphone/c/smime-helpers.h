//
//  openssl-helpers.h
//  
//
//  Created by lazarog98 on 02.03.20.
//

#ifndef openssl_helpers_h
#define openssl_helpers_h
#undef OPENSSL_NO_SSL_INTERN
#undef OPENSSL_NO_CRYPTO_INTERN

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/opensslv.h>
#include <openssl/evp.h> // this header may not be needed
#include <openssl/err.h> // this header may not be needed
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/pkcs7.h>
#include <openssl/cms.h>
#include <openssl/safestack.h>
#include <openssl/x509.h>
#include <openssl/opensslconf.h>
#include "generic-helpers.h"
#include "certificate-helpers.h"

STACK_OF(X509)* create_stack_x509(X509 *arr, int len);
struct pem_with_fp {
    char *pem;
    char *fp;
} typedef pem_with_fp;
pem_with_fp * cert_stack_to_array(STACK_OF(X509) *stack);

void OpenSSL_print_ver(void);

result * OpenSSL_encrypt(const char *text, char **pems, int num_certs);
result * OpenSSL_decrypt(const char *str, const char *pem_cert, const char *pem_key);
result * OpenSSL_sign(const char *text, const char *pem_cert, const char *pem_key, const int detached);
result * OpenSSL_verify(const char *text, char **pem_cert, const int num_certs);
char * get_fingerprint_from_X509(X509 *cert, int md_alg);
result * get_fingerprint_from_pem(const char *pem, int md_alg); // 0 for SHA-256, 1 for SHA-1
result * get_pkey_from_enc_pem(const char *pem, const char *passwd);
result * get_enc_pem_from_pkey(const char *pem, const char *passwd);

#endif /* openssl_helpers_h */
