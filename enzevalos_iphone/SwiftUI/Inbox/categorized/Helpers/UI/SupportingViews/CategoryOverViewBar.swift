//
//  CategoryWrap.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 10.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI
import TagNavVi


/**
The Overview of the Available categories

- Parameters:
    - controller: The InboxModelcontroller used to set and retrieve the active categoriy aswell as  retrieving all categories
*/
struct CategoryOverviewBar: View {
    
    @ObservedObject var controller: HomeModel
    
    var iconSize : CGFloat?
    var usesWrap : Bool = false
    var switchesToHStackIfACategoryIsActive:Bool = true
    
    let innerPadding:CGFloat = 10
    
    private var allCats : [CategoryViewModel] {controller.allCatsSorted}
    var body: some View {
        if usesWrap && (controller.isHome || !switchesToHStackIfACategoryIsActive){
            DynamicSmartWrap(items: allCats , viewGenerator: ui).padding(.horizontal)
        }else{
            ScrollView(.horizontal, showsIndicators : false){
                HStack{
                    Spacer().frame(width: 20)
                    ForEach(allCats){catModel in
                        ui(catModel).padding(.vertical,innerPadding)
                    }
                    Spacer().frame(width: 15)
                }
            }.padding(.vertical, -15)
        }
    }
    
    ///to generate a single category entry
    private func ui(_ categoryVM : CategoryViewModel)-> some View {
       categoryButton(categoryVM)
    }
    private func categoryButton(_ categoryVM : CategoryViewModel) -> some View {
        var isActive : Bool{
            return categoryVM.isActive
        }
        func switchActive(){
           // categoryVM.isActive.toggle()
            controller.setActiveCategory(categoryVM)
        }
        return Button(
            action: switchActive
        ){
            categoryVM.constData.icon
                .sizeTo(iconSize)
                .padding(innerPadding)
                .background(isActive ? Circle().fill(Color.gray).opacity(0.2).padding(-5) : nil)
        }
    }
}
