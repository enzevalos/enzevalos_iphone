//
//  Blur.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 08.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI


///this currently does not work 
struct Blur: UIViewRepresentable {
    let style: UIBlurEffect.Style = .systemThickMaterial

    func makeUIView(context: Context) -> UIVisualEffectView {
        return UIVisualEffectView(effect: UIBlurEffect(style: style))
    }

    func updateUIView(_ uiView: UIVisualEffectView, context: Context) {
        uiView.effect = UIBlurEffect(style: style)
    }
}



struct Blur_Previews: PreviewProvider {
    static var previews: some View {
        ZStack{
            Image("friendly").resizable().frame(width: 100, height: 100, alignment: .center)
            Text("Blur Example").background(Blur().opacity(0.5))
        }
    }
}
