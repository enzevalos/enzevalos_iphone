//
//  SampleCategoryV2.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 18.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

///just a sample category used for debugging play around as you wish
struct SampleCategoryV2: CategoryV {
    
    let name: String = "Sample Category"
    
    var filter: MailFilter = {_ in .canOpen}
    
    
    func body(model:CategoryViewModel) -> AnyView {AnyView(
        VStack{
            HStack{Spacer()}
            if let size = model.size {
                let optimalAmount = min(Int(size)/40 , model.allMails.count)
                ForEach(0 ..< optimalAmount,id:\.self){i in Text(model.allMails[i].subject).font(.footnote)}
            }else{
                Text("\(model.allMails.count)")
                Text("nothing to see here.. this is just a sample, you know?")
            }
        }.frame(height:model.size)
        .withUnreadCounter(model.mailAmount, onPress: {model.isActive=true})
    )}
}

