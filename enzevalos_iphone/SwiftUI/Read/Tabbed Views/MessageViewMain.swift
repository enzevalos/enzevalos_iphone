//
//  MessageViewMain.swift
//  enzevalos_iphone
//
//  Created by melicoa97 on 10.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct MessageViewMain <M: DisplayMail>: View {
    @EnvironmentObject var model: ReadModel<M>
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let innerPadding: CGFloat = 20 // button radius
    let outerPadding: CGFloat = 5
    let extraButtonFactor: CGFloat = 0.8
    
    @State var showExtraButtons: Bool = false
    @State var newMail: PreMailData?
    
    var body: some View {
        ZStack{
            MessageBody.padding(.horizontal)
                .onTapGesture {
                    self.showExtraButtons = false
                }
            FloatingReplyButtons
        }
        .sheet(item: $newMail, content: {mail in ComposeView(preData: mail)})
        .onChange(of: model.dismissView, perform: { value in
            if value {
                presentationMode.wrappedValue.dismiss()
            }
        })
    }
    
    var Subjectbar: some View{
        VStack(alignment: .leading){
            Text(self.model.mail.subject).font(.largeTitle).fontWeight(.light)
        }
        .padding()
    }
    
    func actionButton(image:Image , description:String, action:@escaping ()->Void)->some View{
        return Button(action: action){
            VStack{
                image
                Text(description).font(.callout).fontWeight(.none)
            }
        }
    }
    
    
    var MessageBody : some View {
        GeometryReader {geometry in
            SelectableTextView((self.model.mail.body))
                .frame(
                    width: geometry.size.width,
                    height: geometry.size.height
                )
                .padding(.bottom, 2 * self.innerPadding + self.outerPadding)
        }
    }
    
    var FloatingReplyButtons: some View{
        return VStack{
            Spacer()
            HStack{
                Spacer()
                ZStack{
                    if  model.mail.folderType == FolderType.Archive {
                        forwardButton.offset(y: showExtraButtons ? -innerPadding*9 : 0)
                        replyAllButton.offset(y: showExtraButtons ? -innerPadding*4.5 : 0)
                        replyButton
                        unreadButton.offset(x: showExtraButtons ? -innerPadding*4.5 : 0)
                        deleteButton.offset(x: showExtraButtons ? -innerPadding*9 : 0)
                    }
                    else if model.mail.folderType == FolderType.Trash{
                        forwardButton.offset(y: showExtraButtons ? -innerPadding*9 : 0)
                        replyAllButton.offset(y: showExtraButtons ? -innerPadding*4.5 : 0)
                        replyButton
                        unreadButton.offset(x: showExtraButtons ? -innerPadding*4.5 : 0)
                        archiveButton.offset(x: showExtraButtons ? -innerPadding*9 : 0)
                    }
                    else if model.mail.folderType == FolderType.Sent {
                        forwardButton.offset(y: showExtraButtons ? -innerPadding*4.5  : 0)
                        deleteButton.offset(x: showExtraButtons ? -innerPadding*4.5 : 0)
                    }
                    else if model.mail.folderType == FolderType.Draft {
                        deleteButton.offset(x: showExtraButtons ? -innerPadding*4.5 :0)
                    }
                    else{
                        forwardButton.offset(y: showExtraButtons ? -innerPadding*9 : 0)
                        replyAllButton.offset(y: showExtraButtons ? -innerPadding*4.5 : 0)
                        replyButton
                        unreadButton.offset(x: showExtraButtons ? -innerPadding*4.5 : 0)
                        deleteButton.offset(x: showExtraButtons ? -innerPadding*9 : 0)
                        archiveButton.offset(x: showExtraButtons ? -innerPadding*13.5 : 0)
                    }
                    //options
                    FloatingActionButton(
                        //radius: innerPadding,
                        onShortPress: {self.showExtraButtons.toggle()}
                        /*TODO: fill in stuff*/
                    ){
                        VStack{
                            Image(systemName: "arrowshape.turn.up.left.fill")
                            Text("options").font(.system(size: 7)).fontWeight(.none)
                        }
                    }.opacity(showExtraButtons ? 0 : 1)
                }.animation(.easeInOut(duration: 0.2))
            }.padding(outerPadding)
        }.padding(outerPadding)
    }
    
    var forwardButton: some View {
        FloatingActionButton(
            radius: innerPadding*extraButtonFactor,
            onShortPress: {
                newMail = model.newUserAction(action: .Forward)
            }
        ){
            VStack{
                Image(systemName: "arrowshape.turn.up.right.fill")
                Text("forward").font(.system(size: 7)).fontWeight(.none)
            }
        }
        
    }
    
    var replyAllButton: some View{
        FloatingActionButton(
            radius: innerPadding*extraButtonFactor,
            onShortPress:  {
                newMail = model.newUserAction(action: .ReplyAll)
            }
        ){
            VStack{
                Image(systemName: "arrowshape.turn.up.left.2.fill")
                Text("reply all").font(.system(size: 7)).fontWeight(.none)
            }
        }
    }
    
    var unreadButton: some View{
        FloatingActionButton(
            radius: innerPadding*extraButtonFactor,
            onShortPress: {
                newMail = model.newUserAction(action: .Unread)
            }
        ){
            VStack{
                Image(systemName: "envelope.badge")
                Text("unread").font(.system(size: 7)).fontWeight(.none)
            }
        }
    }
    
    var deleteButton: some View{
        FloatingActionButton(
            radius: innerPadding*extraButtonFactor,
            onShortPress: {
                newMail = model.newUserAction(action: .Delete)
            }){
            VStack{
                Image(systemName: "trash")
                Text("delete").font(.system(size: 7)).fontWeight(.none)
            }
        }
    }
    
    var archiveButton: some View {
        FloatingActionButton(
            radius: innerPadding*extraButtonFactor,
            onShortPress: {
                newMail = model.newUserAction(action: .Archive)
            }){
            VStack{
                Image(systemName: "folder")
                Text("archive").font(.system(size: 7)).fontWeight(.none)
            }
        }
    }
    
    var replyButton: some View{
        FloatingActionButton(
            onShortPress: {
                newMail = model.newUserAction(action: .Reply)
            }
        ){
            VStack{
                Image(systemName: "arrowshape.turn.up.left.fill")
                Text("reply").font(.system(size: 7)).fontWeight(.none)
            }
        }
    }    
}


struct MessageViewMain_Previews: PreviewProvider {
    static var model = ReadModel(mail: ProxyData.SecureMail)
    static var previews: some View {
        model.currentTab = ReadPart.Body.value
        let sim = Simulators<ReadMainView<ProxyMail>>()
        return sim.previews(view: ReadMainView(model: model))
    }
}
