//
//  Attachment.swift
//  enzevalos_iphone
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import Foundation

struct Attachment: DisplayAttachment, Identifiable, Equatable {
    // DisplayAttachment
    var myName: String
    var myData: Data
    
    // Identifiable
    var id = UUID()

    /// a func that removes the calling attachment`s copy from the Documents Directory
    func removeFileFromDocumentsDirectory() {
        /// a func that returns the URL of the Documents Directory
        func getDocumentsDirectory() -> URL {
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            return paths[0]
        }
        let wholePath = getDocumentsDirectory().appendingPathComponent(self.myName)
        
        do {
            try FileManager.default.removeItem(at: wholePath)
        } catch let error as NSError {
            print("Error: \(error)")
        }
    }
    
    /// a func that returns the size of the calling attachment file as a string
    func countData() -> String {
        // size in byte
        var sizeOfData = Double(self.myData.count)
        
        // smaller than 1KB
        if  sizeOfData < 1000 {
            return String(sizeOfData) + " bytes"
        }
        //smaller than 1MB
        else if sizeOfData < 1000000 {
            sizeOfData = round(sizeOfData/1000)
            return String(Int(sizeOfData)) + " KB"
        }
        // everything bigger than 1MB
        else {
            sizeOfData = round(sizeOfData/1000000)
            return String(Int(sizeOfData)) + " MB"
        }
        
    }
    
}

