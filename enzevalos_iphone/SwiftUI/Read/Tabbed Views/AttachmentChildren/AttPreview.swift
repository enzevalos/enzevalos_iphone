//
//  AttPreview.swift
//  enzevalos_iphone
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI
import QuickLook

//To use the QuickLook in SwiftUI
struct QuickLookView: UIViewControllerRepresentable {
    var name: String
    var data: Data?
    
    //if it should download the data as a file if not yet downloaded
    var shallDL: Bool = true
    
    var allowScaling: Bool = true
      
    func makeCoordinator() -> QuickLookView.Coordinator {
        Coordinator(self)
    }
      
    func makeUIViewController(context: Context) -> QLPreviewController {
        let controller = QLPreviewController()
        //the coordinator provides the real preview data as a NSURL
        controller.dataSource = context.coordinator
        return controller
    }
      
    func updateUIViewController(_ controller: QLPreviewController,
                                context: Context) {
        print("updated")
        controller.reloadData()//not beautiful but works
    }
    
    class Coordinator: NSObject, QLPreviewControllerDataSource {
        let parent: QuickLookView
          
        init(_ parent: QuickLookView) {
            self.parent = parent
            super.init()
        }
          
        // The QLPreviewController asks its delegate how many items it has:
        func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
            return 1
        }
          
        // For each item (see method above), the QLPreviewController asks for
        // a QLPreviewItem instance describing that item:
        func previewController(
            _ controller: QLPreviewController,
            previewItemAt index: Int
        ) -> QLPreviewItem {
            
            func getDocumentsDirectory() -> URL {
                let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                return paths[0]
            }
            
            let str = parent.data!
            
            //This is where the file is stored
            let filename = getDocumentsDirectory().appendingPathComponent(parent.name)

            if parent.shallDL{
                do {
                    try str.write(to: filename)
                } catch {
                    // failed to write file – bad permissions, bad filename, missing permissions
                }
            }

            _ = parent.name.components(separatedBy: ".")
            let path = filename
            
            return path as NSURL
            
        }
    }
}

