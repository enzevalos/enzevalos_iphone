//
//  VCSwiftUIView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 03.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

enum ViewID: String{
    case KeyRecordView = "UITableViewController-8Di-x2-cWQ"
    case ReadView = "UITableViewController-Ouw-WD-EV6"
    case FoldersView = "folderViewController"
    case ComposeView = "SendViewController"
    case MailListView = "UITableViewController-ooe-0X-glz"
}

/**
 Generate a SwiftUIView for a UIViewController in the main story board
 */
struct VCSwiftUIView: UIViewControllerRepresentable {    
    var storyboard: String = "Main"
    var vcID: String

     func makeUIViewController(context: UIViewControllerRepresentableContext<VCSwiftUIView>) -> UIViewController {
        //Load the storyboard
        let loadedStoryboard = UIStoryboard(name: storyboard, bundle: nil)
        //Load the ViewController
        let myVc = loadedStoryboard.instantiateViewController(withIdentifier: vcID)
        return myVc
       
     }
    
    func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<VCSwiftUIView>) {
    }
}



   
