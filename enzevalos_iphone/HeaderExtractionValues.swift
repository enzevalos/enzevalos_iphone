//
//  HeaderExtractionValues.swift
//  enzevalos_iphone
//
//  Created by Katharina Müller on 26.02.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation
enum HeaderExtractionValues: String {
    case MIMEVersion = "MIME-Version"
    case Received = "Received"
    case ContentType = "Content-Type"
    case ContentTransferEncoding = "Content-Transfer-Encoding"
    case ContentDisposition = "Content-Disposition" //i.e. used by smime
    case DKIMSignature = "DKIM-Siagnature"//DomainKeys for e-mail authentication
    case InReplyTo = "In-Reply-To"
    case XMailer = "X-Mailer" //Information about the mail server
}
