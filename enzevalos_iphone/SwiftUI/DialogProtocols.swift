//
//  DialogProtocols.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 12.04.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

protocol Dialog {
    var dialogColor: Color { get }
    var title: String { get }
    var body: String { get }
    var img: Image { get }
    var ctaButtonTitle: String? { get }
    var ctaButtonAction: ButtonAction? { get }
    var infoButtonTitle: String? { get }
    
    var moreButtons: [ButtonStruct]  { get }
    
    var dismissButtonTitle: String? { get }
    var dismissButtonAction: ButtonAction? { get }
    
}


struct ButtonStruct {
    let titleKey: String
    let action: ButtonAction
    let type: ButtonType
    
    var title: String {
        get {
            return NSLocalizedString(titleKey, comment: "")
        }
    }
}

enum ButtonType {
    case CTA, DISMISS, MORE
}

enum ButtonAction {
    case AskSenderToConfirm, AskSenderToSendPK, AskSenderToResendForPK, InvitePerson, IgnoreMail, AskUserToImportSK, ImportSK, ImportPK, MoreInformation, ExportSK, OK, IgnoreWarning, SendPK
    
    var title: LocalizedStringKey {
        get {
            switch self {
            case .AskSenderToConfirm:           return "Security.Dialog.Button.Title.Confirmation"
            case .AskSenderToSendPK:            return "Security.Dialog.Button.Title.Ask.ForPK"
            case .AskSenderToResendForPK:       return "Security.Dialog.Button.Title.Ask.Resend.ForPK"
            case .InvitePerson:                 return "Security.Dialog.Button.Title.Invite"
            case .IgnoreMail:                   return "Security.Dialog.Button.Title.Ignore.Mail"
            case .AskUserToImportSK:            return "MISSING"
            case .ImportSK:                     return "Security.Dialog.Button.Title.Import.SK"
            case .ImportPK:                     return "MISSING"
            case .MoreInformation:              return "MoreInformation"
            case .ExportSK:                     return "MISSING"
            case .OK:                           return "Security.Dialog.Button.Title.OK"
            case .IgnoreWarning:                return "MISSING"
            case .SendPK:                       return "Security.Dialog.Button.Title.Send.PK"
            }
        }
    }
    
    var color: Color {
        get {
            let defaultColor = Color.blue
            switch self {
            case .AskSenderToConfirm:           return defaultColor
            case .AskSenderToSendPK:            return defaultColor
            case .AskSenderToResendForPK:       return defaultColor
            case .InvitePerson:                 return defaultColor
            case .IgnoreMail:                   return defaultColor
            case .AskUserToImportSK:            return defaultColor
            case .ImportSK:                     return defaultColor
            case .ImportPK:                     return defaultColor
            case .MoreInformation:              return defaultColor
            case .ExportSK:                     return defaultColor
            case .OK:                           return defaultColor
            case .IgnoreWarning:                return defaultColor
            case .SendPK:                       return defaultColor
            }
        }
    }
}

