//
//  NoSecIconStyleKit.swift
//  ProjectName
//
//  Created by Oliver Wiese on 13.05.19.
//  Copyright © 2019 FU Berlin. All rights reserved.
//
//  Generated by PaintCode
//  http://www.paintcodeapp.com
//



import UIKit

public class NoSecIconStyleKit : NSObject {

    //// Cache

    private struct Cache {
        static var imageOfFavorite: UIImage?
        static var favoriteTargets: [AnyObject]?
    }

    //// Drawing Methods

    @objc dynamic public class func drawFavorite(frame targetFrame: CGRect = CGRect(x: 0, y: 0, width: 19, height: 19), resizing: ResizingBehavior = .aspectFit) {
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()!
        
        //// Resize to Target Frame
        context.saveGState()
        let resizedFrame: CGRect = resizing.apply(rect: CGRect(x: 0, y: 0, width: 19, height: 19), target: targetFrame)
        context.translateBy(x: resizedFrame.minX, y: resizedFrame.minY)
        context.scaleBy(x: resizedFrame.width / 19, y: resizedFrame.height / 19)


        //// Star Drawing
        let starPath = UIBezierPath()
        starPath.move(to: CGPoint(x: 10, y: 0.5))
        starPath.addLine(to: CGPoint(x: 11.74, y: 7.42))
        starPath.addLine(to: CGPoint(x: 19.04, y: 6.89))
        starPath.addLine(to: CGPoint(x: 12.82, y: 10.64))
        starPath.addLine(to: CGPoint(x: 15.58, y: 17.23))
        starPath.addLine(to: CGPoint(x: 10, y: 12.64))
        starPath.addLine(to: CGPoint(x: 4.42, y: 17.23))
        starPath.addLine(to: CGPoint(x: 7.18, y: 10.64))
        starPath.addLine(to: CGPoint(x: 0.96, y: 6.89))
        starPath.addLine(to: CGPoint(x: 8.26, y: 7.42))
        starPath.close()
        UIColor.black.setFill()
        starPath.fill()
        
        context.restoreGState()

    }

    //// Generated Images

    @objc dynamic public class var imageOfFavorite: UIImage {
        if Cache.imageOfFavorite != nil {
            return Cache.imageOfFavorite!
        }

        UIGraphicsBeginImageContextWithOptions(CGSize(width: 19, height: 19), false, 0)
            NoSecIconStyleKit.drawFavorite()

        Cache.imageOfFavorite = UIGraphicsGetImageFromCurrentImageContext()!.resizableImage(withCapInsets: UIEdgeInsets(top: 9, left: 6, bottom: 9, right: 2), resizingMode: .stretch)
        UIGraphicsEndImageContext()

        return Cache.imageOfFavorite!
    }

    //// Customization Infrastructure

    @objc @IBOutlet dynamic var favoriteTargets: [AnyObject]! {
        get { return Cache.favoriteTargets }
        set {
            Cache.favoriteTargets = newValue
            for target: AnyObject in newValue {
                let _ = target.perform(NSSelectorFromString("setImage:"), with: NoSecIconStyleKit.imageOfFavorite)
            }
        }
    }




    @objc(NoSecIconStyleKitResizingBehavior)
    public enum ResizingBehavior: Int {
        case aspectFit /// The content is proportionally resized to fit into the target rectangle.
        case aspectFill /// The content is proportionally resized to completely fill the target rectangle.
        case stretch /// The content is stretched to match the entire target rectangle.
        case center /// The content is centered in the target rectangle, but it is NOT resized.

        public func apply(rect: CGRect, target: CGRect) -> CGRect {
            if rect == target || target == CGRect.zero {
                return rect
            }

            var scales = CGSize.zero
            scales.width = abs(target.width / rect.width)
            scales.height = abs(target.height / rect.height)

            switch self {
                case .aspectFit:
                    scales.width = min(scales.width, scales.height)
                    scales.height = scales.width
                case .aspectFill:
                    scales.width = max(scales.width, scales.height)
                    scales.height = scales.width
                case .stretch:
                    break
                case .center:
                    scales.width = 1
                    scales.height = 1
            }

            var result = rect.standardized
            result.size.width *= scales.width
            result.size.height *= scales.height
            result.origin.x = target.minX + (target.width - result.width) / 2
            result.origin.y = target.minY + (target.height - result.height) / 2
            return result
        }
    }
}
