//
//  StringExtension.swift
//  enzevalos_iphone
//
//  Created by jakobsbode on 03.11.17.
//  Copyright © 2018 fu-berlin.
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import TLDExtract

let extractor = try! TLDExtract(useFrozenData: true)

extension String {
    static func random(length: Int = 20) -> String {
        var randomBytes = Data(count: length)
        
        let result = randomBytes.withUnsafeMutableBytes {
            SecRandomCopyBytes(kSecRandomDefault, length, $0)
        }
        if result == errSecSuccess {
            return randomBytes.base64EncodedString()
        } else {
            return ""
        }
    }
}

extension String {
    func trimmed() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func removeNewLines() -> String {
        let components = self.components(separatedBy: .whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
    
    func toBase64() -> Data? {
        let components = self.components(separatedBy: .whitespacesAndNewlines)
        let res = components.filter { !$0.isEmpty }.joined(separator: "")
        return Data(base64Encoded: res)
    }
    
    func remove(seperatedBy: CharacterSet) -> String {
        let components = self.components(separatedBy: seperatedBy)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
    
    func makeBlocks(size: Int = 4) -> String {
        return String(enumerated().map { $0 > 0 && $0 % size == 0 ? [" ", $1] : [$1]}.joined())
    }
}

extension String {
    /**
     Returns true if a String contains upper case letters.
    */
    func containsUpperCaseLetter() -> Bool {
        let url = self
        let regEx = "[A-Z]"
        let range = NSRange(location: 0, length: url.utf16.count)
        let regex = try! NSRegularExpression(pattern: regEx)
        if regex.firstMatch(in: url, options:[], range: range) != nil {
            return true
        }
        return false
    }
    
    /**
     Returns true if the string contains two or more consecutive dots
     */
    func twoDotsOrMore() -> Bool {
        let url = self
        // Finds 2 or more consecutive dots
        let regEx = "\\.{2,}"
        let range = NSRange(location: 0, length: url.utf16.count)
        let regex = try! NSRegularExpression(pattern: regEx)
        if regex.firstMatch(in: url, options:[], range: range) != nil {
            return true
        }
        return false
    }
}

/**
functions to split an  emailadress  into their domains
*/
extension String {
    /**
    Returns the E-Mail address splitted by "." and "@":
    Example: test@imp.fu-berlin.de -> ["test", "imp", "fu-berlin", "de"]
    */
    func splitAddress() -> [String] {
        if self == ""{
            return []
        }
        let mailAddressArr = self.split(separator: "@").map(String.init)
        let mailLocalIdentity = mailAddressArr[0]
        let subDomainArr = mailAddressArr[1].split(separator: ".").map(String.init)
        let splittetMailArr = [mailLocalIdentity] + subDomainArr
        return splittetMailArr
    }
    
    /**
    Returns the extracted local mail identity from an E-Mail address in
    Example: test@imp.fu-berlin.de -> test
    */
    func getLocalMailIdentity() -> String {
        guard let localIdentity = self.splitAddress().first
            else { return ""}
        return localIdentity
    }
    
    /**
    Returns the extracted sub domain from an E-Mail address in
    Example: test@imp.fu-berlin.de -> ["imp", "fu-berlin"]
    */
    func getSubdomains() -> [String] {
        if self == ""{
            return []
        }
        var subDomains = self.splitAddress()
        subDomains.removeFirst()
        subDomains.removeLast()
        return subDomains
    }

    /**
    Returns the extracted top level domain from an E-Mail address
    Example: test@imp.fu-berlin.de -> de
    */
     func getDomain() -> String {
        guard let domain = self.splitAddress().last
            else {return ""}
        return domain
     }
    
    /**
     Returns the extracted sub - and second evel domain from an E-Mail address in
     Example: test@imp.fu-berlin.de -> imp.fu-berlin OR test@example.de -> example
     */
    func getMailDomains() -> String {
        var domain = ""
        if var domains = self.components(separatedBy: "@").last?.components(separatedBy: ".") {
            domains.removeLast()
            for dom in domains {
                domain += dom + "."
            }
            domain.removeLast()
            return domain
        }
        return ""
    }
}

/**
Functions to find E-mail adresses and URLs
*/
extension String {
    
    func isValidEmail() -> Bool {
        let emailRegEx = "(?!\\u10000-\\uEFFFF.*\\.\\.)([^@\\s]{2,64}+@[^@\\s]+\\.[^@\\s\\.]+$)"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func findEmails() -> [String]{
        let pattern = "[\\u10000-\\uEFFFF.!#$%&'*+/=?^_`{|}~-]+@[\\u10000-\\uEFFFF.!#$%&'*+/=?^_`{|}~-]+\\.[\\u10000-\\uEFFFF.!#$%&'*+/=?^_`{|}~-]{2,64}" //\\U00010000-\\U0010FFFF
        let range = NSRange(location: 0, length: self.utf16.count)
        let regex = try! NSRegularExpression(pattern: pattern)
        let check = regex.matches(in: self, options: [], range: range)
        var foundLinks:[String] = []
        for match in check{
            guard let range = Range(match.range, in: self) else {continue}
            let result = String(self[range])
            if result.isValidEmail(){
                foundLinks.append(result)
            }
        }
        return foundLinks
    }
    
    func url2ip() -> String? {
        let host = CFHostCreateWithName(nil,self as CFString).takeRetainedValue()
        CFHostStartInfoResolution(host, .addresses, nil)
        var success: DarwinBoolean = false
        if let addresses = CFHostGetAddressing(host, &success)?.takeUnretainedValue() as NSArray?,
            let theAddress = addresses.firstObject as? NSData {
            var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
            if getnameinfo(theAddress.bytes.assumingMemoryBound(to: sockaddr.self), socklen_t(theAddress.length),
                           &hostname, socklen_t(hostname.count), nil, 0, NI_NUMERICHOST) == 0 {
                let numAddress = String(cString: hostname)
                return(numAddress)
            }
        }
        return nil
    }
   
    /**
     Returns a list of E-Mail Addresses extracted from a given string
     */
    func findMailAddress() -> [String] {
        let splitString = self.split(separator: " ").map(String.init)
        var results:[String] = []
            for val in splitString {
                if val.contains("@") && val.isValidEmail(){
                    results.append(val)
                }
            }
       return(results)
    }
    
    /**
     For text only, not for hyperlink detection
     Returns URLs from a given String
     */
    func findURL() -> [String?] {
        var urls : [String?] = []
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            detector.enumerateMatches(in: self, options: [], range: NSMakeRange(0, self.count), using: { (result, _, _) in
                if let match = result{
                    if let match2 = match.url?.absoluteString, !match2.contains("@") {
                        if !match2.twoDotsOrMore() {
                            urls.append(match2)
                        }
                    }
                }
            })
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return urls
    }
    
    /**
     To find HTML tags and hyperlinks
     */
    func findHtmlTags() -> [String]{
        let htmlTagPattern = "<(.*?)a.*?href=.*?>(.*?)<\\/.*?a.*?>"
        let range = NSRange(location: 0, length: self.utf16.count)
        let regex = try! NSRegularExpression(pattern: htmlTagPattern)
        let check = regex.matches(in: self, options: [], range: range)
        var foundLinks:[String] = []
        for match in check{
            guard let range = Range(match.range, in: self) else {continue}
            foundLinks.append(String(self[range]))
        }
        return (foundLinks)
    }
    
    func htmlTagURL() -> [String]{
        let htmlTagUrlPattern = "(?<=href=')(.*?)(?='>)|(?<=href=\")(.*?)(?=\">)"
        let range = NSRange(location: 0, length: self.utf16.count)
        let regex = try! NSRegularExpression(pattern: htmlTagUrlPattern)
        let check = regex.matches(in: self, options: [], range: range)
        var foundLinks:[String] = []
        for match in check{
            guard let range = Range(match.range, in: self) else {continue}
            foundLinks.append(String(self[range]))
        }
        return (foundLinks)
        
    }
    
    func htmlTagLinkName() -> [String]{
        let htmlTagLinkNamePattern = "(?<='>)(.*?)(?=<\\/a>)|(?<=\">)(.*?)(?=<)"
        let range = NSRange(location: 0, length: self.utf16.count)
        let regex = try! NSRegularExpression(pattern: htmlTagLinkNamePattern)
        let check = regex.matches(in: self, options: [], range: range)
        var foundLinks:[String] = []
        for match in check{
            guard let range = Range(match.range, in: self) else {continue}
            foundLinks.append(String(self[range]))
        }
        return (foundLinks)
    }
    
}

/**
Functions for splitting a URL  into their domains
*/
extension String {
    
    /**
     Returns a list of all second level domains taken out of all URLs from a String.
     */
    func getSLD()-> [String] {
        let urls = self.findURL()
        var SLD: [String] = []
        for url in urls {
            let urlString: String = url!
            guard let result: TLDResult = extractor.parse(urlString) else { continue }
            if let sld = result.secondLevelDomain {
                SLD.append(sld)
            }
        }
        return SLD
    }
       
    /**
     Returns a list of all top level domains taken out of all URLs from a String.
     */
    func getTLD()-> [String] {
        let urls = self.findURL()
        var TLD: [String] = []
        for url in urls {
            let urlString: String = url!
            guard let result: TLDResult = extractor.parse(urlString) else { continue }
            if let tld = result.topLevelDomain {
                TLD.append(tld)
            }
        }
        return TLD
    }
    
    /**
     Returns a list of all root domains taken out of all URLs from a String.
     */
    func getRD() -> [String] {
        let urls = self.findURL()
        var RD: [String] = []
        for url in urls {
            let urlString: String = url!
            guard let result: TLDResult = extractor.parse(urlString) else { continue }
            if let rd = result.rootDomain {
                RD.append(rd)
            }
        }
        return RD
    }
    
    /**
    Returns the root domain of a URL.
    */
    func getRootDomain() -> String {
        if let result: TLDResult = extractor.parse(self) {
            if let rootDomain = result.rootDomain {
                return rootDomain
            }
        }
        return ""
    }
    
    /**
     Returns the second-level domain of a URL.
     */
    func getSecondLevelDomain() -> String {
        if let result: TLDResult = extractor.parse(self) {
            if let secondLevelDomain = result.secondLevelDomain {
                return secondLevelDomain
            }
        }
        return ""
    }
}

/**
Levenshtein Algorithm to calculate the editdistance
Returns the editdistance betwenn two Strings
Weights for transpositiona and editdistance are 2, weights for the rest is 1
*/
extension String {
    
    /**
    Returns the editdistance between two strings
    */
    func getEditDistance(str: String) -> Int{
       return self.levenshtein(str)
    }
    
    /**
    Returns an dictionary of every editdistance between every string of a given list and a given string
    Key: string, Value: editdistance
    */
    func getEditDistances(list: [String]) -> [String : String]{
        var editDistance = 0
        var editDic : [String:String] = [:]
        for elm in list {
            editDistance = self.getEditDistance(str: elm)
            editDic.updateValue(String(editDistance), forKey: elm)
        }
      return editDic
    }
    
    /**
     Compares two strings with each other. Return true if the edit distance is in the allowed range.
     Not allowed distance: 0 < distance < allowedEditDistance.
     */
    func isAllowedDistance(str: String, allowedEditDistance: Int) -> Bool {
        let distance = self.levenshtein(str)
        if distance < allowedEditDistance && distance != 0 {
            return false
        }
        return true
    }
    
    /**
     Compares a String with a list of strings and checks whether the edit distance between them is in the allowed range.
     Returns a boolean value and a list of strings that had an un-allowed edit distance with the original string
     The Bool values is returned False: if the string has an un-allowed edit distance with any other element from the list.
     */
    func compareWith(strings: [String], allowedEditDistance: Int) -> (Bool, [String]) {
        var isAllowedEditDistance = true
        var strList: [String] = []
        for str in strings {
            if !self.isAllowedDistance(str: str, allowedEditDistance: allowedEditDistance) {
                isAllowedEditDistance = false
                strList.append(str)
            }
        }
        return (isAllowedEditDistance, strList)
    }

    /**
     Levenshtein Algorithm
     Resource: https://gist.github.com/RuiCarneiro/82bf91214e3e09222233b1fc04139c86
     edited the source code: added transposition, edited weights
     */
    public func levenshtein(_ other: String) -> Int {
        let sCount = self.count
        let oCount = other.count
        
        let a = Array(self)
        let b = Array(other)

        guard sCount != 0 else {
            return oCount
        }

        guard oCount != 0 else {
            return sCount
        }

        let line : [Int]  = Array(repeating: 0, count: oCount + 1)
        var mat : [[Int]] = Array(repeating: line, count: sCount + 1)

        for i in 0...sCount {
            mat[i][0] = i
        }

        for j in 0...oCount {
            mat[0][j] = j
        }

        for j in 1...oCount {
            for i in 1...sCount {
                
                if a[i - 1] == b[j - 1] {
                    mat[i][j] = mat[i - 1][j - 1]       // no operation
                }
                    
                else {
                    // weight for transposition and substitution are 2, the others have weight 1
                    let del = mat[i - 1][j] + 1         // deletion
                    let ins = mat[i][j - 1] + 1         // insertion
                    let sub = mat[i - 1][j - 1] + 2     // substitution
                    mat[i][j] = min(min(del, ins), sub)
                    
                    if (i > 1) && (j > 1) && (i < sCount) && (j < oCount) && (a[i - 1] == b[j]) && (a[i] == b[j - 1]) {
                        mat[i][j] = min(mat[i][j], mat[i-2][j-2]+1) // transposition
                    }
                }
            }
        }
        return mat[sCount][oCount]
    }
    
}
