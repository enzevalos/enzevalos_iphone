//
//  OnboardingIntro.swift
//  enzevalos_iphone
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

var buttonheight:CGFloat=60

import SwiftUI

//Main Onboarding
struct OnboardingIntro: View {
    var onClick: ()->Void
    @State var setupView: Bool = false
    
    @ViewBuilder
    var body: some View {
        if setupView {
            AccountSetupView(goBack: $setupView)
        } else {
            onboarding
        }
    }
    var onboarding: some View {
        get {
            VStack {
                Spacer()
            Text(NSLocalizedString("Onboarding.headline", comment: "Welcome"))
                    .font(.system(size: 55))
                    .fontWeight(.heavy)
                ZStack(alignment: .bottom){
                    VStack(alignment: .center){
                        if (UIApplication.shared.windows.first?.windowScene?.interfaceOrientation.isLandscape ?? false){ //this does not work as intended
                            List{
                                OnboardingIntroStack()
                            }
                        }else{
                            OnboardingIntroStack()
                        }
                        Spacer().frame(height:buttonheight)
                    }
                    .padding(.bottom, 100)
                    OnboardingLoginButton(showAccountSetup: $setupView, onClick: onClick)
                }
            }
            .animation(.easeInOut(duration:0.4))
            .edgesIgnoringSafeArea([/*.bottom, .top*/])
            .padding(.bottom, 10)
        }
    }
}

struct OnboardingIntroStack:View{
    var body: some View {
        VStack{
            OnboardingIntroInfosection()
            Spacer().frame(height:40)
            Text(NSLocalizedString("Onboarding.moreInfoInstruction", comment: "Tap to get more Info")).fontWeight(.ultraLight)
        }
    }
}

struct OnboardingLoginButton: View{
    @EnvironmentObject private var viewModel : AuthenticationViewModel
    @State private var loggingIn: Bool = false
    @Binding var showAccountSetup: Bool
    
    var onClick: ()->Void
    
    var body: some View {
        HStack{
            Spacer()
            Button(action:
                {self.loggingIn = true
                self.showAccountSetup = true
            }
            ) {
                if !self.loggingIn {
                    HStack{
                        Spacer()
                        Image(systemName: "person")
                            .resizable()
                            .frame(width: 20, height: 20)
                        Text(NSLocalizedString("Onboarding.loginButtonText", comment: "Login"))
                            .font(.body)
                            .padding(.horizontal, 15)
                        
                        //the google-login (only visable if the button wasnt pressed yet
                        if !self.loggingIn {
                            Button(action:{
                                self.viewModel.startGoogleOauth()
                            }) {
                                HStack{
                                    Text(NSLocalizedString("Onboarding.loginButtonTextGoogle", comment: "Login"))
                                        .font(.body)
                                        .padding(.horizontal, 15)
                                    Image("googleIcon")
                                        .renderingMode(Image.TemplateRenderingMode?.init(Image.TemplateRenderingMode.original))
                                            .resizable()
                                            .frame(width: 25, height: 25)
                                            .padding(.trailing,10)
                                }
                                .padding()
                                .frame(height:buttonheight)
                           
                              }
                            .foregroundColor(.accentColor)
                            .background(Color.white)
                            .cornerRadius(8)
                            .animation(.easeInOut(duration:0.1))
                        }
                        
                        
                        
                    }
                    .frame(height:buttonheight)
                }
            }
            .foregroundColor(.white)
            .background(Color.accentColor)
            .cornerRadius(8)
            .shadow(radius: 5)
            .padding(.horizontal, 10)
           
           Spacer()
       }
        
    }
}

#if DEBUG
struct OnboardingIntro_Previews: PreviewProvider {
   static var previews: some View {
      Group {
        OnboardingIntro(onClick: {})
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("iPhone SE")

         OnboardingIntro(onClick: {})
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("iPhone XS Max")
      }
   }
}
#endif
