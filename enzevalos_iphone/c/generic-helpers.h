//
//  generic-helpers.h
//  enzevalos_iphone
//
//  Created by lazarog98 on 18.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

#ifndef generic_helpers_h
#undef OPENSSL_NO_SSL_INTERN
#undef OPENSSL_NO_CRYPTO_INTERN

#include <string.h>
#include <stdlib.h>
#include <openssl/opensslv.h>
#include <openssl/evp.h> // this header may not be needed
#include <openssl/err.h> // this header may not be needed
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/pkcs7.h>
#include <openssl/cms.h>
#include <openssl/safestack.h>
#include <openssl/x509.h>
#include <openssl/opensslconf.h>


struct linked_list {
    void *content;
    struct linked_list *next;
} typedef linked_list;

struct result {
    char *res;
    unsigned long *errors;
    int num_errors;
    char **certs;
    int num_certs;
    char **fingerprints;
} typedef result;

struct array_with_length {
    int size;
    void *arr;
} typedef array_with_length;

char * pop_error (result *res);
char *get_err_string(unsigned long err);
char *get_err_reason_string(unsigned long err);

void bio_to_str(BIO *bio_in, char **out);
int get_array_size(array_with_length *arr);

char ** init_str_arr(int num);
char ** add_str_to_arr(const char *str, char **arr, int i);
void deallocate_str_arr(char **arr, int len);
char *bin_to_hex ( unsigned char *bin, int len);

result *new_result(void);
void deallocate_result(result *res);

#define generic_helpers_h


#endif /* generic_helpers_h */
