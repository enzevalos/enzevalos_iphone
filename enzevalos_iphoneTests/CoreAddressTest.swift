//
//  CoreAddressTest.swift
//  enzevalos_iphoneTests
//
//  Created by Oliver Wiese on 30.09.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import XCTest

@testable import enzevalos_iphone
class CoreAddressTest: XCTestCase {
    var provider = PersistentDataProvider()

    private func deleteAll() {
        provider = PersistentDataProvider()
        let deleteExpectation = expectation(description: "Delete all data!")
        provider.deleteAll(completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing addresses! \(error)")
            }
            deleteExpectation.fulfill()
        })
        wait(for: [deleteExpectation], timeout: TimeInterval(20))
        provider.reset()
        let frc = provider.fetchedAddressResultController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 0)
        provider = PersistentDataProvider()
    }
    
    override func setUpWithError() throws {
       deleteAll()
    }

    override func tearDownWithError() throws {
        deleteAll()
    }

    func testSimpleCreateAddress() throws {
        var addr1 = AddressProperties(email: "vic@example.com")
        addr1.name = "vic"
        let importExpectation = expectation(description: "Import new addresses!")
        provider.importNewData(from: [addr1], completionHandler: {error  in
            if let error = error {
                XCTFail("Error while importing addresses! \(error)")
            }
            importExpectation.fulfill()
        })
        wait(for: [importExpectation], timeout: TimeInterval(20))
        provider.reset()
        let frc = provider.fetchedAddressResultController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 1, "Missing addresses! \(frc.fetchedObjects?.count ?? 0)")
        if let objects = frc.fetchedObjects {
            for obj in objects {
                XCTAssertEqual(obj.inFromField?.count, 0)
                XCTAssertEqual(obj.email, "vic@example.com")
                XCTAssertEqual(obj.displayname, "vic")
            }
        } else {
            XCTFail("No addresses!")
        }
        
    }
    
    func testCreateAddress() throws {
        let n = 1
        
        var addr1 = AddressProperties(email: "vic@example.com")
        addr1.name = "vic"
        let addr2 = AddressProperties(email: "vic@example.com")
        var importExpectation = expectation(description: "Import new addresses!")
        
        provider.importNewData(from: [addr1], completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing addresses! \(error)")
            }
            importExpectation.fulfill()
        }) 
        wait(for: [importExpectation], timeout: TimeInterval(20))
        
        importExpectation = expectation(description: "Import new addresses!")
        
        provider.importNewData(from: [addr2], completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing addresses! \(error)")
            }
            importExpectation.fulfill()
        })
        wait(for: [importExpectation], timeout: TimeInterval(20))
        provider.reset()
        let frc = provider.fetchedAddressResultController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, n, "Missing addresses! \(frc.fetchedObjects?.count ?? 0)")
        if let objects = frc.fetchedObjects {
            for obj in objects {
                XCTAssertEqual(obj.inFromField?.count, 0)
                XCTAssertEqual(obj.email, "vic@example.com")
                XCTAssertEqual(obj.displayname, nil)
            }
        }
    }
    
    func testCreateAddress2() throws {
        let n = 1
        
        let addr1 = AddressProperties(email: "vic@example.com")
        var addr2 = AddressProperties(email: "vic@example.com")
        addr2.name = "vic"

        var importExpectation = expectation(description: "Import new addresses!")
        
        provider.importNewData(from: [addr1], completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing addresses! \(error)")
            }
            importExpectation.fulfill()
        })
        wait(for: [importExpectation], timeout: TimeInterval(20))
        
        importExpectation = expectation(description: "Import new addresses!")
        
        provider.importNewData(from: [addr2], completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing addresses! \(error)")
            }
            importExpectation.fulfill()
        })
        wait(for: [importExpectation], timeout: TimeInterval(20))
        
        provider.reset()
        let frc = provider.fetchedAddressResultController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, n, "Missing addresses! \(frc.fetchedObjects?.count ?? 0)")
        if let objects = frc.fetchedObjects {
            for obj in objects {
                XCTAssertEqual(obj.inFromField?.count, 0)
                XCTAssertEqual(obj.email, "vic@example.com")
                XCTAssertEqual(obj.displayname, "vic")
            }
        }
    }
    
    
    func testUpdateAddress() throws {
        var addr1 = AddressProperties(email: "vic@example.com")
        addr1.name = "vic"
        let importExpectation = expectation(description: "Import new addresses!")
        provider.importNewData(from: [addr1], completionHandler: {error  in
            if let error = error {
                XCTFail("Error while importing addresses! \(error)")
            }
            importExpectation.fulfill()
        })
        wait(for: [importExpectation], timeout: TimeInterval(20))
        provider.reset()
        let frc = provider.fetchedAddressResultController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 1, "Missing addresses! \(frc.fetchedObjects?.count ?? 0)")
        if let objects = frc.fetchedObjects {
            for obj in objects {
                XCTAssertEqual(obj.inFromField?.count, 0)
                XCTAssertEqual(obj.email, "vic@example.com")
                XCTAssertEqual(obj.displayname, "vic")
                obj.displayname = "Alice"
                try provider.save(taskContext: frc.managedObjectContext)
            }
        } else {
            XCTFail("No addresses!")
        }
        try frc.performFetch()
        if let objects = frc.fetchedObjects {
            for obj in objects {
                XCTAssertEqual(obj.inFromField?.count, 0)
                XCTAssertEqual(obj.email, "vic@example.com")
                XCTAssertEqual(obj.displayname, "Alice")
            }
        } else {
            XCTFail("No addresses!")
        }
        provider.reset()
        let frc3 = try provider.lookUp(entityName: AddressRecord.entityName, sorting: ["email":true], equalPredicates: ["email": "vic@example.com"], differentPredicates: nil, inPredicates: nil, context: nil)
        XCTAssertEqual(frc3.fetchedObjects?.count ?? -1, 1)
        if let objects = frc3.fetchedObjects as? [AddressRecord] {
            for x in objects {
                XCTAssertEqual(x.email, "vic@example.com")
            }
        }
        let frc2 = try provider.lookUp(entityName: AddressRecord.entityName, sorting: ["email":true], equalPredicates: nil, differentPredicates: nil, inPredicates: ["email": ["vic@example.com"]], context: nil)
        XCTAssertEqual(frc2.fetchedObjects?.count ?? -1, 1)
        if let objects = frc2.fetchedObjects as? [AddressRecord] {
            for x in objects {
                XCTAssertEqual(x.email, "vic@example.com")
            }
        }
    }
    
    func testFindRecord() throws {
        
        let a1 = AddressProperties(email: "vic@example.com")
        let a2 = AddressProperties(email: "vic@example.org")
        let a3 = AddressProperties(email: "alex@example.com")
        
        let importExpectation = expectation(description: "Import new addresses!")
        
        provider.importNewData(from: [a1, a2, a3], completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing addresses! \(error)")
            }
            importExpectation.fulfill()
        })
        wait(for: [importExpectation], timeout: TimeInterval(20))
        provider.reset()
        
        let addrs = [a1,a2]
        let controller = try provider.lookUp(entityName: AddressRecord.entityName, sorting: ["email":true], equalPredicates: nil, differentPredicates: nil, inPredicates: ["email": addrs.map{$0.email}], context: nil)
        let emails = addrs.map({$0.email})
        if let myAddrs = controller.fetchedObjects as? [AddressRecord] {
            for x in myAddrs {
                print(x.email)
                if !emails.contains(x.email) {
                    XCTFail("Wrong mail! \(x.email)")
                }
            
            }
        }
    }
}
