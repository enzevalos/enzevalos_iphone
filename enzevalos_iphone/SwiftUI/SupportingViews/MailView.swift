//
//  MailView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 02.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI



struct MailView <M: DisplayMail>: View {
    // TODO: private let coord = AppDelegate.getAppDelegate().inboxCoordinator
    private var mail: M

    init(mail: M){
        self.mail = mail
    }
        
    var body: some View {
        Group{
            HStack {
                subject
                   .frame(minWidth: 10, maxWidth: 200, alignment: .leading)
                   .lineLimit(1)
                Spacer()
                Text(mail.date.dateToString)
                    .font(.footnote)
            }
        }
        .onTapGesture {
           /* if let coord = coord, let m = self.mail as? AppDelegate.M  {
                coord.pushReadView(mail: m)
            }*/
        }
    }
    
    var subject: some View {
        var text = Text(mail.subjectWithFlag)
        if !mail.isRead {
            text = text.bold()
        }
        return text
    }

}



struct MailView_Previews: PreviewProvider {
    static let simulator = Simulators<MailView<ProxyMail>>()
    static let deviceNames: [String] = [
        "iPhone SE",
        "iPhone 11 Pro Max"
    ]
    
  static var previews: some View {
    simulator.previews(view: MailView(mail: ProxyData.SecureMail))//DummyData.SecureMail))
   }
}
