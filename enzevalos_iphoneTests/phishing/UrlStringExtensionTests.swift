//
//  PhishingTests.swift
//  enzevalos_iphoneTests
//
//  Created by Viktoria Sorgalla on 02.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//
import XCTest
@testable import enzevalos_iphone

class UrlStringExtensionTests: XCTestCase {
    
    // Setup for Critical Pattern of a Root Domain
    var testMailText = "https://git.imp.fu-berlin.de/enzevalos/enzevalos_iphone/issues/240http:// www.google.de http://nljbjkjk.de https://www.w3schools.com https://www.w2schools.com https://www.w8schools.com  https://www.w33schools.com https://www.w22schools.com https://www.w99schools.eu.com https://git.imp.fu-ber_lin.de/enzevalos/enzevalos_iphone/issues/240 http://www.google-.de http://nljbj#kjk.de https://www.3schools.com https://www.w2sc..hools.com https://www.w8sch?ools.com https://www.w33sc_hools.com  https://www.w22schooIs.com  https://www.W99schools$.com"
    var vaildRD = ["fu-berlin.de", "google.de", "nljbjkjk.de", "w3schools.com", "w2schools.com", "w8schools.com", "w33schools.com", "w22schools.com", "w99schools.eu.com", "3schools.com"]
    // Setup for get second level doamin of URL
    var finalSLD = ["fu-berlin", "google", "nljbjkjk", "w3schools", "w2schools", "w8schools", "w33schools", "w22schools", "w99schools"]
    // Setup for get root domein of URL
    var testmailURL = "https://git.imp.fu-ber_lin.de/enzevalos/enzevalos_iphone/issues/240 http://www.google-.de http://nljbj#kjk.de https://www.3schools.com https://www.w2sc..hools.com https://www.w8sch?ools.com https://www.w33sc_hools.com  https://www.w22schooIs.com  https://www.W99schools$.com https://git.imp.fu-berlin.de/enzevalos/enzevalos_iphone/issues/240 http://www.google.de http://nljbjkjk.de https://www.w3schools.com https://www.w2schools.com https://www.w8schools.com  https://www.w33schools.com https://www.w22schools.com https://www.w99schools.eu.com bob.alice@zedat.fu-berlin.de https://git.imp.fu-berlin.de/enzevalos/enzevalos_iphone/issues/240 www.google.de kljhl@hjkghgkhj.com nljbjkjk.de https://www.w3#schools.com/ https://www.w2schools.com/ https://www.w8schools.com/ https://www.w33schools.com/ https://www.w22schools.com/ https://www.w99schools.com/ 你好@yahoo.com test@莎士比亚.org 你好@莎士比亚.org Rδοκιμή@παράδειγμα.δοκιμή 管理员@中国互联网络信息中心.中国 你好@yahoo.com test@莎士比亚.org 你好@莎士比亚.org"
    var finalRD = ["fu-berlin.de", "google.de", "nljbjkjk.de", "w3schools.com", "w2schools.com", "w8schools.com", "w33schools.com", "w22schools.com", "w99schools.com"]
    
    // Setup for critical pattern of a root domain
    var testmail = "fu-ber_lin.de google-.de nljbj#kjk.de 3schools.com w2sc..hools.com w8sch?ools.com w33sc_hools.com w22schooIs.com W99schools$.com fu-berlin.de google.de nljbjkjk.de w3schools.com w2schools.com w8schools.com w33schools.com w22schools.com w99schools.eu.com bob.alice@zedat.fu-berlin.de git.imp.fu-berlin.de google.de kljhl@hjkghgkhj.com nljbjkjk.de w3#schools.com w2schools.com w8schools.com w33schools.com w22schools.com w99schools.com 你好@yahoo.com test@莎士比亚.org 你好@莎士比亚.org Rδοκιμή@παράδειγμα.δοκιμή 管理员@中国互联网络信息中心.中国 你好@yahoo.com test@莎士比亚.org 你好@莎士比亚.org"
    var finalunciticalrd = ["3schools.com", "fu-berlin.de", "google.de", "nljbjkjk.de", "w3schools.com", "w2schools.com", "w8schools.com", "w33schools.com", "w22schools.com", "w99schools.eu.com", "git.imp.fu-berlin.de", "google.de", "w2schools.com", "w8schools.com", "w33schools.com", "w22schools.com", "w99schools.com"]
    
    //  General TestText for all (needs to simulate a body of text from which to extract specific Strings
    var TestText = "blabla moep jkjlkj lkjkljknmbjks llhil k. jhkhkih. huhuhj! fsdf bob.alice@zedat.fu-berlin.de dfsf. jhjknjknjkh https://git.imp.fu-berlin.de/enzevalos/enzevalos_iphone/issues/240  hjkhjkhkhn www.google.de kljhl@hjkghgkhj.com nljbjkjk.de url tag html mail <a href='https://www.w3schools.com'>Visit W3Schools</a> hjhkhiuhziu kjhkl <a href=\"https://www.w2schools.com\">Visit W2Schools</a>. lknljnlk. kmm /n lmölmpöl < a href=\"https://www.w8schools.com\">Visit W8Schools</a> gfg fghfghnhg <a      href=\"https://www.w33schools.com\">Visit W33Schools</a> nkjhjkhkjn,mn jnmnklmj j <a href=\"https://www.w22schools.com\">Visit W22Schools</ a> hghjcfgh hfgchnvhj vgjcgj cjghcj <a href=\"https://www.w99schools.com\">Visit W99Schools</a    >    你好@yahoo.com eewfve  test@莎士比亚.org    你好@莎士比亚.org  Rδοκιμή@παράδειγμα.δοκιμή or 管理员@中国互联网络信息中心.中国 你好45@yahoo.com"
    
    // Setup for find URLs in text
    var finalURLs = ["https://git.imp.fu-berlin.de/enzevalos/enzevalos_iphone/issues/240", "http://www.google.de", "http://nljbjkjk.de", "https://www.w3schools.com", "https://www.w2schools.com", "https://www.w8schools.com", "https://www.w33schools.com", "https://www.w22schools.com", "https://www.w99schools.com"]
    
    // Setup for tag tests
    var finalTestTextFindHtmlTags = ["<a href=\'https://www.w3schools.com\'>Visit W3Schools</a>", "<a href=\"https://www.w2schools.com\">Visit W2Schools</a>", "< a href=\"https://www.w8schools.com\">Visit W8Schools</a>", "<a      href=\"https://www.w33schools.com\">Visit W33Schools</a>", "<a href=\"https://www.w22schools.com\">Visit W22Schools</ a>", "<a href=\"https://www.w99schools.com\">Visit W99Schools</a    >"]
    
    var finalTestTextHtmlTagURL = ["https://www.w3schools.com", "https://www.w2schools.com", "https://www.w8schools.com", "https://www.w33schools.com", "https://www.w22schools.com", "https://www.w99schools.com"]
    
    var finalTestTexthttpTagLinkName = ["Visit W3Schools", "Visit W2Schools", "Visit W8Schools", "Visit W33Schools", "Visit W22Schools", "Visit W99Schools"]
    
    var testUrl = "www.google.de"
    var testURL2 = "https://www.mycampus.imp.fu-berlin.de/portal"
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testTextFindURL(){
        let arr = TestText.findURL()
        XCTAssertNotNil(arr)
        XCTAssertEqual(arr[0], finalURLs[0])
        XCTAssertEqual(arr[1], finalURLs[1])
        XCTAssertEqual(arr[2], finalURLs[2])
        XCTAssertEqual(arr[3], finalURLs[3])
    }
    
    func testTextFindHlmlTags(){
        let arr = TestText.findHtmlTags()
        XCTAssertNotNil(arr)
        XCTAssertEqual(arr[0], finalTestTextFindHtmlTags[0])
        XCTAssertEqual(arr[1], finalTestTextFindHtmlTags[1])
        XCTAssertEqual(arr[2], finalTestTextFindHtmlTags[2])
        XCTAssertEqual(arr[3], finalTestTextFindHtmlTags[3])
        XCTAssertEqual(arr[4], finalTestTextFindHtmlTags[4])
        XCTAssertEqual(arr[5], finalTestTextFindHtmlTags[5])
    }
    
    func testTextHtmlTagURL(){
        let arr = TestText.htmlTagURL()
        XCTAssertNotNil(arr)
        XCTAssertEqual(arr[0], finalTestTextHtmlTagURL[0])
        XCTAssertEqual(arr[1], finalTestTextHtmlTagURL[1])
        XCTAssertEqual(arr[2], finalTestTextHtmlTagURL[2])
        XCTAssertEqual(arr[3], finalTestTextHtmlTagURL[3])
        XCTAssertEqual(arr[4], finalTestTextHtmlTagURL[4])
        XCTAssertEqual(arr[5], finalTestTextHtmlTagURL[5])
    }
    
    func testTextHttpTagLinkName(){
        let arr = TestText.htmlTagLinkName()
        XCTAssertNotNil(arr)
        XCTAssertEqual(arr[0], finalTestTexthttpTagLinkName[0])
        XCTAssertEqual(arr[1], finalTestTexthttpTagLinkName[1])
        XCTAssertEqual(arr[2], finalTestTexthttpTagLinkName[2])
        XCTAssertEqual(arr[3], finalTestTexthttpTagLinkName[3])
        XCTAssertEqual(arr[4], finalTestTexthttpTagLinkName[4])
        XCTAssertEqual(arr[5], finalTestTexthttpTagLinkName[5])
    }
    
    func testUrl2Ip(){
        XCTAssertNotNil(testUrl.url2ip())
        //It changes often therefore a direct comparisson is not useful
    }
    
    // Test get Root Domain
    func testGetRD() {
        let rd = testMailText.getRD()
        print(rd)
    }
        
    // Test get Second Level Domain
    func testGetSLD(){
        let sld = TestText.getSLD()
        XCTAssertNotNil(sld)
        XCTAssertEqual(sld[0], finalSLD[0])
        XCTAssertEqual(sld[1], finalSLD[1])
        XCTAssertEqual(sld[2], finalSLD[2])
        XCTAssertEqual(sld[3], finalSLD[3])
    }
    
    func testIsAllowedDistance() {
        let str = "google"
        // #distance:      0         1        2         3         4       <
        let strArray = ["google", "googIe", "gogIe", "gooogIIe", "go", "amazon"]
        XCTAssertTrue(str.isAllowedDistance(str: strArray[0], allowedEditDistance: 4))
        XCTAssertFalse(str.isAllowedDistance(str: strArray[1], allowedEditDistance: 4))
        XCTAssertFalse(str.isAllowedDistance(str: strArray[2], allowedEditDistance: 4))
        XCTAssertFalse(str.isAllowedDistance(str: strArray[3], allowedEditDistance: 6))
        XCTAssertTrue(str.isAllowedDistance(str: strArray[4], allowedEditDistance: 4))
        XCTAssertTrue(str.isAllowedDistance(str: strArray[5], allowedEditDistance: 4))
    }
    
    func testLevenshtein(){
        let str1 = "hallo"
        let str2 = "hlalo"
        let str3 = "haloo"
        let str4 = "halo"
        let str5 = "halllo"
        let str6 = "halol"
        let edd1 = str1.levenshtein(str2)
        let edd2 = str1.levenshtein(str3)
        let edd3 = str1.levenshtein(str4)
        let edd4 = str1.levenshtein(str5)
        let edd5 = str1.levenshtein(str6)
        XCTAssertEqual(edd1, 2, "Wrong distance \(str1) vs \(str2)")
        XCTAssertEqual(edd2, 2, "Wrong distance \(str1) vs \(str3)")
        XCTAssertEqual(edd3, 1, "Wrong distance \(str1) vs \(str4)")
        XCTAssertEqual(edd4, 1, "Wrong distance \(str1) vs \(str5)")
        XCTAssertEqual(edd5, 2, "Wrong distance \(str1) vs \(str6)")
    }

}

