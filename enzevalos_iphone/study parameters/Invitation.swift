//
//  Invitation.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 28.01.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation

enum Inviation: Int, StudyParameterProtocol {
    case InviteMail = 0, PasswordEnc = 1, Censorship = 2, FreeText = 3

    static var defaultValue: StudyParameterProtocol {
        get {
            return Inviation.InviteMail
        }
    }
    
    static func findParameter(rawvalue: Int) -> StudyParameterProtocol {
        switch rawvalue {
        case Inviation.InviteMail.rawValue:
            return Inviation.InviteMail
        case Inviation.PasswordEnc.rawValue:
            return Inviation.PasswordEnc
        case Inviation.Censorship.rawValue:
            return Inviation.Censorship
        case Inviation.FreeText.rawValue:
            return Inviation.FreeText
        default:
            return defaultValue
        }
    }
    
    static var name: String {
        get {
            return "invitation"
        }
    }
    
    static var keyName: String {
        get {
            return "invitation mode"
        }
    }
    
    static var numberOfTreatments: UInt32 {
        get {
            return 4
        }
    }
    
    var value: Int {
        get {
            return self.rawValue
        }
    }
    
    func startStudy() {
        return
    }
    
    var freeTextInvitationTitle: String {
        get {
            switch self {
            case .FreeText, .InviteMail:
                return NSLocalizedString("inviteContacts", comment: "Allows users to invite contacts without encryption key")
            case .Censorship, .PasswordEnc:
                return NSLocalizedString("inviteContacts.Censor", comment: "Allows users to invite contacts without encryption key")
            }
        }
    }
    
}
