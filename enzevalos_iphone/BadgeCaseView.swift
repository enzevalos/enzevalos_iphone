//
//  BadgeCaseView.swift
//  enzevalos_iphone
//
//  Created by Sezzart on 23.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct BadgeCaseView: View {
    init() {
        UITableView.appearance().separatorStyle = .none
    }
    let badges: [Badge] = [Badge]() // GamificationData.sharedInstance.badges.map { Badge(type: $0.type, description: $0.displayName, imageOn: $0.onName, imageOff: $0.offName, isActive: $0.isAchieved()) }
    
    var body: some View {
        NavigationView {
            VStack {
                Divider()
                Spacer()
                List {
                    ForEach(badges) { badge in
                        NavigationLink(destination: BadgeProgressView(badge: badge)) {
                            BadgeItem(badgeImageSrcOn: badge.imageOn, badgeImageSrcOff: badge.imageOff, badgeTitle: badge.description, isActive: badge.isActive)
                        }
                    }
                }
            }.navigationBarTitle(Text(NSLocalizedString("Gamification.Overview.HeaderTitle", comment:"Badge Case Navigation Title" )), displayMode: .inline).navigationBarItems(leading: Button(action: goBack, label: {
                Text(NSLocalizedString("Back", comment: ""))
            }))
        }
    }
    
    func goBack() {
       /* if let coord = AppDelegate.getAppDelegate().inboxCoordinator {
            coord.goBack()
        }*/
    }
}

struct Badge: Identifiable {
    var id = UUID()
    //var type: BadgeType
    var description: String
    var imageOn: String
    var imageOff: String
    var isActive: Bool
}

struct BadgeItem: View {
    
    //let badgeType: BadgeType
    let badgeImageSrcOn: String
    let badgeImageSrcOff: String
    let badgeTitle: String
    var isActive: Bool
    
    init(badgeImageSrcOn: String, badgeImageSrcOff: String, badgeTitle: String, isActive: Bool = false) {
        
        // self.badgeType = badgeType
        self.badgeImageSrcOn = badgeImageSrcOn
        self.badgeImageSrcOff = badgeImageSrcOff
        self.badgeTitle =  badgeTitle
        self.isActive = isActive
    }
    
    var body: some View {
        HStack {
            Image(isActive ? badgeImageSrcOn : badgeImageSrcOff)
                .resizable()
                .scaledToFit()
                .frame(width: 40,height: 40)
                .padding(10)
            Text(self.badgeTitle)
        }
    }
}

struct BadgeCaseView_Previews: PreviewProvider {
    static var previews: some View {
        BadgeCaseView()
    }
}
