//
//  InboxCoordinator.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 04.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation
import SwiftUI

class InboxCoordinator <M: MailRecord, C: DisplayContact> {
    let root: UINavigationController
    
    private let mainStoryboardName = "Main"
    private let mainStoryboard: UIStoryboard
    private var inbox: UIViewController?

    
    init(root: UINavigationController) {
        self.root = root
        mainStoryboard = UIStoryboard(name: self.mainStoryboardName, bundle: nil)
    }
    
    func goBack() {
        root.popViewController(animated: true)
    }
    
    func pushInbox(){
        /* TODO Fix
        try? AppDelegate.getAppDelegate().mailHandler.startIMAPIdleIfSupported()
        AppDelegate.getAppDelegate().mailHandler.updateFolder(folder: MailHandler.INBOX, completionCallback: {_ in
        })
         */
        root.isToolbarHidden = true

        if let vc = inbox {
            if root.viewControllers.contains(vc) {
                root.popToViewController(vc, animated: false)
            } else {
                root.pushViewController(vc, animated: true)
            }
        }
        else {
            /* TODO: SwiftUI and DB?
            KeyRecord.findFirstMails()
            let context = DataHandler.handler.managedObjectContext
            let vc = UIHostingController(rootView: Inbox(coord: self).environment(\.managedObjectContext, context))
            inbox = vc
            root.pushViewController(vc, animated: true)
            */
        }
    }

    func pushFoldersView() {
        let controller = mainStoryboard.instantiateViewController(withIdentifier: ViewID.FoldersView.rawValue)
        root.isToolbarHidden = false
        root.pushViewController(controller, animated: true)
    }
    
    func pushReadView(mail: M) {
       /* if AppDelegate.getAppDelegate().newReadView, let readCoord = AppDelegate.getAppDelegate().readViewCoordinator {
            readCoord.pushReadView(mail: mail)
        } else {
            let vc = mainStoryboard.instantiateViewController(withIdentifier: ViewID.ReadView.rawValue)
            /*
            if let vc = vc as? ReadViewController {
                vc.mail = mail
            }*/
            root.isToolbarHidden = false
            root.pushViewController(vc, animated: true)
        }*/
    }
    
    // TODO FIX!!! Was ist ein Record? Key oder address?
    func pushRecordView(record: C){
        let vc = mainStoryboard.instantiateViewController(withIdentifier: ViewID.KeyRecordView.rawValue)
        /*if let vc = vc as? ContactViewController {
            vc.keyRecord = record
        }*/
        root.isToolbarHidden = false
        root.pushViewController(vc, animated: true)
    }
    
    func pushComposeView() {
        let vc = mainStoryboard.instantiateViewController(identifier: ViewID.ComposeView.rawValue)
        /*if let vc = vc as? SendViewController {
            vc.wasPushed = true
        }*/
        root.isToolbarHidden = true
        root.pushViewController(vc, animated: true)
    }
    
    // TODO FIX!!! Was ist ein Record? Key oder address?
    func pushMailListView(record: C) {
        let vc = mainStoryboard.instantiateViewController(identifier: ViewID.MailListView.rawValue)
        /*if let vc = vc as? ListViewController {
            vc.contact = record
        }*/
        root.isToolbarHidden = true
        root.pushViewController(vc, animated: true)
    }
}
