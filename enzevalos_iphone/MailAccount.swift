//
//  MailAccount.swift
//  enzevalos_iphone
//
//  Created by SWP Usable Security für Smartphones on 05.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation

class MailAccount {
    
    let emailAddress: String
    let password: String
    let username: String?
    let imapServer: String
    let imapPort: Int
    let imapEncryption: Int
    let smtpServer: String
    let smtpPort: Int
    let smtpEncryption: Int
    let authType: Int?
    
    init(emailAddress: String, password: String, username: String? = nil, imapServer: String, imapPort: Int, imapEncryption: Int, smtpServer: String, smtpPort: Int, smtpEncryption: Int, authType: Int? = nil) {
        self.emailAddress = emailAddress
        self.password = password
        self.username = username
        self.imapServer = imapServer
        self.imapPort = imapPort
        self.imapEncryption = imapEncryption
        self.smtpServer = smtpServer
        self.smtpPort = smtpPort
        self.smtpEncryption = smtpEncryption
        self.authType = authType
    }
    
    init(emailAddress: String, password: String, username: String? = nil) {
        self.emailAddress = emailAddress
        self.password = password
        username == nil ? (self.username = emailAddress) : (self.username = username)
        self.imapServer = ""
        self.imapPort = 0
        self.imapEncryption = 0
        self.smtpServer = ""
        self.smtpPort = 0
        self.smtpEncryption = 0
        self.authType = nil
    }
}
