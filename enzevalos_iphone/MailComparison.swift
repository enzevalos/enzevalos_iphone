//
//  MailComparison.swift
//  enzevalos_iphone
//
//  Created by Katharina Müller and Viktoria Sorgalla on 04.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation
import CoreData


public enum ResultCompareSenderToContacts {
       case isContact, isSender, OnlyIdentity, Unknown
   }


//TODO: Move to Mail Address?
extension String {
    
    /**
     compares the given mail address to the persistant mails and returns the number of matches
     ***/
    private func countMatches(_ mailAddr: String, _ inboxMails: [MailRecord]) -> Int {
        var senders: [String] = []
        for sender in inboxMails{
            if let sender: String = sender.fromAddress?.email {
                senders.append(sender)
            }
        }
        var numberOfFoundMatches = 0
        for addr in senders{
            if addr == mailAddr{
                numberOfFoundMatches += 1
            }
        }
        return numberOfFoundMatches
    }
    
    /**
     returns a mail address, wich has the same identity as the given one but a different domain
     ***/
    private func getIdentityWithDifferentDomain(_ mailAdd: String, inboxMails: [MailRecord]) -> String{
        let senderIdentity = mailAdd.getLocalMailIdentity()
        var senderDomain = mailAdd.splitAddress()
            senderDomain.removeFirst()
        
        var senders: [String] = []
        for sender in inboxMails{
            if let sender: String = sender.fromAddress?.email {
             senders.append(sender)
            }
        }
        var foundMatch = ""
        for addr in senders{
            let foundIdentity = addr.getLocalMailIdentity()
            var foundDomain = addr.splitAddress()
            foundDomain.removeFirst()
            if senderIdentity == foundIdentity && senderDomain != foundDomain{
                foundMatch = addr
                return foundMatch
            }
        }
        return foundMatch
    }
    
    /**
     looks if the mail address of the sender is already known
     ***/
    public func compareSenderToContacts() -> ResultCompareSenderToContacts {
        guard let inboxMails = PersistentDataProvider.dataProvider.fetchedMailResultsController.fetchedObjects else {
            let contacts =  ContactHandler().findContactBy(email: self)
            if contacts.count > 0 {
                return ResultCompareSenderToContacts.isContact
            }
            return ResultCompareSenderToContacts.Unknown
        }
        // Case 1: Is the address of the sender known through previous mails?
        if countMatches(self, inboxMails) >= 2{
            return ResultCompareSenderToContacts.isSender
        }
        // Case 2: Is the address of the sender in the address book?
        let contacts =  ContactHandler().findContactBy(email: self)
        if contacts.count > 0 {
            return ResultCompareSenderToContacts.isContact
        }
        // Case 3: The identity of an mail address is known, but not the (sub-)domain
        let foundIdentity = getIdentityWithDifferentDomain(self, inboxMails: inboxMails)
        if foundIdentity != ""{
            return ResultCompareSenderToContacts.OnlyIdentity
        }
        return ResultCompareSenderToContacts.Unknown
       }
}
