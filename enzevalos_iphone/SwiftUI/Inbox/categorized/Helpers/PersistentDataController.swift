//
//  PersistentDataController.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 31.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//
//  https://www.iosapptemplates.com/blog/ios-development/data-persistence-ios-swift


import Foundation

//TODO this is not working yet

class PersistentDataController {
    enum Key: String, CaseIterable {
        case classicActive, sort
    }
    
    let userDefaults: UserDefaults
    // MARK: - Lifecycle
    init(userDefaults: UserDefaults = .standard) {
        self.userDefaults = userDefaults
    }
    // MARK: - API
    
    var inboxSettings:(Bool?,SortCategoriesBy?){
        get{
            return getInboxSettings()
        }
        set(new){
            setInboxSettings(isClassicActive: new.0,sortBy: new.1)
        }
    }
    
    func setInboxSettings(isClassicActive: Bool?, sortBy: SortCategoriesBy?) {
        saveValue(forKey: .classicActive, value: isClassicActive)
        saveValue(forKey: .sort, value: sortBy)
    }
    
    func getInboxSettings() -> (isClassicActive: Bool?, sortBy: SortCategoriesBy?) {
        let isClassicActive: Bool? = readValue(forKey: .classicActive)
        let sortBy: SortCategoriesBy? = readValue(forKey: .sort)
        return (isClassicActive, sortBy)
    }
    

    // MARK: - Private
    private func saveValue(forKey key: Key, value: Any?) {
        userDefaults.set(value, forKey: key.rawValue)
    }
    private func readValue<T>(forKey key: Key) -> T? {
        return userDefaults.value(forKey: key.rawValue) as? T
    }
}
