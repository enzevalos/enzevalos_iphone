//
//  ChatCategoryModel.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 26.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

///some extra functionality on top of the standort category functionality
///for the ChatCategory obv.
class ChatCategoryVM: ObservableObject {
    
    init(_ categoryModel : CategoryViewModel) {
        categoryBaseModel = categoryModel
    }
    
    @ObservedObject var categoryBaseModel : CategoryViewModel
    
    var mails : [eMail] {
        categoryBaseModel.allMails //group by sender?
    }
    
    var singleChats : [SingleChat] {
        //would probably be cooler to fetch the users directly and filter their mails with an NSPredicate ?
        var chatsByUser : [eMail.C : SingleChat] = [:]
        for mail in mails {
            
            if mail.addresses.count > 2 {continue} //group chat
            
            guard let candidat1 = mail.addresses[safe: 0] else{continue}
            guard let candidat2 = mail.addresses.last else{continue}
            
            let _/*user*/ = !candidat1.isUser ? candidat1 : candidat2
            let partner = !candidat1.isUser ? candidat2 : candidat1
            
            if var alreadyCreatedChat = chatsByUser[partner]{
                alreadyCreatedChat.messages.append(mail)
            }else{
                chatsByUser[partner]=SingleChat(
                    timeStamp: mail.date,
                    partner: partner,
                    messages: [mail]
                )
            }
        }
        //turn to array and sort by timeStamp
        return chatsByUser.map{$1}.sorted(by: {$0.timeStamp >= $1.timeStamp})
    }
    
    //future work
    var groupChatUsers : [GroupChat] {
        //TODO
        []
    }
    
    /*
    var isFullscreen : Bool {categoryBaseModel.size == nil}
    func attachmentsize(_ attachmentCount:Int)->CGFloat {
        return isFullscreen ? 250 : ((categoryBaseModel.size! - 50) / CGFloat(attachmentCount))
    }
    */
    
}

struct SingleChat:Hashable{
    var timeStamp : Date
    var partner : eMail.C
    var messages : [eMail]
}

//TODO future work
struct GroupChat{
    var timeStamp : Date
    var users : [eMail.C]
    var messages : [eMail]
}

