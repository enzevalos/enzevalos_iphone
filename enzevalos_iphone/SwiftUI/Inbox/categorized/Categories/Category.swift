//
//  Category.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 18.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

typealias CategoryV = Category

///what a category has (and needs) for values
protocol Category {
    var name            : String                        {get}
    var description     : String                        {get}
    var icon            : Image                         {get}
    var isBigger        : Bool                          {get}
    var isResizable     : Bool                          {get}
    var searchFields    : [SearchFilter]                {get}
    var filter          : MailFilter                    {get}
    var andPredicate    : NSPredicate                   {get}
    
    func body(model : CategoryViewModel)->AnyView
}

///standart values for a category if not specified otherways
extension CategoryV {
    var description     : String    {""}
    var icon            : Image     {Image(systemName: "envelope.fill")}
    var isBigger        : Bool      {false}
    var isResizable     : Bool      {true}
    var searchFields    : [SearchFilter] {SearchFilterCollections.Standarts}
    var andPredicate    : NSPredicate {NSPredicate(value: true)}
}

///to which extend the category will open a specific mail
enum MailCategoryOpenability : Int16, Comparable{
    static func < (lhs: MailCategoryOpenability, rhs: MailCategoryOpenability) -> Bool {
        lhs.rawValue < rhs.rawValue
    }
    
    case cannotOpen = 0,    ///:  means this category wont open this email, wont display it and if forced to might cause undefined behavior
         canOpen    = 1,    ///:  the category can display this mail , but the category wont float to the top of the inbox
         shouldOpen = 2,    ///:  the category displays this mails and floats to the top to show that it has new mail
         mustOpen   = 3     ///:  overrides all other categories to force them to not open this mail (e.g. cause it is dangerous phishing)
    
}


typealias eMail = MailRecord

///each category needs a filter to return to which extend the category will open a specific mail
typealias MailFilter = (eMail)->MailCategoryOpenability

///what all the ViewModels use to compare to categories to see if they are the same (also the Database)
typealias CategoryIDType = String
