//
//  Searchview_2.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 09.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

/**
 A SearchView for mails with a search segmented Picker to choose the search area.
 Open Problems: Deplay the search, s.t. we do not start a search after each input.
 
 */
struct CustomizableSearchView: View {
    
    @Binding var search:Search
    
    var body: some View {
        VStack{
            HStack {
                searchFieldView
                if search.isExpanded  {
                    Button("Cancel") {
                            UIApplication.shared.endEditing(true)
                        self.search.text = ""
                            self.search.isExpanded = false
                        self.search.now = false
                    }
                    .foregroundColor(Color(.systemBlue))
                }
            }
           if search.isExpanded {
                searchSegment
            }
        }
        .padding(.horizontal)
    }
    
    var searchFieldView: some View {
        HStack {
            Image(systemName: "magnifyingglass")
            TextField(NSLocalizedString("Searchbar.Title", comment: "Search"), text: $search.text, onEditingChanged: { isEditing in
                self.search.isExpanded = true
            }, onCommit: {
                self.search.now = true
            })
                .foregroundColor(.primary)
            Button(action: {
                self.search.text = ""
            }) {
                Image(systemName: "xmark.circle.fill").opacity(search.text == "" ? 0 : 1)
            }
        }
        .padding(EdgeInsets(top: 8, leading: 6, bottom: 8, trailing: 6))
        .foregroundColor(.secondary)
        .background(Color(.secondarySystemBackground))
        .cornerRadius(10.0)
    }
    
    var searchSegment: some View {
        Picker(selection: $search.activeIndex, label: EmptyView()) {
            ForEach(0 ..< search.fields.count) { index in
                Text(search.fields[safe: index]?.text ?? "").tag(index)
                    .onTapGesture {
                        self.search.activeIndex = index
                }
            }
        }
        .pickerStyle(SegmentedPickerStyle())
    }
}
