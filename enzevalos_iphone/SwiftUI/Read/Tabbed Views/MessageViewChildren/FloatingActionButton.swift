//
//  FloatingActionButton.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 12.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct FloatingActionButton<Content: View>: View{
    
    var innerPadding:CGFloat, onLongPress:()->Void, onShortPress:()->Void = {}, color: Color, pressColor: Color
    
    let content: Content
    
    @GestureState var tapDown: Bool = false
    
    init(color:Color = Color.accentColor, pressColor:Color = Color.init(red: 0.5, green: 0.5, blue: 1), radius:CGFloat = 15, onLongPress:@escaping ()->Void = {}, onShortPress:@escaping ()->Void = {}, @ViewBuilder content: () -> Content) {
        self.onLongPress = onLongPress
        self.onShortPress = onShortPress
        self.innerPadding = radius
        self.content = content()
        
        self.color = color
        self.pressColor = pressColor
    }
    
    
    var body: some View {
        VStack{
            content
        }
        .foregroundColor(.white)
        .padding(innerPadding)
        .background(self.tapDown ? pressColor : color)
        .clipShape(Capsule())
        .shadow(radius: self.tapDown ? 0 : 3)
        .gesture (
            TapGesture()
                .onEnded{ _ in
                    self.onShortPress()
                }
                .simultaneously(with:
                    LongPressGesture(minimumDuration: 0.5)
                        .updating($tapDown, body: { (currentState, state, transaction) in
                            state = currentState
                        })
                        .onEnded({ _ in
                            self.onLongPress()
                        })
                )
        )
    }
}

