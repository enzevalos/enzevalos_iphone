//
//  AddressRecord+CoreDataProperties.swift
//  
//
//  Created by Oliver Wiese on 30.10.20.
//
//

import Foundation
import CoreData


extension AddressRecord {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AddressRecord> {
        return NSFetchRequest<AddressRecord>(entityName: "AddressRecord")
    }

    @NSManaged public var displayname: String?
    @NSManaged public var image: Data?
    @NSManaged public var email: String
    @NSManaged public var phoneBookID: String?
    @NSManaged public var inBCCField: NSSet?
    @NSManaged public var inCCField: NSSet?
    @NSManaged public var inFromField: NSSet?
    @NSManaged public var inToField: NSSet?
    @NSManaged public var usedPublicKeys: NSSet?
    @NSManaged public var usedSecretKeys: NSSet?
    @NSManaged public var primaryKey: PublicKeyRecord?

}

// MARK: Generated accessors for inBCCField
extension AddressRecord {

    @objc(addInBCCFieldObject:)
    @NSManaged public func addToInBCCField(_ value: MailRecord)

    @objc(removeInBCCFieldObject:)
    @NSManaged public func removeFromInBCCField(_ value: MailRecord)

    @objc(addInBCCField:)
    @NSManaged public func addToInBCCField(_ values: NSSet)

    @objc(removeInBCCField:)
    @NSManaged public func removeFromInBCCField(_ values: NSSet)

}

// MARK: Generated accessors for inCCField
extension AddressRecord {

    @objc(addInCCFieldObject:)
    @NSManaged public func addToInCCField(_ value: MailRecord)

    @objc(removeInCCFieldObject:)
    @NSManaged public func removeFromInCCField(_ value: MailRecord)

    @objc(addInCCField:)
    @NSManaged public func addToInCCField(_ values: NSSet)

    @objc(removeInCCField:)
    @NSManaged public func removeFromInCCField(_ values: NSSet)

}

// MARK: Generated accessors for inFromField
extension AddressRecord {

    @objc(addInFromFieldObject:)
    @NSManaged public func addToInFromField(_ value: MailRecord)

    @objc(removeInFromFieldObject:)
    @NSManaged public func removeFromInFromField(_ value: MailRecord)

    @objc(addInFromField:)
    @NSManaged public func addToInFromField(_ values: NSSet)

    @objc(removeInFromField:)
    @NSManaged public func removeFromInFromField(_ values: NSSet)

}

// MARK: Generated accessors for inToField
extension AddressRecord {

    @objc(addInToFieldObject:)
    @NSManaged public func addToInToField(_ value: MailRecord)

    @objc(removeInToFieldObject:)
    @NSManaged public func removeFromInToField(_ value: MailRecord)

    @objc(addInToField:)
    @NSManaged public func addToInToField(_ values: NSSet)

    @objc(removeInToField:)
    @NSManaged public func removeFromInToField(_ values: NSSet)

}

// MARK: Generated accessors for usedPublicKeys
extension AddressRecord {

    @objc(addUsedPublicKeysObject:)
    @NSManaged public func addToUsedPublicKeys(_ value: PublicKeyRecord)

    @objc(removeUsedPublicKeysObject:)
    @NSManaged public func removeFromUsedPublicKeys(_ value: PublicKeyRecord)

    @objc(addUsedPublicKeys:)
    @NSManaged public func addToUsedPublicKeys(_ values: NSSet)

    @objc(removeUsedPublicKeys:)
    @NSManaged public func removeFromUsedPublicKeys(_ values: NSSet)

}

// MARK: Generated accessors for usedSecretKeys
extension AddressRecord {

    @objc(addUsedSecretKeysObject:)
    @NSManaged public func addToUsedSecretKeys(_ value: SecretKeyRecord)

    @objc(removeUsedSecretKeysObject:)
    @NSManaged public func removeFromUsedSecretKeys(_ value: SecretKeyRecord)

    @objc(addUsedSecretKeys:)
    @NSManaged public func addToUsedSecretKeys(_ values: NSSet)

    @objc(removeUsedSecretKeys:)
    @NSManaged public func removeFromUsedSecretKeys(_ values: NSSet)

}
