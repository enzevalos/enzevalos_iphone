//
//  AuthenticationModel.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 18.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation
import Combine

/**
 Model of the Authentication according to MVVP pattern. Performs all the necessary server calls according to the data provided by the ViewModel
 */
class AuthenticationModel: NSObject {
    
    enum AuthenticationResult {case Success, Timeout, Error(value: MailServerConnectionError)}
    
    static let instance: AuthenticationModel = AuthenticationModel()
    
    var extendedValidation: Bool = false
    
    private var currentIMAP: MailSession?
    private var currentSMTP: MailSession?
    var imapCallback: AuthenticationCallback? = nil
    var smtpCallback: AuthenticationCallback? = nil
    
    var imapConfigurationSuccessful = false
    var smtpConfigurationSuccessful = false

    var dispatchGroup = DispatchGroup()
        
    /**
     Start asynchronous tasks for checking IMAP and SMTP configuration with a *timeoutDelay* after which the tasks are not getting consumed anymore.
     
     - Parameters:
        - mailAccount: data class that holds the properties used for establishing the connection
        - extendedValidation: indicates whether the imap/smtp configuration got specified by the user or should use default values
     
     -  Returns: a Future that produces an AuthenticationResult once the server calls are done
     */
    func checkConfig(mailAccount: MailAccount, extendedValidation: Bool) -> Future<AuthenticationResult, Never> {
        self.extendedValidation = extendedValidation
        var future: Future<AuthenticationResult, Never>
        dispatchGroup = DispatchGroup()
        imapCallback = AuthenticationCallback(callback: onImapCompleted)
        let imapItem = DispatchWorkItem { self.checkIMAPConfig(mailAccount: mailAccount, self.imapCallback! ) }
        dispatchGroup.enter()
        DispatchQueue.global().async(execute: imapItem)

        smtpCallback = AuthenticationCallback(callback: onSmtpCompleted)
        let smtpItem = DispatchWorkItem { self.checkSMTPConfig(mailAccount: mailAccount,  self.smtpCallback!) }
        dispatchGroup.enter()
        DispatchQueue.global().async(execute: smtpItem)

        future = Future { promise in
            DispatchQueue.global().async {
                let timeoutDelay: DispatchTimeInterval = DispatchTimeInterval.seconds(10)
                let result = self.dispatchGroup.wait(timeout: DispatchTime.now() + timeoutDelay)
                DispatchQueue.main.async {
                    promise(.success(self.onTimeout(timeoutResult: result)))
                }
            }
        }
        return future
    }
    
    func checkIMAPConfig(mailAccount: MailAccount, _ callback: AuthenticationCallback) {
        imapConfigurationSuccessful = false
        let mailSession: MailSession = setupIMAPSession(mailAccount: mailAccount, callback: callback)
        print(mailSession.server.port)
        currentIMAP = mailSession
        if extendedValidation && mailSession.startTestingServerConfig(){
            imapConfigurationSuccessful = true
        } else {
            if !mailSession.hasJsonFile && mailSession.startLongSearchOfServerConfig(hostFromAdr: false){
                imapConfigurationSuccessful = true
            } else {
                if mailSession.startTestingServerConfigFromList() || mailSession.startLongSearchOfServerConfig(hostFromAdr: true){
                    imapConfigurationSuccessful = true
                }
            }
        }
    }
    
    func checkSMTPConfig(mailAccount: MailAccount, _ callback: AuthenticationCallback) {
        smtpConfigurationSuccessful = false
        let mailSession: MailSession = setupSMTPSession(mailAccount: mailAccount, callback: callback)
        currentSMTP = mailSession
        if extendedValidation && mailSession.startTestingServerConfig() {
            smtpConfigurationSuccessful = true
        } else {
            if !mailSession.hasJsonFile && mailSession.startLongSearchOfServerConfig(hostFromAdr: false) {
                smtpConfigurationSuccessful = true
            } else {
                if mailSession.startTestingServerConfigFromList() || mailSession.startLongSearchOfServerConfig(hostFromAdr: true){
                    smtpConfigurationSuccessful = true
                }
            }
        }
    }
    
    private func setupIMAPSession(mailAccount: MailAccount, callback: AuthenticationCallback) -> MailSession {
        let mailSession = MailSession(configSession: SessionType.IMAP, mailAddress: mailAccount.emailAddress, password: mailAccount.password, username: mailAccount.username)
        if extendedValidation {
            let imapConnValue = mailAccount.imapEncryption
            mailSession.setServer(hostname: mailAccount.imapServer, port: UInt32(mailAccount.imapPort), connType: imapConnValue, authType: mailAccount.authType)
        }
        let listenerIMAP = Listener(callback: callback, mailAccount: mailAccount)
        mailSession.addListener(listener: listenerIMAP)
        return mailSession
    }
    
    private func setupSMTPSession(mailAccount: MailAccount, callback: AuthenticationCallback) -> MailSession {
        let mailSession = MailSession(configSession: SessionType.SMTP, mailAddress: mailAccount.emailAddress, password: mailAccount.password, username: mailAccount.username)
        if extendedValidation {
            let smtpConnValue = mailAccount.smtpEncryption
            mailSession.setServer(hostname: mailAccount.smtpServer, port: UInt32(mailAccount.smtpPort), connType: smtpConnValue, authType: mailAccount.authType)
        }
        let listenerSMTP = Listener(callback: callback, mailAccount: mailAccount)
        mailSession.addListener(listener: listenerSMTP)
        return mailSession
    }
    
    func onTimeout(timeoutResult: DispatchTimeoutResult) -> AuthenticationResult {
        imapCallback?.callback = nil
        smtpCallback?.callback = nil
        if timeoutResult == .success {
            if imapConfigurationSuccessful && smtpConfigurationSuccessful {
                return AuthenticationResult.Success
            } else {
                var error = MailServerConnectionError.AuthenticationError
                if let smtp = currentIMAP, let e = MailServerConnectionError.findPrioError(errors: smtp.errors) {
                    error = e
                }
                if let imap = currentIMAP, let e = MailServerConnectionError.findPrioError(errors: imap.errors) {
                    error = e
                }

                return AuthenticationResult.Error(value: error)
            }
        } else {
            return AuthenticationResult.Timeout
        }
    }
    
    func onImapCompleted(imapWorks: Bool, _ login: String, _ password: String) {
        if imapWorks {
            _ = currentIMAP?.storeToUserDefaults()
            imapConfigurationSuccessful = true
        } else {
            imapConfigurationSuccessful = false
        }
        dispatchGroup.leave()
    }
    
    func onSmtpCompleted(smtpWorks: Bool, _ login: String, _ password: String) {
        if smtpWorks {
            _ = currentSMTP?.storeToUserDefaults()
            smtpConfigurationSuccessful = true
        } else {
            smtpConfigurationSuccessful = false
        }
        dispatchGroup.leave()
    }
    
    /**
     A listner class that notifies the AuthenticationCallback's about the result once the server call is done
     */
    class Listener: MailSessionListener {
        let callback: AuthenticationCallback
        let mailAccount: MailAccount
        
        init(callback: AuthenticationCallback, mailAccount: MailAccount) {
            self.callback = callback
            self.mailAccount = mailAccount
        }
        
        func testFinish(result: Bool) {
            callback.onResult(worked: result, login: mailAccount.emailAddress, password: mailAccount.password)
        }
    }
}

class AuthenticationCallback {
    var callback: ((Bool, String, String) -> Void)?

    init(callback: @escaping (Bool, String, String) -> Void) {
        self.callback = callback
    }

    func onResult(worked: Bool, login: String, password: String) {
        if let callback = callback {
            callback(worked, login, password)
        }
    }
}

