//
//  CryptoObject.swift
//  ObjectivePGP
//
//  Created by Oliver Wiese on 25.09.17.
//

import Foundation

enum Rating {
    case Positiv, Neutral, Negativ
}

enum SignatureState: Int16 {
    case NoSignature = 0 // -> no authenticity -> no actions possible
    case NoPublicKey = 1 // -> no authenticity -> Possible actions: Ask sender to send keys, import key, Would be nice to verify mails later.
    case InvalidSignature = -1 // -> ERROR -> context is manipulated (either attack or MTA...)
    case ValidSignature = 2
    
    var loggingTag: String {
        get {
            switch self {
            case .NoSignature:
                return "NoSignature"
            case .NoPublicKey:
                return "NoPublicKey"
            case .InvalidSignature:
                return "InvalidSignature"
            case .ValidSignature:
                return "ValidSignature"
            }
        }
    }
    
    var rating: Rating {
        switch self {
        case .NoSignature, .NoPublicKey:        return .Neutral
        case .ValidSignature:                   return .Positiv
        case .InvalidSignature:                 return .Negativ
        }
    }
    public static let allStates = [NoSignature, NoPublicKey, InvalidSignature, ValidSignature]
}

enum EncryptionState: Int16 {
    case NoEncryption = 0
    case UnableToDecrypt = 1
    case ValidEncryptedWithOldKey = 2
    case ValidedEncryptedWithCurrentKey = 3
    
    var loggingTag: String {
        get {
            switch self {
            case .NoEncryption:
                return "NoEnc"
            case .UnableToDecrypt:
                return "UnableToDec"
            case .ValidedEncryptedWithCurrentKey:
                return "DecWithPrefKey"
            case .ValidEncryptedWithOldKey:
                return "DecWithNoPrefKey"
            }
        }
    }
    
    var rating: Rating {
        switch self {
        case .NoEncryption:                                                 return .Neutral
        case .ValidedEncryptedWithCurrentKey, .ValidEncryptedWithOldKey:    return .Positiv
        case .UnableToDecrypt:                                              return .Neutral
        }
    }
    
    public static let allStates = [NoEncryption, UnableToDecrypt, ValidEncryptedWithOldKey, ValidedEncryptedWithCurrentKey]
}

public enum CryptoScheme {
    case PGP
    case SMIME
    case UNKNOWN

    var description: String {
        switch self {
        case .PGP:
            return "PGP"
        case .SMIME:
            return "SMIME"
        default:
            return ""
        }
    }

    static func find(i: Int) -> CryptoScheme {
        switch i {
        case 0:
            return CryptoScheme.PGP
        case 1:
            return CryptoScheme.SMIME
        default:
            return CryptoScheme.UNKNOWN
        }
    }
    func asInt() -> Int16 {
        switch self {
        case CryptoScheme.PGP:
            return 0
        case CryptoScheme.SMIME:
            return 1
        case CryptoScheme.UNKNOWN:
            return 99
        }
    }
}

public class CryptoObject {
    let chiphertext: Data?
    let plaintext: String?
    let decryptedData: Data?
    let signatureState: SignatureState
    var encryptionState: EncryptionState
    let signKey: String? // TODO Remove
    // TODO Add decryption keys
    let signedKeys: [String]
    let encType: CryptoScheme
    let passcode: String?
    let signedAdrs: [String]

    var decryptedText: String? {
        if let data = decryptedData {
            return String.init(data: data, encoding: .utf8)
        }
        return nil
    }
    
    var chiperString: String? {
        if let data = chiphertext {
            return String.init(data: data, encoding: .utf8)
        }
        return nil
    }

    init(chiphertext: Data?, plaintext: String?, decryptedData: Data?, sigState: SignatureState, encState: EncryptionState, signKey: String?, encType: CryptoScheme, signedAdrs: [String]) {
        self.chiphertext = chiphertext
        self.plaintext = plaintext
        self.decryptedData = decryptedData
        self.signatureState = sigState
        self.encryptionState = encState
        self.signKey = signKey
        self.encType = encType
        self.passcode = nil
        self.signedAdrs = signedAdrs
        if signKey != nil
        {
            self.signedKeys = [signKey!]
        }
        else{
            self.signedKeys = []
        }
    }

    init(chiphertext: Data?, plaintext: String?, decryptedData: Data?, sigState: SignatureState, encState: EncryptionState, signKey: String?, encType: CryptoScheme, signedAdrs: [String], signedKeys: [String]) {
        self.chiphertext = chiphertext
        self.plaintext = plaintext
        self.decryptedData = decryptedData
        self.signatureState = sigState
        self.encryptionState = encState
        self.signKey = signKey
        self.encType = encType
        self.passcode = nil
        self.signedAdrs = signedAdrs
        self.signedKeys = signedKeys
    }
}
