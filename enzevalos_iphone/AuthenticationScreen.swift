//
//  AuthenticationScreen.swift
//  enzevalos_iphone
//
//  Created by SWP Usable Security für Smartphones on 02.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

/**
 View of the Authentication according to MVVP pattern. Observes the AuthenticationViewModel and adjusts the displayed data accordingly
 */
struct AuthenticationScreen: View {
    @State private var login: String = ""
    @State private var password: String = ""
    @State private var username: String = ""
    @State private var imapServer: String = ""
    @State private var imapPort: String = ""
    @State private var smtpServer: String = ""
    @State private var smtpPort: String = ""
    @State private var imapEncryption = 0
    @State private var smtpEncryption = 0
    
    @ObservedObject private var viewModel = AuthenticationViewModel(authenticationModel: AuthenticationModel.instance)
    
    var body: some View {
        ScrollView {
            ZStack {
                Color.white.edgesIgnoringSafeArea(.all)
                
                LoadingView(isShowing: self.$viewModel.showProgressSpinner) {
                    VStack {
                        Text("Please enter Your credentials").padding().foregroundColor(Color.yellow)
                        Text("Login")
                        TextField("Please enter your login", text: self.$login).textFieldStyle(RoundedBorderTextFieldStyle())
                        Text("Password")
                        SecureField("Please enter your password", text: self.$password).textFieldStyle(RoundedBorderTextFieldStyle())
                        
                        HStack {
                            Toggle(isOn: self.$viewModel.isDetailedAuthentication) {
                                Text("Advanced options")
                            }
                            Spacer()
                            Button(action: {self.viewModel.isDetailedAuthentication ?
                                self.viewModel.detailValidation(self.login, self.password, self.username, self.imapServer, self.imapPort, AuthenticationViewModel.encryptionOptions[self.imapEncryption].value, self.smtpServer, self.smtpPort, AuthenticationViewModel.encryptionOptions[self.smtpEncryption].value) :
                                self.viewModel.validate(self.login, self.password)
                            }) {
                                Text("Login")
                            }
                        }
                        
                        if self.viewModel.isDetailedAuthentication {
                            Text("Username")
                            TextField("Please enter your nickname", text: self.$username).textFieldStyle(RoundedBorderTextFieldStyle())
                            
                            HStack {
                                Text("Imap server")
                                TextField("e.g. imap.web.de", text: self.$imapServer)
                            }
                            HStack {
                                Text("Imap port")
                                TextField("e.g. 993", text:self.$imapPort).keyboardType(.numberPad)
                            }
                            Picker(selection: self.$imapEncryption, label: Text("IMAP-Transportencryption")) {
                                ForEach(0..<AuthenticationViewModel.encryptionOptions.count) {
                                    Text(AuthenticationViewModel.encryptionOptions[$0].name)
                                }
                            }
                            HStack {
                                Text("Smtp server")
                                TextField("e.g. smtp.web.de", text: self.$smtpServer)
                            }
                            HStack {
                                Text("Smtp port")
                                TextField("e.g. 587", text: self.$smtpPort).keyboardType(.numberPad)
                            }
                            
                            Picker(selection: self.$smtpEncryption, label: Text("SMTP-Transportencryption")) {
                                ForEach(0..<AuthenticationViewModel.encryptionOptions.count) {
                                    Text(AuthenticationViewModel.encryptionOptions[$0].name)
                                }
                            }
                        }
                        
                        Button(action: { self.viewModel.startGoogleOauth() }) {
                            Text("Google login")
                        }
                        
                    }.padding()
                }
                
                if shouldDisplayErrorMessage() {
                    VStack {
                        Text(self.viewModel.errorMessage!)
                            .foregroundColor(Color.white)
                            .background(Color.red)
                        Spacer()
                    }
                }
            }
        }
    }
    
    func shouldDisplayErrorMessage() -> Bool {
        return (self.viewModel.errorMessage != nil) && (!self.viewModel.errorMessage!.isEmpty)
    }
}
