//
//  Stroke.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 03.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

/**
 Creates a gray stroke
 */
struct Stroke: View{
    var color = Color.secondary
    var lineWidth = CGFloat(0.2)
    var offsetY = CGFloat(0)
    
    var body: some View {
        GeometryReader { geometry in
            Path { path in
                path.move(to: .zero)
                path.addLine(to: CGPoint(x: geometry.size.width, y: 0))
            }
            .strokedPath(StrokeStyle(lineWidth: self.lineWidth))
            .foregroundColor(self.color)
            .offset(y: self.offsetY)
            .frame(width: geometry.size.width, height: self.lineWidth)
        }
    }
}

struct Stroke_Previews: PreviewProvider {
    static var previews: some View {
        Stroke()
    }
}
