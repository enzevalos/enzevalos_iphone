//
//  InboxRefreshModel.swift
//  enzevalos_iphone
//
//  Created by Chris Offner on 29.03.21.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import Foundation

/// Model for Pull to Refresh and updating inbox.
class InboxRefreshModel: ObservableObject {
    @Published var updating = false             // updating inbox
    @Published var refreshStarted = false       // pull to refresh sequence started
    var folderPath: String
    var pullOffsetBaseline: CGFloat?
    var pullOffset: CGFloat = 0
    
    init(folderPath: String) {
        self.folderPath = folderPath
    }
    
    /// Text representation of inbox update state.
    var lastUpdate: String {
        var text = NSLocalizedString("Updating", comment: "updating...")
        
        if !updating {
            let last = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale.current
            dateFormatter.timeStyle = .medium
            let dateString = dateFormatter.string(from: last)
            text = NSLocalizedString("LastUpdate", comment: "") + " " + dateString
        }
        
        return text
    }
    
    /// Retrieves new emails and updates the respective email folder.
    func updateMails() {
        guard !updating else {
            return
        }
        
        LetterboxModel
            .instance
            .mailHandler
            .updateFolder(folderpath: folderPath) { error in
                if error == nil {
                    self.updating = false
                }
                // TODO: Add error message
            }
        updating = true
    }
    
    /// Refreshes inbox and resets pull to refresh states.
    func refresh() {
        DispatchQueue.main.async {
            self.updateMails()
            self.refreshStarted = false
        }
    }
}
