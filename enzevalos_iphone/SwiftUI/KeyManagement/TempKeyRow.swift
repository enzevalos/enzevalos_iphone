//
//  TempKeyRow.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 01.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

struct TempKeyRow: View {
    public var model: KeyManagementModel
    public var key: TempKey
    @State var enterPW = false
    @State var password: String = ""
    @State var description: LocalizedStringKey = "Dialog.Password.Body.First"
    @State var imported = false
    
    var body: some View {
        /**
         Name,  Favorite,  Datum
         */
        HStack {
            VStack (alignment: .leading){
                Text(key.keyID)
                if key.hasPassword && !imported {
                    Text("Import.Password.Required")
                        .font(.caption)
                        .foregroundColor(.gray)
                }
                if imported {
                    Text("Import.Password.Imported")
                        .font(.caption)
                        .foregroundColor(.gray)
                }
                HStack{
                    Text("ContactView.KnownSince")
                        .font(.caption)
                        .foregroundColor(.gray)
                    Text(key.importDate.dateToString)
                        .font(.caption)
                        .foregroundColor(.gray)
                }
            }
            Spacer()
            if !imported {
                Button(action: {
                    if key.hasPassword {
                        enterPW = true
                    } else {
                        let result = model.importKey(key: key, password: password)
                        if result {
                            imported = true
                        }
                    }
                }, label: {
                    Image(systemName: "square.and.arrow.down")
                        .imageScale(.large)
                })
            } else {
                Image(systemName: "checkmark")
            }
            
            
        }
        .padding(.horizontal)
        .sheet(isPresented: $enterPW, onDismiss: {
            enterPW = false
            password = ""
        }, content: {
            VStack{
                Text("Dialog.Password.Title")
                    .font(.title)
                    .padding(.vertical)
                Text(description)
                PasswordView(password: $password)
                Button(action: {
                    let result = model.importKey(key: key, password: password)
                    if !result && key.hasPassword {
                        self.description = "Dialoag.Password.Body.Failed"
                        return
                    }
                    enterPW = false
                    imported = true
                }, label: {
                    Text("Dialog.Password.Button.Ok")
                        .frame(maxWidth: .infinity, minHeight: 20, maxHeight: 40)
                        .overlay(RoundedRectangle(cornerRadius: 20)
                                    .stroke(Color.blue, lineWidth: 2)
                                    )
                        .padding(.vertical, 10)
                        .padding(.horizontal, 20)
                })
                Button(action: {
                    enterPW = false
                }, label: {
                    Text("Dialog.Password.Button.No")
                        .foregroundColor(Color.red)
                        .frame(maxWidth: .infinity, minHeight: 20, maxHeight: 40)
                        .overlay(RoundedRectangle(cornerRadius: 20)
                                    .stroke(Color.red, lineWidth: 2)
                                    )
                        .padding(.vertical, 10)
                        .padding(.horizontal, 20)
                })
                Spacer()
            }
        })
    }
        
}

struct TempKeyRow_Previews: PreviewProvider {
    
    static var pgpKey: Key? {
        let pgp = SwiftPGP()
        let id = pgp.generateKey(adr: "alice@example.com")
        let key = pgp.loadKey(id: id)
        return key
    }
    
    
    static var previews: some View {
        TempKeyRow(model: KeyManagementModel(), key: TempKey(pgpKey: pgpKey!, mailAddresses: ["alice@example.com"]))
    }
}
