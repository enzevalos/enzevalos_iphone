 //
 //  MailHandler.swift
 //  mail_dynamic_icon_001
 //
 //  Created by jakobsbode on 22.08.16.
 //  //  This program is free software: you can redistribute it and/or modify
 //  it under the terms of the GNU General Public License as published by
 //  the Free Software Foundation, either version 3 of the License, or
 //  (at your option) any later version.
 //
 //  This program is distributed in the hope that it will be useful,
 //  but WITHOUT ANY WARRANTY; without even the implied warranty of
 //  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 //  GNU General Public License for more details.
 //
 //  You should have received a copy of the GNU General Public License
 //  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 //
 
 import Foundation
 import Contacts
 // FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
 // Consider refactoring the code to use the non-optional operators.
 fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
 }
 
 // FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
 // Consider refactoring the code to use the non-optional operators.
 fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
 }
 
 enum SendViewMailSecurityState {
    case letter
    case postcard
 }
 
 enum FolderError: Error {
    case WrongUidValidity
 }
 
 class MailHandler {
    private static let MAXMAILS = 25
    private static let extraHeaders = Autocrypt.EXTRAHEADERS
    
    private let dataProvider = PersistentDataProvider.dataProvider
    
    static var INBOX: String {
        return UserManager.backendInboxFolderPath
    }
    static var SENT: String {
        return UserManager.backendSentFolderPath
    }
    private var IMAPSes: MCOIMAPSession?
    var IMAPSession: MCOIMAPSession? {
        if IMAPSes == nil {
            IMAPSes = MailHandler.createIMAPSession()
        }
        return IMAPSes
    }
    var IMAPIdleSession: MCOIMAPSession?
    var IMAPIdleSupported: Bool?
    
    private var smtpSession: MCOSMTPSession? {
        if let session = try? MailSession.init(loadUserDefaults: .SMTP) {
            if let smtpsession = session.defaultSMTPSession {
                return smtpsession
            }
        }
        return nil
    }
    
    var shouldTryRefreshOAUTH: Bool {
        if let imapAuthType = UserManager.loadUserValue(.imapConnectionType) as? Int,
           let smtpAuthType = UserManager.loadUserValue(.smtpAuthType) as? Int{
            return (imapAuthType == MCOAuthType.xoAuth2.rawValue || smtpAuthType == MCOAuthType.xoAuth2.rawValue) &&
                !(EmailHelper.singleton().authorization?.authState.isTokenFresh() ?? false)
        }
        return false
    }
    
    private static func createIMAPSession() -> MCOIMAPSession? {
        if let session = try? MailSession.init(loadUserDefaults: SessionType.IMAP) {
            return session.defaultIMAPSession
        }
        return nil
    }
    
    func sendSecretKey(keyID: String, key: String, callback: @escaping (MailServerConnectionError?) -> Void) {
        let mail = OutgoingMail.createSecretKeyExportMail(keyID: keyID, keyData: key)
        sendSMTP(mail: mail, callback: callback)
    }
    
    func sendSMTP(mail: OutgoingMail, callback: ((MailServerConnectionError?) -> Void)?) {
        guard LetterboxModel.currentReachabilityStatus != .notReachable else {
            if let call = callback {
                call(MailServerConnectionError.NoInternetconnection)
            }
            return
        }
        
        guard let session = smtpSession else {
            if let call = callback {
                call(MailServerConnectionError.NoData)
            }
            return
        }
        var sent = false
        if mail.encReceivers.count > 0 {
            let data = mail.pgpData
            if let sendOperation = session.sendOperation(with: data, from: mail.sender, recipients: mail.encReceivers){
                sendOperation.start({[unowned self] error in
                    guard error == nil else {
                        let connerror = MailServerConnectionError.findErrorCode(error: error!)
                        self.errorhandling(error: connerror, originalCall: {self.sendSMTP(mail: mail, callback: callback)}, completionCallback: callback)
                        return
                    }
                    if let callback = callback{
                        callback(nil)
                    }
                })
                sent = true
            }
        }
        if mail.plainReceivers.count > 0 {
            let sender = mail.sender
            if let sendOperation = session.sendOperation(with: mail.plainData, from: sender, recipients: mail.plainReceivers) {
                sendOperation.start({[unowned self]error in
                    guard error == nil else {
                        let connerror = MailServerConnectionError.findErrorCode(error: error!)
                        self.errorhandling(error: connerror, originalCall: {self.sendSMTP(mail: mail, callback: callback)}, completionCallback: callback)
                        return
                    }
                    if let callback = callback{
                        callback(nil)
                    }
                })
                sent = true
            }
        }
        if sent {
            _ = mail.logMail()
            var copyFolder = UserManager.backendSentFolderPath
            if mail.loggingMail {
                copyFolder = UserManager.loadUserValue(.loggingFolderPath) as! String
            }
            //TODO Maybe we should consider a connection error.
            self.storeIMAP(mail: mail, folder: copyFolder, callback: nil)
        }
        else if let callback = callback{
            callback(nil)
        }
    }
    
    // TODO SEND MAIL... later...
    func storeIMAP(mail: OutgoingMail, folder: String, callback: ((MailServerConnectionError?) -> Void)?) {
        // 1. Test if folder exists
        let existFolderController = dataProvider.generateFetchedFolderResultsController(folderpath: folder, moc: nil)
        if existFolderController.fetchedObjects != nil {
            // 2. Store Mail in test
            // We can always store encrypted data on the imap server because the user has a key pair and it is users imap account.
            let op = self.IMAPSession?.appendMessageOperation(withFolder: folder, messageData: mail.pgpData, flags: MCOMessageFlag.mdnSent)
            op?.start({[unowned self] error, _ in
                guard error == nil else {
                    let connerror = MailServerConnectionError.findErrorCode(error: error!)
                    self.errorhandling(error: connerror, originalCall: {self.storeIMAP(mail: mail, folder: folder, callback: callback)}, completionCallback: callback)
                    self.IMAPSes = nil
                    return
                }
                if let callback = callback {
                    callback(nil)
                }
            })
        }
        else {
            if let op = IMAPSession?.createFolderOperation(folder) {
                op.start({[unowned self] error in
                    guard error == nil else {
                        let connerror = MailServerConnectionError.findErrorCode(error: error!)
                        self.errorhandling(error: connerror, originalCall: {self.storeIMAP(mail: mail, folder: folder, callback: callback)}, completionCallback: callback)
                        self.IMAPSes = nil
                        return
                    }
                    // Create folder on local
                    let folderProperty = FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: folder, flags: 0)
                    dataProvider.importNewData(from: [folderProperty], completionHandler: { error in
                        guard error == nil else {
                            // TODO: Error handling
                            print("Error while creating a folder: \(String(describing: error))")
                            return
                        }
                        self.storeIMAP(mail: mail, folder: folder, callback: callback)
                    })
                })
            }
        }
    }
    
    func createDraft(_ toAddresses: [String], ccAddresses: [String], bccAddresses: [String], subject: String, message: String, callback: @escaping (MailServerConnectionError?) -> Void) {
        let mail = OutgoingMail.createDraft(toAddresses: toAddresses, ccAddresses: ccAddresses, bccAddresses: bccAddresses, subject: subject, textContent: message, htmlContent: nil)
        let folder = UserManager.backendDraftFolderPath
        self.storeIMAP(mail: mail, folder: folder, callback: callback)
        _ = mail.logMail()
    }
    
    func startIMAPIdleIfSupported() throws {
        if let supported = IMAPIdleSupported {
            let id = UInt32(0) // TODO FIX!!!! UInt32(DataHandler.handler.findFolder(with: MailHandler.INBOX).maxID)
            if supported && IMAPIdleSession == nil, let session = MailHandler.createIMAPSession(), let op = session.idleOperation(withFolder: MailHandler.INBOX, lastKnownUID: id){
                IMAPIdleSession = session
                op.start({[unowned self] error in
                    guard error == nil else {
                        let connerror = MailServerConnectionError.findErrorCode(error: error!)
                        self.errorhandling(error: connerror, originalCall: {do {
                            try self.startIMAPIdleIfSupported()
                        } catch{}
                        }, completionCallback: nil)
                        return
                    }
                    self.IMAPIdleSession = nil
                    updateFolder(folderpath: MailHandler.INBOX, completionCallback: { error in
                                    guard error == nil else {
                                        self.errorhandling(error: error, originalCall: { do {
                                            try self.startIMAPIdleIfSupported()
                                        } catch{}}, completionCallback: nil)
                                        return
                                    }
                                    do {
                                        try self.startIMAPIdleIfSupported()
                                    } catch{} })
                })
            }
        } else {
            try  checkIdleSupport()
        }
    }
    
    private func checkIdleSupport() throws {
        if let session = MailHandler.createIMAPSession(), let op = session.capabilityOperation() {
            op.start({[unowned self] (error, capabilities) in
                guard error == nil else {
                    let connerror = MailServerConnectionError.findErrorCode(error: error!)
                    self.errorhandling(error: connerror, originalCall: {
                                        do {
                                            try self.checkIdleSupport()
                                        }catch{}}, completionCallback: nil)
                    return
                }
                if let c = capabilities {
                    self.IMAPIdleSupported = c.contains(UInt64(MCOIMAPCapability.idle.rawValue))
                    do {
                        try self.startIMAPIdleIfSupported()
                    } catch{}
                }
            })
        }
    }
    
    func setFlag(_ uid: UInt64, flags: MCOMessageFlag, folder: String = INBOX) {
        changeFlag(uid: uid, flags: flags, kind: MCOIMAPStoreFlagsRequestKind.set, folderName: folder)
    }
    
    func removeFlag(_ uid: UInt64, flags: MCOMessageFlag, folder: String = INBOX) {
        changeFlag(uid: uid, flags: flags, kind: MCOIMAPStoreFlagsRequestKind.remove, folderName: folder)
    }
    
    private func changeFlag(uid: UInt64, flags: MCOMessageFlag, kind: MCOIMAPStoreFlagsRequestKind, folderName: String){
        guard IMAPSession != nil else {
            return
        }
        if let f = dataProvider.generateFetchedFolderResultsController(folderpath: folderName, moc: nil).fetchedObjects?.first {
            let folderstatus = IMAPSession?.folderStatusOperation(folderName)
            folderstatus?.start {[unowned self] (error, status) -> Void in
                guard error == nil else {
                    let conerror = MailServerConnectionError.findErrorCode(error: error!)
                    self.errorhandling(error: conerror, originalCall: {self.changeFlag(uid: uid, flags: flags, kind: kind, folderName: folderName)}, completionCallback: nil)
                    return
                }
                if let status = status {
                    let uidValidity = status.uidValidity
                    let folderUID = UInt32(truncating: f.uidValidity ?? 0) // TODO IS THIS TRUE????
                    if uidValidity == folderUID {
                        let op = self.IMAPSession?.storeFlagsOperation(withFolder: folderName, uids: MCOIndexSet.init(index: uid), kind: kind, flags: flags)
                        op?.start{[unowned self] error -> Void in
                            guard error == nil else {
                                let conerror = MailServerConnectionError.findErrorCode(error: error!)
                                self.errorhandling(error: conerror, originalCall: {self.changeFlag(uid: uid, flags: flags, kind: kind, folderName: folderName)}, completionCallback: nil)
                                return
                            }
                            if flags.contains(MCOMessageFlag.deleted) && kind == MCOIMAPStoreFlagsRequestKind.add {
                                let operation = self.IMAPSession?.expungeOperation(folderName)
                                operation?.start({[unowned self] err in
                                    guard err == nil else {
                                        let conerror = MailServerConnectionError.findErrorCode(error: error!)
                                        self.errorhandling(error: conerror, originalCall: {self.changeFlag(uid: uid, flags: flags, kind: kind, folderName: folderName)}, completionCallback: nil)
                                        return
                                    }
                                    // TODO: DELETE MAIL DataHandler.handler.deleteMail(with: uid)
                                })
                            }
                        }
                    }
                }
            }
        }
        // TODO WHAT IF Folder does not exists?
    }
    
    func loadMailsForInbox(completionCallback: @escaping ((_ error: MailServerConnectionError?) -> ())) {
        guard IMAPSession != nil else {
            completionCallback(MailServerConnectionError.NoData)
            return
        }
        if let folder = dataProvider.generateFetchedFolderResultsController(folderpath: MailHandler.INBOX, moc: nil).fetchedObjects?.first {
            let folderstatus = IMAPSession?.folderStatusOperation(folder.path)
            folderstatus?.start {[unowned self] (error, status) -> Void in
                guard error == nil else {
                    let connerror = MailServerConnectionError.findErrorCode(error: error!)
                    self.errorhandling(error: connerror, originalCall: {self.loadMailsForInbox(completionCallback: completionCallback)}, completionCallback: completionCallback)
                    return
                }
                if let status = status {
                    let uidValidity = status.uidValidity
                    folder.uidValidity = NSDecimalNumber(value: uidValidity)
                    self.olderMails(folder: folder, completionCallback: completionCallback)
                }
            }
        } else {
            initInbox(completionCallback: completionCallback)
        }
    }
    
    func backgroundUpdate(completionCallback: @escaping (_ newMails: UInt32, _ completionHandler: @escaping (UIBackgroundFetchResult) -> Void)  -> (), performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void){
        guard IMAPSession != nil else {
            completionCallback(0, completionHandler)
            return
        }
        if let folder = dataProvider.generateFetchedFolderResultsController(folderpath: MailHandler.INBOX, moc: nil).fetchedObjects?.first {
            let folderstatus = IMAPSession?.folderStatusOperation(folder.path)
            // Work only in background thread....
            var backgroundTaskID: Int?
            DispatchQueue.global(qos: .background).async {
                backgroundTaskID = UIApplication.shared.beginBackgroundTask (withName: "Finish Network Tasks"){
                    UIApplication.shared.endBackgroundTask(convertToUIBackgroundTaskIdentifier(backgroundTaskID!))
                    backgroundTaskID = convertFromUIBackgroundTaskIdentifier(UIBackgroundTaskIdentifier.invalid)
                }.rawValue
                folderstatus?.start{(error, status) -> Void in
                    guard error == nil else {
                        UIApplication.shared.endBackgroundTask(convertToUIBackgroundTaskIdentifier(backgroundTaskID!))
                        backgroundTaskID = convertFromUIBackgroundTaskIdentifier(UIBackgroundTaskIdentifier.invalid)
                        completionCallback(0, completionHandler)
                        return
                    }
                    if let status = status {
                        let uidValidity = status.uidValidity
                        let uid = status.uidNext
                        var newMails: UInt32 = 0
                        let max = UInt32(truncating: folder.maxUID ?? 0) // TODO CHECK WHAT SHOULD BE MAX?
                        let folderUidValidity = UInt32(truncating: folder.uidValidity ?? 0) // TODO CHECK!
                        var diff = uid.subtractingReportingOverflow(max)
                        diff = diff.partialValue.subtractingReportingOverflow(1)
                        if diff.overflow {
                            newMails = 0
                        }
                        else {
                            newMails = diff.partialValue
                        }
                        if (uidValidity != folderUidValidity || max < uid - 1) {
                            UIApplication.shared.endBackgroundTask(convertToUIBackgroundTaskIdentifier(backgroundTaskID!))
                            backgroundTaskID = convertFromUIBackgroundTaskIdentifier(UIBackgroundTaskIdentifier.invalid)
                            completionCallback(newMails, completionHandler)
                        }
                        else {
                            UIApplication.shared.endBackgroundTask(convertToUIBackgroundTaskIdentifier(backgroundTaskID!))
                            backgroundTaskID = convertFromUIBackgroundTaskIdentifier(UIBackgroundTaskIdentifier.invalid)
                            completionCallback(0, completionHandler)
                        }
                    }
                }
            }
        }
    }
    
    private func loadMessagesFromServer(_ uids: MCOIndexSet, folderPath: String, maxLoad: Int = MailHandler.MAXMAILS, completionCallback: @escaping ((_ error: MailServerConnectionError?) -> ())) {
        guard let session = IMAPSession else {
            completionCallback(MailServerConnectionError.NoData)
            return
        }
        guard uids.count() > 0 else {
            completionCallback(nil)
            return
        }
        // First fetch -> get flags
        let requestKind = MCOIMAPMessagesRequestKind(rawValue: MCOIMAPMessagesRequestKind.headers.rawValue | MCOIMAPMessagesRequestKind.flags.rawValue)
        let fetchOperation: MCOIMAPFetchMessagesOperation = session.fetchMessagesOperation(withFolder: folderPath, requestKind: requestKind, uids: uids)
        fetchOperation.extraHeaders = MailHandler.extraHeaders
        
        fetchOperation.start{[unowned self] (err, msg, vanished) -> Void in
            guard err == nil else {
                let connerror = MailServerConnectionError.findErrorCode(error: err!)
                self.errorhandling(error: connerror, originalCall: {self.loadMessagesFromServer(uids, folderPath: folderPath, maxLoad: maxLoad, completionCallback: completionCallback)}, completionCallback: completionCallback)
                return
            }
            var calledMails = 0
            if let msgs = msg {
                let dispatchGroup = DispatchGroup()
                for message in msgs.reversed() {
                    dispatchGroup.enter()
                    let op = self.IMAPSession?.fetchMessageOperation(withFolder: folderPath, uid: message.uid)
                    op?.start {[unowned self] err, data in
                        guard err == nil else {
                            let connerror = MailServerConnectionError.findErrorCode(error: err!)
                            self.errorhandling(error: connerror, originalCall: {self.loadMessagesFromServer(uids, folderPath: folderPath, maxLoad: maxLoad, completionCallback: completionCallback)}, completionCallback: completionCallback)
                            return
                        }
                        if let parser = data {
                            let id = UInt64(message.uid)
                            let incomingMail = IncomingMail(rawData: parser, uID: id, folderPath: folderPath, flags: message.flags)
                            dataProvider.importNewData(from: [incomingMail.export()], completionHandler: {(error) in
                            })
                        }
                        dispatchGroup.leave()
                    }
                    calledMails += 1
                    if calledMails > maxLoad {
                        break
                    }
                }
                dispatchGroup.notify(queue: DispatchQueue.main) {
                    self.IMAPSession?.disconnectOperation().start({[unowned self] err2 in
                        guard err2 == nil else {
                            let connerror = MailServerConnectionError.findErrorCode(error: err2!)
                            self.errorhandling(error: connerror, originalCall: {self.loadMessagesFromServer(uids, folderPath: folderPath, maxLoad: maxLoad, completionCallback: completionCallback)}, completionCallback: completionCallback)
                            return
                        }
                    })
                    completionCallback(nil)
                }
            }
        }
    }
 
    
    private func createFolders(of paths: [String], completionHandler: @escaping(_ error: Error?) -> Void) {
        guard let session = IMAPSession else {
            completionHandler(MailServerConnectionError.ConnectionError)
            return
        }
        session.fetchAllFoldersOperation()?.start({error, folders in
            guard error != nil else {
                completionHandler(error)
                return
            }
            var myError: Error? = nil
            var createFolders = [String]()
            if let names = folders?.map({$0.path}) {
                for path in paths {
                    if !names.contains(path) {
                        createFolders.append(path)
                    }
                }
            }
            let myGroup = DispatchGroup()
            let queue = DispatchQueue(label: "com.letterbox.mailhandler")
            queue.async {
                for path in createFolders {
                    myGroup.enter()
                    session.createFolderOperation(path)?.start({error in
                        myError = error
                        myGroup.leave()
                    })
                }
                myGroup.notify(queue: queue, execute: {
                    completionHandler(myError)
                })
            }
        })
    }
    
    private func moveOnServer(uidValidity: UInt32, mails: [UInt64], fromPath: String, toPath: String, completionHandler: @escaping(_ error: Error?) -> Void) {
        guard let session = IMAPSession else {
            completionHandler(MailServerConnectionError.ConnectionError)
            return
        }
        // 1. Check uid valilidity
        session.folderInfoOperation(fromPath)?.start({error, infos  in
            guard error == nil, let infos = infos else {
                completionHandler(error)
                return
            }
            guard infos.uidValidity == uidValidity else {
                completionHandler(FolderError.WrongUidValidity)
                return
            }
            // 2. Move mails if uid is valid
            let uidSet = MCOIndexSet()
            mails.forEach({uidSet.add($0)})
            session.moveMessagesOperation(withFolder: fromPath, uids: uidSet, destFolder: toPath)?.start({(error, _) in
                guard error == nil else {
                    session.copyMessagesOperation(withFolder: fromPath, uids: uidSet, destFolder: toPath)?.start({(error, _) in
                        completionHandler(error)
                    })
                    return
                }
                completionHandler(nil)
                return
            })
        })
    }
    
    func move(mails: [UInt64], fromPath: String, toPath: String)  {
        // 1. Check if we have to create a folder.
        createFolders(of: [fromPath, toPath], completionHandler: {error in
            guard error == nil else {
                return
            }
            // 2. Move mail on server
            guard let fromFolder = self.dataProvider.generateFetchedFolderResultsController(folderpath: fromPath, moc: nil).fetchedObjects?.first else {
                return
            }
            if let uidValidity = fromFolder.uidValidity as? UInt32 {
                self.moveOnServer(uidValidity: uidValidity, mails: mails, fromPath: fromPath, toPath: toPath, completionHandler: {error in
                    // 3. Move mail in core data
                    self.dataProvider.moveMails(with: mails, from: fromPath, to: toPath)
                })
                    
            }
        })
    }
    
    
    func allFolders(_ completion: @escaping (Error?) -> Void) {
        guard IMAPSession != nil else {
            completion(MailServerConnectionError.NoData)
            return
        }
        let op = IMAPSession?.fetchAllFoldersOperation()
        op?.start({(error, array) in
            guard error == nil else {
                completion(error)
                return
            }
            // TODO HOW TO handle subfolders etc? Add them here and  store them in persitentant storage...
            if let folders = array {
                var properities = [FolderProperties]()
                for folder in folders {
                    let property = FolderProperties(delimiter: String(Character(UnicodeScalar(UInt8(folder.delimiter)))), uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: folder.path, parent: nil, children: nil, flags: Int16(folder.flags.rawValue))
                    properities.append(property)
                }
                self.dataProvider.importNewData(from: properities, completionHandler: completion)
            }
        })
    }
    
    private func initFolder(folderPath: String, completionCallback: @escaping ((MailServerConnectionError?) -> ())) {
        guard let session = IMAPSession else {
            self.errorhandling(error: .ConnectionError, originalCall: {self.initFolder(folderPath: folderPath, completionCallback: completionCallback)}, completionCallback: completionCallback)
            return
        }
        session.folderStatusOperation(folderPath)?.start({error, status in
            if let error = error {
                let conerror = MailServerConnectionError.findErrorCode(error: error)
                self.errorhandling(error: conerror, originalCall: {self.initFolder(folderPath: folderPath, completionCallback: completionCallback)}, completionCallback: completionCallback)
                return
            }
            guard let status = status else {
                completionCallback(nil)
                return
            }
        
            let requestKind = MCOIMAPMessagesRequestKind(rawValue: MCOIMAPMessagesRequestKind.headers.rawValue)
            let min =  max(status.uidNext - 1000, 1)
            let uids = MCOIndexSet(range: MCORangeMake(UInt64(min), UInt64(status.uidNext)))
            let toFetchIDs = MCOIndexSet()
            
            let fetchOperation: MCOIMAPFetchMessagesOperation = session.fetchMessagesOperation(withFolder: folderPath, requestKind: requestKind, uids: uids)
            fetchOperation.start {[unowned self] (err, msg, vanished) -> Void in
                guard err == nil else {
                    let conerror = MailServerConnectionError.findErrorCode(error: err!)
                    self.errorhandling(error: conerror, originalCall: {self.initFolder(folderPath: folderPath, completionCallback: completionCallback)}, completionCallback: completionCallback)
                    return
                }
                if let msgs = msg {
                    FolderRecord.lastDate(folder: folderPath, date: Date())
                    for message in msgs {
                        toFetchIDs.add(UInt64(message.uid))
                    }
                    self.loadMessagesFromServer(toFetchIDs, folderPath: folderPath, maxLoad: 50, completionCallback: completionCallback)
                } else {
                    completionCallback(nil)
                }
            }
        })
    }
    
    private func initInbox(completionCallback: @escaping ((MailServerConnectionError?) -> ())) {
        initFolder(folderPath: MailHandler.INBOX, completionCallback: completionCallback)
    }
    
    func updateFolder(folderpath: String, completionCallback: @escaping ((MailServerConnectionError?) -> ())) {
        guard IMAPSession != nil else {
            completionCallback(MailServerConnectionError.NoData)
            return
        }
        if let folder = dataProvider.generateFetchedFolderResultsController(folderpath: folderpath).fetchedObjects?.first {
            if folder.mailsInFolder?.count ?? 0 > 0 {
                self.loadMailsByNum(folder: folder, completionCallback: completionCallback, multipleMails: false)
            } else {
                self.initFolder(folderPath: folderpath, completionCallback: completionCallback)
            }
        } else {
            self.initFolder(folderPath: folderpath, completionCallback: completionCallback)
        }
    }
    
    private func calculateIndicies(indicies: MCOIndexSet, folder: FolderRecord, multipleMails: Bool) -> MCOIndexSet {
        var factor = 1
        if multipleMails {
            factor = 3
        }
        let knownIds = folder.uids
        let requestIds = indicies
        requestIds.remove(knownIds)
        let max = UInt32(factor * MailHandler.MAXMAILS)
        var dif = requestIds.count()
        if requestIds.count() > max {
            dif = requestIds.count() - max
        }
        var range = MCORange(location: 0, length: UInt64(dif-1))
        if dif > MailHandler.MAXMAILS {
            requestIds.remove(range)
        }
        var lastMinElem = dif
        while requestIds.count() < factor * MailHandler.MAXMAILS && lastMinElem > 0 {
            if lastMinElem > UInt32(MailHandler.MAXMAILS) {
                lastMinElem = lastMinElem - UInt32(MailHandler.MAXMAILS)
            }
            else {
                break
            }
            range = MCORange(location: UInt64(lastMinElem), length: UInt64(MailHandler.MAXMAILS))
            requestIds.add(range)
            requestIds.remove(knownIds)
            if UInt64(truncating: folder.minUID ?? 0) > UInt64(lastMinElem) {
                folder.minUID = NSDecimalNumber(value: UInt64(lastMinElem))
            }
        }
        if lastMinElem < MailHandler.MAXMAILS {
            range = MCORange(location: UInt64(1), length: UInt64(MailHandler.MAXMAILS))
            requestIds.add(range)
            folder.minUID = 1
        }
        if multipleMails {
            let min = UInt64(truncating: folder.minUID ?? 0)
            var (start, overflow) = min.subtractingReportingOverflow(UInt64(MailHandler.MAXMAILS))
            if overflow && min > 1 {
                // 1 < folder.min < MailHandler.MAXMAILs -> start with uid = 1
                start = 1
                overflow = false
            }
            if !overflow {
                range = MCORange(location: start, length: UInt64(MailHandler.MAXMAILS))
                requestIds.add(range)
                folder.minUID = NSDecimalNumber(value: start)
            }
        }
        return requestIds
    }
    
    private func loadMailsByNum(folder: FolderRecord, completionCallback: @escaping ((MailServerConnectionError?) -> ()), multipleMails: Bool) {
        guard let session = IMAPSession else {
            completionCallback(MailServerConnectionError.NoData)
            return
        }
        
        if  let path = folder.path, let statusOP = session.folderStatusOperation(path) {
            statusOP.start{[unowned self](error, status) -> Void in
                guard error == nil else {
                    let conerror = MailServerConnectionError.findErrorCode(error: error!)
                    self.errorhandling(error: conerror, originalCall: {self.loadMailsByNum(folder: folder, completionCallback: completionCallback, multipleMails: multipleMails)}, completionCallback: completionCallback)
                    self.IMAPSes = nil
                    return
                }
                if let status = status, let ids = MCOIndexSet(range: MCORange(location: 0, length: UInt64(status.uidNext))) {
                    let newIds = self.calculateIndicies(indicies: ids, folder: folder, multipleMails: multipleMails)
                    self.loadMessagesFromServer(newIds, folderPath: path, completionCallback: completionCallback)
                }
            }
        }
    }
    
    private func olderMails(folder: FolderRecord, completionCallback: @escaping ((MailServerConnectionError?) -> ())) {
        guard IMAPSession != nil else {
            completionCallback(MailServerConnectionError.NoData)
            return
        }
        if let mails = folder.mailsInFolder, mails.count > 0 {
            loadMailsByNum(folder: folder, completionCallback: completionCallback, multipleMails: true)
        } else if let path = folder.path {
            initFolder(folderPath: path, completionCallback: completionCallback)
        }
    }
    private func loadMailsSinceDate(folder: FolderRecord, since: Date, maxLoad: Int = MailHandler.MAXMAILS, completionCallback: @escaping ((MailServerConnectionError?) -> ())) {
        guard IMAPSession != nil else {
            completionCallback(MailServerConnectionError.NoData)
            return
        }
        let folderPath = folder.path
        let searchExp = MCOIMAPSearchExpression.search(since: since)
        let searchOperation = self.IMAPSession?.searchExpressionOperation(withFolder: folderPath, expression: searchExp)
        
        searchOperation?.start{[unowned self] (err, uids) -> Void in
            guard err == nil else {
                let conerror = MailServerConnectionError.findErrorCode(error: err!)
                self.errorhandling(error: conerror, originalCall: {self.loadMailsSinceDate(folder: folder, since: since, completionCallback: completionCallback)}, completionCallback: completionCallback)
                self.IMAPSes = nil
                return
            }
            if let ids = uids {
                folder.lastUpdate = Date()
                ids.remove(folder.uids)
                if let folderPath = folder.path {
                    self.loadMessagesFromServer(ids, folderPath: folderPath, completionCallback: completionCallback)
                }
            } else {
                completionCallback(nil)
            }
        }
        
    }
    
    func retryWithRefreshedOAuth(completion: @escaping () -> ()) {
        guard shouldTryRefreshOAUTH else {
            return
        }
        EmailHelper.singleton().checkIfAuthorizationIsValid({ authorized in
            if authorized {
                self.IMAPSes = nil
            }
            completion()
        })
    }
    
    private func errorhandling(error: MailServerConnectionError?, originalCall: @escaping () -> (), completionCallback: (((MailServerConnectionError?) -> ()))?){
        // maybe refreshing oauth?
        if self.shouldTryRefreshOAUTH {
            self.retryWithRefreshedOAuth {
                originalCall()
            }
            return
        }
        if completionCallback != nil {
            completionCallback!(error)
        }
    }
 }
 
 // Helper function inserted by Swift 4.2 migrator.
 fileprivate func convertToUIBackgroundTaskIdentifier(_ input: Int) -> UIBackgroundTaskIdentifier {
    return UIBackgroundTaskIdentifier(rawValue: input)
 }
 
 // Helper function inserted by Swift 4.2 migrator.
 fileprivate func convertFromUIBackgroundTaskIdentifier(_ input: UIBackgroundTaskIdentifier) -> Int {
    return input.rawValue
 }
