//
//  GeometryGetter.swift
//  enzevalos_iphone
//
//  Created by melicoa97 on 05.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct GeometryGetter: View {
    @Binding var rect: CGRect

    var body: some View {
        GeometryReader { geometry in
            Group { () -> AnyView in
                DispatchQueue.main.async {
                    self.rect = geometry.frame(in: .global)
                }

                return AnyView(Color.clear)
            }
        }
    }
}
