//
//  PermissionRequestView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 16.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct PermissionRequestView: View {
    @ObservedObject private var model = PermissionModel()
    
    // contact access request background text
    var contactPermissionTitle: LocalizedStringKey = "Permission.AccessContacts.Title"
    var contactPermissionDescription: LocalizedStringKey = "Permission.AccessContacts.Description"
    
    // notification access request background text
    var notificationPermissionTitle: LocalizedStringKey = "Permission.Notification.Title"
    var notificationPermissionDescription: LocalizedStringKey = "Permission.Notification.Description"
    
    // contact access denied background text
    var contactPermissionDeniedTitle: LocalizedStringKey = "Permission.contactDenied.Title"
    var contactPermissionDeniedDescription: LocalizedStringKey = "Permission.contactDenied.Description"
    
    // icons
    var contactIcon = Image(systemName: "person.crop.circle.fill")
    var notificationIcon = Image(systemName: "bell.fill")
    var contactDeniedIcon = Image(systemName: "person.fill.xmark")

    var body: some View {
        // view progression based on state of permission model
        switch model.currentState {
        
        case .CONTACTPERMISSION : VStack{
            Text(contactIcon).font(.largeTitle)
            Text(contactPermissionTitle).font(.largeTitle).padding(.all)
            Text(contactPermissionDescription).font(.body).multilineTextAlignment(.center).padding(.all)
            Spacer()
        }
        
        case .NOTIFICATIONPERMISSION : VStack{
            Text(notificationIcon).font(.largeTitle)
            Text(notificationPermissionTitle).font(.largeTitle).padding(.all)
            Text(notificationPermissionDescription).font(.body).multilineTextAlignment(.center).padding(.all)
            Spacer()
            }
        
        case .CONTACTDENIED : VStack{
            Text(contactDeniedIcon).font(.largeTitle)
            Text(contactPermissionDeniedTitle).font(.largeTitle).padding(.all)
            Text(contactPermissionDeniedDescription).font(.body).multilineTextAlignment(.center).padding(.all)
            Spacer()
            }
        }
    }
    

struct PermissionRequestView_Previews: PreviewProvider {
    static var previews: some View {
        PermissionRequestView()
    }
}
}
