//
//  TempKey.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 09.04.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation

class TempKey: Hashable {
    static func == (lhs: TempKey, rhs: TempKey) -> Bool {
        return lhs.keyID == rhs.keyID && lhs.type == rhs.type
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(keyID)
        hasher.combine(type)
    }
    
    
    let type: CryptoScheme
    let keyID: String
    let isSecret: Bool
    
    var mailAddresses: [String]
    var name: String
    
    let importDate: Date
    var password: String?
    let hasPassword: Bool
    var pgpKey: Key?
    var smimeCert: Certificate?
    
    init(pgpKey: Key, mailAddresses: [String], importDate: Date = Date()) {
        self.type = .PGP
        self.mailAddresses = mailAddresses
        self.importDate = importDate

        self.pgpKey = pgpKey
        self.keyID = pgpKey.keyID.longIdentifier
        self.hasPassword = pgpKey.isEncryptedWithPassword
        self.isSecret = pgpKey.isSecret
        if let sk = pgpKey.secretKey {
            name = sk.primaryUser?.userID ?? ""
        }
        else if let pk = pgpKey.publicKey {
            name =  pk.primaryUser?.userID ?? ""
        } else {
            name = ""
        }
    }
    
    init(smimeCert: Certificate, mailAddresses: [String], importDate: Date = Date()) {
        self.mailAddresses = mailAddresses
        self.importDate = importDate
        self.type = .SMIME
        
        self.smimeCert = smimeCert
        self.keyID = smimeCert.fingerPrint
        self .name = smimeCert.eMails?.first ?? mailAddresses.first ?? ""
        if let emails = smimeCert.eMails {
            self.mailAddresses.append(contentsOf: emails)
            self.mailAddresses = Array(Set(self.mailAddresses))
        }
        self.isSecret = smimeCert.hasSecretKey
        self.hasPassword = self.isSecret // TODO: Is it always true?
        
    }
    
    func testPassword(guess: String) -> Bool{
        guard hasPassword else {
            return true
        }
        if let pgpKey = pgpKey, let key = try? pgpKey.decrypted(withPassphrase:  guess) {
            self.pgpKey = key
            password = guess
            return true
        }
        if let cert = smimeCert {
            let res = getPKeyFromEncryptedPem(pem: cert.pem, passwd: guess)
            if res.1 == nil,  let decryptedPEM = res.0 {
                if let cert = try? Certificate(pem: decryptedPEM) {
                    smimeCert = cert // TODO: Do we like to store the decrypted PEM or the original one? (Discussion)
                    return true
                }
            }
            return false
        }
        return false
    }
    
    func isNew() -> Bool {
        guard let res = PersistentDataProvider.dataProvider.generateFetchedPublicKeysResultController(keyIds: [keyID])?.fetchedObjects else {
            return true
        }
        if res.count  == 0 {
            return true
        }
        for key in res {
            if key.type == self.type {
                return false
            }
        }
        return true
    }
    
    func save() -> Bool {
        let keyIDs = SwiftPGP().store(tempKeys: [self])
        let provider = PersistentDataProvider.dataProvider
        if let id = keyIDs.first {
            let property = SecretKeyProperties(fingerprint: id, cryptoProtocol: type, usedAddresses: mailAddresses.map({AddressProperties(email: $0)}))
            provider.importSecretKeys(secretProperties: [property], origin: .FileTransfer, completionHandler: {
                error in 
                print("Import done: \(String(describing: error))")
            })
            return true
        }
        return false
    }
    
    func savePK(type: LogData.TransferType) {
        let keyIDs = SwiftPGP.init().store(tempKeys: [self])
        var keys = [PublicKeyProperties]()
        for key in keyIDs {
            let property = PublicKeyProperties(fingerprint: key, cryptoProtocol: self.type, origin: .FileTransfer, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: nil, usedAddresses: mailAddresses.map({AddressProperties(email: $0)}))
            keys.append(property)
        }
        PersistentDataProvider.dataProvider.importNewData(from: keys, completionHandler: {error in print("Import done: \(String(describing: error))")})
    }
}


