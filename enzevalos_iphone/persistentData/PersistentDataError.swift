//
//  PersistentDataError.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.09.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation

enum PersistentDataError: Error {
    case urlError
    case networkUnavailable
    case wrongDataFormat
    case missingData
    case creationError
    case batchInsertError
    case batchDeleteError
    case wrongPredicateError
}
