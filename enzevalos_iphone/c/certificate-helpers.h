//
//  certificate-helpers.h
//  enzevalos_iphone
//
//  Created by lazarog98 on 18.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

#ifndef certificate_helpers_h
#define certificate_helpers_h
#undef OPENSSL_NO_SSL_INTERN
#undef OPENSSL_NO_CRYPTO_INTERN

#include "generic-helpers.h"
#include <string.h>
#include <stdlib.h>
#include <openssl/opensslv.h>
#include <openssl/evp.h> // this header may not be needed
#include <openssl/err.h> // this header may not be needed
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/pkcs7.h>
#include <openssl/cms.h>
#include <openssl/safestack.h>
#include <openssl/x509.h>
#include <openssl/opensslconf.h>
#include <openssl/crypto.h>
// subject
// issuer

struct cert_info {
    char *issuer;
    char *subject;
    char *date_start;
    char *date_end;
    char ** emails;
    int num_emails;
} typedef cert_info;


char * get_issuer(X509 *cert);
char * get_subject(X509 *cert);
char * get_start_date(X509 *cert);
char * get_end_date(X509 *cert);
array_with_length *get_emails(X509 *cert);
cert_info * get_cert_data(const char *pem);

cert_info * new_cert_info(void);
void deallocate_cert_info(cert_info *c);

#endif /* certificate_helpers_h */
