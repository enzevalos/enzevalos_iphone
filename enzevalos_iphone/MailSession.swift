//
//  MailSessionConfiguration.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 26.02.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation

enum SessionType {
    case IMAP, SMTP
}

protocol MailSessionListener {
    func testFinish(result: Bool)
}


class MailServer: Comparable {
    static func < (lhs: MailServer, rhs: MailServer) -> Bool {
        if lhs.authType == nil {
            return false
        }
        if rhs.authType == nil {
            return true
        }
        if let lhsAuth = lhs.authType, let rhsAuth = rhs.authType {
            return lhsAuth < rhsAuth
        }
        return false
    }
    
    static func == (lhs: MailServer, rhs: MailServer) -> Bool {
        return lhs.sessionType == rhs.sessionType
            && lhs.hostname == rhs.hostname
            && lhs.port == rhs.port
            && lhs.connectionType == rhs.connectionType
            && lhs.authType == rhs.authType
    }
    
    private static let maxWaitingSeconds = 15
    private static let maxFailedAttempts = 20 // = 10*2 = Auth * Conn type
    private var failedAttempts = 0
    
    let sessionType: SessionType
    var hostname: String
    var port: UInt32
    var connectionType: MCOConnectionType
    private var authType: MCOAuthType?
    var authenticationType: MCOAuthType {
        get{
            if let a = authType {
                return a
            }
            return MCOAuthType.init(rawValue: 0)
        }
    }
    
    var callback: (_ error: MailServerConnectionError?, _ server: MailServer) -> ()
    
    private var username: String
    private let password: String
    
    var possibleAuthTypes: [MCOAuthType] = []
    var toTestConnType: Set<MCOConnectionType> = []
    var toTestServers: [(hostname: String,
                         port: UInt32,
                         authTyp: MCOAuthType,
                         connType: MCOConnectionType)] = []
    var createdTestServers = false
    var receivedAuthTypes = false
    var loggerCalled = false
    var works = false
    var sendCallback = false
    
    var isTLS: Bool {
        get {
            return connectionType == MCOConnectionType.TLS
        }
    }
    
    var isPlain: Bool {
        get {
            return authType == MCOAuthType.saslPlain
        }
    }
    var discription: String {
        get{
            let msg = """
            \(sessionType): \(hostname):\(port) Conntype: \(connectionType.description) Authtype: \(authenticationType.description)
            Possible AuthTypes: \(possibleAuthTypes.map{$0.description})
            To test ConnTypes: \(toTestConnType.map{$0.description})
            works: \(works) and logger called: \(loggerCalled) and authtypes from server: \(receivedAuthTypes)
            """
            return msg
        }
    }
    
    
    init(sessionType: SessionType,
         username: String,
         password: String,
         hostname: String,
         port: UInt32,
         connectionType: MCOConnectionType?,
         authType: MCOAuthType? = nil,
         callback: @escaping (_ error: MailServerConnectionError?, _ server: MailServer) -> ()) {
        self.sessionType = sessionType
        self.hostname = hostname
        self.port = port
        self.authType = authType
        self.toTestConnType = [MCOConnectionType.TLS, MCOConnectionType.startTLS]
        if let connectionType = connectionType {
            self.connectionType = connectionType
            self.toTestConnType = []
        } else if port == 143 || port == 587 || port == 25 {
            self.connectionType = .startTLS
        } else {
            self.connectionType = .TLS
        }
        self.username = username
        self.password = password
        self.callback = callback
    }
    
    convenience init(sessionType: SessionType,
                     username: String,
                     password: String,
                     serverConfig: MCONetService,
                     callback: @escaping (_ error: MailServerConnectionError?,
                                          _ server: MailServer) -> ()) throws {
        if let serverInfo = serverConfig.info(),
           let hostname = serverInfo["hostname"] as? String,
           let port = serverInfo["port"] as? UInt32 {
            var conn: MCOConnectionType? = nil
            
            if let trans = serverInfo["ssl"] as? Bool, trans {
                conn = MCOConnectionType.TLS
            }
            
            if let trans = serverInfo["starttls"] as? Bool, trans {
                conn = MCOConnectionType.startTLS
            }
            
            self.init(sessionType: sessionType,
                      username: username,
                      password: password,
                      hostname: hostname,
                      port: port,
                      connectionType: conn,
                      callback: callback)
        } else {
            throw MailServerConnectionError.NoData
        }
    }
    
    convenience init(userValues sessionType: SessionType,
                     callback: @escaping (_ error: MailServerConnectionError?,
                                          _ server: MailServer) -> ()) throws {
        if let username = UserManager.loadUserValue(Attribute.accountname) as? String,
           let password = UserManager.loadUserValue(Attribute.userPW) as? String {
            if sessionType == SessionType.IMAP,
               let hostname = UserManager.loadUserValue(Attribute.imapHostname) as? String,
               let port = UserManager.loadUserValue(Attribute.imapPort) as? UInt32 {
                var connType: MCOConnectionType? = nil
                if let rawtype = UserManager.loadUserValue(Attribute.imapConnectionType) as? Int {
                    connType = MCOConnectionType.init(rawValue: rawtype)
                }
                
                var authType: MCOAuthType? = nil
                if let rawtype = UserManager.loadUserValue(Attribute.smtpAuthType) as? Int {
                    authType = MCOAuthType.init(rawValue: rawtype)
                }
                
                self.init(sessionType: sessionType,
                          username: username,
                          password: password,
                          hostname: hostname,
                          port: port,
                          connectionType: connType,
                          authType: authType,
                          callback: callback)
                if let authType = UserManager.loadUserValue(Attribute.imapAuthType) as? Int {
                    self.authType = MCOAuthType.init(rawValue: authType)
                }
            } else if let hostname = UserManager.loadUserValue(Attribute.smtpHostname) as? String,
                      let port = UserManager.loadUserValue(Attribute.smtpPort) as? UInt32 {
                var connType: MCOConnectionType? = nil
                
                if let rawtype = UserManager.loadUserValue(Attribute.smtpConnectionType) as? Int {
                    connType = MCOConnectionType.init(rawValue: rawtype)
                }
                
                var authType: MCOAuthType? = nil
                if let rawtype = UserManager.loadUserValue(Attribute.smtpAuthType) as? Int {
                    authType = MCOAuthType.init(rawValue: rawtype)
                }
                
                self.init(sessionType: sessionType,
                          username: username,
                          password: password,
                          hostname: hostname,
                          port: port,
                          connectionType: connType,
                          authType: authType,
                          callback: callback)
                
                if let authType = UserManager.loadUserValue(Attribute.smtpAuthType) as? Int {
                    self.authType = MCOAuthType.init(rawValue: authType)
                }
            } else {
                throw MailServerConnectionError.NoData
            }
        } else {
            throw MailServerConnectionError.NoData
        }
    }
    
    convenience init(defaultValues sessionType: SessionType,
                     mailAddr: String,
                     username: String,
                     password: String,
                     callback: @escaping (_ error: MailServerConnectionError?,
                                          _ server: MailServer) -> ()) {
        var auth = MCOAuthType.init(rawValue: 0)
        var conn = MCOConnectionType.TLS
        var port: UInt32 = 111
        var  hostname = "example.org"
        let (_, domain) = MailSession.splitAddr(userAddr: mailAddr)
        if let domain = domain {
            hostname = domain
        }
        
        if sessionType == SessionType.IMAP {
            if let authTypeValue = Attribute.imapAuthType.defaultValue as? Int {
                auth = MCOAuthType.init(rawValue: authTypeValue)
            }
            
            if let connectionTypeValue = Attribute.imapConnectionType.defaultValue as? Int {
                conn = MCOConnectionType.init(rawValue: connectionTypeValue)
            }
            
            if let name = Attribute.imapHostname.defaultValue as? String, domain == nil {
                hostname = name
            } else if !MailSession.IMAPPREFIX.isEmpty{
                hostname = MailSession.IMAPPREFIX[0] + "." + hostname
            }
            
            if let p = Attribute.imapPort.defaultValue as? UInt32 {
                port = p
            } else if !MailSession.IMAPPORT.isEmpty {
                port = MailSession.IMAPPORT[0]
            }
        } else {
            if let authTypeValue = Attribute.smtpAuthType.defaultValue as? Int {
                auth = MCOAuthType.init(rawValue: authTypeValue)
            }
            
            if let connectionTypeValue = Attribute.smtpConnectionType.defaultValue as? Int {
                conn = MCOConnectionType.init(rawValue: connectionTypeValue)
            }
            
            if let name = Attribute.smtpHostname.defaultValue as? String, domain == nil {
                hostname = name
            } else if !MailSession.SMTPPREFIX.isEmpty{
                hostname = MailSession.SMTPPREFIX[0]+"."+hostname
            }
            
            if let p = Attribute.smtpPort.defaultValue as? UInt32 {
                port = p
            } else if !MailSession.SMTPPORT.isEmpty {
                port = MailSession.SMTPPORT[0]
            }
        }
        self.init(sessionType: sessionType,
                  username: username,
                  password: password,
                  hostname: hostname,
                  port: port,
                  connectionType: conn,
                  authType: auth,
                  callback: callback)
    }
    
    func storeToUserDefaults(mailAddr: String) -> Bool {
        if let authType = authType {
            let connectionTypeValue = connectionType.rawValue
            let authTypeValue = authType.rawValue
            if sessionType == SessionType.IMAP {
                UserManager.storeUserValue(hostname as AnyObject,
                                           attribute: Attribute.imapHostname)
                UserManager.storeUserValue(port as AnyObject,
                                           attribute: Attribute.imapPort)
                UserManager.storeUserValue(connectionTypeValue as AnyObject,
                                           attribute: Attribute.imapConnectionType)
                UserManager.storeUserValue(authTypeValue as AnyObject,
                                           attribute: Attribute.imapAuthType)
            } else {
                UserManager.storeUserValue(hostname as AnyObject,
                                           attribute: Attribute.smtpHostname)
                UserManager.storeUserValue(port as AnyObject,
                                           attribute: Attribute.smtpPort)
                UserManager.storeUserValue(connectionTypeValue as AnyObject,
                                           attribute: Attribute.smtpConnectionType)
                UserManager.storeUserValue(authTypeValue as AnyObject,
                                           attribute: Attribute.smtpAuthType)
            }
            UserManager.storeUserValue(password as AnyObject,
                                       attribute: Attribute.userPW)
            UserManager.storeUserValue(username as AnyObject,
                                       attribute: Attribute.accountname)
            UserManager.storeUserValue(mailAddr.lowercased() as AnyObject,
                                       attribute: Attribute.userAddr)
            UserManager.storeUserValue(mailAddr.lowercased() as AnyObject,
                                       attribute: Attribute.userDisplayName)
            return true
        }
        return false
    }
    
    func createSMTPSession(logging: Bool = true,
                           credentials: Bool = false,
                           withAuthType: Bool = true) -> MCOSMTPSession? {
        loggerCalled = false
        guard sessionType == .SMTP else {
            return nil
        }
        let session = MCOSMTPSession()
        session.timeout = TimeInterval(MailServer.maxWaitingSeconds)
        session.hostname = hostname.remove(seperatedBy: .whitespacesAndNewlines)
        session.port = port
        session.connectionType = connectionType
        
        if logging {
            session.connectionLogger = parseLog
        }
        
        if withAuthType, let auth = authType {
            session.authType = auth
            if session.authType == MCOAuthType.xoAuth2 {
                session.oAuth2Token = EmailHelper
                    .singleton()
                    .authorization?
                    .authState
                    .lastTokenResponse?
                    .accessToken
            }
        }
        
        if credentials {
            session.username = username.lowercased()
            session.password = password
        }
        
        return session
    }
    
    func createIMAPSession(logging: Bool = true,
                           credentials: Bool = false,
                           withAuthType: Bool = true) -> MCOIMAPSession? {
        loggerCalled = false
        
        guard sessionType == .IMAP else {
            return nil
        }
        
        let session = MCOIMAPSession()
        session.timeout = TimeInterval(MailServer.maxWaitingSeconds)
        session.hostname = hostname.remove(seperatedBy: .whitespacesAndNewlines)
        session.port = port
        session.connectionType = connectionType
        
        if logging {
            session.connectionLogger = parseLog
        }
        
        if withAuthType, let auth = authType {
            session.authType = auth
            if session.authType == MCOAuthType.xoAuth2 {
                session.oAuth2Token = EmailHelper
                    .singleton()
                    .authorization?
                    .authState
                    .lastTokenResponse?
                    .accessToken
            }
        }
        
        if credentials {
            session.username = username
            session.password = password
        }
        
        return session
    }
    
    func findHost() -> Bool{
        guard LetterboxModel.currentReachabilityStatus != .notReachable else {
            sendCallback = true
            self.callback(MailServerConnectionError.NoInternetconnection, self)
            return false
        }
        
        toTestConnType.remove(connectionType)
        if sessionType == SessionType.IMAP {
            return findIMAPHost()
        } else {
            if self.possibleAuthTypes.isEmpty {
                self.possibleAuthTypes = MailSession.AUTHTYPE
            }
            return findSMTPHost()
        }
    }
    
    private func findIMAPHost() -> Bool{
        if let session = createIMAPSession(logging: true, credentials: false, withAuthType: true) {
            if let op = session.connectOperation(){
                op.start({[unowned self](error: Error?) -> () in
                    guard error == nil else {
                        if let newType = self.toTestConnType.popFirst() {
                            self.connectionType = newType
                            _ = self.findHost()
                        } else if self.toTestConnType.isEmpty && !self.sendCallback {
                            self.sendCallback = true
                            self.callback(MailServerConnectionError.ImapSetupError, self)
                        }
                        return
                    }
                    // No error: -> conntectionType is correct and server is reachable
                    if self.possibleAuthTypes.isEmpty {
                        self.possibleAuthTypes = MailSession.AUTHTYPE
                    }
                    self.possibleAuthTypes.sort()
                    self.testAuthTypes()
                })
                return true
            }
        }
        return false
    }
    
    private func findSMTPHost() -> Bool {
        guard let session = createSMTPSession(logging: true,
                                              credentials: true,
                                              withAuthType: true) else {
            return false
        }
        
        guard let op = session.loginOperation() else {
            return false
        }
        
        op.start({[unowned self] (error: Error?) -> () in
            guard error == nil else {
                self.failedAttempts = self.failedAttempts + 1
                if let error = error {
                    let serverError = MailServerConnectionError.findErrorCode(error: error)
                    if self.sendCallback(session: session, error: serverError) {
                        self.callback(MailServerConnectionError.SmtpSetupError, self)
                    } else {
                        if !self.findSMTPHost() {
                            self.callback(MailServerConnectionError.SmtpSetupError, self)
                        }
                    }
                }
                return
            }
            self.authType = session.authType
            self.connectionType = session.connectionType
            // We have a working connection
            self.works = true
            if !self.sendCallback {
                self.sendCallback = true
                self.callback(nil, self)
            }
            print("SMTP works for \(self.username)!")
        })
        return true
    }
    
    private func sendCallback(session: MCOSMTPSession, error: MailServerConnectionError) -> Bool{
        if failedAttempts > MailServer.maxFailedAttempts {
            return true
        }
        switch error {
        case .AuthenticationError,
             .AuthenticationRequiredError,
             .CertificateError,
             .GmailIMAPNotEnabledError:
            return true
        default:
            if toTestServers.count == 0 && createdTestServers {
                return true
            }
            if toTestServers.count == 0 {
                createServerCombinations()
            }
            let next = toTestServers.removeFirst()
            self.hostname = next.hostname
            self.port = next.port
            self.authType = next.authTyp
            self.connectionType = next.connType
            return false
        }
    }
    
    private func createServerCombinations() {
        let authTypes = MailSession.AUTHTYPE
        for authType: MCOAuthType in authTypes {
            toTestServers.append((self.hostname, self.port, authType, self.connectionType))
        }
        createdTestServers = true
    }
    
    
    private func parseLog(logger: Any, type: MCOConnectionLogType, data: Data?) {
        self.loggerCalled = true
        if let data = data, let msg = String(data: data, encoding: .utf8) {
            let auths = MCOAuthType.parseAuthType(msg: msg)
            if !auths.isEmpty {
                self.possibleAuthTypes = auths
                self.receivedAuthTypes = true
            }
        }
    }
    
    private func testAuthTypes() {
        guard !possibleAuthTypes.isEmpty else {
            self.callback(MailServerConnectionError.ConnectionError, self)
            return
        }
        guard sessionType == .IMAP else {
            return
        }
        if let auth = possibleAuthTypes.last {
            possibleAuthTypes.removeLast()
            self.authType = auth
            if let session = createIMAPSession(logging: true,
                                               credentials: false,
                                               withAuthType: true) {
                if let x = session.connectOperation(){
                    x.start({[unowned self](error: Error?) -> () in
                        guard error == nil else {
                            self.testAuthTypes()
                            return
                        }
                        self.testUsernameAndPW()
                    })
                }
            }
        }
    }
    
    private func testUsernameAndPW() {
        if let session = createIMAPSession(logging: true, credentials: true, withAuthType: true) {
            session.checkAccountOperation().start({[unowned self] (error: Error?) -> () in
                if let error = error {
                    let conError = MailServerConnectionError.findErrorCode(error: error)
                    if session.authType == .xoAuth2, let refresh = EmailHelper.singleton().authorization?.authState.isTokenFresh(), !refresh {
                        EmailHelper.singleton().checkIfAuthorizationIsValid({ authorized in
                            self.testUsernameAndPW()
                        })
                        return
                    }
                    if !self.receivedAuthTypes && !self.possibleAuthTypes.isEmpty {
                        self.authType = self.possibleAuthTypes.removeLast()
                        self.testUsernameAndPW()
                        return
                    } else if conError != MailServerConnectionError.AuthenticationError
                                && (!self.receivedAuthTypes || !self.possibleAuthTypes.isEmpty)
                                && !self.possibleAuthTypes.isEmpty {
                        self.authType = self.possibleAuthTypes.removeLast()
                        self.testUsernameAndPW()
                        return
                    } else if !self.sendCallback {
                        self.sendCallback = true
                        self.callback(conError, self)
                    }
                } else {
                    self.works = true
                    if !self.sendCallback {
                        self.sendCallback = true
                        self.callback(nil, self)
                    }
                    print("IMAP works for \(self.username)")
                }
            })
        }
    }
}

class MailSession {
    static let SMTPPORT: [UInt32] = [587, 465, 25]
    static let SMTPPREFIX = ["smtp", "mail", "outgoing", "", "mx"]
    static let IMAPPREFIX = ["imap", "mail", "", "mx"]
    static let IMAPPORT: [UInt32] = [993, 143]
    static let AUTHTYPE = [MCOAuthType.saslPlain,
                           MCOAuthType.saslLogin,
                           MCOAuthType.SASLNTLM,
                           MCOAuthType.saslKerberosV4,
                           MCOAuthType.SASLCRAMMD5,
                           MCOAuthType.SASLDIGESTMD5,
                           MCOAuthType.SASLGSSAPI,
                           MCOAuthType.SASLSRP,
                           MCOAuthType.init(rawValue: 0)]
    // We do not test for plain connections!
    static let CONNTECTIONTYPE = [MCOConnectionType.TLS, MCOConnectionType.startTLS]
    
    var defaultIMAPSession: MCOIMAPSession? {
        get {
            if let server = try? MailServer(userValues: sessionType, callback: callback) {
                if let session = server.createIMAPSession(logging: false,
                                                          credentials: true,
                                                          withAuthType: true) {
                    return session
                }
            }
            return nil
        }
    }
    
    var defaultSMTPSession: MCOSMTPSession? {
        get {
            if let server = try? MailServer(userValues: sessionType,
                                            callback: callback),
               let session = server.createSMTPSession(logging: false,
                                                      credentials: true,
                                                      withAuthType: true) {
                return session
            }
            return nil
        }
    }
    
    var sessionType: SessionType
    var mailAddr: String
    var username: String
    var password: String
    var errors: Set<MailServerConnectionError> = Set()
    
    private var possibleServers: [MailServer] = []
    private var counter = -1
    private var success = false
    private var workingServers: [MailServer] = []
    private var listeners: [MailSessionListener] = [MailSessionListener]()
    private var notified = false
    
    var server: MailServer {
        get {
            workingServers.sort()
            if let server = workingServers.last {
                return server
            }
            possibleServers.sort()
            if let server = possibleServers.last {
                return server
            }
            if let server = try? MailServer(userValues: sessionType, callback: callback) {
                return server
            }
            if loadFromProviderJson() {
                possibleServers.sort()
                if let server = possibleServers.last {
                    return server
                }
            }
            return MailServer(defaultValues: sessionType,
                              mailAddr: mailAddr,
                              username: username,
                              password: password,
                              callback: callback)
        }
    }
    
    init(configSession sessionType: SessionType,
         mailAddress: String,
         password: String,
         username: String?) {
        self.sessionType = sessionType
        self.mailAddr = mailAddress
        self.password = password
        if let name = username, !name.isEmpty {
            self.username = name
        } else {
            self.username = mailAddr
        }
    }
    
    init(loadUserDefaults sessionType: SessionType) throws{
        self.sessionType = sessionType
        
        if let username = UserManager.loadUserValue(Attribute.accountname) as? String {
            self.username = username
        } else if let username = UserManager.loadUserValue(Attribute.userAddr) as? String {
            self.username = username
        } else {
            throw MailServerConnectionError.AuthenticationError
        }
        
        if let pw = UserManager.loadUserValue(Attribute.userPW) as? String {
            self.password = pw
        } else {
            throw MailServerConnectionError.AuthenticationError
        }
        
        if let addr = UserManager.loadUserValue(Attribute.userAddr) as? String {
            mailAddr = addr
        } else {
            mailAddr = username
        }
    }
    
    func addListener(listener: MailSessionListener) {
        listeners.append(listener)
    }
    
    
    /// Stores a working server configuration to user defaults.
    func storeToUserDefaults() -> Bool {
        workingServers.sort()
        workingServers.reverse()
        for server in workingServers {
            if server.works {
                return server.storeToUserDefaults(mailAddr: mailAddr)
            }
        }
        return false
    }
    
    func startTestingServerConfigFromList() -> Bool{
        if !loadFromProviderJson() {
            return false
        }
        return searchForServerConfig(hostFromAdr: false)
    }
    
    func setServer(hostname: String, port: UInt32, connType: Int, authType: Int?) {
        possibleServers = []
        var auth: MCOAuthType? = nil
        if let rawValue = authType {
            auth = MCOAuthType.init(rawValue: rawValue)
        }
        let server = MailServer(sessionType: sessionType,
                                username: username,
                                password: password,
                                hostname: hostname,
                                port: port,
                                connectionType: MCOConnectionType.init(rawValue: connType),
                                authType: auth,
                                callback: callback)
        possibleServers.append(server)
    }
    
    func startTestingServerConfig() -> Bool {
        return searchForServerConfig(hostFromAdr: false)
    }
    
    // We test all common server configurations (common prefixe x common ports x {TLS, startTLS} ).
    // This may take some time.
    func startLongSearchOfServerConfig(hostFromAdr: Bool) -> Bool {
        // Try to find a possible config
        let (_, server) = MailSession.splitAddr(userAddr: self.mailAddr)
        if let domain = server {
            if self.sessionType == SessionType.IMAP {
                createServers(domain: domain,
                              prefixes: MailSession.IMAPPREFIX,
                              ports: [UInt32(111)])
            } else {
                createServers(domain: domain,
                              prefixes: MailSession.SMTPPREFIX,
                              ports: MailSession.SMTPPORT)
            }
        }
        return searchForServerConfig(hostFromAdr: hostFromAdr)
    }
    
    
    private func searchForServerConfig(hostFromAdr: Bool) -> Bool {
        var search = false
        let (_, host) = MailSession.splitAddr(userAddr: self.mailAddr)
        // Setup new search
        counter = 0
        success = false
        workingServers = []
        notified = false
        for p in possibleServers {
            if hostFromAdr, let host = host {
                p.hostname = host
            }
            let start = p.findHost()
            if start {
                counter = counter + 1
            }
            search = search || start
        }
        return search
    }
    
    private func createServers(domain: String, prefixes: [String], ports: [UInt32]) {
        possibleServers = []
        for prefix in prefixes {
            for port in ports {
                // We check both connection types in parallel because it takes a long
                // time if we start with the wrong conntection type (TLS or startTLS)
                for conType in MailSession.CONNTECTIONTYPE {
                    var hostname = prefix + "." + domain
                    
                    if prefix.isEmpty {
                        hostname = domain
                    }
                    
                    let server = MailServer(sessionType: sessionType,
                                            username: username,
                                            password: password,
                                            hostname: hostname,
                                            port: port,
                                            connectionType: conType,
                                            authType: nil,
                                            callback: callback)
                    possibleServers.append(server)
                }
            }
        }
    }
    
    private func notifyListener() {
        if !notified {
            notified = true
            for l in listeners {
                l.testFinish(result: success)
            }
        }
    }
    
    private func callback(error: MailServerConnectionError?, server: MailServer) {
        DispatchQueue.main.async {
            if error == nil {
                self.success = true
                self.workingServers.append(server)
                self.notifyListener()
            } else {
                if let error = error {
                    self.errors.insert(error)
                }
                
                self.counter = self.counter - 1
                if self.counter <= 0 {
                    self.notifyListener()
                }
            }
        }
    }
    
    private func readJson() -> [MCONetService] {
        var servers: [MCONetService] = []
        let manager = MCOMailProvidersManager.shared()
        if let path = Bundle.main.path(forResource: "providers", ofType: "json") {
            manager.registerProviders(withFilename: path)
            if let provider = manager.provider(forEmail: mailAddr){
                if sessionType == SessionType.IMAP {
                    servers = provider.imapServices()
                } else {
                    servers = provider.smtpServices()
                }
            }
        }
        return servers
    }
    
    var hasJsonFile: Bool {
        get {
            return !readJson().isEmpty
        }
    }
    
    func loadFromProviderJson() -> Bool {
        let servers: [MCONetService] = readJson()
        for elem in servers {
            if let server = try? MailServer(sessionType: sessionType, username: username, password: password, serverConfig: elem, callback: callback){
                possibleServers.append(server)
            }
        }
        return !servers.isEmpty
    }
    
    static func splitAddr(userAddr: String) -> (local: String?, domain: String?) {
        let tokens = userAddr.split(separator: "@", maxSplits: 1)
        if tokens.count == 2 {
            let hostname = String(tokens[1])
            let local = String(tokens[0])
            return (local, hostname)
        }
        return (nil, nil)
    }
}

extension MCOConnectionType: Equatable, Hashable {
    public static func == (lhs: MCOConnectionType, rhs: MCOConnectionType) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.rawValue)
    }
    
    var  description: String {
        switch self {
        case MCOConnectionType.startTLS:
            return "startTLS"
        case MCOConnectionType.TLS:
            return "TLS"
        default:
            return self.rawValue.description
        }
    }
    
    static func parseConnType(msg: String) -> MCOConnectionType? {
        /*
         RFC 3501 (IMAP):
         See: https://tools.ietf.org/html/rfc3501#section-6.1.1
         */
        let msg = msg.lowercased()
        if msg.contains("starttls".lowercased()) {
            return MCOConnectionType.startTLS
        }
        else if msg.contains("tls".lowercased()) && msg.contains("CAPABILITY".lowercased()) {
            return MCOConnectionType.TLS
        }
        return nil
    }
}

extension MCOAuthType: Comparable {
    public static func < (lhs: MCOAuthType, rhs: MCOAuthType) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
    
    public static func == (lhs: MCOAuthType, rhs: MCOAuthType) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
    public var description: String {
        get {
            switch self.rawValue {
            case MCOAuthType.SASLCRAMMD5.rawValue:
                return "SASLCRAMMD5"
            case MCOAuthType.SASLDIGESTMD5.rawValue:
                return "SASLDIGESTMD5"
            case MCOAuthType.SASLGSSAPI.rawValue:
                return "SASLGSSAPI"
            case MCOAuthType.saslKerberosV4.rawValue:
                return "saslKerberosV4"
            case MCOAuthType.saslLogin.rawValue:
                return "saslLogin"
            case MCOAuthType.SASLNTLM.rawValue:
                return "SASLNTLM"
            case MCOAuthType.saslPlain.rawValue:
                return "saslPlain"
            case MCOAuthType.SASLSRP.rawValue:
                return "SASLSRP"
            case MCOAuthType.xoAuth2.rawValue:
                return "xoAuth2"
            case MCOAuthType.xoAuth2Outlook.rawValue:
                return "xoAuth2Outlook"
            default:
                if self.rawValue == 0 {
                    return "NONE"
                }
                return self.rawValue.description
            }
        }
    }
    
    static func parseAuthType(msg: String) -> [MCOAuthType] {
        // RFC 3501 (IMAP):
        // See: https://tools.ietf.org/html/rfc3501#section-6.1.1
        let msg = msg.lowercased()
        var authTypes: [MCOAuthType] = []
        if msg.contains("CAPABILITY".lowercased()) { // IMAP
            if msg.contains("AUTH=XOAUTH2".lowercased()) {
                authTypes.append(MCOAuthType.xoAuth2)
            }
            if msg.contains("AUTH=LOGIN".lowercased()) {
                authTypes.append(MCOAuthType.saslLogin)
            }
            if msg.contains("AUTH=PLAIN".lowercased()) {
                authTypes.append(MCOAuthType.saslPlain)
            }
            if msg.contains("AUTH=GSSAPI".lowercased()) {
                authTypes.append(MCOAuthType.SASLGSSAPI)
            }
            if msg.contains("AUTH=Kerberos".lowercased()) {
                authTypes.append(MCOAuthType.saslKerberosV4)
            }
            if msg.contains("AUTH=LSRP".lowercased()) {
                authTypes.append(MCOAuthType.SASLSRP)
            }
            if msg.contains("AUTH=LNTLM".lowercased()) {
                authTypes.append(MCOAuthType.SASLNTLM)
            }
            if msg.contains("AUTH=CRAMMD5".lowercased()) {
                authTypes.append(MCOAuthType.SASLCRAMMD5)
            }
            if msg.contains("AUTH=DIGESTMD5".lowercased()) {
                authTypes.append(MCOAuthType.SASLDIGESTMD5)
            }
        } else if msg.contains("250-AUTH".lowercased()) {
            // SMTP
            if msg.contains("XOAUTH2".lowercased()) {
                authTypes.append(MCOAuthType.xoAuth2)
            }
            if msg.contains("LOGIN".lowercased()) {
                authTypes.append(MCOAuthType.saslLogin)
            }
            if msg.contains("PLAIN".lowercased()) {
                authTypes.append(MCOAuthType.saslPlain)
            }
            if msg.contains("GSSAPI".lowercased()) {
                authTypes.append(MCOAuthType.SASLGSSAPI)
            }
            if msg.contains("Kerberos".lowercased()) {
                authTypes.append(MCOAuthType.saslKerberosV4)
            }
            if msg.contains("LSRP".lowercased()) {
                authTypes.append(MCOAuthType.SASLSRP)
            }
            if msg.contains("LNTLM".lowercased()) {
                authTypes.append(MCOAuthType.SASLNTLM)
            }
            if msg.contains("CRAMMD5".lowercased()) {
                authTypes.append(MCOAuthType.SASLCRAMMD5)
            }
            if msg.contains("DIGESTMD5".lowercased()) {
                authTypes.append(MCOAuthType.SASLDIGESTMD5)
            }
        }
        return authTypes
    }
}
