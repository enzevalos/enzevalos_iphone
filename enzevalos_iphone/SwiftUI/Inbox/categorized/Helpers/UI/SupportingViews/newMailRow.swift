//
//  SimpleMailRow.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 05.02.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

///just a simple mail row displaying the title and first lines of the content next to a small sender-usericon
struct SimpleMailRow: View {
    
    let mail : eMail
    
    var body: some View {
        HStack(alignment: .center) {
            icon
            VStack {
                HStack{
                    Text(mail.subject)
                        .font(.caption)
                        .lineLimit(1)
                    Spacer()
                    Text(mail.date.timeAgoText())
                        .font(.caption).foregroundColor(.gray)
                }
                nameView
            }
        }
    }
    
    private var icon: some View{
        mail.sender.avatar
            .sizeTo(30)
            .openOnTap(tag:1){
                ContactView(
                    contact: mail.sender
                )
            }
               
    }
    
    private var nameView: some View {
        Text(mail.sender.name)
            .frame(maxWidth: 300, alignment: .leading)
            .lineLimit(1)
            .font(.caption)
    }
}


struct OptionsMailRow : View {
    
    let mail : eMail
    
    var body : some View {
        SimpleMailRow(mail: mail)
            .capsuledMailOptions(mail: mail)
    }
}

struct SimpleMailRow_Previews: PreviewProvider {
    static var previews: some View {
        Text("T0D0")
    }
}
