//
//  CHelpers.swift
//  enzevalos_iphone
//
//  Created by lazarog98 on 10.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation

/**
 Creates an arraz of C Strings from an array of swift strings
 Always call deallocateCStrArr after using this!
 */
func createCStrArr(sarr: [String]) -> UnsafeMutablePointer<UnsafeMutablePointer<Int8>?>?{
    let len = sarr.count
    let carr = init_str_arr(Int32(len))
    for i in 0..<len
    {
        let str = sarr[i]
        add_str_to_arr(str,carr,Int32(i))
    }
    return carr
}

/**
 Deallocates an array of c strings
 */
func deallocateCStrArr(arr: UnsafeMutablePointer<UnsafeMutablePointer<Int8>?>?, len: Int){
    return deallocate_str_arr(arr, Int32(len))
}
