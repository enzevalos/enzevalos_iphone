//
//  FolderRecord.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 05.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

import CoreData

extension FolderRecord {
    static var FetchRequest: NSFetchRequest<FolderRecord> {
        let request: NSFetchRequest<FolderRecord> = NSFetchRequest<FolderRecord>(entityName: FolderRecord.entityName)
        request.sortDescriptors = [NSSortDescriptor(key: "flags", ascending: false), NSSortDescriptor(key: "path", ascending: true)]
        return request
    }
    
    static var onlySpecialFolderFetchRequest: NSFetchRequest<FolderRecord> {
        let request: NSFetchRequest<FolderRecord> = NSFetchRequest<FolderRecord>(entityName: FolderRecord.entityName)
        request.predicate = NSPredicate(format: " flags > 0")
        request.sortDescriptors = [NSSortDescriptor(key: "flags", ascending: false), NSSortDescriptor(key: "path", ascending: true)]
        return request
    }
    
    static var onlyUserDefinedFolderFetchRequest: NSFetchRequest<FolderRecord> {
        let request: NSFetchRequest<FolderRecord> = NSFetchRequest<FolderRecord>(entityName: FolderRecord.entityName)
        request.predicate = NSPredicate(format: " flags == 0")
        request.sortDescriptors = [NSSortDescriptor(key: "path", ascending: true)]
        return request
    }
}

extension FolderRecord {
    static let entityName = "FolderRecord"
    
    func update(properties: FolderProperties) {
        if let delimiter = properties.delimiter {
            self.delimiter = delimiter
        }
        if let uidValidity = properties.uidValidity {
            self.uidValidity = uidValidity
        }
        if let lastUpdate = properties.lastUpdate {
            self.lastUpdate = lastUpdate
        }
        if let maxUID = properties.maxUID {
            self.maxUID = maxUID
        }
        if let minUID = properties.minUID {
            self.minUID = minUID
        }
        if properties.flags != 0 {
            self.flags = properties.flags
        }
        if UserManager.backendInboxFolderPath == properties.path {
            var flag = MCOIMAPFolderFlag(rawValue: Int(self.flags))
            flag.formUnion(.inbox)
            self.flags = Int16(flag.rawValue)
        }
        path = properties.path
    }
    
    var uids: MCOIndexSet {
        get {
            let indexSet = MCOIndexSet()
            if let set = self.mailsInFolder, set.count > 0,
               let mails = set.allObjects as? [MailRecord] {
                mails.forEach {
                    indexSet.add(UInt64($0.uID))
                }
            }
            return indexSet
        }
    }
    
    var type: FolderType {
        let flags = MCOIMAPFolderFlag(rawValue: Int(self.flags))
        if flags.contains(MCOIMAPFolderFlag.drafts) {
            return .Draft
        }
        if flags.contains(MCOIMAPFolderFlag.sentMail) {
            return .Sent
        }
        if flags.contains(MCOIMAPFolderFlag.trash) {
            return .Trash
        }
        if flags.contains(MCOIMAPFolderFlag.archive) {
            return .Archive
        }
        if flags.contains(MCOIMAPFolderFlag.inbox) {
            return .Inbox
        }
        return .Other
    }
    
    static func lastDate(folder: String, date: Date) {
        try? PersistentDataProvider.dataProvider.updateFolder(folder: folder) {
            $0.lastUpdate = date
        }
    }
}

extension FolderRecord: DisplayFolder {
    var name: String {
        if let delimiter = delimiter?.first,
           let n = path?.split(separator: delimiter),
           let last = n.last {
            return String(last)
        }
        return path ?? "NO NAME"
    }
    
    var mails: Int {
        return self.mailsInFolder?.count ?? 0
    }
    
    var icon: Image {
        var label = "folder"
        switch type {
        case .Archive:
            label = "archivebox"
        case .Trash:
            label = "trash"
        case .Inbox:
            label = "tray"
        case .Sent:
            label = "paperplane"
        case .Draft:
            label = "doc"
        case .Other:
            label = "folder"
            if subfolderLevel > 1 {
                label = "tray.2"
            }
        }
        return Image(systemName: label)
    }
    
    var subfolderLevel: Int {
        if let delimiter = delimiter?.first, let n = path?.split(separator: delimiter) {
            return n.count
        }
        return 0
    }
}
