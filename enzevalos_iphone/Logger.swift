//
//  LoggerFirstStudy.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 09.05.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation

enum IntroState {
    case Overview
}

class Logger {
    
    static var logging = StudySettings.studyMode
    static let loggingInterval = 86400 // 21600 = 60*60*6 seconds = 6 hours
    static let resendInterval = 5 * 60
    static let logReceiver = LOGGING_MAIL_ADR
    
    static var nextDeadline = (UserManager.loadUserValue(Attribute.nextDeadline) as? Date) ?? Date()
    
    
    static fileprivate func sendCheck() {
        /* TODO
        let deadlineIsNow = nextDeadline <= Date()
        if  deadlineIsNow && AppDelegate.getAppDelegate().currentReachabilityStatus != .notReachable && UserManager.loadUserValue(Attribute.accountname) != nil && UserDefaults.standard.bool(forKey: "launchedBefore"){
            //Do not send duplicate mails
            let tmpNextDeadline = Date(timeIntervalSinceNow: TimeInterval(loggingInterval))
            nextDeadline = tmpNextDeadline
            UserManager.storeUserValue(nextDeadline as AnyObject?, attribute: Attribute.nextDeadline)
            sendLog()
        } */
       
    }
    
    static func log(setupStudy studypara: [StudyParameterProtocol.Type], alreadyRegistered: Bool) {
        if !logging {
            return
        }
        sendCheck()
    }
    
    static func log(startApp onboarding: Bool) {
        if !logging {
            return
        }
        LogData.newSimpleEvent(event: .newSession)
        sendCheck()
    }
    
    static func log(terminateApp: Void) {
        if !logging {
            return
        }
    }
    
    static func log(background goto: Bool) {
        if !logging {
            return
        }
        if !goto {
            LogData.newSimpleEvent(event: .newSession)
        }
        sendCheck()
    }
    
    
    static func log(onboardingState onboardingSection: IntroState, duration: Double) {
        if !logging {
            return
        }
        LogData.viewOnboardingSlide(type: onboardingSection, duration: duration)
        sendCheck()
    }
    
    static func log(onboardingPageTransition from: Int, to: Int, onboardingSection: String) {
        if !logging {
            return
        }
    }
    /* TODO
    static func log(contactViewOpen keyRecord: KeyRecord?, otherRecords: [KeyRecord]?, isUser: Bool) {
        if !logging {
            return
        }
    }
    */
    /* TODO
    static func log(contactViewClose keyRecord: KeyRecord?, otherRecords: [KeyRecord]?, isUser: Bool) {
        if !logging {
            return
        }
    }
    */
    static func log(keyViewOpen keyID: String) {
        if !logging {
            return
        }
        LogData.newSimpleEvent(event: .keyDetailsView)
        
    }
    
    static func log(keyViewClose keyID: String, secondsOpened: Int) {
        if !logging {
            return
        }
    }
    
    static func log(exportKeyViewOpen view: Int) {
        if !logging {
            return
        }
    }
    
    static func log(exportKeyViewClose view: Int) {
        if !logging {
            return
        }
    }
    
    static func log(exportKeyViewButton send: Bool) {
        if !logging {
            return
        }
        if send {
            LogData.exportKey(keyType: .secretKey, transferType: .autocrypt)
        }
        sendCheck()
    }
    
    /*
    static func log(importPrivateKeyPopupOpen mail: PersistentMail?) {
        if !logging {
            return
        }
    }
    
    static func log(importPrivateKeyPopupClose mail: PersistentMail?, doImport: Bool) {
        if !logging {
            return
        }
    }
    */
    /* TODO
    static func log(importPrivateKey mail: PersistentMail?, success: Bool) {
        if !logging {
            return
        }
        LogData.importKey(keyType: .secretKey, transferType: .mail, known: false, successful: success)

        sendCheck()
    }
    */
    static func log(importPrivateKey type: LogData.TransferType, known: Bool, success: Bool) {
        if !logging {
            return
        }
        if success {
            LogData.importKey(keyType: .secretKey, transferType: type, known: known, successful: success)
        }
        sendCheck()
    }
    
    static func log(importKey transferType: LogData.TransferType, keyType: LogData.KeyType, known: Bool, success: Bool) {
        if !logging {
            return
        }
        if success {
            LogData.importKey(keyType: keyType, transferType: transferType, known: known, successful: success)
        }
        sendCheck()
    }
    
    /*
    static func log(createDraft to: [Mail_Address?], cc: [Mail_Address?], bcc: [Mail_Address?], subject: String, bodyLength: Int, isEncrypted: Bool, isSigned: Bool, myKeyID: String) {
        if !logging {
            return
        }
    }
 */
    /* TODO
    static func log(sent from: Mail_Address, to: [Mail_Address], cc: [Mail_Address], bcc: [Mail_Address], subject: String, bodyLength: Int, isEncrypted: Bool, decryptedBodyLength: Int, decryptedWithOldPrivateKey: Bool = false, isSigned: Bool, isCorrectlySigned: Bool = true, signingKeyID: String, myKeyID: String, secureAddresses: [Mail_Address] = [], encryptedForKeyIDs: [String] = [], inviteMailContent: String?, invitationMail: Bool) {
        
        if !logging {
            return
        }
        var encState = EncryptionState.NoEncryption
        if isEncrypted {
            encState = EncryptionState.ValidedEncryptedWithCurrentKey
        }
        var sigState = SignatureState.NoSignature
        if isSigned {
            sigState = SignatureState.ValidSignature
        }
        LogData.addMail(enc: encState , sig: sigState, received: false)
        sendCheck()
    }
    
    static func log(readViewOpen mail: PersistentMail, message: String, draft: Bool = false) {
        if !logging {
            return
        }
    }
    
    static func log(readViewClose message: String, draft: Bool = false) {
        if !logging {
            return
        }
    }
    
    static func log(received mail: PersistentMail) {
        if !logging {
            return
        }
        LogData.addMail(mail: mail, received: true)
        sendCheck()
    }
    
    static func log(bitcoinMail gotIt: Bool) {
        if !logging {
            return
        }
    }
    
    
    static func log(delete mail: PersistentMail, toTrash: Bool) {
        if !logging {
            return
        }
    }
    
    static func log(archive mail: PersistentMail) {
        if !logging {
            return
        }
    }
    
    static func log(open indicatorButton: String, mail: PersistentMail?) {
        if !logging {
            return
        }
    }
    
    static func log(close indicatorButton: LogData.IndicatorButton, mail: PersistentMail?, action: LogData.UserInteractionPopUp, duration: Double) {
        if !logging {
            return
        }
        LogData.clickOnIndicator(type: indicatorButton, userReaction: action, duration: duration)
        sendCheck()
    }
    
    static func log(showBroken mail: PersistentMail?) {
        if !logging {
            return
        }
    }
    
    static func log(reactTo mail: PersistentMail?) {
        if !logging {
            return
        }
        
    }
    
    static func log(discover publicKeyID: String, mailAddress: Mail_Address?, importChannel: LogData.TransferType, knownPrivateKey: Bool, knownBefore: Bool) {
        if !logging {
            return
        }
        LogData.importKey(keyType: .publicKey, transferType: importChannel, known: knownBefore, successful: true)
        sendCheck()
    }
    */
    static func log(backgroundFetch newMails: UInt32, duration: Double) {
        if !logging {
            return
        }
    }
    
    static func log(verify keyID: String, open: Bool, success: Bool? = nil) {
        if !logging {
            return
        }
        LogData.newVerification(successful: success)
        sendCheck()
    }
    
    static func log(search nrOfTrays: Int, category: Int, opened: String, keyRecordMailList: [AddressRecord]? = nil) {
        guard logging else {
            return
        }
    }

    static private func sendLog(logMailAddress: String = logReceiver) {
        let jsonEncoder = JSONEncoder()
        if let data = try? jsonEncoder.encode(Maildata()), let text = String(data: data, encoding: .utf8), text.count > 0 {
            let mail = OutgoingMail.createLoggingMail(addr: logMailAddress, textcontent: text)
            mail.send()
        }
        LogInUserDefaults.handler.reset()
    }
    
    static func sendCallback(error: Error?) {
        guard error == nil else {
            return
        }
    }
    static func log(warning: LogData.WarningType) {
        LogData.warning(type: warning)
    }
    
    static func log(reactWarning: LogData.WarningType, reaction: LogData.UserWarningReaction) {
        LogData.warning(type: reactWarning, reaction: reaction)
    }
}

/*
    Logged data according privacy policy (see: https://letterbox-app.org/privacy.html)
 */

public class LogInUserDefaults {
    static let handler = LogInUserDefaults()
    
    private let domain = "letterbox.logging"
    private let logger: UserDefaults
    private let startDateName = "startDate"
    private let prefix = "letterbox"
    
    init() {
        if let newDefault =  UserDefaults.init(suiteName: domain) {
            logger = newDefault
        }
        else {
            logger = UserDefaults.standard
        }
        if logger.data(forKey: startDateName) == nil {
            logger.set(Date(), forKey: "\(prefix).\(startDateName)")
        }
    }
    
    func incrementEventCounter(eventName: String) {
        let oldValue = logger.integer(forKey: "\(prefix).\(eventName)")
        let counter =  oldValue + 1
        logger.set(counter, forKey: "\(prefix).\(eventName)")
        
    }
    
    func reset() {
        for (key, value) in logger.dictionaryRepresentation() {
            if let _ = value as? Int {
                logger.set(0, forKey: key)
            } else {
                logger.set(nil, forKey: key)
            }
        }
        logger.set(Date(), forKey: "\(prefix).\(startDateName)")
    }
    
    func calculateDuration(eventName: String, duration: Double) {
        // Use online algo: See https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
        let nameMean = "|mean"
        let nameSqD = "|sqD" // squared distance from the mean
        // load values
        // we assume that event was logged before!
        let n = logger.integer(forKey: "\(prefix).\(eventName)")
        var mean = logger.double(forKey: "\(prefix).\(eventName+nameMean)")
        var sqD = logger.double(forKey: "\(prefix).\(eventName+nameSqD)")
        
        // See:
        //      Welford, B. P. (1962). "Note on a method for calculating corrected sums of squares and products". or
        //      Donald E. Knuth (1998). The Art of Computer Programming, volume 2: Seminumerical Algorithms, 3rd edn., p. 232
        let delta = duration - mean
        mean = mean + (delta / Double(n))
        let delta2 = duration - mean
        sqD = sqD + delta * delta2
        
        // update values
        logger.set(sqD, forKey: "\(prefix).\(eventName+nameSqD)")
        logger.set(mean, forKey: "\(prefix).\(eventName+nameMean)")
    }
    
    var loggedEvents: [String: Int] {
        get {
            var events = [String: Int]()
            for (key, value) in logger.dictionaryRepresentation() {
                if key.starts(with: prefix), let v = value as? Int {
                    events[key] = v
                }
            }
            return events
        }
    }
    
    var loggedDuration: [String: Double] {
        var events = [String: Double]()
        for (key, value) in logger.dictionaryRepresentation() {
            if key.starts(with: prefix), let v = value as? Double {
                events[key] = v
            }
        }
        return events
    }
}

public class LogData {
    
    static func newSimpleEvent(event: SimpleEvents) {
        LogInUserDefaults.handler.incrementEventCounter(eventName: event.name)
    }
    static func addMail(mail: MailRecord, received: Bool) {
        let enc = mail.encryptionState
        let sig = mail.signatureState
        addMail(enc: enc, sig: sig, received: received)
    }
 
    
    static func addMail(enc: EncryptionState, sig: SignatureState, received: Bool) {
        let key = LogData.makeMailKey(enc: enc, sig: sig, received: received)
        LogInUserDefaults.handler.incrementEventCounter(eventName: key)
    }
    
    private static func makeMailKey(enc: EncryptionState, sig: SignatureState, received: Bool) -> String {
        var key = "\(enc.loggingTag)&\(sig.loggingTag)"
        if received {
            key = "in|" + key
        }
        else {
            key = "out|"+key
        }
        return key
    }
    
    static func newVerification(successful: Bool?) {
        var event = "verification"
        if let successful = successful {
            if successful {
                event = "successful|"+event
            }
            else {
                event = "failed|"+event
            }
        }
        else {
            event = "unknown|"+event
        }
       
        LogInUserDefaults.handler.incrementEventCounter(eventName: event)
    }
    
    static func importKey(keyType: KeyType, transferType: TransferType, known: Bool, successful: Bool) {
        var new = "strange"
        if known {
            new = "known"
        }
        LogInUserDefaults.handler.incrementEventCounter(eventName: "importKey|\(keyType.name)&\(transferType.name)&\(new)&\(successful)")
    }
    
    static func exportKey(keyType: KeyType, transferType: TransferType) {
        LogInUserDefaults.handler.incrementEventCounter(eventName: "exportKey|\(keyType.name)&\(transferType.name)")
    }

    
    static func warning(type: WarningType, reaction: UserWarningReaction) {
        let name = "Warning|\(type.name)&\(reaction.name)"
        LogInUserDefaults.handler.incrementEventCounter(eventName: name)
    }
    
    static func warning(type: WarningType) {
        let name = "Warning|\(type.name)"
        LogInUserDefaults.handler.incrementEventCounter(eventName: name)
    }
    
    static func clickOnIndicator(type: IndicatorButton, userReaction: UserInteractionPopUp, duration: Double) {
        let name = "clickIndicator:\(type.name)&\(userReaction)"
        LogInUserDefaults.handler.incrementEventCounter(eventName: name)
        LogInUserDefaults.handler.calculateDuration(eventName: name, duration: duration)
    }
    
    static func viewOnboardingSlide(type: IntroState, duration: Double) {
        //LogInUserDefaults.handler.incrementEventCounter(eventName: type.name)
        //LogInUserDefaults.handler.calculateDuration(eventName: type.name, duration: duration)
    }
    
    enum SimpleEvents {
        case newSession, keyDetailsView, invationMail, loginAttempt, pasteKeyView
        
        var name: String {
            get {
                switch self {
                case .newSession:
                    return "newSession"
                case .keyDetailsView:
                    return "keyDetailsView"
                case .invationMail:
                    return "inviation"
                case .loginAttempt:
                    return "loginAttempt"
                case .pasteKeyView:
                    return "pasteKey"
                }
            }
        }
    }
    
    enum KeyType {
        case publicKey, secretKey
        
        var name: String {
            switch self {
            case .publicKey:
                return "PublicKey"
            case .secretKey:
                return "SecretKey"
            }
        }
    }
    
    enum TransferType {
        case autocrypt, mail, iTunes, qr, generated, paste
        
        var name: String {
            switch self {
            case .autocrypt:
                return "autocrypt"
            case .mail:
                return "mail"
            case .iTunes:
                return "iTunes"
            case .qr:
                return "QR-Code"
            case .generated:
                return "generated"
            case .paste:
                return "paste"
            }
        }
    }
    
    enum WarningType {
        case newKey, notConfidential, error, none, unableToDecrypt, unableToDecryptTravel, deletedWhileTravel
        var name: String {
            get {
                switch self {
                case .newKey:
                    return "newKey"
                case .notConfidential:
                    return "notConfidential"
                case .error:
                    return "error"
                case .none:
                    return "none"
                case .unableToDecrypt:
                    return "unableToDecrypt"
                case .unableToDecryptTravel:
                    return "unableToDecryptTravel"
                case .deletedWhileTravel:
                    return "deletedWhileTravel"
                }
            }
        }
    }
    
    enum UserWarningReaction {
        case ignore, requestConfirmation, clickButton
        
        var name: String {
            get {
                switch self {
                case .ignore:
                    return "ignore"
                case .requestConfirmation:
                    return "requestConfirmation"
                case .clickButton:
                    return "clickButton"
                }
            }
        }
    }
    
    enum IndicatorButton {
        case confidential, signedOnly, encryptedOnly, notConfidential, unableToDecrypt, error
        
        var name: String {
            get {
                switch self {
                case .confidential:
                    return "confidential"
                case .signedOnly:
                    return "signedOnly"
                case . encryptedOnly:
                    return "encryptedOnly"
                case .notConfidential:
                    return "NotConfidential"
                case .unableToDecrypt:
                    return "unableToDecrypt"
                case .error:
                    return "error"
                }
            }
        }
    }
    
    enum UserInteractionPopUp {
        case close, inviteOther, disableConf, enableConf, exportKey, moreInfo, travelFollowUp, EnableEnforceConf, DisableEnforceConf
        
        var name: String {
            get {
                switch self {
                case .close:
                    return "close"
                case .disableConf:
                    return "disableConf"
                case .enableConf:
                    return "enableConfAgain"
                case .inviteOther:
                    return "invite"
                case .exportKey:
                    return "exportKey"
                case .moreInfo:
                    return "moreInformation"
                case .travelFollowUp:
                    return "travelFollowUp"
                case .EnableEnforceConf:
                    return "enforceConf"
                case .DisableEnforceConf:
                    return "turnEnforceConfOff"
                    
                }
            }
        }
    }
}

/*
 Outgoing data:
 reads stored values and
 calculates other data
 send data
 */
public class Maildata: Encodable {
    let studyID = StudySettings.studyID
    var studySetting: String {
        get {
            let symbol = StudySettings.securityIndicator.rawValue
            return "studyparameter|\(symbol)"
        }
    }
    var AddresseswithoutKeys: Int = 0
    var addressesWithKeys: [Int: Int] = [:]
    let storedDurations = LogInUserDefaults.handler.loggedDuration

    
    
    init() {
        self.collectPublicKeyInfos()
    }
    
    
    private func collectPublicKeyInfos() {
        /* TODO
        let handler = DataHandler.handler
        let numOfNoKeyAddr = handler.findMailAddress(withKey: false).count
        let numOfKeyAddr = handler.findMailAddress(withKey: true)
        var numOfKeysPerAddr = [Int: Int] ()
        for addr in numOfKeyAddr {
            let counterKeys = addr.publicKeys.count
            let before = numOfKeysPerAddr[counterKeys] ?? 0
            numOfKeysPerAddr[counterKeys] = before + 1
        }
        AddresseswithoutKeys = numOfNoKeyAddr
        addressesWithKeys = numOfKeysPerAddr
 */
    }
}
