//
//  TempAttachment.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 15.01.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation

//TODO: avoid capslock names, since it is not swifty
enum MIMETYPE: String {
    case PDF = "pdf"
    case IMAGE = "image"
    case travelRepeal = "application/x.travel.repeal"
    case travelUse = "application/x.travel.use"
    case autocryptSetup = "application/autocrypt-setup"
    
    static func findMIMETYPE(attachment: MCOAttachment) -> MIMETYPE? {
        switch attachment.mimeType{
        case "application/pdf", "pdf":
            return MIMETYPE.PDF
        case "image",
             "image/png",
             "image/jpg",
             "image/jpeg":
            return MIMETYPE.IMAGE
        case MIMETYPE.travelRepeal.rawValue:
            return MIMETYPE.travelRepeal
        case MIMETYPE.travelUse.rawValue:
            return MIMETYPE.travelUse
        case MIMETYPE.autocryptSetup.rawValue:
            return MIMETYPE.autocryptSetup
        default:
            return nil
        }
    }
}

class TempAttachment: Hashable {
    
    var name: String
    var data: Data
    var mimeType: MIMETYPE
    
    var encState: EncryptionState
    var sigState: SignatureState
    
    init(name: String, data: Data, mimeType: MIMETYPE, encState: EncryptionState = EncryptionState.NoEncryption, sigState: SignatureState = SignatureState.NoSignature) {
        self.name = name
        self.data = data
        self.mimeType = mimeType
        self.encState = encState
        self.sigState = sigState
    }
    
    func store(mail: MailRecord) {
        
    }
    
    static func == (lhs: TempAttachment, rhs: TempAttachment) -> Bool {
        return lhs.name == rhs.name && lhs.mimeType == rhs.mimeType
    }
    
    func equal(at: AttachmentRecord) -> Bool {
        if let n = at.name, let t = at.mimeType {
            return name == n && mimeType == MIMETYPE.init(rawValue: t)
        }
        return false
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine((name+mimeType.rawValue).hashValue)
    }
}
