//
//  DebugSettings.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 28.09.17.
//  Copyright © 2018 fu-berlin.
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

private let pgp = SwiftPGP()
private let datahandler = PersistentDataProvider.dataProvider


let SUPPORT_MAIL_ADR = "letterbox@inf.fu-berlin.de"
let LOGGING_MAIL_ADR = "letterbox-reports@inf.fu-berlin.de"

func setupStudyPublicKeys() {
    let logging_pk = "logging_pk"
    let support_pk = "support_pk"
    let keys = ["noreply@bitcoin.de": "bitcoinde", "letterbox@zedat.fu-berlin.de": support_pk, "letterbox-hilfe@inf.fu-berlin.de": support_pk, "enzevalos@inf.fu-berlin.de": support_pk, SUPPORT_MAIL_ADR: support_pk, LOGGING_MAIL_ADR: logging_pk]
    importPublicKeyDic(keys: keys, type: "asc")
    //TODO datahandler.save(during: "init study keys")
}


private func userdefaults(defaults: [Attribute: AnyObject?]){
    for (att, value) in defaults{
        UserManager.storeUserValue(value, attribute: att)
    }
}

private func web(name: String, pw: String) -> [Attribute: AnyObject?] {
    return [.accountname: name as AnyObject?, .accountname: name as Optional<AnyObject>, .userAddr: name + "@web.de" as Optional<AnyObject>, .userPW: pw as Optional<AnyObject>, .smtpHostname: "smtp.web.de" as Optional<AnyObject>, .smtpPort: 587 as Optional<AnyObject>, .smtpConnectionType: MCOConnectionType.startTLS.rawValue as AnyObject?, .smtpAuthType: MCOAuthType.saslPlain.rawValue as AnyObject?, .imapHostname: "imap.web.de" as Optional<AnyObject>, .imapPort: 993 as AnyObject?, .imapConnectionType: MCOConnectionType.TLS.rawValue as AnyObject?, .imapAuthType: MCOAuthType.saslPlain.rawValue as AnyObject?]
}

private func enzevalos(name: String, pw: String) -> [Attribute: AnyObject?] {
    return [.accountname: name as AnyObject?, .accountname: name as Optional<AnyObject>, .userAddr: name + "@enzevalos.de" as Optional<AnyObject>, .userPW: pw as Optional<AnyObject>, .smtpHostname: "mail.enzevalos.de" as Optional<AnyObject>, .smtpPort: 465 as Optional<AnyObject>, .smtpConnectionType: MCOConnectionType.TLS.rawValue as AnyObject?, .smtpAuthType: MCOAuthType.saslPlain.rawValue as AnyObject?, .imapHostname: "mail.enzevalos.de" as Optional<AnyObject>, .imapPort: 993 as AnyObject?, .imapConnectionType: MCOConnectionType.TLS.rawValue as AnyObject?, .imapAuthType: MCOAuthType.saslPlain.rawValue as AnyObject?]
}


private func importPublicKeyDic(keys: [String: String], type: String) {
    for (adr, file) in keys {
        importPublicKey(file: file, type: type, adr: adr)
    }
}

private func importPublicKey(file: String, type: String, adr: String) {
    if let path = Bundle.main.path(forResource: file, ofType: type) {
        do {
            let ids = try pgp.importKeysFromFile(file: path, pw: nil)
            var pubKeyProperties = [PublicKeyProperties]()
            for id in ids {
                pubKeyProperties.append(PublicKeyProperties(fingerprint: id, cryptoProtocol: .PGP, origin: .FileTransfer, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: nil, usedAddresses: [AddressProperties(email: adr)]))
            }
            PersistentDataProvider.dataProvider.importNewData(from: pubKeyProperties, completionHandler: {error in print("Import public keys: \(pubKeyProperties.count)")})
        } catch _ {

        }
    }
}
