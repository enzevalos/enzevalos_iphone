//
//  SearchFilters.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 09.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import Foundation

///some possibilities to filter for specific mails
class SearchFilters {

///search in sender names
static let Sender:SearchFilter.Filter={eMail, searchText in
    containsSearchTerms(content: eMail.sender.displayname, searchTerm: searchText) ||
        containsSearchTerms(content: eMail.sender.email, searchTerm: searchText) ||
        eMail.addresses.filter({containsSearchTerms(content: $0.email, searchTerm: searchText)}).count > 0
}

///search the subjects
    static let Subject:SearchFilter.Filter={eMail, searchText in containsSearchTerms(content: eMail.subject, searchTerm: searchText)}

    static let Body:SearchFilter.Filter={eMail, searchText in containsSearchTerms(content: eMail.subject, searchTerm: searchText)}

static let AllStandarts:SearchFilter.Filter={eMail, searchText in
    Sender(eMail,searchText) || Subject(eMail,searchText) || Body(eMail,searchText)
}

///search in the attachments
static let Filename:SearchFilter.Filter={eMail, searchText in
    for attachment in eMail.displayAttachments {
        if attachment.myName.contains(searchText){
            return true
        }
    }
    return false
}

}
