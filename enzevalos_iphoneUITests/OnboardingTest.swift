//
//  OnboardingTest.swift
//  enzevalos_iphoneUITests
//
//  Created by Oliver Wiese on 21.05.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import XCTest
// Deprecated!

class OnboardingTest: XCTestCase {
    var app: XCUIApplication!
    let introTextName = "OnboardSubTextAccessibilityIdentifier"
    let timer: TimeInterval = 40

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        app = XCUIApplication()
        app.launch()
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    
    func testWrongName(){
        let accounts = OnboardingTest.loadAccounts()
        guard let account = accounts.first?.value else {
            XCTFail("No Accounts...")
            return
        }
        // Scroll intro
        app.otherElements.containing(.pageIndicator, identifier:"page 1 of 5").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.swipeLeft()
        app.staticTexts[introTextName].swipeLeft()
        app.staticTexts[introTextName].swipeLeft()
        app.staticTexts[introTextName].swipeLeft()
        enterSimpleAccountData(adr: "test@exampleDasdasd.org", pw: "noPassword")
        app.buttons["OnboardActionButtonAccessibilityIdentifier"].tap()
        var exist = app.textFields["Address"].waitForExistence(timeout: timer)
        var goToDetail = false
        if !exist {
            goToDetail = app.staticTexts["Too bad"].waitForExistence(timeout: timer)
            if !goToDetail {
                exist = app.textFields["Address"].waitForExistence(timeout: timer)
            }
        }
        if exist {
            app.textFields["Address"].tap()
            app.textFields["Address"].clear()
            app.secureTextFields["Password"].tap()
            app.secureTextFields["Password"].clear()
            enterSimpleAccountData(adr: account.adr, pw: account.pw)
            app.buttons["OnboardActionButtonAccessibilityIdentifier"].tap()
            let finish = app.staticTexts["Access contacts"].waitForExistence(timeout: 2*timer)
            XCTAssert(finish)
        }
    }
    
    
    func testDetailOnboarding() {
        let accounts = OnboardingTest.loadAccounts()
        XCTAssertGreaterThan(accounts.count, 0)
        var succ = 0
        for (_, account) in accounts {
            setUp()
            let result = runDetailOnboarding(adr: account.adr, pw: account.pw, hostIMAP: account.hostIMAP, portIMAP: account.portIMAP, conTypeIMAP: account.conTypeIMAP, hostSMTP: account.hostSMTP, portSMTP: account.portSMTP, conTypeSMTP: account.conTypeSMTP)
            if !result {
                print("#################\nOnboarding failed!\n\(account.adr)\n#################")
            }
            else {
                succ = succ + 1
            }
            XCTAssertTrue(result)
            tearDown()
        }
        XCTAssertEqual(succ, accounts.count)
    }
    
    func testMultipleAttempts() {
        let accounts = OnboardingTest.loadSimpleAccounts()
        guard let account = accounts["1"] else {
            XCTFail("No Accounts...")
            return
        }
        testSimpleAccount(account: account, wrongAttempts: 1)
        tearDown()
        setUp()
        testSimpleAccount(account: account, wrongAttempts: 2)
        tearDown()
    }
    
    func testSimpleAccounts(){
        let accounts = OnboardingTest.loadSimpleAccounts()
        for (_, account) in accounts {
            setUp()
            testSimpleAccount(account: account, wrongAttempts: 0)
            tearDown()
        }
    }
    
    func testOneSimpleAccount(){
        let accounts = OnboardingTest.loadSimpleAccounts()
        if let account = accounts["3"] {
            setUp()
            testSimpleAccount(account: account, wrongAttempts: 0)
            tearDown()
        }
        else {
            XCTFail("NO ACCOUNT")
        }
    }
    
    private func enterSimpleAccountData(adr: String, pw: String) {
        let adrField = app.textFields["Address"]
        adrField.tap()
        adrField.typeText(adr)
        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(pw)
        passwordSecureTextField.tap()
    }
    
    private func runDetailOnboarding (adr: String, pw: String, hostIMAP: String, portIMAP: String, conTypeIMAP: String, hostSMTP: String, portSMTP: String, conTypeSMTP: String) -> Bool{
        // Scroll intro
        app.otherElements.containing(.pageIndicator, identifier:"page 1 of 5").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.swipeLeft()
        app.staticTexts[introTextName].swipeLeft()
        app.staticTexts[introTextName].swipeLeft()
        app.staticTexts[introTextName].swipeLeft()
        // Make wrong attempts....
        var i = 0
        var goToDetail = false
        while !goToDetail{
            if app.textFields["Address"].waitForExistence(timeout: timer) {
                app.textFields["Address"].tap()
                app.textFields["Address"].clear()
                app.secureTextFields["Password"].tap()
                app.secureTextFields["Password"].clear()
                enterSimpleAccountData(adr: adr, pw: "wrongPassword")
                app.buttons["OnboardActionButtonAccessibilityIdentifier"].tap()
                //app.secureTextFields["Password"].swipeLeft()
                var exist = app.textFields["Address"].waitForExistence(timeout: timer)
                if !exist {
                    goToDetail = app.staticTexts["Too bad"].waitForExistence(timeout: timer)
                    if !goToDetail {
                        exist = app.textFields["Address"].waitForExistence(timeout: timer)
                    }
                }
                XCTAssertTrue(exist || goToDetail)
                XCTAssertLessThan(i, 3)
                i += 1
            }
            else {
                app.staticTexts[introTextName].swipeLeft()
                app.staticTexts[introTextName].swipeLeft()
                app.staticTexts[introTextName].swipeLeft()
            }
        }
        // but we failed...
        let exist = app.staticTexts["Too bad"].waitForExistence(timeout: timer)
        XCTAssertTrue(exist)
        // ... try detail onboarding
        enterDetailAccountData(adr: adr, pw: pw, hostIMAP: hostIMAP, portIMAP: portIMAP, conTypeIMAP: conTypeIMAP, hostSMTP: hostSMTP, portSMTP: portSMTP, conTypeSMTP: conTypeSMTP)
        
        // But detail onboarding should work!
        let finish = app.staticTexts["Access contacts"].waitForExistence(timeout: 2*timer)
        return finish
    }
    
    private func enterDetailAccountData(adr: String, pw: String, hostIMAP: String, portIMAP: String, conTypeIMAP: String, hostSMTP: String, portSMTP: String, conTypeSMTP: String) {
        
        let onboardinputviewaccessibilityidentifierElement = app.otherElements["OnboardInputViewAccessibilityIdentifier"]
        let onboardactionbuttonaccessibilityidentifierButton = app/*@START_MENU_TOKEN@*/.buttons["next"]/*[[".buttons[\"next\"]",".buttons[\"OnboardActionButtonAccessibilityIdentifier\"]"],[[[-1,1],[-1,0]]],[1]]@END_MENU_TOKEN@*/

        app.otherElements.containing(.pageIndicator, identifier:"page 1 of 8").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.swipeLeft()
        
        // Enter adr and pw:
        let adrField = app/*@START_MENU_TOKEN@*/.textFields["Address"]/*[[".otherElements[\"OnboardInputViewAccessibilityIdentifier\"].textFields[\"Address\"]",".textFields[\"Address\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        let pwField = app.secureTextFields["Password"]

        adrField.tap()
        adrField.clear()
        adrField.typeText(adr)
        pwField.tap()
        pwField.clear()
        pwField.typeText(pw)
        
        // Enter username:
        app.otherElements.containing(.pageIndicator, identifier:"page 2 of 8").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.swipeLeft()
        let userField = app.textFields.allElementsBoundByIndex[0]
        userField.tap()
        userField.clear()
        userField.typeText(adr)
        
        
        // Enter IMAP
        app.otherElements.containing(.pageIndicator, identifier:"page 3 of 8").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.swipeLeft()
        
        let textField = onboardinputviewaccessibilityidentifierElement.children(matching: .textField).element(boundBy: 0)
        textField.tap()
        textField.clear()
        textField.typeText(hostIMAP)
        
        let textField2 = onboardinputviewaccessibilityidentifierElement.children(matching: .textField).element(boundBy: 1)
        textField2.tap()
        textField2.clear()
        textField2.typeText(portIMAP)
        
        app.otherElements.containing(.pageIndicator, identifier:"page 4 of 8").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.swipeLeft()
        let tlsSslPickerWheel = app.pickerWheels.allElementsBoundByIndex[0]
        tlsSslPickerWheel.adjust(toPickerWheelValue: conTypeIMAP)
        tlsSslPickerWheel.swipeLeft()
        
        // Enter SMTP:
        textField.tap()
        textField.clear()
        textField.typeText(hostSMTP)
        
        textField2.tap()
        textField2.clear()
        textField2.typeText(portSMTP)
        
        app.otherElements.containing(.pageIndicator, identifier:"page 6 of 8").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.swipeLeft()
        let picker = app.pickerWheels.allElementsBoundByIndex[0]
        picker.adjust(toPickerWheelValue: conTypeSMTP)
        
        // confirm input
        onboardinputviewaccessibilityidentifierElement.swipeLeft()
        onboardactionbuttonaccessibilityidentifierButton.tap()
    }
    
    private func testSimpleAccount(account: (name: String, pw: String), wrongAttempts: Int, recursiv: Bool = false){
        if !recursiv {
            app.otherElements.containing(.pageIndicator, identifier:"page 1 of 5").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.swipeLeft()
            app.staticTexts[introTextName].swipeLeft()
            app.staticTexts[introTextName].swipeLeft()
            app.staticTexts[introTextName].swipeLeft()
        } else {
            app.textFields["Address"].tap()
            app.textFields["Address"].clear()
            app.secureTextFields["Password"].tap()
            app.secureTextFields["Password"].clear()
        }
        var pw = account.pw
        if wrongAttempts > 0 {
            pw = "wrongPassword"
        }
        enterSimpleAccountData(adr: account.name, pw: pw)
        
        app.buttons["OnboardActionButtonAccessibilityIdentifier"].tap()
        if wrongAttempts == 0 {
            let x = app.buttons["OnboardActionButtonAccessibilityIdentifier"].waitForExistence(timeout: timer)
            XCTAssertTrue(x)
        }
        else {
            let x = app.textFields["Address"].waitForExistence(timeout: timer)
            XCTAssertTrue(x)
            testSimpleAccount(account: account, wrongAttempts: wrongAttempts - 1, recursiv: true)
        }
    }
    
    private static func loadSimpleAccounts() -> [String:(name: String, pw: String)] {
        let acc = loadAccounts()
        var dic: [String:(name: String, pw: String)] = [:]
        acc.forEach{dic[$0.key] = ($0.value.adr, $0.value.pw)}
        return dic
    }
    
    private static func loadAccounts() -> [String:(adr: String, pw: String, hostIMAP: String, portIMAP: String, conTypeIMAP: String, hostSMTP: String, portSMTP: String, conTypeSMTP: String)] {
        let bundle = Bundle(for: self)
        var newAccounts = [String:(adr: String, pw: String, hostIMAP: String, portIMAP: String, conTypeIMAP: String, hostSMTP: String, portSMTP: String, conTypeSMTP: String)]()
        guard let url = bundle.url(forResource: "accounts", withExtension: "json"), let data = try? Data(contentsOf: url),  let jsonDic = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)  else {
            XCTFail("No file!")
            return [:]
        }
        if let dic = jsonDic as? Dictionary<String, Any>{
            if let accounts = dic["accounts"], let array = accounts as? Array<Any> {
                for elem in array {
                    if let account = elem as? Dictionary<String, String> {
                        if let id = account["id"], let username = account["username"] , let pw = account["password"], let hostIMAP = account["hostIMAP"], let portIMAP = account["portIMAP"], let conTypeIMAP = account["conTypeIMAP"], let hostSMTP = account["hostSMTP"], let portSMTP = account["portSMTP"], let conTypeSMTP = account["conTypeSMTP"]{
                            newAccounts[id] = (username, pw, hostIMAP, portIMAP, conTypeIMAP, hostSMTP, portSMTP, conTypeSMTP)
                        }
                    }
                }
            }
            
        }
        XCTAssertGreaterThan(newAccounts.count, 0)
        return newAccounts
    }
}
extension String  {
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    var hasSpecialChar: Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if self.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        }
        return false
    }
}

extension XCUIElement {
    func clear(){
        guard let stringValue = self.value as? String else {
            return
        }
        var delString = String()
        for _ in stringValue {
            delString += XCUIKeyboardKey.delete.rawValue
            delString += XCUIKeyboardKey.delete.rawValue
        }
        typeText(delString)
    }
}
