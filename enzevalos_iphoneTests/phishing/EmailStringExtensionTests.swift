//
//  EmailAdressTests.swift
//  enzevalos_iphoneTests
//
//  Created by Katharina Müller on 17.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//
import XCTest
@testable import enzevalos_iphone
    
class EmailStringExtensionTests: XCTestCase {
        
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
        
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    //  General TestText for all (needs to simulate a body of text from which to extract specific Strings
    var TestText = "blabla moep jkjlkj lkjkljknmbjks llhil k. jhkhkih. huhuhj! fsdf bob.alice@zedat.fu-berlin.de dfsf. jhjknjknjkh https://git.imp.fu-berlin.de/enzevalos/enzevalos_iphone/issues/240  hjkhjkhkhn www.google.de kljhl@hjkghgkhj.com nljbjkjk.de url tag html mail <a href='https://www.w3schools.com'>Visit W3Schools</a> hjhkhiuhziu kjhkl <a href=\"https://www.w2schools.com\">Visit W2Schools</a>. lknljnlk. kmm /n lmölmpöl < a href=\"https://www.w8schools.com\">Visit W8Schools</a> gfg fghfghnhg <a      href=\"https://www.w33schools.com\">Visit W33Schools</a> nkjhjkhkjn,mn jnmnklmj j <a href=\"https://www.w22schools.com\">Visit W22Schools</ a> hghjcfgh hfgchnvhj vgjcgj cjghcj <a href=\"https://www.w99schools.com\">Visit W99Schools</a    >    你好@yahoo.com eewfve  test@莎士比亚.org    你好@莎士比亚.org  Rδοκιμή@παράδειγμα.δοκιμή or 管理员@中国互联网络信息中心.中国 你好45@yahoo.com"
        
    //    setup for find mail address in text
    var finalMailAddresses = ["bob.alice@zedat.fu-berlin.de",
                                "kljhl@hjkghgkhj.com",
                                "你好@yahoo.com",
                                "test@莎士比亚.org",
                                "你好@莎士比亚.org",
                                "Rδοκιμή@παράδειγμα.δοκιμή",
                                "管理员@中国互联网络信息中心.中国",
                                "你好45@yahoo.com"]
        
    func testFindEmails(){
        let arr = TestText.findEmails()
        XCTAssertNotNil(arr)
        XCTAssertEqual(arr[0], finalMailAddresses[0])
        XCTAssertEqual(arr[1], finalMailAddresses[1])
        XCTAssertEqual(arr[2], finalMailAddresses[2])
        XCTAssertEqual(arr[3], finalMailAddresses[3])
        XCTAssertEqual(arr[4], finalMailAddresses[4])
        XCTAssertEqual(arr[5], finalMailAddresses[5])
        XCTAssertEqual(arr[6], finalMailAddresses[6])
        XCTAssertEqual(arr[7], finalMailAddresses[7])
    }
    
    //    setup email validation
    var validEmails = ["alice.bob@moep.blubb.de",
                        "test@google.com",
                        "test@google.co.uk",
                        "你好@yahoo.com",
                        "test@莎士比亚.org",
                        "你好@莎士比亚.org",
                        "Rδοκιμή@παράδειγμα.δοκιμή",
                        "管理员@中国互联网络信息中心.中国",
                        "你好45@yahoo.com"]
        
    var notValidEmails = ["test@@google.com",
                            "test@google",
                            "Abc.example.com",
                            "A@b@c@example.com",
                            "this is\"not\\allowed@example.com",
                            "this\\ still\\\"not\\\\allowed@example.com",
                            "1234567890123456789012345678901234567890123456789012345678901234+x@example.com"]
    
    //Finding Email adresses etc.
    func testIsValidEmail(){
        for v in validEmails {
            XCTAssertTrue(v.isValidEmail())
        }
        for n in notValidEmails{
            XCTAssertFalse(n.isValidEmail())
        }
    }
        
    //    setup for mail address splitting
    var correctSplit_0 = ["alice.bob", "moep", "blubb", "de"]
    var correctSplit_1 = ["test", "google", "com"]
    var correctSplit_2 = ["test", "google", "co", "uk"]
    var correctSplit_3 = ["你好", "yahoo", "com"]
    var correctSplit_4 = ["test", "莎士比亚", "org"]
    var correctSplit_5 = ["你好", "莎士比亚", "org"]
    var correctSplit_6 = ["Rδοκιμή", "παράδειγμα", "δοκιμή"]
    var correctSplit_7 = ["管理员", "中国互联网络信息中心", "中国"]
    var correctSplit_8 = ["你好45", "yahoo", "com"]
    var spliEmails:[Any] {
        return [correctSplit_0, correctSplit_1, correctSplit_2, correctSplit_3, correctSplit_4, correctSplit_5, correctSplit_6, correctSplit_7, correctSplit_8]
    }
        
    func testSplitMailAddress(){
        for (i, email) in validEmails.enumerated(){
            let arr = (email.splitAddress())
            XCTAssertNotNil(arr)
            for (j, _) in arr.enumerated()
            {
                let array = spliEmails[i] as? [String]
                XCTAssertEqual(arr[j], array?[j])
            }
        }
    }
        
    var finalTestEMailIdentity = ["alice.bob", "test", "test", "你好", "test", "你好", "Rδοκιμή", "管理员", "你好45"]
    func testGetMailIdentity(){
        for (i, email) in validEmails.enumerated(){
            let arr = (email.getLocalMailIdentity())
            XCTAssertNotNil(arr)
            XCTAssertEqual(arr, finalTestEMailIdentity[i])
        }
    }
        
    var eMailSubDomain_0 = ["moep", "blubb"]
    var eMailSubDomain_1 = ["παράδειγμα"]
    var eMailSubDomain_2 = ["中国互联网络信息中心"]
    var eMailSubDomain_3 = ["google", "co"]
    func testGetSubdomains(){
        let arr_0 = validEmails[0].getSubdomains()
        XCTAssertNotNil(arr_0)
        XCTAssertEqual(arr_0[0], eMailSubDomain_0[0])
        XCTAssertEqual(arr_0[1], eMailSubDomain_0[1])
        
        let arr_1 = validEmails[6].getSubdomains()
        XCTAssertNotNil(arr_1)
        XCTAssertEqual(arr_1[0], eMailSubDomain_1[0])
            
        let arr_2 = validEmails[7].getSubdomains()
        XCTAssertNotNil(arr_2)
        XCTAssertEqual(arr_2[0], eMailSubDomain_2[0])
        
        let arr_3 = validEmails[2].getSubdomains()
        XCTAssertNotNil(arr_3)
        XCTAssertEqual(arr_3[0], eMailSubDomain_3[0])
        XCTAssertEqual(arr_3[1], eMailSubDomain_3[1])
    }
    
    var finalTestEMailDomain = ["de", "com", "uk", "com", "org", "org", "δοκιμή", "中国", "com"]
    func testGetDomain(){
        for (i, email) in validEmails.enumerated(){
            let arr = (email.getDomain())
            XCTAssertNotNil(arr)
            XCTAssertEqual(arr, finalTestEMailDomain[i])
        }
    }
        
    func testTextFindMailAddress() {
        let arr = TestText.findMailAddress()
        XCTAssertNotNil(arr)
        XCTAssertEqual(arr[0], finalMailAddresses[0])
        XCTAssertEqual(arr[1], finalMailAddresses[1])
    }
    
    func testGetMailDomain() {
        XCTAssertEqual("test@google.com".getMailDomains(), "google")
        XCTAssertEqual("test@google.co.uk".getMailDomains(), "google.co")
    }
}

