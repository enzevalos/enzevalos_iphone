//
//  ReadMainView.swift
//  enzevalos_iphone
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

struct ReadMainView <M: DisplayMail>: View {
    @StateObject var model: ReadModel<M>
    
    var body: some View {
        TabView(selection: $model.currentTab) {
            ForEach(Tabs, id: \.id ){ tab in
                tab.content
                    .tabItem {
                        tab.image
                        Text(tab.description)
                    }
                    .tag(tab.tab)
            }
        }
        .navigationBarTitle(subject)
        .onDisappear(perform: {
            model.onDisappear()
        })
    }
    
    var subject: Text {
        Text(model.mail.subject)
    }
    
    var Tabs: [Tab] {
        get {
            return [
                Tab(
                    tab: ReadPart.Security.value,
                    image: Image(systemName: "shield"),
                    description: "Tab.Label.Security",
                    content: AnyView(SecurityBriefingView<M>()
                                .environmentObject(model))
                ),
                Tab(
                    tab: ReadPart.Header.value,
                    image: Image(systemName: "person.fill"),
                    description: "Tab.Label.Sender",
                    content: AnyView(SenderViewMain<M>()
                                .environmentObject(model))
                ),
                Tab(
                    tab: ReadPart.Body.value,
                    image: Image(systemName: "text.bubble.fill"),
                    description: "Tab.Label.Message",
                    content: AnyView(MessageViewMain<M>()
                                .environmentObject(model))
                ),
                Tab(
                    tab: ReadPart.Attachments.value,
                    image: Image(systemName: "rectangle.and.paperclip"),
                    description: "Tab.Label.Attachments",
                    content: AnyView(AttachmentsViewMain(attachments: model.mail.displayAttachments, downloadAll: true))
                )                
            ]
        }
    }
}

#if DEBUG
//struct Layout_Previews: PreviewProvider {
//    static let simulator = Simulators<ReadMainView<ProxyMail>>()
//    static let deviceNames: [String] = [
//        "iPhone SE",
//        "iPhone 11 Pro Max"
//    ]
//
//  static var previews: some View {
//        simulator.previews(view: ReadMainView(composeModel: ReadModel(mail: ProxyData.SecureMail)))
//   }
//}
#endif

