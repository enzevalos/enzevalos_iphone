//
//  StatefulPreviewWrapper.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 08.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

///Wrap ure Stateful Previews with this
struct StatefulPreviewWrapper<Value, Content: View>: View {
    @State var value: Value
    var content: (Binding<Value>) -> Content

    var body: some View {
        content($value)
    }

    init(_ value: Value, content: @escaping (Binding<Value>) -> Content) {
        self._value = State(wrappedValue: value)
        self.content = content
    }
}

struct StatefulPreviewWrapper_Previews: PreviewProvider {
    static var previews: some View {
        Text("this is only a wrapper to help show views that have a @Binding var, so no useful Preview")
    }
}
