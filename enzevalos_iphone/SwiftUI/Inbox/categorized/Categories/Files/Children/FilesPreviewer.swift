//
//  FilesPreviewer.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 22.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI
import TagNavVi


///shows all the files of each and every email
struct FilesPreviewer: View {
    
    var height: CGFloat?
    @ObservedObject var filesCategoryVM : FilesCategoryVM
    
    @State var openEmail = false
    
    var body: some View {
        ScrollView(.horizontal,showsIndicators:false){HStack{ForEach(0..<filesCategoryVM.mailsAttachments.count){ mailIndex in
            if let mailAttachments = filesCategoryVM.mailsAttachments[safe: mailIndex] {
                if filesCategoryVM.isFullscreen {
                    ScrollView(.vertical, showsIndicators:false){attachmentColumn(filesCategoryVM.mails[mailIndex],attachments:mailAttachments)}
                    
                } else {
                    attachmentColumn(filesCategoryVM.mails[mailIndex],attachments:mailAttachments)
                }
            }
        }}.padding(.horizontal)}
    }
    
    ///the attachments of a single mail 
    @ViewBuilder func attachmentColumn(_ mail: eMail, attachments:[DisplayAttachment])->some View{
        let height = filesCategoryVM.attachmentsize(attachments.count)
        VStack{
            Text(mail.subject)
                .capsuledMailOptions(mail: mail)
            ForEach(0..<attachments.count){ attachmentIndex in if let attachment = attachments[safe: attachmentIndex] {
                AttPrev(
                    attachment: attachment
                ) //TODO: make this have the exact aspect ratio ?
                .frame(width: 200).frame(height: height)
                .padding(.horizontal, 40).padding(.bottom, -25).padding(.top, -7)
            }}
        }
    }
}

struct FilesPreviewer_Previews: PreviewProvider {
    static var previews: some View {
        EmptyView()//FilesPreviewer()
    }
}
