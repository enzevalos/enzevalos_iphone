//
//  AuthenticationScreen.swift
//  enzevalos_iphone
//
//  Created by Cezary Pilaszewicz on 02.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

/*import SwiftUI

struct AuthenticationScreen: View {
    @State private var login: String = ""
    @State private var password: String = ""
    @State private var username: String = ""
    @State private var imapServer: String = ""
    @State private var imapPort: String = ""
    @State private var smtpServer: String = ""
    @State private var smtpPort: String = ""
    @State private var imapEncryption = 0
    @State private var smtpEncryption = 0

    @ObservedObject private var viewModel = AuthenticationViewModel()
    var encryptionOptions = ["Plaintext", "StartTLS", "TLS/SSL"]
    
    var body: some View {
        ZStack {
            Color.white.edgesIgnoringSafeArea(.all)
        
            VStack {
                
                Text("Please enter Your credentials").padding().foregroundColor(Color.yellow)
                Text("Login")
                TextField("Please enter your login", text: $login).textFieldStyle(RoundedBorderTextFieldStyle())
                Text("Password")
                SecureField("Please enter your password", text: $password).textFieldStyle(RoundedBorderTextFieldStyle())
                
                HStack {
                    /*Toggle(isOn: self.$viewModel.isDetailedAuthentication) {
                        Text("Advanced options")
                    }*/
                    Spacer()

                    Button(action: {self.viewModel.isDetailedAuthentication ?
                        self.viewModel.detailValidation(self.login, self.password, self.username, self.imapServer, self.imapPort, self.imapEncryption, self.smtpServer, self.smtpPort, self.smtpEncryption) :
                        self.viewModel.validate(self.login, self.password)
                    }) {
                        Text("Button")
                    }
                }
                
                /*if self.viewModel.isDetailedAuthentication {
                    Text("Username")
                    TextField("Please enter your nickname", text: $username).textFieldStyle(RoundedBorderTextFieldStyle())

                    HStack {
                        Text("Imap server")
                        TextField("e.g. imap.web.de", text: $imapServer)
                    }
                    HStack {
                        Text("Imap port")
                        TextField("e.g. 993", text:$imapPort).keyboardType(.numberPad)
                    }
                    Picker(selection: $imapEncryption, label: Text("IMAP-Transportencryption")) {
                        ForEach(0..<encryptionOptions.count) {
                            Text(self.encryptionOptions[$0])
                        }
                    }
                    HStack {
                        Text("Smtp server")
                        TextField("e.g. smtp.web.de", text: $smtpServer)
                    }
                    HStack {
                        Text("Smtp port")
                        TextField("e.g. 587", text: $smtpPort).keyboardType(.numberPad)
                    }
                
                    Picker(selection: $smtpEncryption, label: Text("SMTP-Transportencryption")) {
                        ForEach(0..<encryptionOptions.count) {
                            Text(self.encryptionOptions[$0])
                        }
                    }

                }*/
                
                Button(action: { self.viewModel.startGoogleOauth() }) {
                    Text("Google button")
                }
            }.padding()
            
            //TODO: once SWIFTUI supports optionals improve this if statement
            if self.viewModel.errorMessage != nil && !self.viewModel.errorMessage!.isEmpty {
                VStack {
                    Text(self.viewModel.errorMessage!)
                        .foregroundColor(Color.white)
                        .background(Color.red)
                    Spacer()
                }
            }
        }
    }
}


struct LoginAdvancedOptions:View{
    var body: some View {
        Text("no")
    }
}

struct LoginCredentials:View{
    var body: some View {
        VStack {
            Text("no")
        }
    }
}
*/


