//
//  WarningMessageView.swift
//  enzevalos_iphone
//
//  Created by melicoa97 on 11.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct DialogView <D: Dialog> : View {

    let option: D
    var ctaAction: (() -> Void)?
    var additionalAction: (() -> Void)?
    var dismissAction: (() -> Void)?
    var extend: Bool
    
    private let color = Color(UIColor.systemGray6)
    
    
    var body: some View {
        simpleUI
    }
      
    var simpleUI: some View {
         VStack {
            Text(option.title)
                .font(.headline)
                .lineLimit(3)
                .frame(maxWidth: .infinity)
                .padding(.vertical, 20)
                .addBorder(option.dialogColor, width: 2, cornerRadius: 30)
            Text(option.body)
                .fontWeight(.light)
                .font(.body)
                .lineLimit(10)
                .fixedSize(horizontal: false, vertical: true)
                .padding(20)
            if option.ctaButtonTitle != nil && ctaAction != nil {
                Button(action: ctaAction!) {
                    Text(option.ctaButtonTitle!)
                        .frame(maxWidth: .infinity, minHeight: 50, maxHeight: 50)
                        .background(RoundedCorners(color: .white, radius: 30))
                        .padding(.vertical, 10)
                        .padding(.horizontal, 20)
                }
            }
            ForEach(0..<option.moreButtons.count) { index in
                Button(action: {
                    if let action = self.additionalAction {
                        action()
                    }
                }) {
                    Text(self.option.moreButtons[index].title)
                      .frame(maxWidth: .infinity,  minHeight: 50, maxHeight: 50)
                        .background(RoundedCorners(color: self.color, radius: 30))
                      .padding(.vertical, 10)
                      .padding(.horizontal, 20)
               }
            }
            if option.dismissButtonTitle != nil && dismissAction != nil {
               Button(action: dismissAction!) {
                   Text(option.dismissButtonTitle!)
                    .foregroundColor(.red)
                      .frame(maxWidth: .infinity, minHeight: 50, maxHeight: 50)
                      .background(RoundedCorners(color: color, radius: 30))
                      .padding(.vertical, 10)
                      .padding(.horizontal, 20)
                }
           }
            if extend {
                Text("").padding(20)
            }
        }
        .background(RoundedCorners(color: color, radius: 30))
        .padding(10)
    }

}

struct DialogView_Previews: PreviewProvider {
    static let options  = ProxyData.SecureMail.dialog
    static var previews: some View {
        ScrollView{
            DialogView(option: options, ctaAction: action, additionalAction: action, dismissAction: action, extend: false)
            DialogView(option: DialogOption.corrupted, ctaAction: action, additionalAction: action, dismissAction: action, extend: false)
            DialogView(option: DialogOption.couldNotDecrypt, ctaAction: action, additionalAction: action, dismissAction: action, extend: false)
        }
    }
    
    static func action() {
        print("Button click!")
    }
}
 

extension View {
    public func addBorder<S>(_ content: S, width: CGFloat = 1, cornerRadius: CGFloat) -> some View where S : ShapeStyle {
        return overlay(RoundedRectangle(cornerRadius: cornerRadius).strokeBorder(content, lineWidth: width))
    }
}
