//
//  CiricleImage.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 02.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct CircleImage: View {
    var image: Image
    var radius: CGFloat = 60

    var body: some View {
        image
            .resizable()
            .frame(width: radius, height: radius)
            .shadow(radius: radius/60*5)
    }
    
    struct CircleImage_Previews: PreviewProvider {
        static var previews: some View {
            CircleImage(image: ProxyContact.makeImg("Alice", color: .red))
            
        }
    }
}
