//
//  MapView.swift
//  enzevalos_iphone
//
//  Created by Sabina on 16.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import MapKit

/**
 
    This file handles the configuration of the MapView  from the SenderViewMain.
 
 */

final class LandmarkAnnotation: NSObject, MKAnnotation {
    let id: String
    let title: String?
    let domainName: String?
    let coordinate: CLLocationCoordinate2D

    init(landmark: Landmark) {
        self.id = landmark.id
        self.title = landmark.name
        self.domainName = landmark.domain
        self.coordinate = landmark.location
    }
}

/**
     The Landmark struct is defined by three parameters.
 
        id, name, domain and location.
 
 */

struct Landmark: Equatable {
    static func ==(lhs: Landmark, rhs: Landmark) -> Bool {
        lhs.id == rhs.id
    }
    
    let id = UUID().uuidString
    let name: String
    let domain: String
    let location: CLLocationCoordinate2D
}


struct MapView: UIViewRepresentable {
    
    var landmarks: [Landmark] /// all displayed landmaeeks are stored in an array of type Landmark
    @Binding var selectedLandmark: Landmark? /// this variable stored the next landmark from the array which is selected/displayed
    @Binding var showingLandmarkDetails: Bool /// this Boolean shows more details about the selected landmark
   
    
    func makeUIView(context: Context) -> MKMapView {
        /// configuration of the UIView
        let map = MKMapView()
        map.delegate = context.coordinator
        return map
    }
    
    func updateUIView(_ uiView: MKMapView, context: Context) {
        /**
            This function gets calles whenever the values being sent into the UIViewReprensentable have changed.
         
            When a new landmark is selected the updateAnnotation and the addPolyline functions are called again in order to update the annotations in the MapView and add a polyline between them.
         */
        updateAnnotations(from: uiView)
        addPolyline(from: uiView)
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
   
    final class Coordinator: NSObject, MKMapViewDelegate {
        
        var parent: MapView

        init(_ parent: MapView) {
            self.parent = parent
        }
        
        
        func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
            /// this method configures the zoom level when the next landmark/annotation is selcted
            guard let coordinates = view.annotation?.coordinate else {
                return
            }
            // https://stackoverflow.com/questions/4189621/setting-the-zoom-level-for-a-mkmapview
            let span = mapView.region.span
            let region = MKCoordinateRegion(center: coordinates, span: span)
            mapView.setRegion(region, animated: true)
            mapView.setZoomByDelta(delta: 10, animated: false)
        }
        
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            /**
                This method uses a custom view to representan annotation, reuses views for performance and has a button that can be tapped for more information.
             */
            guard let annotation = annotation as? LandmarkAnnotation else { return nil }
            let identifier = "Annotation"
            var annotationView: MKMarkerAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView
            if annotationView == nil {
                annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        
        func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
            
            /**
             
                This method gets called when the button for more details is tapped.
             
                It checks if there is a selected landmark and if so, it uses it to set the selected landmark porperty to the parent MapView with the help of the getLandMarkByCoordinates method.
             
                Also the showingLandmarkDetails is set to true, which will trigger the alert in the SenderViewMain.
             
             */
            
            guard let placemark = view.annotation as? LandmarkAnnotation?/*MKPointAnnotation?*/ else { print("failed conversion") ;return }
            parent.selectedLandmark = getLandMarkByCoordinates(placemark!.coordinate)
            parent.showingLandmarkDetails = true
        }  
        
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            /// This function creates and configures a polyline
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = UIColor.orange
            renderer.lineWidth = 3
            return renderer
        }
        
        func getLandMarkByCoordinates(_ coords:CLLocationCoordinate2D)->Landmark{
            
            func equals(_ lm1:CLLocationCoordinate2D,_ lm2:CLLocationCoordinate2D)->Bool{
                return lm1.latitude==lm2.latitude&&lm1.longitude==lm2.longitude
            }
            
            let marks:[Landmark]=parent.landmarks
            var i:Int=0
            while !equals(marks[i].location,coords) {
                i+=1
            }
            return marks[i]
        }
        
    }
    
    private func updateAnnotations(from mapView: MKMapView) {
        /// this method updates the displayed annotations in the MapView according to the selected landmark
        mapView.removeAnnotations(mapView.annotations)
        let newAnnotations = landmarks.map { LandmarkAnnotation(landmark: $0) }
        mapView.addAnnotations(newAnnotations)
        if let selectedAnnotation = newAnnotations.filter({ $0.id == selectedLandmark?.id }).first {
            mapView.selectAnnotation(selectedAnnotation, animated: true)
        }
    }
    
    private func addPolyline(from mapView: MKMapView){
        /// this method adds a polyline between the landmarks in the MapView
        var locations = landmarks.map { $0.location }
        let polyline = MKPolyline (coordinates: &locations, count: locations.count)
        mapView.addOverlay(polyline)
    }
}

extension MKMapView {

       // delta is the zoom factor
       // 2 will zoom out x2
       // .5 will zoom in by x2

       func setZoomByDelta(delta: Double, animated: Bool) {
           var _region = region;
           var _span = region.span;
           _span.latitudeDelta *= delta;
           _span.longitudeDelta *= delta;
           _region.span = _span;

           setRegion(_region, animated: animated)
       }
   }
