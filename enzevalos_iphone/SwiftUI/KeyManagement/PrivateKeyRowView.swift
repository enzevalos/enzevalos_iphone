//
//  PrivateKeyRowView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.12.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct KeyRowView: View {
    
    public var key: DisplayKey
    var body: some View {
        /**
         Name,  Favorite,  Datum
         */
        HStack {
            VStack {
                // TODO: Add an icon (PGP vs SMIME or a fingerprint or avatar ...
                Text(key.keyID)
                HStack{
                    Text("ContactView.KnownSince")
                        .font(.caption)
                        .foregroundColor(.gray)
                    Text(key.discovery?.dateToString ?? "???")
                        .font(.caption)
                        .foregroundColor(.gray)
                }
            }
            Spacer()
            if key.isPreferedKey {
                Image(systemName: "star.fill")
                    .imageScale(.medium)
                    .foregroundColor(.yellow)

            }
        }
        .padding()
    }
}

struct PrivateKeyRowView_Previews: PreviewProvider {
    static var previews: some View {
        KeyRowView(key: ProxyPublicKey(keyID: "232312323", discovery: nil, lastSeen: nil, type: .PGP, signedMailsCounter: 0, sentMailsCounter: 0))
    }
}
