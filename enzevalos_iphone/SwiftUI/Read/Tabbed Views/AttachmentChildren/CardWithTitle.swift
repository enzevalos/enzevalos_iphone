//
//  CardWithTitle.swift
//  enzevalos_iphone
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

// MARK: a CardView with title
struct CardV<Content:View>: View{
    var elevation:CGFloat = 3
    var title:String?
    
    var actions:[AnyView]?
    var actionsVertical:Bool=false
    
    var content: () -> Content
    
    @State private var isAdvancedVisable:Bool = false
    @State private var isTapDownB:Bool = false
    
    
    private let colo=Color(red:0.85,green:0.85,blue:0.88)
    
    var body: some View{
        ZStack {
            VStack{
                VStack{
                    content()
                    .background(Color.white)
                    .clipShape(
                        RoundedRectangle(cornerRadius: 15)
                    )
                    .padding(3)
                    .opacity(isTapDownB ? 0.7 : 1)
                    if title != nil {
                        Text(title!).padding(7)
                    }
                }
                .background(isTapDownB ? colo : Color.white)
                .clipShape(
                    RoundedRectangle(cornerRadius: 15)
                )
                .gesture (
                    TapGesture()
                        .onEnded{ _ in
                            self.isTapDownB.toggle()
                            self.isAdvancedVisable.toggle()
                        }
                )
                
                if isAdvancedVisable{
                    Advanced.padding(.horizontal,10)
                }
            }
            .shadow(color: isTapDownB ? Color.clear : colo , radius: 2*elevation, x: elevation, y:elevation)
        }
        .padding(.leading, 10)
        .padding(2*elevation)
        .padding(.bottom,isTapDownB ? elevation : 2*elevation)
        .animation(.easeIn(duration: 0.2))
        .scaleEffect(isTapDownB ? 0.82 : 1)
        .opacity(isTapDownB ? 0.9 : 1)
    }
    
    private func Actions(_ nr:Int)-> some View{
        self.actions![nr]
            .padding(10)
            .background(Color.white)
            .clipShape(
                RoundedRectangle(cornerRadius: 15)
            )
            .shadow(color: colo , radius: 2*elevation, x: elevation, y:elevation)
    }
    
    //the button section beneath
    private var Advanced: AnyView{
        if actions != nil{
            if actionsVertical{
                return AnyView(VStack{
                    ForEach(0..<actions!.count){ i in
                        self.Actions(i)
                    }
                })
            }else{
                return AnyView(HStack{
                    ForEach(0..<actions!.count){ i in
                        self.Actions(i)
                    }
                })
            }
        }
        return AnyView(EmptyView())
    }

}

