//
//  Bubble.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 23.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI


///shows a single message in a chat (todo needs some refinement)
struct ChatMessageView : View {
    
    var checkedChatMessage: eMail
    var body: some View {
        HStack(alignment: .bottom, spacing: 15) {
            if !checkedChatMessage.sender.isUser{
                checkedChatMessage.sender.avatar
                .resizable()
                .frame(width: 40, height: 40, alignment: .center)
                .cornerRadius(20)
            } else {
                Spacer()
            }
            Bubble(contentChatMessage: checkedChatMessage.body, isCurrentChatUser: checkedChatMessage.sender.isUser)
                .capsuledMailOptions(mail: checkedChatMessage)
        }
    }
}

struct Bubble: View {
    var contentChatMessage: String
    var isCurrentChatUser: Bool
    
    var body: some View {
        Text(contentChatMessage)
            .padding(10)
            .foregroundColor(isCurrentChatUser ? Color.white : Color.black)
            .background(isCurrentChatUser ? Color.blue : Color(UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)))
            .cornerRadius(10)
    }
}

struct Bubble_Previews: PreviewProvider {
    static var previews: some View {
        Bubble(contentChatMessage: "Hi, I am your friend", isCurrentChatUser: false)
    }
}

