//
//  ContentView.swift
//  enzevalos_iphone
//
//  Created by Fatima on 19/03/2020.
//  Copyright © 2020 fu-berlin. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
        
        The view displays Phishing information in a VStack and uses a list to present details.
 
 
 */

struct AllPhishingContent: Identifiable  {
    let id: Int
    let name: String
    let message: String
}

struct PhishingView : View {
    /// This content stores the phishing information in a list
  var PhishingList = [
    AllPhishingContent(id: 0, name: "no declaration", message: "content empty"),
    AllPhishingContent(id: 1, name: "no declaration", message: "content empty"),
    AllPhishingContent(id: 2, name: "no declaration", message:  "content empty"),
    AllPhishingContent(id: 3, name: "no declaration", message:  "content empty"),
  ]

    var body: some View {
        List(PhishingList) { AllPhishingContent in
            VStack {
                Text("Identification").bold()
                Text(AllPhishingContent.name)
                Text(AllPhishingContent.message).italic()
            }
        }
    }
}



struct PhishingList_Previews: PreviewProvider {
    static var previews: some View {
       PhishingView()
    }
}

