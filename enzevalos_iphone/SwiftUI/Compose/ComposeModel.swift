//
//  ComposeModel.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 12.11.20.
//  Modified by Chris Offner & Claire Bräuer in March 2021.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import Foundation

/// A model used to compose and send an email.
class ComposeModel: ObservableObject {
    @Published var subject = ""
    @Published var body = ""
    @Published var attachments: [Attachment] = []
    @Published var encryptionOn = true
    @Published var recipientsModel: RecipientsModel = RecipientsModel()
    // properties for the attachment check/reminder
    @Published var showAttachmentAlert = false
    @Published var resumeSend = true
    
    init(preData: PreMailData?) {
        if let preData = preData {
            subject = preData.subject
            body = preData.body
            addAddresses(preData.to, model: recipientsModel.toModel)
            addAddresses(preData.cc, model: recipientsModel.ccModel)
            addAddresses(preData.bcc, model: recipientsModel.bccModel)
        }
        recipientsModel.parentComposeModel = self
    }
    
    /// computed property checks whether an attachment is mentioned in the mail (subject or body)
    var mentionsAttachments: Bool {
        let germanWords = ["Anhang", "anhang", "Angehängt", "angehängt", "Angehangen", "angehangen", "Anhänge", "anhänge"]
        let englishWords = ["Attachment", "attachment", "Attachments", "attachments", "Attached", "attached", "Attach", "attach"]
        // TODO: more languages?
        let allWords = germanWords + englishWords
        return allWords.contains(where: self.body.contains) || allWords.contains(where: self.subject.contains)
    }
    
    /// a func that deletes all the copies of the attachment files in the Documents Directory to keep it clean
    func removeAttachmentCopiesFromDocumentsDirectory() {
        for current in attachments {
            current.removeFileFromDocumentsDirectory()
        }
    }
    
    // TODO: Add security state functionality
    
    /// Generates mail and sends it.
    func sendMail() {
        generateMail().send()
    }
    
    /// Checks if encryption state can be toggled.
    func toogleEncryption() {
        if encryptionOn {
            encryptionOn = false
        } else {
            recipientsModel.checkEncryption()
        }
    }
    
    /// Adds email addresses to given RecipientFieldModel.
    ///
    /// - Parameters:
    ///   - addresses: String array of email addresses to add.
    ///   - model: RecipientFieldModel to which to add the addresses.
    private func addAddresses(_ addresses: [String], model: RecipientFieldModel) {
        for address in addresses {
            model.addNewAddress(address)
        }
        let frc = PersistentDataProvider.dataProvider.generateFetchedAddresesWithKeyResultsController(addresses: addresses)
        if let records = frc.fetchedObjects {
            if encryptionOn && records.count != addresses.count {
                encryptionOn = false
            }
        }
        
    }
    
    /// Generates OutgoingMail with given email contents.
    ///
    /// - Returns: Outgoing mail filled out with relevant information from RecipientsModel.
    private func generateMail() -> OutgoingMail {
        // before sending the mail we need to converted all attachments to MCOAttachments
        var convertedAttachments: [MCOAttachment] = []
        for current in attachments{
            do {
                if let newMCOAttachment = MCOAttachment.init(data: current.myData, filename: current.myName) {
                    convertedAttachments.append(newMCOAttachment)
                    print("Converted and attached file")
                }
            }
        }
        // TODO: something seems to go wrong with sending the attachments
        // has to be fixed in the future
        
        return OutgoingMail(toAddresses: recipientsModel.toEMails,
                     ccAddresses: recipientsModel.ccEMails,
                     bccAddresses: recipientsModel.bccEMails,
                     subject: subject,
                     textContent: body,
                     htmlContent: nil,
                     textparts: 1,
                     sendEncryptedIfPossible: !encryptionOn,
                     attachments: convertedAttachments)
    }
}
