//
//  AddressHandler.swift
//  mail_dynamic_icon_001
//
//  Created by jakobsbode on 14.07.16.
//  //  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import Contacts
import SwiftUI


// TODO: Mark displayname as from CNContact
class ContactHandler {
    static var cartoons = StudySettings.useAvatars
    
    private let store = CNContactStore()
    static let handler = ContactHandler()
    let keysToFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactNicknameKey, CNContactIdentifierKey, CNContactImageDataKey] as [CNKeyDescriptor]
    
    
    func findContactBy(email: String) -> [CNContact] {
        let predicate = CNContact.predicateForContacts(matchingEmailAddress: email)
        if let contacts = try? store.unifiedContacts(matching: predicate, keysToFetch: keysToFetch) {
            return contacts
        }
        return []
    }
    
    private func findContactsBy(ids:  [String]) -> [CNContact] {
        let predicate = CNContact.predicateForContacts(withIdentifiers: ids)
        if let contacts = try? store.unifiedContacts(matching: predicate, keysToFetch: keysToFetch) {
            return contacts
        }
        return []
    }
    
    func findContacts(update: Bool) {
        // TODO: Move to background thread?
        if update, let frp = try? PersistentDataProvider.dataProvider.lookUp(entityName: AddressRecord.entityName, sorting: ["email": true], equalPredicates: nil, differentPredicates: ["phoneBookID": "nil"], inPredicates: nil, propertiesToFetch: ["email", "phoneBookID", "displayname", "image"]), let addresses = frp.fetchedObjects as? [AddressRecord]{ // ["phoneBookID": "nil"]
            let contacts = self.findContactsBy(ids: addresses.map({($0.phoneBookID ?? "")}))
            for addr in addresses {
                var hit = false
                for c in contacts {
                    if c.identifier == addr.phoneBookID {
                        hit = true
                        if c.nickname.count > 0 && c.nickname != addr.displayname {
                            addr.displayname = c.nickname
                        } else if c.name.count > 0 && c.name != addr.displayname {
                            addr.displayname = c.name
                        }
                        if c.imageData != addr.image {
                            addr.image = c.imageData
                        }
                    }
                }
                if hit == false {
                    addr.displayname = nil
                    addr.image = nil
                    addr.phoneBookID = nil
                }
            }
            do {
                try PersistentDataProvider.dataProvider.save(taskContext: frp.managedObjectContext)
            } catch {
                print("Can not store updates!")
            }
        }
        
        if let frp = try? PersistentDataProvider.dataProvider.lookUp(entityName: AddressRecord.entityName, sorting: ["email": true], equalPredicates: ["phoneBookID": "nil"], differentPredicates: nil, inPredicates: nil, propertiesToFetch: ["email", "phoneBookID", "displayname", "image"]), let addresses = frp.fetchedObjects as? [AddressRecord]{ // ["phoneBookID": "nil"]
            for addr in addresses {
                if let contact = self.findContactBy(email: addr.email).first {
                    addr.phoneBookID = contact.identifier
                    if contact.nickname.count > 0 {
                        addr.displayname = contact.nickname
                    } else if contact.name.count > 0 {
                        addr.displayname = contact.name
                    }
                    if contact.isKeyAvailable("CNContactImageDataKey"), let data = contact.imageData {
                        addr.image = data
                    }
                }
            }
            do {
                try PersistentDataProvider.dataProvider.save(taskContext: frp.managedObjectContext)
            } catch {
                print("Can not store updates!")
            }
        }
    }
    
    
    private static func mapToCartoon(name: String, key: String?) -> Image {
        let maxCartoons = 50
        var id = abs(name.hash % maxCartoons)
        if let key = key {
            id = abs(key.hash % maxCartoons)
        }
        return Image("cartoon\(id)")
                .resizable()
    }
    
    public static func makeImageFromName(_ name: String, color: UIColor) -> UIImage {
        var text: NSAttributedString
        var tag = String()
        if name.count > 0 {
            let seperated = name.components(separatedBy: " ")
            tag = seperated.map({ if let a = $0.first { return "\(a)" }; return "" }).joined()
        }

        text = NSAttributedString(string: tag.uppercased(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 32.2)])

        var myBounds = CGRect()
        myBounds.size.width = 70
        myBounds.size.height = 70
        UIGraphicsBeginImageContextWithOptions(myBounds.size, false, 2) //try 200 here

        let context = UIGraphicsGetCurrentContext()

        //
        // Clip context to a circle
        //
        let path = CGPath(ellipseIn: myBounds, transform: nil);
        context!.addPath(path);
        context!.clip();

        //
        // Fill background of context
        //
        context!.setFillColor(color.cgColor)
        context!.fill(CGRect(x: 0, y: 0, width: myBounds.size.width, height: myBounds.size.height));


        //
        // Draw text in the context
        //
        let textSize = text.size()

        text.draw(in: CGRect(x: myBounds.size.width / 2 - textSize.width / 2, y: myBounds.size.height / 2 - textSize.height / 2, width: textSize.width, height: textSize.height))


        let snapshot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return snapshot!

    }

    static func getImageOrDefault(addr: AddressRecord) -> Image {
        if let imageData = addr.image, let image = UIImage(data: imageData) {
            return Image(uiImage: image)
        }
        if cartoons {
            // TODO: Fix mapping to key.
            return mapToCartoon(name: addr.name, key: nil)
        }
        return Image(uiImage: makeImageFromName(addr.name, color: getColor(name: addr.name)  ))
    }

    static func getColor(name: String) -> UIColor {
        let prim = 653

        let hash = (abs(name.hash)) % prim
        return UIColor(hue: CGFloat(hash) / CGFloat(prim), saturation: 1, brightness: 0.75, alpha: 1)
    }
}
