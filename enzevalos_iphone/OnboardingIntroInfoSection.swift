//
//  OnboardingIntroInfosection.swift
//  enzevalos_iphone
//
//  Created by melicoa97 on 03.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct OnboardingIntroInfosection: View {
    
    var sections:[OnboardingIntroInfoSubsection] = [
        OnboardingIntroInfoSubsection(
            headline:NSLocalizedString("OnboardingIntroSection.Confidential.headline", comment: ""),
            previewText:NSLocalizedString("OnboardingIntroSection.Confidential.preview", comment: ""),
            description:NSLocalizedString("OnboardingIntroSection.Confidential.description", comment: ""),
            image: StudySettings.securityIndicator.imageOfSecureIndicatorSwiftUI(background: false, open: false, button: false)),
        OnboardingIntroInfoSubsection(
            headline:NSLocalizedString("OnboardingIntroSection.Detection.headline", comment: ""),
            previewText:NSLocalizedString("OnboardingIntroSection.Detection.preview", comment: ""),
            description:NSLocalizedString("OnboardingIntroSection.Detection.description", comment: ""),
            image: StudySettings.securityIndicator.imageOfInsecureIndicatorSwiftUI(background: false, button: false))
    ]
    
    var body: some View {
        VStack(alignment: .leading){
            ForEach(sections,id: \.id){section in
                OnboardingIntroInfosubsection(image: section.image, headline: section.headline, previewText: section.previewText, description: section.description).padding(.vertical, 10)
            }
        }
        .padding(.horizontal, 40)
    }
}

struct OnboardingIntroInfosubsection: View {
    
    var image:Image
    var headline:String
    var previewText:String
    var description:String
    @State private var pressed:Bool=true
    
    
    var body: some View {
        Button(action: {
            self.pressed.toggle()
        }){
            HStack(alignment: .top){
                image
                    .renderingMode(Image.TemplateRenderingMode?.init(Image.TemplateRenderingMode.original))
                    .resizable()
                    .frame(width: 49, height:37)
                VStack(alignment: .leading){
                    Text(headline).font(.body).fontWeight(.bold)
                    if(pressed){
                        Text(previewText).fontWeight(.thin).font(.subheadline)
                    }else {
                        Text(description)
                            .fontWeight(.thin)
                            .font(.subheadline)
                    }
                }.padding(.leading, 20)
                Spacer()
            }
        }
        .foregroundColor(.primary)

    }
}

class OnboardingIntroInfoSubsection{
    var image:Image
    var headline:String
    var previewText:String
    var description:String
    var id:UUID
    init(headline:String, previewText:String, description:String, image:Image){
        self.id = UUID()
        self.headline = headline
        self.previewText = previewText
        self.description = description
        self.image = image
    }
    
}
