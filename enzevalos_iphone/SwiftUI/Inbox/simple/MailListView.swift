//
//  MailListView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 16.10.20.
//  Modified by Chris Offner & Claire Bräuer in March 2021.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import SwiftUI
import CoreData

// TODO: Fix refresh on view creation!
// Currently "pull to refresh" appears to get triggered every time MailListView (re-)appears.

/// A view that lists emails in a particular folder together with a search function.
struct MailListView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    var fetchRequest: FetchRequest<MailRecord>
    var mails: FetchedResults<MailRecord>{fetchRequest.wrappedValue}
    var folderPath: String
    var folderName: String
    @ObservedObject var model: InboxRefreshModel
    @State private var searchText = ""
    @State private var searchType = SearchType.All
    
    init(folderPath: String, folderName: String, refreshModel: InboxRefreshModel) {
        fetchRequest = MailRecord.mailsInFolderFetchRequest(folderpath: folderPath)
        self.folderPath = folderPath
        self.folderName = folderName
        self.model = refreshModel
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            SearchView(searchText: $searchText, searchType: $searchType).padding()
            
            ScrollView {
                refreshTrigger.frame(width: 0, height: 0)
                
                ZStack(alignment: topCenter) {
                    mailList.frame(height: 600) // TODO: Make mailList use max height dynamically
                    pullToRefreshIndicator
                }
                .offset(y: model.updating ? 35 : -10)
                .animation(.default)
            }
        }
        .navigationBarTitle(folderName, displayMode: .inline)
    }
    
    private var mailList: some View {
        List {
            ForEach(self.mails.filter(filterKeyRecord), id: \.self) { email in
                NavigationLink(destination: ReadMainView(model: ReadModel(mail: email))) {
                    MailRowView(mail: email)
                }
            }
            .onDelete(perform: deleteMails(at:))
        }
        .listStyle(PlainListStyle())
    }
    
    @ViewBuilder
    private var pullToRefreshIndicator: some View {
        if model.updating {
            ProgressView().offset(y: -32)
        } else {
            Image(systemName: "arrow.down.circle")
                .foregroundColor(.secondary)
                .font(.system(size: 28, weight: .regular))
                .rotationEffect(Angle(degrees: model.refreshStarted ? 180 : 0))
                .offset(y: -32)
                .animation(.easeIn)
        }
    }
    
    private var topCenter: Alignment {
        Alignment(horizontal: .center, vertical: .top)
    }
    
    /// Measures pulldown offset and triggers inbox refresh.
    private var refreshTrigger: GeometryReader<AnyView> {
        GeometryReader { geo -> AnyView in
            DispatchQueue.main.async {
                // Set initial baseline for pulldown based on dynamic layout
                guard let baseline = model.pullOffsetBaseline else {
                    model.pullOffsetBaseline = geo.frame(in: .global).minY
                    return
                }
                
                // Update offset dynamically as user pulls down
                model.pullOffset = geo.frame(in: .global).minY
                
                // Indicate refresh start if pulldown threshold is reached
                if model.pullOffset - baseline > 80 && !model.refreshStarted {
                    model.refreshStarted = true
                }
                
                // Trigger refresh once pulldown is fully released
                if model.refreshStarted && model.pullOffset == baseline {
                    withAnimation(.linear) {
                        model.refresh()
                    }
                }
            }
            
            return AnyView(EmptyView())
        }
    }
    
    private func deleteMails(at offsets: IndexSet) {
        var ids = [UInt64]()
        for index in offsets {
            let mail = mails[index]
            ids.append(UInt64(mail.uID))
        }
        MailRecord.delete(with: ids, folderPath: folderPath)
    }
    
    /// Filters emails based on search criteria.
    func filterKeyRecord(keyRecord: MailRecord) -> Bool {
        if searchText.isEmpty
            || searchText == NSLocalizedString("Searchbar.Title", comment: "Search") {
            return true
        }
        
        let query = searchText.lowercased()
        if (searchType == .All || searchType == .Sender)
            && (containsSearchTerms(content: keyRecord.sender.displayname, searchTerm: query)
                    || containsSearchTerms(content: keyRecord.sender.email, searchTerm: query)) {
            return true
        } else if (searchType == .All || searchType == .Sender)
                    && (keyRecord.addresses.filter {
                            containsSearchTerms(content: $0.email,
                                                searchTerm: query) }
                            .count > 0) {
            return true
        } else if (searchType == .All || searchType == .Subject)
                    && containsSearchTerms(content: keyRecord.subject, searchTerm: query) {
            return true
        } else if (searchType == .All || searchType == .Body)
                    && containsSearchTerms(content: keyRecord.body, searchTerm: query) {
            return true
        }
        return false
    }
}
