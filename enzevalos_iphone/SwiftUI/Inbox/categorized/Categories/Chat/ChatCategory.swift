//
//  ChatCategory.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 23.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

let ChatCategory = CategoryView(
    name    : NSLocalizedString("Category.Chats.name", comment: "People"),
    icon    : Image(systemName: "message.fill"),
    searchFields : SearchFilterCollections.Files,
    filter  : filterForShortMail //TODO: create an actually good filter
){model in
    ChatCategoryView(model:model)
}



fileprivate func filterForShortMail(_ email : eMail)->MailCategoryOpenability{
    
    let  maxLength:Int = 200
    
    guard email.body.count < maxLength else {
        return .cannotOpen
    }
    return .shouldOpen
}
