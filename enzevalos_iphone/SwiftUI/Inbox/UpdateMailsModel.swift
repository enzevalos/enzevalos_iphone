//
//  UpdateMailsModel.swift
//  enzevalos_iphone
//
//  Created by Chris Offner on 29.03.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import Foundation

class InboxRefreshModel: ObservableObject {
    @Published var updating = false
    var folderPath: String
    
    init(folderPath: String) {
        self.folderPath = folderPath
    }
    
    var lastUpdate: String {
        var text = NSLocalizedString("Updating", comment: "updating...")
        
        if !updating {
            let last = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale.current
            dateFormatter.timeStyle = .medium
            let dateString = dateFormatter.string(from: last)
            text = NSLocalizedString("LastUpdate", comment: "") + " " + dateString
        }
        
        return text
    }
    
    func updateMails() {
        guard !updating else {
            return
        }
        
        LetterboxModel
            .instance
            .mailHandler
            .updateFolder(folderpath: folderPath) { error in
                if error == nil {
                    self.updating = false
                }
                // TODO: Add error message
            }
        updating = true
    }
}
