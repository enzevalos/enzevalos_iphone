//
//  Wrap.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 10.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

/// a hybrid between HStack and VStack that orders its items along the main axis and overflows into the other stack
struct Wrap<V:View>: View {
    
    let views: [V]
    
    var mainAxisItemAmount : Int = 5
    var axis : Axis = .horizontal
    var alignment : WrapAlignment = .spaceEqually
    
    var body: some View{
        DynamicWrap(
            items: views, viewGenerator: {v in v},
            mainAxisItemAmount: mainAxisItemAmount,
            axis:axis,
            alignment:alignment
        )
    }
}

/// a wrap that automatically finds the best amount of items in a single row ( or column)
struct SmartWrap<V:View>: View {
    
    let views: [V]
    
    var minItemAmount : Int = 3
    var maxItemAmount : Int = 10
    
    var axis : Axis = .horizontal
    var alignment : WrapAlignment = .spaceEqually
    
    var body: some View{
        DynamicSmartWrap(
            items: views, viewGenerator: {v in v},
            minItemAmount : minItemAmount,
            maxItemAmount : maxItemAmount,
            axis:axis,
            alignment:alignment
        )
    }
}

/// same as smartwrap but with viewbuilder
struct DynamicSmartWrap<T,V:View>: View {
    
    let items : [T]
    let viewGenerator: (T)->V
    
    var minItemAmount : Int = 3
    var maxItemAmount : Int = 10
    
    var axis : Axis = .horizontal
    var alignment : WrapAlignment = .spaceEqually
    
    var body: some View {
        
        //calc optimal length to have the least free space
        let length = items.count
        var optimalRelation:CGFloat = 0
        var optimalLength:Int=maxItemAmount
        for i in (minItemAmount ... maxItemAmount){
            let itemsInLastRowAmount:CGFloat = CGFloat(length % i)
            let relation:CGFloat = itemsInLastRowAmount / CGFloat(i)
            if relation >= optimalRelation {
                optimalRelation = relation
                optimalLength = i
            }
        }
        
        return DynamicWrap(
            items: items,
            viewGenerator: viewGenerator,
            mainAxisItemAmount: optimalLength,  //pass optimal Length
            axis:axis,
            alignment:alignment
        )
    }
}

/// same as smartwrap but with viewbuilder
struct DynamicWrap<T,V:View>: View {
    
    let items : [T]
    let viewGenerator: (T)->V
    
    var mainAxisItemAmount : Int = 5
    var axis : Axis = .horizontal
    var alignment : WrapAlignment = .spaceEqually
    
    var body: some View {
        
        ///places as much items as specified in the main axis and overflows the rest into the next stack
        @ViewBuilder func innerGenerator(_ outerIndex:Int)->some View{
            var isFirstOutOfIndex=true
            ForEach(0 ..< mainAxisItemAmount){innerIndex -> AnyView in
                let index = outerIndex * mainAxisItemAmount+innerIndex
                if index >= items.count{ //if the amount of categories isnt a multiple of iconsPerRow
                    switch alignment{
                    case .start, .center, .spaceEqually:
                        if isFirstOutOfIndex {
                            isFirstOutOfIndex = false
                            return AnyView(Spacer())
                        }
                        fallthrough
                    default:
                        return AnyView(EmptyView())
                    }
                }
                return AnyView(VHStack(true){
                    if(alignment == .spaceEqually || alignment == .spaceBetween){Spacer()}
                    viewGenerator(items[index])
                    if(alignment == .spaceEqually || alignment == .spaceBetween){Spacer()}
                })
            }
        }
        
        ///creates a stack perpendicular to the main axis in which all the inner stack come
        let outerGenerator =
            ForEach (0 ..< items.count % mainAxisItemAmount){outerIndex in
                VHStack(true){
                    switch alignment{
                    case .end, .center, .spaceEqually:
                        AnyView(Spacer())
                    default:
                        AnyView(EmptyView())
                    }
                    innerGenerator(outerIndex)
                }
            }
        
        return VHStack(false){outerGenerator}
    }
    
    ///chooses Axis depending on mainaxis and whether this is the inner or the outer stack
    @ViewBuilder private func VHStack<V2:View>(_ isMain:Bool = true, @ViewBuilder content: @escaping ()->V2)-> some View{
        if ((axis == .horizontal && isMain) || (axis == .vertical && !isMain)) {
            HStack{content()}
        }else{
            VStack{content()}
        }
    }
}


/// how to space the items inside a wrap
enum WrapAlignment{
    case start, spaceEqually, spaceBetween, center, end
}




struct Wrap_Previews: PreviewProvider {
    
    static var previews: some View {
        let views:[Text] = (0..<12).map{ i in Text("item \(i)") }
        return Wrap(views: views)
    }
}
