//
//  CategoryView.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 01.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI
import TagNavVi

/**
A View that should be used for the Categories

- parameters
   - category: the categorie that this view draws
   - description: a short discription for the ChatUser
   - icon: an Image that describes this kind of eMail
*/
struct CategoryViewInner<Content:View>: View {
   
    @ObservedObject var category : CategoryViewModel
    
    var content : ()->Content
   
    var body: some View {
        NavigatorCard(
            icon        : category.constData.icon,
            headline    : category.name,
            timestmp    : category.lastMailTimestamp,
            isResizable : category.constData.isResizable,
            size        : category.Ssize,
            onOpen      : {category.isActive.toggle()},
            isOpen      : category.isActive
        ){
           content()
        }.padding(.horizontal,  7).padding(.top, StudySettings.categorizedInterface_flat ? 5 : 10)
        .animation(.easeInOut)
    }
}


/**
 A View that should be used for the Categories
 
 Providing a navigationStack usable by pushing views via @EnvironmentObject var nav: NavigationStack
 nav.advance(newView())
 
- parameters
    - headline: the title
    - description: a short discription for the ChatUser
    - icon: an Image that describes this kind of eMail
    - tmestamp: a Date object of the last usage
    - isResizable: a Bool whether to use a resizable Card
    - size : a Binding of the current Size of the content
 */
struct NavigatorCard<Content:View> : View {
    
    init(
        icon : Image,
        headline : String,
        timestmp : Date? = nil,
        isResizable:Bool = true,
        size:Binding<CGFloat?> = Binding.constant(nil),
        onOpen : @escaping ()->() = {},
        isOpen : Bool=false,
        @ViewBuilder _ content: @escaping () -> Content
    ) {
        self.icon=icon
        self.headline=headline
        self.timestmp=timestmp
        self.isResizable=isResizable
        self.size=size
        self.onOpen=onOpen
        self.isOpen=isOpen
        self.content = content
    }
    
    var icon : Image
    var headline : String
    var timestmp : Date?
    
    var isResizable:Bool = true
    
    var size:Binding<CGFloat?> = Binding.constant(nil)
    private var _size : CGFloat? {size.wrappedValue}
    
    var onOpen : ()->() = {}
    var isOpen : Bool = false
    
    var content: ()->Content

    
    var body: some View {
        if _size != nil && isResizable {
            DragResizableCard(
                shadowElevation: StudySettings.categorizedInterface_flat ? 0 : 15,
                effect3D: StudySettings.categorizedInterface_flat ? 0 : 5, padding:0, height:size
            ){
                inner
                    .padding(.bottom,5)
            }
        }else{
            if _size != nil {
                Card(padding:0){
                    inner
                }
            }else{
                Card(padding:0){
                    inner
                }
                .padding(.bottom,-20)
                .edgesIgnoringSafeArea(.bottom)
            }
        }
        
    }
    
    private var inner:some View{
        TagNavigationView(tag: 1){
            content()
        }.withTitle(withHomeButton: true){
            HStack{
                icon
                    .sizeTo(25)
                    .padding(.trailing,5)
                headline.font(.title3)
                Spacer()
                Text(timestmp?.timeAgoText() ?? "")
                    .font(.system(size: 15))
                    .lineLimit(2)
                    .frame(width: 70)
                    .foregroundColor(.gray)
                Button(action: onOpen){
                    Image(systemName: isOpen ?
                        "arrow.down.right.and.arrow.up.left" :
                        "arrow.up.left.and.arrow.down.right"
                    )
                }
            }
        }
        .frame(height: _size != nil ? (_size ?? 0) + 50 : nil)
        .padding()
        //TODO disable scrolling
        .gesture(DragGesture())
    }
}

struct CategoryView2_Previews: PreviewProvider {
    static var previews: some View {
        Group{
            ScrollView{
                VStack{
                    StatefulPreviewWrapper(200){value in
                        NavigatorCard(icon: Image(systemName: "envelope.fill"), headline: "Test-Kategorie",size: value){
                            Text("mois")
                        }.padding()
                    }
                    Spacer()
                }
            }
        }
    }
}
