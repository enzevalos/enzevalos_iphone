//
//  ViewModelProvider.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 17.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

/**
 A singleton used to create all the Category ViewModels without unecessarily createing them to often
 */
class CategoryViewModelProvider : ReasonedObservableObject{
    
    static let s = CategoryViewModelProvider()
    private override init() {}
    
    ///this is used to let all subscribers know if any categoryVM published changes , the cool thing is, it also supports custom publishers
    private var cancellable:(AnyCancellable,AnyCancellable) = (AnyCancellable({}),AnyCancellable({}))
    func observe()->CategoryViewModelProvider{
        cancellable = ( viewModels.objectWillChangeR.sink{ reason in self.objectWillChangeR.send(reason) } ,
                        viewModels.objectWillChange.sink {      _ in self.objectWillChange.send ()       } )
        return self
    }
    
    ///the ViewModel Cache
    @/*Reasoned*/ObservedObject private var viewModels : ReasonedObservableDict<String , CategoryViewModel> = ReasonedObservableDict(Dictionary<String,CategoryViewModel>()).observeChildrenChanges()
    
    var viewModelList : ObservableArray<CategoryViewModel> {ObservableArray(array: viewModels.dict.map{$0.value}).observeChildrenChanges()}
    
    func createViewModel(
        for     name    :   String,
        with    filter  :   @escaping MailFilter,
        and     andPred :   NSPredicate,
        in      folder  :   String = MailHandler.INBOX,
        data    consts  :   CategoryData = CategoryData()
    )->CategoryViewModel{
        
        var viewModel:CategoryViewModel
        var shouldedit:Bool = false
        
        let alreadyCreated = viewModels.dict[name]
        if (alreadyCreated != nil && alreadyCreated?.andPred == andPred) {
            viewModel = alreadyCreated!
            if !viewModel.hasThisData(name: name, folder: folder, consts: consts){shouldedit=true}
        }else{
            let newVM = CategoryViewModel(
                name    : name,
                filter  : filter,
                andPredicate: andPred
            )
            viewModels = viewModels.append(k:name,v:newVM).observeChildrenChanges()
            viewModel = newVM
            shouldedit = true
        }
        
        //everything modifying the VM must go inside this area. otherwise it results in a loop
        if shouldedit{
            viewModel.updateFolder(folder)
            viewModel.constData = consts
        }
        return viewModel
    }
}












class ReasonedObservableDict<S:Hashable,T>: ReasonedObservableObject {

    @Published var dict:[S:T] = [:]
    var cancellables = [S:AnyCancellable]()
    var cancellables2 = [S:AnyCancellable]()

    init(_ dict: [S:T]) {
        self.dict = dict
    }
    
    func append(k:S,v:T) -> ReasonedObservableDict<S,T> {
        self.dict[k]=v
        return self
    }

    func observeChildrenChanges<T: ReasonedObservableObject>() -> ReasonedObservableDict<S,T> {
        let dict2 = dict as! [S:T]
        dict2.forEach({
            let v = $0.value.objectWillChangeR.sink{ reason in self.objectWillChangeR.send(reason) }
            let v2 = $0.value.objectWillChange.sink{      _ in self.objectWillChange.send ()       }
            let k = $0.key
            // Important: You have to keep the returned value allocated,
            // otherwise the sink subscription gets cancelled
            self.cancellables[k]=v
            self.cancellables2[k]=v2
        })
        return self as! ReasonedObservableDict<S,T>
    }
}











//see https://stackoverflow.com/a/57920136


import Combine
class ObservableDict<S:Hashable,T>: ObservableObject {

    @Published var dict:[S:T] = [:]
    var cancellables = [S:AnyCancellable]()

    init(_ dict: [S:T]) {
        self.dict = dict
    }
    
    func append(k:S,v:T) -> ObservableDict<S,T> {
        self.dict[k]=v
        return self
    }

    func observeChildrenChanges<T: ObservableObject>() -> ObservableDict<S,T> {
        let dict2 = dict as! [S:T]
        dict2.forEach({
            let v = $0.value.objectWillChange.sink(receiveValue: { _ in self.objectWillChange.send() })
            let k = $0.key
            // Important: You have to keep the returned value allocated,
            // otherwise the sink subscription gets cancelled
            self.cancellables[k]=v
        })
        return self as! ObservableDict<S,T>
    }
}

class ObservableArray<T>: ObservableObject {

    @Published var array:[T] = []
    var cancellables = [AnyCancellable]()

    init(array: [T]) {
        self.array = array

    }

    func observeChildrenChanges<T: ObservableObject>() -> ObservableArray<T> {
        let array2 = array as! [T]
        array2.forEach({
            let c = $0.objectWillChange.sink(receiveValue: { _ in self.objectWillChange.send() })

            // Important: You have to keep the returned value allocated,
            // otherwise the sink subscription gets cancelled
            self.cancellables.append(c)
        })
        return self as! ObservableArray<T>
    }

}
