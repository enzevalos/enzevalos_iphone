//
//  LetterboxApp.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 16.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

@main
struct LetterboxApp: App {
    @StateObject private var model = LetterboxModel()
    var testing = false
    
    var body: some Scene {
        WindowGroup {
            if testing {
                NavigationView {
                    KeyManagementOverview()
                }
            } else {
                switch model.currentState {
                case .PERMISSIONS:
                    PermissionRequestView()
                case .FIRSTLAUNCH:
                    NewOnboardingView()
                case .ACCOUNTSETTING:
                    NewOnboardingView()
                case .GENERATEKEYS:
                    PermissionRequestView() // TODO Wait
                case .LAUNCHEDBEFORE:
                    if StudySettings.showCategorizedInterface {
                        InboxHome(controller: HomeModel())
                            //.navigationBarItems(leading: self.folderButton)
                            .environment(\.managedObjectContext, PersistentDataProvider
                                            .dataProvider
                                            .persistentContainer
                                            .viewContext)
                    }else{
                        FolderListView()
                            .environment(\.managedObjectContext,
                                         PersistentDataProvider
                                            .dataProvider
                                            .persistentContainer
                                            .viewContext)
                    }
                }
            }
        }
    }
}
