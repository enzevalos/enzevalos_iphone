//
//  AddButton.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.12.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct AddButton: View {
    let action: () -> ()
    
    var body: some View {
        Button(action: action, label: {
            Image(systemName: "plus")
                .imageScale(.large)
        })
    }
    
}

struct AddButton_Previews: PreviewProvider {
    static var previews: some View {
        AddButton(action: {
            print("Action!")
        }
)
    }
}
