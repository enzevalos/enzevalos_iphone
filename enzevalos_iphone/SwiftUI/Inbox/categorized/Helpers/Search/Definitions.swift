//
//  Definitions.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 09.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import Foundation

struct Search{
    var text = ""
    var fields:[SearchFilter] = SearchFilterCollections.Standarts
    var activeIndex:Int = 0
    var now = false
    var isExpanded = false
    var activeField:SearchFilter {
        fields[activeIndex]
    }
    func filter(eMail:eMail)->Bool{
        if now == false{
            return true
        }
        return self.activeField.filter(eMail,self.text)
    }
}

struct SearchFilter : Equatable{
    static func == (lhs: SearchFilter, rhs: SearchFilter) -> Bool {
        lhs.text==rhs.text
    }
    
    typealias Filter = (eMail,String)->Bool
    var text:String
    var filter: Filter
}


//Depricated
protocol SearchTypes:CaseIterable,Hashable where AllCases:RandomAccessCollection {
    var text:String {get}
}
