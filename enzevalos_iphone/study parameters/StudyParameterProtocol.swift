//
//  StudyParameterProtocol.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 22.01.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation
import KeychainAccess

protocol StudyParameterProtocol {
    static var name: String {get}
    static var keyName: String {get}
    static var numberOfTreatments: UInt32 {get}
    static var defaultValue: StudyParameterProtocol {get}
    static func findParameter(rawvalue: Int) -> StudyParameterProtocol
    
    var value: Int {get}
    func startStudy()
}

extension StudyParameterProtocol {
    
    static func load() -> StudyParameterProtocol {
        if let param = loadKeyChain() {
            return param
        }
        return self.defaultValue
    }
    
    static func reset() -> Bool {
        UserDefaults.standard.removeObject(forKey: self.keyName)
        let keychain = Keychain(service: "Enzevalos/Study")
        do {
            try keychain.remove(self.keyName)
        }
        catch {
            return false
        }

        return true
    }
    
    private static func loadUserDefault() -> StudyParameterProtocol? {
        let value = UserDefaults.standard.integer(forKey: self.keyName)
        return self.findParameter(rawvalue: value)
    }
    
    private static func loadKeyChain() -> StudyParameterProtocol? {
        guard StudySettings.isStudyParameter(type: self) else {
            return nil
        }
        let keychain = Keychain(service: "Enzevalos/Study")
        var value: Int?
        if let state = keychain[self.keyName], let num = Int(state) {
            value = num
        } else {
            value = Int(arc4random_uniform(self.numberOfTreatments))
            if let value = value {
                keychain[self.keyName] = String(value)
            }
        }
        if let v = value {
            UserDefaults.standard.set(v, forKey: self.keyName)
            return self.findParameter(rawvalue: v)
        }
        return nil
    }
}


