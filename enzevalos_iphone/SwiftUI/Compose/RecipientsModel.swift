//
//  RecipientsModel.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 09.11.20.
//  Modified by Chris Offner & Claire Bräuer in March 2021.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import Foundation
import Combine
import SwiftUI

/// Holds models for "To", "Cc", and "Bcc" fields.
class RecipientsModel: ObservableObject {
    let toModel: RecipientFieldModel
    let ccModel: RecipientFieldModel
    let bccModel: RecipientFieldModel
    @Published var showBccField = false
    var parentComposeModel: ComposeModel?
    
    /// Initializes models for "To", "Cc", and "Bcc" fields.
    init() {
        toModel = RecipientFieldModel(type: .to)
        ccModel = RecipientFieldModel(type: .ccBcc)
        bccModel = RecipientFieldModel(type: .bcc)
        toModel.parentRecipientModel = self
        ccModel.parentRecipientModel = self
        bccModel.parentRecipientModel = self
    }
    
    /// Used to show or hide Bcc field
    var isEditingCcOrBcc: Bool = false {
        didSet {
            updateShowBccField()
        }
    }
    
    /// Used to deactivate Send button if email has no recipients.
    var hasNoRecipients: Bool {
        toModel.selectedContacts.isEmpty
            && ccModel.selectedContacts.isEmpty
            && bccModel.selectedContacts.isEmpty
    }
    
    /// String array of email addresses in "To" field.
    var toEMails: [String] {
        get {
            toModel.selectedContacts.map {
                $0.email
            }
        }
    }
    
    /// String array of email addresses in "Cc" field.
    var ccEMails: [String] {
        get {
            ccModel.selectedContacts.map {
                $0.email
            }
        }
    }
    
    /// String array of email addresses in "Bcc" field.
    var bccEMails: [String] {
        get {
            bccModel.selectedContacts.map {
                $0.email
            }
        }
    }
    
    /// Updates whether to display  "Bcc" field based on state of recipient fields.
    func updateShowBccField() {
        showBccField = isEditingCcOrBcc
            || !ccModel.selectedContacts.isEmpty
            || !bccModel.selectedContacts.isEmpty
        ccModel.type = showBccField ? .cc : .ccBcc
    }
    
    /// Turnes encryption on if all recipients have a key.
    func checkEncryption() {
        for addr in toModel.selectedContacts {
            if !addr.hasPublicKey {
                return
            }
        }
        for addr in ccModel.selectedContacts {
            if !addr.hasPublicKey {
                return
            }
        }
        for addr in bccModel.selectedContacts {
            if !addr.hasPublicKey {
                return
            }
        }
        parentComposeModel?.encryptionOn = true
    }
}

/// Type of recipient field (to, cc, bcc).
enum RecipientType: LocalizedStringKey {
    case to = "To"
    case cc = "Cc"
    case bcc = "Bcc"
    case ccBcc = "CcBcc"
}
