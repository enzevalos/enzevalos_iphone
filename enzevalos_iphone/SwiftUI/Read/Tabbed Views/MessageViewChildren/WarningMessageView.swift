//
//  WarningMessageView.swift
//  enzevalos_iphone
//
//  Created by melicoa97 on 11.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct DialogView: View {

    let option: DialogOption
    
    var body: some View {
        VStack {
            Text(NSLocalizedString("corruptedHeadline", comment: "corrupted mail"))
                .font(.system(size: 25))
                .frame(maxWidth: .infinity)
                .background(Color.yellow)
            HStack{
                Text("!")
                .foregroundColor(Color.red)
                .font(.system(size: 60))
                .padding([.leading, .top, .bottom])
                Text(NSLocalizedString("corruptedText", comment: "corrupted mail"))
                .padding()
            }
        }
    }
}

struct WarningMessageView_Previews: PreviewProvider {
    static var previews: some View {
        DialogView(option: .postcard)
    }
}
