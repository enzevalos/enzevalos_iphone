//
//  ChatView.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 23.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

///a complete chat with all its messages
struct ChatView: View {
    
    @State var typingChatMessage: String = ""
    var chat : SingleChat
    
    var body: some View {
        VStack {
          ScrollView {
            ForEach(chat.messages) { msg in
                    ChatMessageView(checkedChatMessage: msg)
               }
           }
           HStack {
               TextField("ChatMessage...", text: $typingChatMessage)
                  .textFieldStyle(RoundedBorderTextFieldStyle())
                  .frame(minHeight: CGFloat(30))
                Button(action: {/*TODO*/}) {
                    Text("Send")
                 }
            }.frame(minHeight: CGFloat(50)).padding()
        }
    }
}

struct ChatView_Previews: PreviewProvider {
    static var previews: some View {
        EmptyView()//ChatView()
    }
}
