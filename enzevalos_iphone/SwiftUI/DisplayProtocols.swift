//
//  DisplayProtocols.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 09.04.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation
import SwiftUI
// TODO: DocumentView?? StateObject

/*
 Here are protocols to display an email, key, address and a contact
 */
/// Enums
enum FolderType {
    case Archive
    case Trash
    case Inbox
    case Sent
    case Draft
    case Other
}

enum ResponseType {
    case Reply
    case Forward
    case Draft
    
    func addPrefix(subject: String) -> String {
        switch self {
        case .Reply:
            return "Re: " + subject
        case .Forward:
            return "Fwd: " + subject
        case .Draft:
            return subject
        }
    }
    
    func addBodyPrefix(body: String) -> String {
        return body
    }
}

enum CryptoState {
    case UnableToDecrypt
    case InvalidSignature
    case NoCrypto
    case PlainMissingPublicKeyToVerify
    case PlainButValidSignature
    case EncValidSign
    case EncNoSignature
    case EncButMissingPublicKeyToVerify
    
    var buttonActions: [ButtonAction] {
        get {
            switch self {
            case .UnableToDecrypt:                  return [.AskUserToImportSK, .AskSenderToResendForPK]
            case .InvalidSignature:                 return [.AskSenderToConfirm, .IgnoreMail]
            case .NoCrypto:                         return [.InvitePerson]
            case .PlainMissingPublicKeyToVerify:    return [.AskSenderToSendPK]
            case .PlainButValidSignature:           return [.SendPK]
            case .EncValidSign:                     return []
            case .EncNoSignature:                   return [.AskSenderToConfirm]
            case .EncButMissingPublicKeyToVerify:   return [.AskSenderToSendPK]
            }
        }
    }
}

enum ContactSecurityRating {
    case Trustworthy
    case Forgable
    
    var name: LocalizedStringKey {
        switch self {
        case .Trustworthy: return "ContactView.Rating.Trustworthy"
        case .Forgable: return "ContactView.Rating.Forgable"
        }
    }
    
    var color: Color {
        switch self {
        case .Trustworthy: return Color(ThemeManager.encryptedVerifiedMessageColor())
        case .Forgable: return Color(ThemeManager.unencryptedMessageColor())
        }
    }
    
    var shield: String {
        switch self {
        case .Trustworthy:
            return "checkmark.shield.fill"
        case .Forgable:
            return "exclamationmark.shield.fill"
        }
    }
    
}


protocol DisplayKey {
    var keyID: String { get }
    var discovery: Date? { get }
    var lastSeen: Date? { get }
    var type: CryptoScheme { get }
    var isPreferedKey: Bool { get }
    var signedMailsCounter: Int { get }
    var sentMailsCounter: Int { get }
}

protocol DisplayFolder {
    var name: String { get }
    var mails: Int { get }
    var icon: Image { get }
    var subfolderLevel: Int { get }
}

protocol DisplayAttachment {
    var myName: String { get }
    var myData: Data { get }
}

protocol DisplayContact {
    associatedtype K: DisplayKey
    
    // General
    var name: String { get }
    var email: String { get }
    var avatar: Image { get }
    var emails: [String] { get }
    var isInContactBook: Bool { get }
    var discovery: Date? { get }
    var last: Date? { get }
    
    // Crypto related
    // var keys: [K] { get }
    var primaryKey: K? { get }
    
    // Phishing related
    var previousMails: Int { get }
    var previousResponses: Int { get }
    var hasSimilarContacts: Bool { get }
    var similarContacts: [String] { get }
}

extension DisplayContact {

    func findAddress(temp: Bool) -> AddressRecord {
        if let addr = PersistentDataProvider.dataProvider.fetchedAddressResultController.fetchedObjects?.first {
            return addr
        }
        fatalError("MISSING ADDR")
    }
    
    func addressRating <C:DisplayContact> (_: C) -> ContactSecurityRating {
        
        // TODO: Add another check of the keys array
        if self.primaryKey != nil  /* || !self.keys.isEmpty */ {
            return .Trustworthy
        }
        
        // TODO Think about Rating in case of a unsigned mail from a sender with aprimary key...
        return .Forgable
    }
}

protocol DisplayMail {
    associatedtype C: DisplayContact
    associatedtype K: DisplayKey
    
    var subject: String { get }
    var subjectWithFlag: String { get }
    var sender: C { get }
    var tos: [C] { get }
    var ccs: [C] { get }
    var bccs: [C] { get }
    var routingStops: [Landmark] { get }
    
    var date: Date { get }
    
    var isRead: Bool {get set}
    var folderType: FolderType { get }
    
    var body: String { get }
    var displayAttachments: [DisplayAttachment] { get }
    
    // Crypto
    var signatureState: SignatureState { get }
    var encryptionState: EncryptionState { get }
    var encryptionType: CryptoScheme? { get }
    var transportEnc: Bool { get }
    var signatureKey: K? { get }
    
    var persistentMail: MailRecord? { get }
    func markAsRead(isRead: Bool)
}

extension DisplayMail {
    
    /* TODO: MORE DIALOGS
     //else if (mail.isNewPubKey) {}
     // message contained new public key
     */
    
    var signatureType: CryptoScheme? {
        get {
            return signatureKey?.type ?? nil
        }
    }
    
    var title: String {
        get {
            var key = ""
            switch evaluateSecurity() {
            case (_,.NoCrypto):                         key = "Security.Dialog.Title.No.Crypto"
            case (_, .UnableToDecrypt):                 key = "Security.Dialog.Title.UnableToDecrypt"
            case (_, .InvalidSignature):                key = "Security.Dialog.Title.InvalidSignature"
            case (_, .PlainMissingPublicKeyToVerify):   key = "Security.Dialog.Title.PlainMissingPublicKeyToVerify"
            case (_, .PlainButValidSignature):          key = "Security.Dialog.Title.Plain+Sig"
            case (_, .EncValidSign):                    key = "Security.Dialog.Title.Enc+Sign"
            case (_, .EncNoSignature):                  key = "Security.Dialog.Title.Enc+NoSign"
            case (_, .EncButMissingPublicKeyToVerify):  key = "Security.Dialog.Title.EncMissingPublicKeyToVerify"
            }
            return NSLocalizedString(key, comment: "")
        }
    }
    
    var icon: Image {
        get {
            switch evaluateSecurity() {
            case (_, .EncValidSign):
                return StudySettings.securityIndicator.imageOfSecureIndicatorSwiftUI()
            case (_, .UnableToDecrypt), (_, .InvalidSignature):
                return StudySettings.securityIndicator.imageOfCorruptedIndicatorSwiftUI()
            case (_, .NoCrypto), (_, .PlainMissingPublicKeyToVerify), (_, .PlainButValidSignature), (_, .EncNoSignature), (_, .EncButMissingPublicKeyToVerify):
                return StudySettings.securityIndicator.imageOfInsecureIndicatorSwiftUI()
            }
        }
    }
    
    var titleIcon: UIImage {
        get {
            switch evaluateSecurity() {
            case (_, .EncValidSign):
                return StudySettings.securityIndicator.imageOfSecureIndicator()
            case (_, .UnableToDecrypt), (_, .InvalidSignature):
                return StudySettings.securityIndicator.imageOfCorruptedIndicator()
            case (_, .NoCrypto), (_, .PlainMissingPublicKeyToVerify), (_, .PlainButValidSignature), (_, .EncNoSignature), (_, .EncButMissingPublicKeyToVerify):
                return StudySettings.securityIndicator.imageOfInsecureIndicator()
            }
        }
    }
    
    var dialog: DialogStruct {
        get {
            // TODO: Do we add new public key stuff?
            var color: UIColor
            var bodyKey: String
            let infoTitle = NSLocalizedString("Security.Dialog.Button.Title.MoreInfo", comment: "")
            
            var ctaButtonTitleKey: String = "Security.Dialog.Button.Title.OK"
            var ctaButtonAction: ButtonAction = .OK
            
            var moreButtons: [ButtonStruct] = []
            
            var dismissButtonTitleKey: String?
            var dismissButtonAction: ButtonAction?
            
            switch evaluateSecurity() {
            case (_, .EncValidSign):
                color = ThemeManager.encryptedMessageColor()
                bodyKey = "OnboardingIntroSection.Confidential.description"
            // ctaButton -> Ok
            
            case (_, .UnableToDecrypt):
                color = ThemeManager.troubleMessageColor()
                bodyKey = "ReceiveInsecureInfoDecryptionFailed"
                // cta Button -> OK
                // add -> Import Key
                var btn = ButtonStruct(titleKey: "Security.Dialog.Button.Title.Import.SK", action: .AskUserToImportSK, type: .MORE)
                moreButtons.append(btn)
                // add -> Ask to resend for differnet PK
                btn = ButtonStruct(titleKey: "Security.Dialog.Button.Title.Ask.Resend.ForPK", action: .AskSenderToResendForPK, type: .MORE)
                moreButtons.append(btn)
                
            case (_, .InvalidSignature):
                color = ThemeManager.troubleMessageColor()
                bodyKey = "ReceiveDamagedInfo"
                // cta Button -> Ignore Mail
                ctaButtonTitleKey = "Security.Dialog.Button.Title.Ignore.Mail"
                ctaButtonAction = .IgnoreMail
                // add -> ask sender
                let btn = ButtonStruct(titleKey: "Security.Dialog.Button.Title.Confirmation", action: .AskSenderToConfirm, type: .MORE)
                moreButtons.append(btn)
                // dismiss -> Ignore Warning
                dismissButtonTitleKey = "Security.Dialog.Button.Title.OK"
                dismissButtonAction = .IgnoreWarning
                
            case (_, .NoCrypto):
                color = ThemeManager.unencryptedMessageColor()
                bodyKey = "ReceiveInsecureInfo"
                // cta Button -> Invite user
                ctaButtonTitleKey = "Security.Dialog.Button.Title.Invite"
                ctaButtonAction = .InvitePerson
                // add -> OK
                let btn = ButtonStruct(titleKey: "Security.Dialog.Button.Title.OK", action: .OK, type: .MORE)
                moreButtons.append(btn)
                
            case (_, .PlainMissingPublicKeyToVerify):
                color = ThemeManager.unencryptedMessageColor()
                bodyKey = "Security.Dialog.Body.PlainMissingPublicKeyToVerify"
                // cta Button -> Ask to send PK
                ctaButtonTitleKey = "Security.Dialog.Button.Title.Ask.ForPK"
                ctaButtonAction = .AskSenderToSendPK
                // add -> OK
                let btn = ButtonStruct(titleKey: "Security.Dialog.Button.Title.OK", action: .OK, type: .MORE)
                moreButtons.append(btn)
                
            case (_, .PlainButValidSignature):
                color = ThemeManager.unencryptedMessageColor()
                bodyKey = "ReceiveInsecureInfoVerified"
                // cta Button -> send PK
                ctaButtonTitleKey = "Security.Dialog.Button.Title.Send.PK"
                ctaButtonAction = .SendPK
                // add -> OK
                let btn = ButtonStruct(titleKey: "Security.Dialog.Button.Title.OK", action: .OK, type: .MORE)
                moreButtons.append(btn)
                
            case (_, .EncNoSignature):
                color = ThemeManager.unencryptedMessageColor()
                bodyKey = "Information.General.OnlyEncryted"
                // cta button -> Ask to confirm
                //  add -> OK
                let btn = ButtonStruct(titleKey: "Security.Dialog.Button.Title.OK", action: .OK, type: .MORE)
                moreButtons.append(btn)
                
            case (_, .EncButMissingPublicKeyToVerify):
                color = ThemeManager.unencryptedMessageColor()
                bodyKey = "Security.Dialog.Body.EncButMissingPublicKeyToVerify"
                // cta Button -> Ask to send PK
                ctaButtonTitleKey = "Security.Dialog.Button.Title.Ask.ForPK"
                ctaButtonAction = .AskSenderToSendPK
                // add -> OK
                let btn = ButtonStruct(titleKey: "Security.Dialog.Button.Title.OK", action: .OK, type: .MORE)
                moreButtons.append(btn)
            }
            
            let body = NSLocalizedString(bodyKey, comment: "")
            let ctaButtonTitle = NSLocalizedString(ctaButtonTitleKey, comment: "")
            var dismissButtonTitle: String?
            if let key = dismissButtonTitleKey {
                dismissButtonTitle = NSLocalizedString(key, comment: "")
            }
            
            return DialogStruct(dialogColor: Color(color), title: title, body: body, img: icon, messageImage: nil, ctaButtonTitle: ctaButtonTitle, ctaButtonAction: ctaButtonAction, infoButtonTitle: infoTitle, moreButtons: moreButtons, dismissButtonTitle: dismissButtonTitle, dismissButtonAction: dismissButtonAction)
        }
    }
    
    var buttonActions: [ButtonAction] {
        get {
            let (_, state) = evaluateSecurity()
            return state.buttonActions
        }
    }
    
    var warnings: [DialogStruct] {
        get {
            var result = [DialogStruct] ()
            switch evaluateSecurity() {
            case (_, .EncValidSign), (_, .NoCrypto), (_, .PlainButValidSignature), (_, .EncNoSignature):
                break // No warning required
            case (_, .UnableToDecrypt), (_, .InvalidSignature) , (_, .PlainMissingPublicKeyToVerify), (_, .EncButMissingPublicKeyToVerify):
                result.append(dialog) // warning required
            }
            if self.sender.primaryKey != nil && self.signatureState == .NoSignature  {
                let icon = Image(systemName: "exclamationmark.triangl.fill")
                let missingSignature = DialogStruct(dialogColor: Color(ThemeManager.unencryptedMessageColor()),
                                                    title: NSLocalizedString("encryptedBeforeHeadline",
                                                                             comment: "encrypted by sender before"),
                                                    body: NSLocalizedString("encryptedBeforeText", comment: "encrypted by sender before"),
                                                    img: icon, messageImage: icon,
                                                    ctaButtonTitle: NSLocalizedString("Security.Dialog.Button.Title.Confirmation", comment: ""),
                                                    ctaButtonAction: .AskSenderToConfirm,
                                                    infoButtonTitle: NSLocalizedString("Security.Dialog.Button.Title.MoreInfo", comment: ""),
                                                    moreButtons: [],
                                                    dismissButtonTitle: NSLocalizedString("Security.Dialog.Button.Title.OK", comment: ""),
                                                    dismissButtonAction: .IgnoreWarning)
                result.append(missingSignature)
            }
            return result
        }
    }
    
    private func evaluateSecurity() -> (isPhish: Bool, cryptoState: CryptoState) {
        // TODO Phishing handling
        let isPhish = false
        let cryptoState = evaluateCryptoState()
        
        return (isPhish, cryptoState)
    }
    
    private func evaluateCryptoState() -> CryptoState {
        switch (self.encryptionState, self.signatureState) {
        // General error cases -> cast other states
        case (.UnableToDecrypt, _):                                 return .UnableToDecrypt
        case (_, .InvalidSignature):                                return .InvalidSignature
        case (.NoEncryption, .NoSignature):                         return .NoCrypto
        case (.NoEncryption, .NoPublicKey):                         return .PlainMissingPublicKeyToVerify
        case (.NoEncryption, .ValidSignature):                      return .PlainButValidSignature
        case (.ValidEncryptedWithOldKey, .ValidSignature):          return .EncValidSign
        case (.ValidedEncryptedWithCurrentKey, .ValidSignature):    return .EncValidSign
        case (.ValidEncryptedWithOldKey, .NoSignature):             return .EncNoSignature
        case (.ValidEncryptedWithOldKey, .NoPublicKey):             return .EncButMissingPublicKeyToVerify
        case (.ValidedEncryptedWithCurrentKey, .NoSignature):       return .EncNoSignature
        case (.ValidedEncryptedWithCurrentKey, .NoPublicKey):       return .EncButMissingPublicKeyToVerify
        }
    }
    
    
    func preparePreviousMailBody() -> String{
        var body = NSLocalizedString("mail from", comment: "describing who send the mail") + " "
        body.append(self.sender.email)
        let time = DateFormatter.init()
        time.dateStyle = .short
        time.timeStyle = .short
        time.locale = Locale.current
        body.append(" " + NSLocalizedString("sent at", comment: "describing when the mail was send") + " " + time.string(from: date))
        body.append("\n" + NSLocalizedString("To", comment: "describing adressee") + ": ")
        for addr in self.tos {
            body.append("\(addr.email), ")
        }
        if self.ccs.count > 0 {
            body.append("\n\(NSLocalizedString("Cc", comment: "")): ")
            for addr in self.ccs {
                body.append("\(addr.email), ")           }
        }
        body.append("\n" + NSLocalizedString("subject", comment: "describing what subject was choosen") + ": " + (self.subject))
        body.append("\n------------------------\n\n" + (self.body))
        body = body.components(separatedBy: "\n").map { line in
            if line.hasPrefix(">") {
                return ">" + line
            }
            return "> " + line
        }.reduce("", { $0 + "\n" + $1 })
        return body
    }
}

struct DisplayProtocols_Previews: PreviewProvider {
    static let mails = ProxyData.mails
    static let deviceName = "iPhone 11 Pro Max"
    
    static var previews: some View {
        ForEach(0..<mails.count) { index in
            ReadMainView<ProxyMail>(model: ReadModel(mail: mails[index]))
                .previewDisplayName(mails[index].previewName)
                .previewDevice(.init(rawValue: deviceName))
        }
    }
}
