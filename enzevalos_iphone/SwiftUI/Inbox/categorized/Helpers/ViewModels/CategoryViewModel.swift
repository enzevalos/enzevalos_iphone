//
//  CategoryViewModel.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 17.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI
import Combine
import CoreData

/**
 The ViewModel every category gets provided
 */
class CategoryViewModel : ReasonedObservableObject,Identifiable,Equatable{
        

    let id : String
    let name:String
    
    // MARK: - Accessors

    @PPublished(.custom("For HomeModel")) var isActive     = false
    
    @Published var constData    = CategoryData()
    
    var Ssize : Binding<CGFloat?> { Binding(
        get:{
            self.isActive ? nil : self.constData.size
        },
        set:{
            if let newSize = $0{
                self.constData.size = newSize
            }else{
                self.isActive = false
            }
        }
    )}
    var size : CGFloat? {Ssize.wrappedValue}
    
    ///- returns: all the Mails in this category in this folder matching the current search
    var allMails : [eMail]{
        let mailsInFolder = mailsInCategoryController.fetchedObjects ?? []
        return mailsInFolder.filter(search.filter)
    }
    
    ///- returns: all the .shouldOpen Mails in this category in this folder matching the current search
    var shouldOpenMails : [eMail] {
        mailsInCategoryController.fetchedObjects?.filter(search.filter) ?? []
    }
    
    ///- returns: the unread and the total amount of mail in this category as a Tuple
    var mailAmount : (Int,Int) {
        return(allMails.filter({mail in mail.isRead}).count, allMails.count)
    }
    
    ///- returns: the timestamp when this categor was last active
    var lastMailTimestamp : Date? {
        guard shouldOpenMails.count > 0 else{
            return nil
        }
        return shouldOpenMails[0].date
    }
    
// MARK: - helper/private vars
    let filter:MailFilter
    let andPred:NSPredicate
    private let dataProvider = ProcessInfo.processInfo.environment["XCODE_RUNNING_FOR_PREVIEWS"] != "1" ? PersistentDataProvider.dataProvider : PersistentDataProvider.proxyPersistentDataProvider
    
    @Published private var mailsInCategoryController : NSFetchedResultsController<MailRecord> = PersistentDataProvider.dataProvider.generateNoMailsController()
    @Published private var importantMailsInCategoryController : NSFetchedResultsController<MailRecord> = PersistentDataProvider.dataProvider.generateNoMailsController()
    
    
    @Published var search = Search()
    private var folder = MailHandler.INBOX
    
// MARK: - helper functions
    func hasThisData(
        name    :   String,
        folder  :   String,
        consts  :   CategoryData
    )->Bool{
        self.name==name && self.folder==folder && self.constData==consts
    }
    
    
    func updateSearch(_ search : Search){
        self.search=search
        objectWillChange.send()
    }
    
    func updateFolder(_ folderpath : String){
        mailsInCategoryController = dataProvider.generateFetchedMailsInFolderAndCategoryResultsController(
            folderpath: folderpath,
            category: self.name,
            andPredicate: andPred
        )
        mailsInCategoryController = dataProvider.generateFetchedMailsInFolderAndCategoryResultsController(
            folderpath: folderpath,
            category: self.name,
            andPredicate: andPred,
            important: true
        )
        refresh()
    }
    
    ///refresh the database to accomondate for new mails / new category
    func deepRefresh(){
        do {
            try self.dataProvider.setCategoryOpenability(category:name, filter: self.filter)
        }catch{
            //mmm handle error
        }
        refresh()
    }
    
    func refresh(){
        //refetch
        try? mailsInCategoryController.performFetch()
        objectWillChange.send()
    }
    
    
    
    //MARK: - initialization
    
    init(
        name        :   String,
        filter      :   @escaping MailFilter,
        andPredicate:   NSPredicate
    ) {
        let standardFolder = MailHandler.INBOX
        
        self.id             = name
        self.name           = name
        self.filter         = filter
        self.andPred        = andPredicate
        
        self.mailsInCategoryController =
            dataProvider.generateFetchedMailsInFolderAndCategoryResultsController(
                folderpath: standardFolder,
                category: self.name,
                andPredicate: andPredicate
            )
        
        self.importantMailsInCategoryController =
            dataProvider.generateFetchedMailsInFolderAndCategoryResultsController(
                folderpath: standardFolder,
                category: self.name,
                andPredicate: andPredicate,
                important: true
            )
        
        
        super.init()
        
        deepRefresh()
    }
    static func == (lhs: CategoryViewModel, rhs: CategoryViewModel) -> Bool {
        lhs.id == rhs.id
    }
}


struct CategoryData:Equatable{
    var icon        :   Image   = Image(systemName: "envelope.fill")
    var description :   String  = ""
    var size        :   CGFloat = smallerPreviewSize
    var isResizable :   Bool    = true
    var searchFields:   [SearchFilter] = SearchFilterCollections.Standarts
}
