//
//  AddressRecord+CoreDataClass.swift
//  
//
//  Created by Oliver Wiese on 16.10.20.
//
//

import Foundation
import CoreData

@objc(AddressRecord)
public class AddressRecord: NSManagedObject, Identifiable {
    public let id = UUID()
}
