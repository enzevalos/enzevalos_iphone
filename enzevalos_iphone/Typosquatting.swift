//
//  Typosquatting.swift
//  enzevalos_iphone
//
//  Created by Lauren Elden on 29.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation
import TLDExtract

class Typosquatting {
    
    /**
     Compares a url's root domain with all root domain of all elements in a given url list.
     false: if the URL's root domain has an unallowed edit distance with any other root domain.
     */
    func compareURLs(myUrl: String, urls: [String]) -> (Bool, [String]) {
        let rootDomain = myUrl.getRootDomain()
        var isAllowedEditDistance = true
        var closeStrings = [String]()
        for url in urls {
            let rd = url.getRootDomain()
            if !rootDomain.isAllowedDistance(str: rd, allowedEditDistance: 4) {
                // 2 domains contain less than 4 differences --> warning here needed
                isAllowedEditDistance = false
                closeStrings.append(url)
            }
        }
        return (isAllowedEditDistance , closeStrings)
    }
    
    /**
     Compares a url's root domain with all root domain of all elements in a given url list.
     Returns an dictionary with the urls as the keys and and a bollean if it fits the allowed editdistance as  value
     isAllowedDistance is false: if the URL's root domain has an unallowed edit distance with any other root domain.
     */
    func compareURLsAsDic(url: String, urls: [String], allowedEditDistance: Int) -> [String:Bool] {
        let rootDomain = url.getRootDomain()
        var isAllowedEditDistance = true
        var typoDic: [String:Bool] = [:]
        for url in urls {
            let rd = url.getRootDomain()
            if !rootDomain.isAllowedDistance(str: rd, allowedEditDistance: allowedEditDistance) {
                // 2 domains contain less than 4 differences --> warning here needed
                isAllowedEditDistance = false
                typoDic.updateValue(isAllowedEditDistance, forKey : url)
                isAllowedEditDistance = true
            }
            else{
                typoDic.updateValue(isAllowedEditDistance, forKey : url)
            }
        }
        return typoDic
    }

    /**
    Compares a url's root domain with all root domain of all elements in a givin url list.
    Returns an dictionary with the URLs as keys and the Editdistance as value
    */
      func getURLEditdistanceAsDic(url: String, urls: [String]) -> [String:Int] {
          let rootDomain = url.getRootDomain()
          var urlEditDic: [String:Int] = [:]
          var editDistance = 0
          for url in urls {
              let rd = url.getRootDomain()
              editDistance = rootDomain.getEditDistance(str: rd)
              urlEditDic.updateValue(editDistance, forKey : url)
          }
          return urlEditDic
      }
    
    /**
     Compares a URLs second-level domain with all elements of a givin domain list.
     false: if the URL's second-level domain has an unallowed edit distance with any other domain.
     */
    func compareURLWithSLDList(url: String, domains: [String]) -> Bool {
        let secondLevelDomain = url.getSecondLevelDomain()
        var isAllowedEditDistance = true
        for domain in domains {
            if !secondLevelDomain.isAllowedDistance(str: domain, allowedEditDistance: 4) {
                // 2 domains contain less than 4 differences --> warning here needed
                isAllowedEditDistance = false
            }
        }
        return isAllowedEditDistance
    }
    
    /**
    Compares a url's second level domain with all  domains of all elements in a givin second level domain list.
    Returns an dictionary with the domains as the keys and and a boolean if it fits the allowed edit-distance as  value
    isAllowedDistance is false: if the URL's second level domain has an unallowed edit distance with any other domain.
    */
    func compareURLWithSLDListAsDic(url: String, domains: [String], allowedEditDistance: Int) -> [String:Bool] {
           let secondLevelDomain = url.getSecondLevelDomain()
           var isAllowedEditDistance = true
           var typoDic: [String:Bool] = [:]
           for domain in domains {
               if !secondLevelDomain.isAllowedDistance(str: domain, allowedEditDistance: allowedEditDistance) {
                   // 2 domains contain less than 4 differences --> warning here needed
                   isAllowedEditDistance = false
                   typoDic.updateValue(isAllowedEditDistance, forKey : domain)
                   isAllowedEditDistance = true
               }
               else{
                   typoDic.updateValue(isAllowedEditDistance, forKey : domain)
               }
           }
           return typoDic
       }
    
    /**
    Compares a url's second-level domain with all elements of a givin domain list.
    Returns an dictionary with the second level domain as keys and the editdistance as value
     */
     func getSLDEditdistanceAsDic(url: String, domains: [String]) -> [String:Int] {
         let secondLevelDomain = url.getSecondLevelDomain()
         var sldEditDic: [String:Int] = [:]
         var editDistance = 0
         for domain in domains {
             editDistance = secondLevelDomain.getEditDistance(str: domain)
             sldEditDic.updateValue(editDistance, forKey : domain)
         }
         return sldEditDic
     }
    
    /**
     Compares a second-level domain with all elements of a givin second-level domain list.
     false: if the second-level domain has an unallowed edit distance with any other domain.
    */
    func compareDomainWithDomians(secondLvlDomain: String, domains: [String], allowedEditDistance: Int) -> Bool {
        var isAllowedEditDistance = true
        for domain in domains {
            if !secondLvlDomain.isAllowedDistance(str: domain, allowedEditDistance: allowedEditDistance) {
                // 2 domains contain less than 4 differences --> warning here needed
                isAllowedEditDistance = false
            }
        }
        return isAllowedEditDistance
    }
    
    /**
    Compares a second level domain with all elements of a givin second-level domain list.
    Returns an dictionary with the domains as the keys and and a boolean if it fits the allowed editdistance as  value
    isAllowedDistance is false: if the  second level domain has an unallowed edit distance with any other domain.
    */
    func compareDomainWithDomiansAsDic(secondLvlDomain: String, domains: [String], allowedEditDistance: Int) -> [String:Bool] {
           var isAllowedEditDistance = true
           var typoDic: [String:Bool] = [:]
           for domain in domains {
               if !secondLvlDomain.isAllowedDistance(str: domain, allowedEditDistance: allowedEditDistance) {
                   // 2 domains contain less than 4 differences --> warning here needed
                   isAllowedEditDistance = false
                   typoDic.updateValue(isAllowedEditDistance, forKey : domain)
                   isAllowedEditDistance = true
               }
               else{
                   typoDic.updateValue(isAllowedEditDistance, forKey : domain)
               }
           }
           return typoDic
       }
    
    /**
     Compares a domain with all elements of a givin domain list.
     Returns an dictionary with domains as keys and the editdistance as value
    */
    func getDomainEditDistanceAsDic(domain: String, domains: [String]) -> [String:Int] {
        var domainEditDic: [String:Int] = [:]
        var editDistance = 0
        for dom in domains {
            editDistance = domain.getEditDistance(str: dom)
            domainEditDic.updateValue(editDistance, forKey : dom)
        }
        return domainEditDic
    }
    
    /**
     Receives a mail body and takes out the URLs and filters valid URLs by verifying their Root Domain
     Return a list of valid URLs
     */
    func isValidRD(mailBody: String) -> [String] {
        let urls = mailBody.findURL()
        var foundRDs: [String] = []
        for url in urls {
            let urlString: String = url!
            if !urlString.containsUpperCaseLetter() {
                guard let result: TLDResult = extractor.parse(urlString) else { continue }
                if let rd = result.rootDomain {
                    let rdPatternRegEx = "^([a-z0-9])*([a-z0-9-]+\\.)*[a-z0-9]*([a-z0-9-]+)*[a-z0-9]+\\.[a-z]{2,11}?$" // for hostname
                    let range = NSRange(location: 0, length: rd.utf16.count)
                    let regex = try! NSRegularExpression(pattern: rdPatternRegEx)
                    if regex.firstMatch(in: rd, options:[], range: range) != nil {
                        foundRDs.append(rd)
                    } else {
                        // domain did not pass the regex --> warning here needed
                    }
                }
            } else {
                // domain contains capital letters --> warning here needed
            }
        }
        // Returns non-Critical Root Domain list
        return (foundRDs)
    }
}
