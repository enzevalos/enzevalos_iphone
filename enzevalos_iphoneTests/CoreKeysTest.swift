//
//  CoreKeysTest.swift
//  enzevalos_iphoneTests
//
//  Created by Oliver Wiese on 19.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import XCTest

@testable import enzevalos_iphone
class CoreKeysTest: XCTestCase {
    
    var provider = PersistentDataProvider()
    let email = "vic@example.com"
    
    private func deleteAll() {
        provider = PersistentDataProvider()
        let deleteExpectation = expectation(description: "Delete all data!")
        provider.deleteAll(completionHandler: {error in
            if let error = error {
                XCTFail("Error while deleting all! \(error)")
            }
            deleteExpectation.fulfill()
        })
        wait(for: [deleteExpectation], timeout: TimeInterval(20))
        provider.reset()
        let frc = provider.fetchedMailResultsController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 0)
        provider = PersistentDataProvider()
    }

    override func setUpWithError() throws {
        deleteAll()
    }

    override func tearDownWithError() throws {
        deleteAll()
    }
    
    func testMultipleCreateSecretKeys() throws {
        
        // Concurency Problem. Errors happens only when we use backgroundContext.
        testCreateSecretKeys(i: 3)
    }
    
    func testCreateSecretKey() {
        testCreateSecretKeys(i: 1)
    }
    
    func testMultipleCreatePublicKeys() {
        testCreateSecretKeys(i: 10)
    }
    
    //TODO ADD TEST CASE impport one public key using core data

    func testCreatePublicKey(){
        testCreatePublicKeys(i: 1)
    }
    
    private func testCreateSecretKeys(i: Int) {
        for _ in 1...i {
            do {
                try createSecretKey(email: email)
            } catch {
                XCTFail("Could not create key")
            }
        }
        provider.reset()
        let frc = provider.fetchedSecretKeyResultsController
        try? frc.performFetch()
        if let keys = frc.fetchedObjects {
            XCTAssertEqual(keys.count, i)
            guard let key = keys.first, let addresses = key.usedAddress?.allObjects as? [AddressRecord] else {
                XCTFail("No addresses")
                return
            }
            XCTAssertEqual(addresses.count, 1)
            for a in addresses {
                XCTAssertEqual(a.usedSecretKeys?.count ?? -1, i)
                XCTAssertEqual(a.usedPublicKeys?.count ?? -1, i)
            }
        }
    }
    
    private func testCreatePublicKeys(i: Int) {
        for _ in 1...i {
            do {
                try createPublicKey(email: email)
            } catch {
                XCTFail("Could not create key")
            }
        }
        provider.reset()
        let frc = provider.fetchedPublicKeyResultsController
        try? frc.performFetch()
        if let keys = frc.fetchedObjects {
            XCTAssertEqual(keys.count, i)
            guard let key = keys.first, let addresses = key.usedAddress?.allObjects as? [AddressRecord]else {
                XCTFail("No addresses")
                return
            }
            XCTAssertEqual(addresses.count, 1)
            for a in addresses {
                XCTAssertEqual(a.usedSecretKeys?.count ?? -1, 0)
                XCTAssertEqual(a.usedPublicKeys?.count ?? -1, i)
            }
        }
    }


    private func createSecretKey(email: String) throws {
        let createExpectation = expectation(description: "Create key!")
        var keyCounter = 0
        if let keys = provider.fetchedSecretKeyResultsController.fetchedObjects {
            keyCounter = keys.count
        }
        let keyID = provider.createNewSecretKey(addr: email, completionHandler: {error in
            if let error = error {
                XCTFail("Error while creating a new key: \(error)")
            }
            createExpectation.fulfill()
        })
        wait(for: [createExpectation], timeout: TimeInterval(30))
        try provider.fetchedSecretKeyResultsController.performFetch()
        if let keys = provider.fetchedSecretKeyResultsController.fetchedObjects {
            XCTAssertEqual(keys.count, keyCounter + 1, "Wrong keys! #keys: \(keys.count)")
            var foundKey = false
            for key in keys {
                if key.fingerprint == keyID {
                    foundKey = true
                    XCTAssertNotNil(key.myPublicKeyRecord)
                    if let pk = key.myPublicKeyRecord {
                        XCTAssertEqual(key.fingerprint, pk.fingerprint,"Fingerprint mismatch: \(String(describing: key.fingerprint)) != \(String(describing: pk.fingerprint))")
                        XCTAssertEqual(pk.mySecretKeyRecord, key, "Missing SecretKeyRecord")
                    }
                    if let addr = key.usedAddress?.allObjects.first as? AddressRecord{
                        XCTAssertEqual(addr.email, email, "Wrong name... \(addr.email)")
                        XCTAssertGreaterThanOrEqual(addr.usedPublicKeys?.count ?? -1, 1, "No public key recoord...")
                        XCTAssertGreaterThanOrEqual(addr.usedSecretKeys?.count ?? -1, 1, "No secret key recoord...")

                    } else {
                        XCTFail("No address...")
                    }
                }
            }
            XCTAssertTrue(foundKey)
        } else {
            XCTFail("No key after creating one...")
        }
    }
    
    private func createPublicKey(email: String) throws {
        let createExpectation = expectation(description: "Create key!")
        var keyCounter = 0
        if let keys = provider.fetchedPublicKeyResultsController.fetchedObjects {
            keyCounter = keys.count
        }
        let pgp = SwiftPGP()
        let id = pgp.generateKey(adr: email)
        let prop = PublicKeyProperties(fingerprint: id, cryptoProtocol: .PGP, origin: .KeyServer, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: nil, usedAddresses: [AddressProperties(email: email)])
        
        provider.importNewData(from: [prop], completionHandler: {error in
            if let error = error {
                XCTFail("Error while creating a new key: \(error)")
            }
            createExpectation.fulfill()
        })
        wait(for: [createExpectation], timeout: TimeInterval(300000))
        try provider.fetchedPublicKeyResultsController.performFetch()
        if let keys = provider.fetchedPublicKeyResultsController.fetchedObjects {
            XCTAssertEqual(keys.count, keyCounter + 1, "Wrong keys! #keys: \(keys.count)")
            var foundKey = false
            for key in keys {
                XCTAssertNotNil(key.fingerprint)
                if key.fingerprint == id {
                    foundKey = true
                    XCTAssertEqual(key.mySecretKeyRecord, nil, "Missing SecretKeyRecord")
                    if let addr = key.usedAddress?.allObjects.first as? AddressRecord{
                        XCTAssertEqual(addr.email, email, "Wrong name... \(addr.email)")
                        XCTAssertGreaterThanOrEqual(addr.usedPublicKeys?.count ?? -1, 1, "No public key recoord...")
                        XCTAssertGreaterThanOrEqual(addr.usedSecretKeys?.count ?? -1, 0, "No secret key recoord...")

                    } else {
                        XCTFail("No address...")
                    }
                }
            }
            XCTAssertTrue(foundKey)
        } else {
            XCTFail("No key after creating one...")
        }
    }
    
    private func createAddress(email: String) {
        let addr1 = AddressProperties(email: email)
        let importExpectation = expectation(description: "Import new addresses!")
        provider.importNewData(from: [addr1], completionHandler: {error  in
            if let error = error {
                XCTFail("Error while importing addresses! \(error)")
            }
            importExpectation.fulfill()
        })
        wait(for: [importExpectation], timeout: TimeInterval(20))
        provider.reset()
        let frc = provider.fetchedAddressResultController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 1, "Missing addresses! \(frc.fetchedObjects?.count ?? 0)")
        if let objects = frc.fetchedObjects {
            for obj in objects {
                XCTAssertEqual(obj.inFromField?.count, 0)
                XCTAssertEqual(obj.email, email)
            }
        } else {
            XCTFail("No addresses!")
        }
    }
    
    func testAddSecretKeyToExistingAddress(){
        createAddress(email: email)
        let createExpectation = expectation(description: "Create a first key!")
        let id = provider.createNewSecretKey(addr: email, completionHandler: {error in
            if let error = error {
                XCTFail("Error while creating a new key: \(error)")
            }
            createExpectation.fulfill()
        })
        wait(for: [createExpectation], timeout: TimeInterval(30))
        let frc = provider.fetchedAddressResultController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 1, "Missing addresses! \(frc.fetchedObjects?.count ?? 0)")
        if let addresses = frc.fetchedObjects {
            for a in addresses {
                if a.email == email {
                    XCTAssertEqual(a.usedSecretKeys?.count ?? -1, 1)
                    XCTAssertTrue(a.hasPublicKey)
                    XCTAssertEqual(a.primaryKey?.keyID ?? "NO KEY", id)
                } else {
                    XCTAssertNotEqual(a.email, "")
                    XCTFail("Unknown address: \(a.email)")
                }
            }
        }
    }

}
