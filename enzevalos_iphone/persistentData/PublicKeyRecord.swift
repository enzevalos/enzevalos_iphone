//
//  PublicKeyRecord.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 10.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import CoreData

enum Origin: Int16 {
    case AutocryptHeader = 0
    case AutocryptGossip = 1
    case MailAttachment = 2
    case KeyServer = 3
    case Generated = 4
    case FileTransfer = 5
}
 /*
 TODO: signed mails should be sorted! ACCORDING TO date
 */


extension PublicKeyRecord {
    static var FetchRequest: NSFetchRequest<PublicKeyRecord> {
        let request: NSFetchRequest<PublicKeyRecord> = NSFetchRequest<PublicKeyRecord>(entityName: PublicKeyRecord.entityName)
        request.sortDescriptors = [NSSortDescriptor(key: "discovery", ascending: true)]
        return request
    }
}
extension PublicKeyRecord {
    static let entityName = "PublicKeyRecord"
    
    func update(property: PublicKeyProperties) {
        self.fingerprint = property.fingerprint
        self.cryptoProtocol = property.cryptoProtocol.asInt()
        self.isSecret = false
        
        if discovery == nil {
            discovery = Date()
        }
        if let origin = property.origin?.rawValue {
            self.origin = origin
        }
        if let lastSeen = property.lastSeenInAutocryptHeader {
            if let last = self.lastSeenInAutocryptHeader {
                if last < lastSeen  {
                    self.lastSeenInAutocryptHeader = lastSeen
                }
            }
            else {
                self.lastSeenInAutocryptHeader = lastSeen
            }
            
        }
        if let prefEnc = property.preferEncryption {
            self.preferEncryption = prefEnc.asInt()
        }
        if let lastSeen = property.lastSeenSignedMail {
            if let last = self.lastSeenSignedMail {
                if last < lastSeen  {
                    self.lastSeenSignedMail = lastSeen
                }
            }
            else {
                self.lastSeenSignedMail = lastSeen
            }
        }
    }
}

extension PublicKeyRecord: DisplayKey {
    var keyID: String {
        return fingerprint ?? ""
    }
    
    var signedMailsCounter: Int {
        return self.signedMails?.count ?? 0
    }
    
    var sentMailsCounter: Int {
        // TODO: Create a relationship for encrypted mails...
        return 0
    }
    var lastSeen: Date? {
        return self.lastSeenSignedMail
    }
    
    var type: CryptoScheme {
        return CryptoScheme.find(i: Int(self.cryptoProtocol))
    }
    
    var isPreferedKey: Bool {
        return self.isPrimaryKeyOf != nil
    }
}
