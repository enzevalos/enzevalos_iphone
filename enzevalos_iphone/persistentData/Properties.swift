//
//  Properties.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 05.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//
protocol DataPropertyProtocol {
    var entityName: String { get }

    func update(m: Any) -> Bool
}

/**
 A struct encapsulating the properties of a E-Mail.
 TODO: routing information, gmailMessageID, gmailThreadID, ThreadID
  */
struct MailProperties: DataPropertyProtocol {
    
    let entityName = "MailRecord"
    
    
    // Header properties
    let messageID: String
    let uid: UInt64
    let subject: String
    let date: Date
    let flags: Int16
    let from: AddressProperties?
    var to: [AddressProperties] = []
    var cc: [AddressProperties] = []
    var bcc: [AddressProperties] = []
    let xMailer: String? = nil
    
    let folder: FolderProperties
    
    // Content properties
    var body: String = ""
    var attachments: [AttachmentProperties] = []
    
    // Security properties & keys
    let signatureState: Int16
    let encryptionState: Int16
    var signatureKey: PublicKeyProperties?
    var decryptionKey: SecretKeyProperties?
    
    var autocryptHeaderKey: [PublicKeyProperties] = []
    var attachedPublicKeys: [PublicKeyProperties] = []
    var attachedSecretKeys: [SecretKeyProperties] = []
    
    
    
    func update(m: Any) -> Bool {
        if let m = m as? MailRecord {
            m.update(with: self)
            return true
        }
        return false
    }
}

struct AddressProperties: DataPropertyProtocol {
    let entityName = "AddressRecord"
    
    let email: String 
    var name: String? = nil
    
    func update(m: Any) -> Bool {
        if let m = m as? AddressRecord {
            m.update(with: self)
            return true
        }
        return false
    }
}

struct FolderProperties: DataPropertyProtocol{
    let entityName = "FolderRecord"
    
    var delimiter: String?
    var uidValidity: NSDecimalNumber?
    
    var lastUpdate: Date?
    var maxUID: NSDecimalNumber?
    var minUID: NSDecimalNumber?
    
    let path: String
    
    var parent: [FolderProperties]?
    var children: [FolderProperties]?
    var flags: Int16
    
    func update(m: Any) -> Bool {
        if let folder = m as? FolderRecord {
            folder.update(properties: self)
            return true
        }
        return false
    }
}

struct PublicKeyProperties: DataPropertyProtocol {
    let entityName = "PublicKeyRecord"
    
    let fingerprint: String
    let cryptoProtocol: CryptoScheme
    
    var origin: Origin?
    var preferEncryption: AutocryptState?

    var lastSeenInAutocryptHeader: Date?
    var lastSeenSignedMail: Date?
    
    var secretKeyProperty: SecretKeyProperties?
    var usedAddresses: [AddressProperties]?
    
    func update(m: Any) -> Bool {
        if let pk = m as? PublicKeyRecord {
            pk.update(property: self)
            return true
        }
        return false
    }
}

struct SecretKeyProperties: DataPropertyProtocol {
    let entityName = "SecretKeyRecord"
    
    let fingerprint: String
    let cryptoProtocol: CryptoScheme
    var preferedKey = false
    
    var usedAddresses: [AddressProperties]?

    
    func update(m: Any) -> Bool {
        if let sk = m as? SecretKeyRecord {
            sk.update(property: self)
            return true
        }
        return false
    }
    
}

struct AttachmentProperties: DataPropertyProtocol {
    let entityName = "AttachmentRecord"
    
    let type: Int16
    let name: String
    let mimeType: String
    let contentID: String
    let data: Data
    
    func update(m: Any) -> Bool {
        return false
    }
    
    
}


extension MCOAddress {
    func export() -> AddressProperties {
        return AddressProperties(email: self.mailbox)
    }
}
