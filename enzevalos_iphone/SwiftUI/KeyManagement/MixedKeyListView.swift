//
//  MixedKeyListView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 31.12.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct MixedKeyListView: View {
    let model = KeyManagementModel()
    
    var body: some View {
        if model.storedKeys.count == 0 {
            Text("Import.List.Hint")
                .navigationTitle("Import.List.Title")
        } else {
            List(model.storedKeys, id: \.self){ key in
                TempKeyRow(model: model, key: key)
            }
            .navigationTitle("Import.List.Title")
        }
    }
        
}

struct MixedKeyListView_Previews: PreviewProvider {
    static var previews: some View {
        MixedKeyListView()
    }
}
