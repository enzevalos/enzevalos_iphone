//
//  SampleCategoryV2.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 17.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI
import TagNavVi

let biggerPreviewSize:CGFloat = 400 , smallerPreviewSize:CGFloat = 200

///this manages to create the logic a category needs with aut other devs needing to worry. just use this struct for all your categories
struct CategoryView<UI:View>:View,CategoryViewToAnyView,Identifiable{
    
    typealias Content = (CategoryViewModel)->UI
    
    init(
        name            : String        ,
        description     : String    = "",
        icon            : Image     = Image(systemName: "envelope.fill"),
        isBigger        : Bool      = false,
        isResizable     : Bool      = true,
        searchFields    : [SearchFilter] = SearchFilterCollections.Standarts,
        filter          : @escaping MailFilter,
        andPredicate    : NSPredicate = NSPredicate(value: true),
        _ content       : @escaping Content
    ) {
        self.id             = name
        self.name           = name
        self.description    = description
        self.icon           = icon
        self.content        = content
        self.categoryVM     =   CategoryViewModelProvider.s.createViewModel(
                                    for         : name,
                                    with        : filter,
                                    and         : andPredicate,
                                    data        : CategoryData(
                                                    icon        : icon,
                                                    description : description,
                                                    size        : isBigger ? biggerPreviewSize : smallerPreviewSize,
                                                    isResizable : isResizable,
                                                    searchFields : searchFields
                                                )
                                )
    }
    
    @ObservedObject
            var categoryVM  : CategoryViewModel
            var id                          : CategoryIDType
            var name                        : String
    private var description                 : String
    private var icon                        : Image
    private var content                     : Content
    
    var body: some View {
        CategoryViewInner<UI>(
            category: categoryVM
        ){
            content(categoryVM)
        }
        .home(0)
    }
    
    
    
    
    func toAnyView()->CategoryView<AnyView>{
        CategoryView<AnyView>(vm: categoryVM){_ in
            AnyView(content(categoryVM))
        }
    }
    private init( vm : CategoryViewModel, _ content : @escaping Content) {
        self.id             = vm.name
        self.name           = vm.name
        self.description    = vm.constData.description
        self.icon           = vm.constData.icon
        self.content        = content
        self.categoryVM     = vm
    }

}

protocol CategoryViewToAnyView {
    func toAnyView()->CategoryView<AnyView>
}
