//
//  SelectableTextView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 17.04.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

class TextViewDelegate:NSObject, UITextViewDelegate {

    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if url.scheme == "mailto" {
            /*
            if let coord = AppDelegate.getAppDelegate().readViewCoordinator {
                let to = [MailAddress] () //DataHandler.handler.getMailAddress(url.absoluteString.replacingOccurrences(of: "mailto:", with: ""), temporary: true)
                coord.pushComposeView(to: to, cc: [], bcc: [], subject: "", body: "", responseType: .Reply)
            }
 */
           return false
        }
        return true
       }
    
}

struct SelectableTextView: UIViewRepresentable {
    var text: String
    var textDelegate = TextViewDelegate()
    


    init(_ text: String) {
        self.text = text
    }

    func makeUIView(context: Context) -> UITextView {
        let view = UITextView()
        view.isSelectable = true
        view.isEditable = false
        view.isScrollEnabled = true
        view.font = .preferredFont(forTextStyle: .body)
        view.dataDetectorTypes = .link
        view.delegate = textDelegate
        return view
    }

    func updateUIView(_ label: UITextView, context: Context) {
        label.text = text
    }
    
   
}


struct AttributedTextView_Previews: PreviewProvider {
    static let text = "Hello World www.fu-berlin.de https://www.google.com"
    static var previews: some View {
        VStack{
            SelectableTextView(text)
            Text(text)
        }
    }
}
