//
//  Card.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 08.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

/// a beautiful Card with a slight, customizable 3D effect 
struct Card<Content: View> : View  {

    var shadowElevation:CGFloat = 30
    var cornerRadius:CGFloat = 35
    var fade:CGFloat = 0
    var effect3D: CGFloat = 4
    var padding : CGFloat? 
    
    var content: () -> Content
    
    var body: some View {
        ZStack{
            shadow
            ZStack{
                Rectangle().fill(Color(UIColor.systemBackground))
                    .cornerRadius(cornerRadius)
                    .blur(radius: min(effect3D,(cornerRadius+shadowElevation)/10))
                Rectangle().fill(Color.gray.opacity(0.2))
                    .cornerRadius(cornerRadius)
                    .blur(radius: fade).clipped()
                content()
                    .padding(padding ?? 20)
            }.cornerRadius(cornerRadius)
            
        }
    }
    
    var shadow : some View {
        Rectangle().fill(Color.gray)
            .cornerRadius(cornerRadius)
            .padding((shadowElevation+effect3D/2)/5*3)
            .shadow(radius:shadowElevation,y:shadowElevation/5*4)
    }

}


struct Card_Previews: PreviewProvider {
    static var previews: some View {
        Group{
            Card(){
                Text("Sample Content")
            }.scaledToFit().padding()
        }
    }
}
