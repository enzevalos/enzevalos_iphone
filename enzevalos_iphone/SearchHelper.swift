//
//  SearchHelper.swift
//  enzevalos_iphone
//
//  Created by lazarog98 on 26.02.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation

/// Function used to find mails that contain the search terms. All terms (separated by spaces) need to be contained in the search text.
/// - Parameters:
///   - content: The String that will be searched
///   - searchText: Search terms (space-separated) that will be searched for
func containsSearchTerms(content: String?, searchTerm: String) -> Bool {
    guard searchTerm.count > 0 else {
        return true             // Case empty search
    }
    
    guard let content = content else {
        return false            // Case Mail has no body/subject
    }
        
    var longterms: [String] = []
    var terms: [String] = []
    
    // Break string into substrings separated by quotation marks
    longterms = searchTerm.components(separatedBy: "\"")
    
    // Even elements will be outside the quotation marks and need to be separated again
    var i = 0
    while (i < longterms.count) {
        if i % 2 == 0 {
            terms.append(contentsOf: longterms[i].lowercased().components(separatedBy: " "))
        } else {
            terms.append(longterms[i].lowercased())
        }
        i += 1
    }
    
    var found = true
    for t in terms {
        if !t.isEmpty {
            found = found && content.lowercased().contains(t)
        }
    }
    return found
}
