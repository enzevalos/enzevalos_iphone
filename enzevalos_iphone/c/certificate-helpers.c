//
//  certificate-helpers.c
//  enzevalos_iphone
//
//  Created by lazarog98 on 18.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

#include "certificate-helpers.h"

cert_info * get_cert_data(const char *pem)
{
    BIO *cert_bio = NULL;
    // recipient certificate
    X509 *cert = NULL;
    cert_info *info = malloc(sizeof(cert_info));
    array_with_length *emails = NULL;
    int size=0;
    
    cert_bio = BIO_new_mem_buf(pem, (int) strlen(pem));
    
    if (!cert_bio) {
        printf("Failed reading mykey.pem!\n");
        goto deinit;
    }
    
    cert = PEM_read_bio_X509(cert_bio, NULL, 0, NULL);
    if (!cert) {
        printf("Failed reading pem\n");
        goto deinit;
    }
    
    emails = get_emails(cert);
    size = emails->size;
        
    //ELIMINATED MAIN MEMORY LEAK; STILL HAVE A SMALL ONE: after running a couple of minutes nonstop loading certs 70mb
    
    info->num_emails = size;
    info->emails = malloc(size*sizeof(char*));
    char ** arr = (char**) emails->arr;
    for (int i = 0; i<size; i++) {
        unsigned long len = strlen(arr[i]);
        info->emails[i] = malloc(len + 1);
        info->emails[i][len] = 0;
        memcpy(info->emails[i], arr[i], len);
        free(arr[i]);
    }
    free(arr);
    free(emails);
    
    info->date_end = get_end_date(cert);
    info->date_start = get_start_date(cert);
    info->subject = get_subject(cert);
    info->issuer = get_issuer(cert);
deinit:
    BIO_free(cert_bio);
    X509_free(cert);
    return info;
}

char * get_issuer(X509 *cert) {

    char *res = NULL;
    BIO *out = NULL;
    out = BIO_new(BIO_s_mem());
    X509_NAME *name = NULL;
    name = X509_get_issuer_name(cert);
    X509_NAME_print_ex(out, name, 0, XN_FLAG_MULTILINE);
    bio_to_str(out, &res);
deinit:
    BIO_free(out);
    return res;
}

char * get_subject(X509 *cert) {
    char *res = NULL;
    BIO *out = NULL;
    out = BIO_new(BIO_s_mem());
    X509_NAME *name = NULL;
    name = X509_get_subject_name(cert);
    X509_NAME_print_ex(out, name, 0, XN_FLAG_MULTILINE);
    bio_to_str(out, &res);
deinit:
    BIO_free(out);
    return res;
}

char * get_start_date(X509 *cert) {
    char *res = NULL;
    BIO *out = NULL;
    out = BIO_new(BIO_s_mem());
    ASN1_TIME_print(out, X509_get0_notBefore(cert));
    bio_to_str(out, &res);
deinit:
    BIO_free(out);
    return res;
}

char * get_end_date(X509 *cert) {
    char *res = NULL;
    BIO *out = NULL;
    out = BIO_new(BIO_s_mem());
    ASN1_TIME_print(out, X509_get0_notAfter(cert));
    bio_to_str(out, &res);
deinit:
    BIO_free(out);
    return res;
}

array_with_length *get_emails(X509 *cert) {
    array_with_length *res = malloc(sizeof(array_with_length));
    int i = 0;
    STACK_OF(OPENSSL_STRING) *list = NULL;
    
    if (cert == NULL) {
        printf("Failed to get emails cause null cert!\n");
        goto deinit;
    }
    
    list = X509_get1_email(cert);
    
    const int emails_num = sk_OPENSSL_STRING_num(list);
    res->size = emails_num;
    
    if (emails_num == 0)
        goto deinit;
    
    char ** arr = init_str_arr(emails_num);
        
    for (i = 0; i < emails_num; i++) {
        char *temp = sk_OPENSSL_STRING_value(list, i);
        unsigned long size = strlen(temp);
        arr[i] = malloc(size+1);
        
        memcpy(arr[i], temp, size);
        arr[i][size] = 0;
        free(temp);
    }
    res->arr = arr;
deinit:
    sk_OPENSSL_STRING_free(list);
    return res;
}

X509 *get_cert_from_pem(const char *pem) {
    BIO *rec_cert_bio = NULL;
    // recipient certificate
    X509 *rec_cert = NULL;
    
    rec_cert_bio = BIO_new_mem_buf(pem, (int) strlen(pem));
    
    if (!rec_cert_bio) {
        printf("Failed reading mykey.pem!\n");
        goto deinit;
    }
    
    rec_cert = PEM_read_bio_X509(rec_cert_bio, NULL, 0, NULL);
    
    if (!rec_cert) {
        printf("Failed reading pem\n");
        goto deinit;
    }
deinit:
    BIO_free(rec_cert_bio);
    return rec_cert;
}

cert_info * new_cert_info()
{
    cert_info *i = malloc(sizeof(cert_info));
    i->date_end = NULL;
    i->date_start = NULL;
    i->emails = NULL;
    i->issuer = NULL;
    i->num_emails = 0;
    i->subject = NULL;
    return i;
}

void deallocate_cert_info( cert_info *c)
{
    free(c->date_end);
    free(c->date_start);
    if (c->num_emails > 0)
    {
        deallocate_str_arr(c->emails, c->num_emails);
    }
    free(c->issuer);
    free(c->subject);
    free(c);
}
