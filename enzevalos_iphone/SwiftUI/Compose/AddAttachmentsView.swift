//
//  AddAttachmentsView.swift
//  enzevalos_iphone
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import SwiftUI

/// A view that enables uploading an previewing files and pictures as attachments.
struct AddAttachmentsView: View {
    @ObservedObject var model: ComposeModel
    @State private var attachFile = false
    @State private var imageAttachment: UIImage?
    
    // Two sheet states possible: full screen preview OR image picker
    @State private var sheetState: SheetState?
    
    // TODO: image import and preview are currently not fully supported
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                // both upload buttons
                VStack {
                    attachFileButton
                    Spacer()
                    imageUploadButton
                        .padding(.bottom, 3) 
                }
                
                filePreviews
            }
            .padding(4)
            // sheet used for image import OR full screen attachment preview
            .frame(maxHeight: 141)
        }
        .sheet(item: $sheetState) { state in
            switch state {
            case .imagePicker:
                ImagePicker(image: $imageAttachment)
            case let .fullScreenPreview(attachment):
                Text(attachment.myName)
                    .font(Font.body.weight(.semibold))
                QuickLookView(name: attachment.myName,
                              data: attachment.myData,
                              shallDL: false)
                    .aspectRatio(100/141, contentMode: .fit)
            }
        }
    }
    
    /// a view that contains the upload button for files
    var attachFileButton: some View {
        Button {
            attachFile = true
        } label: {
            Image(systemName: "doc.badge.plus").font(.system(size: 50))
        }
        //  file import when button is pressed
        .fileImporter(isPresented: $attachFile, allowedContentTypes: [.plainText, .image, .pdf]) {
            res in
            do {
                // get fileURL of selected file
                let fileURL = try res.get()
                // get fileName from URL of that file
                let fileName = fileURL.lastPathComponent
                // get file data
                let fileData = try Data(contentsOf: fileURL)
                
                //  create Attachment and add in to the attachments list
                let newAttachment = Attachment(myName: fileName, myData: fileData)
                model.attachments.append(newAttachment)
            } catch {
                //  Error while loading file
                print("Error while importing file.")
            }
        }
    }
    
    /// a view that contains the upload button for pictures
    var imageUploadButton: some View {
        Button {
            sheetState = .imagePicker
        } label: {
            // try to match SF Symbol "doc.badge.plus" with "photo"
            ZStack(alignment: .bottomLeading) {
                Image(systemName: "photo").font(.system(size: 50))
                
                Image(systemName: "plus")
                    .font(Font.system(size: 19, weight: .bold))
                    .foregroundColor(Color(.systemBackground))
                    .padding(3)
                    .background(Circle().fill(Color.accentColor))
                    .padding(3)
                    .background(Circle().fill(Color(.systemBackground)))
                    .offset(x: -8, y: 8)
            }
        }
    }
    
    /// a view that contains several file previews together with their delete buttons
    var filePreviews: some View {
        ForEach(model.attachments.reversed()) { attachment in
            ZStack {
                // file preview using Quicklook
                // shallDL has to be true here, because QuickLookView will download
                // the file into the Documents Directory for us
                // it then uses that DD file to create a preview of the content
                QuickLookView(name: attachment.myName, data: attachment.myData, shallDL: true)
                    .padding(3)
                    .background(RoundedRectangle(cornerRadius: 5)
                                    .stroke(Color.secondary, lineWidth: 2))
                
                // a window that disables the interaction with QuickLookView
                Button {
                    sheetState = .fullScreenPreview(attachment)
                } label: {
                    Rectangle().fill(Color(.systemBackground).opacity(0.1))
                }
                
                VStack(alignment: .trailing) {
                    // delete button in upper right corner
                    Button {
                        // remove from attachments list and remove copy from Documents Directory
                        // to keep DD clean
                        if let deleteIndex = model.attachments.firstIndex(of: attachment) {
                            model.attachments[deleteIndex].removeFileFromDocumentsDirectory()
                            model.attachments.remove(at: deleteIndex)
                        }
                    } label: {
                        Image(systemName: "xmark").font(.system(size: 20))
                    }
                    .padding(5)
                    
                    Spacer()
                    // display file name and size
                    Text(attachment.myName + ", " + attachment.countData())
                        .lineLimit(1)
                        .font(.caption)
                        .foregroundColor(.secondary)
                        .truncationMode(.middle)
                        .frame(maxWidth: .infinity)
                        .padding(3)
                        .background(RoundedRectangle(cornerRadius: 5)
                                        .fill(Color(.systemBackground).opacity(0.8)))
                }
                .frame(maxWidth: .infinity)
            }
            .frame(width: 100)
        }
    }
    
    ///  States to control several sheet views
    enum SheetState: Identifiable {
        case imagePicker                    // user presses imageUploadButton
        case fullScreenPreview(Attachment)  // user taps on a file preview
        
        var id: Int {
            switch self {
            case .imagePicker: return 0
            case .fullScreenPreview: return 1
            }
        }
    }
}
