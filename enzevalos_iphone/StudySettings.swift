//
//  StudySettings.swift
//  enzevalos_iphone
//
//  Created by jakobsbode on 08.02.18.
//  Copyright © 2018 fu-berlin.
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import KeychainAccess


class StudySettings {
    static let allAvailableParameters: [StudyParameterProtocol.Type] = [SecurityIndicator.self, Warning.self, Inviation.self]
    static var parameters: [StudyParameterProtocol.Type] = [] {
        didSet{
            securityIndicator = SecurityIndicator.load() as! SecurityIndicator
            invitationsmode = Inviation.load() as! Inviation
        }
    }
    static var hideWarning: Bool {
        get {
            return true
        }
    }
    
    static var hideBadges: Bool {
        get {
            return true
        }
    }
    
    static var studyMode = true
    static var studyID: String {
        return UserDefaults.standard.string(forKey: "studyID") ?? ""
    }

    static var presentFirstQuestionaireMail = false
    static var showBothSecurityIconsInSend = false
    
    static var securityIndicator: SecurityIndicator = SecurityIndicator.load() as! SecurityIndicator
    static var invitationsmode: Inviation = Inviation.load() as! Inviation
    
    /// whether to show a normal inbox (false) or a (currently slightly buggy) categorized interface (true)
    static var showCategorizedInterface =  false //|| true
    static var categorizedInterface_flat = false || true
    static var useAvatars = true
    
    public static var invitationEnabled: Bool {
        get {
            return false
        }
    }
    
    static func isStudyParameter(type: StudyParameterProtocol.Type) -> Bool {
        for para in parameters {
            if type == para {
                return true
            }
        }
        return false
    }
    
    static var bitcoinMails: Bool { //do we recived a mail from bitcoin.de
        get {
            return UserDefaults.standard.bool(forKey: "bitcoin")
        }
        set(newBool) {
            if !UserDefaults.standard.bool(forKey: "bitcoin") && newBool {
                let keychain = Keychain(service: "Enzevalos/Study")
                keychain["bitcoin"] = "true"
                UserDefaults.standard.set(true, forKey: "bitcoin")
                //TODO Logger.log(bitcoinMail: true)
            }
        }
    }
    static func setupStudy() {
        if !studyMode {
            Logger.logging = false
            return
        }
        else {
            Logger.logging = true
        }
        if UserDefaults.standard.string(forKey: "studyID") != nil { //no need to refill this fields, they are already loaded
            return
        }
        let keychain = Keychain(service: "Enzevalos/Study")
        if let studyID = keychain["studyID"] {
            UserDefaults.standard.set(studyID, forKey: "studyID")
        } else {
            let studyID = String.random(length: 30)
            presentFirstQuestionaireMail = true
            keychain["studyID"] = studyID
            UserDefaults.standard.set(studyID, forKey: "studyID")
        }

        if let bitcoin = keychain["bitcoin"] { //do we received a mail from bitcoin.de?
            UserDefaults.standard.set(Bool(bitcoin) ?? false, forKey: "bitcoin")
        } else {
            keychain["bitcoin"] = "false"
            UserDefaults.standard.set(false, forKey: "bitcoin")
        }
        Logger.log(setupStudy: parameters, alreadyRegistered: !presentFirstQuestionaireMail)
    }

    public static func setupStudyKeys() {
        if studyMode {
            setupStudyPublicKeys()
        }
    }

}
