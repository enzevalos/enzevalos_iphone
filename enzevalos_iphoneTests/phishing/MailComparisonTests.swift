//
//  MailComparisonTests.swift
//  enzevalos_iphoneTests
//
//  Created by Viktoria Sorgalla on 29.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//
import XCTest
import Contacts

@testable import enzevalos_iphone
class MailComparisonTests: XCTestCase {
    let userAdr = "alice@example.com"
    let userName = "alice"
    var user = AddressProperties(email: "alice@example.com", name: "alice")
    let userAdr_1 = "bob@example.de"
    let userName_1 = "bob"
    var user_1 = AddressProperties(email: "bob@example.com", name: "bob")
    let userAdr_2 = "carl@test.com"
    let userName_2 = "carl"
    var user_2 = AddressProperties(email: "carl@example.com", name: "carl")
    let userAddr_3_1 = "david@123.com"
    let userAddr_3_2 = "david@1234.com"
    let userName_3 = "david"
    var user_3_1 = AddressProperties(email: "david@123.com", name: "david")
    var user_3_2 = AddressProperties(email: "david@1234.com", name: "david")
    
    private func deleteAll() {
        let provider = PersistentDataProvider()
        let deleteExpectation = expectation(description: "Delete all data!")
        provider.deleteAll(completionHandler: {error in
            if let error = error {
                XCTFail("Error while importing addresses! \(error)")
            }
            deleteExpectation.fulfill()
        })
        wait(for: [deleteExpectation], timeout: TimeInterval(20))
        provider.reset()
        let frc = provider.fetchedMailResultsController
        XCTAssertEqual(frc.fetchedObjects?.count ?? 0, 0)
    }

    
    override func setUp() {
        super.setUp()
        deleteAll()
    }

    override func tearDown() {
        deleteAll()
        super.tearDown()
    }
    
    func testMail(from: AddressProperties, to: [AddressProperties], cc: [AddressProperties], bcc: [AddressProperties], flags: MCOMessageFlag = MCOMessageFlag.init(rawValue: 0), folder: FolderProperties = FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: "Inbox", parent: nil, children: nil, flags: 0), date: Date = Date(timeIntervalSince1970: TimeInterval(arc4random())), cryptoObject: CryptoObject? = nil, body: String = String.random(length: 20)) -> MailProperties {

        let subject = String.random(length: 20)
        let id = UInt64(arc4random())
        var body = body

        var sigState = SignatureState.NoSignature
        var encState = EncryptionState.NoEncryption
        if let cryptoObject = cryptoObject {
            sigState = cryptoObject.signatureState
            encState = cryptoObject.encryptionState
        }
        if let decryptedBody = cryptoObject?.decryptedText {
            body = decryptedBody
        }
        let mail = MailProperties(messageID: "\(id)", uid: UInt64(arc4random()), subject: subject, date: date, flags: Int16(flags.rawValue), from: from, to: to, cc: cc, bcc: bcc, folder: folder, body: body, attachments: [], signatureState: sigState.rawValue, encryptionState: encState.rawValue, signatureKey: nil, decryptionKey: nil, autocryptHeaderKey: [], attachedPublicKeys: [], attachedSecretKeys: [])
        
        PersistentDataProvider.dataProvider.importNewData(from: [mail], completionHandler: {
            error in
            print("Done!")
        })
        return mail
    }
    
    private func createAddressBook(_ senders: [AddressProperties]) {
        for sender in senders {
            let con = CNMutableContact()
            let value = sender.email as AnyObject
            
            let name = sender.name
            let nameArray = name!.split(separator: " ").map(String.init)
            con.givenName = nameArray.first!
            con.emailAddresses.append(CNLabeledValue(label: CNLabelOther, value: value as! NSString))
            // Saving the newly created contact
            let store = CNContactStore()
            let saveRequest = CNSaveRequest()
            saveRequest.add(con, toContainerWithIdentifier:nil)
            try! store.execute(saveRequest)
        }
        
    }
    private func resetAddressBook(_ senders: [AddressProperties]) {
        for sender in senders {
            do {
                let name = sender.name ?? ""
                let predicate = CNContact.predicateForContacts(matchingName: name)
                let store = CNContactStore()
                let contacts = try store.unifiedContacts(matching: predicate,keysToFetch: [CNContactGivenNameKey as CNKeyDescriptor, CNContactFamilyNameKey as CNKeyDescriptor, CNContactImageDataKey as CNKeyDescriptor])
                guard contacts.count > 0, let selectedContact = contacts.first  else { throw Error.self as! Error }
             
                let request = CNSaveRequest()
                let mutableContact = selectedContact.mutableCopy() as! CNMutableContact
                request.delete(mutableContact)
                try store.execute(request)
            } catch { }
        }
    }
    
    func testCompareSenderToContacts() {
        let mail_1 = testMail(from: user, to: [], cc: [], bcc: [])
        let mail_2 = testMail(from: user_1, to: [], cc: [], bcc: [])
        let mail_3 = testMail(from: user_2, to: [], cc: [], bcc: [])
        let mail_4 = testMail(from: user_2, to: [], cc: [], bcc: [])
        let mail_5 = testMail(from: user_3_1, to: [], cc: [], bcc: [])
        let mail_6 = testMail(from: user_3_2, to: [], cc: [], bcc: [])
        
        let mailsInInbox: [MailProperties] = [mail_1, mail_2, mail_3, mail_4, mail_5, mail_6]
        
        
        createAddressBook([user])
        
        XCTAssertEqual(ResultCompareSenderToContacts.isContact, mailsInInbox[0].from?.email.compareSenderToContacts())
        XCTAssertEqual(ResultCompareSenderToContacts.Unknown, mailsInInbox[1].from?.email.compareSenderToContacts())
        XCTAssertEqual(ResultCompareSenderToContacts.isSender, mailsInInbox[2].from?.email.compareSenderToContacts())
        
        XCTAssertEqual(ResultCompareSenderToContacts.OnlyIdentity, mailsInInbox[4].from?.email.compareSenderToContacts()) // userAddr_3_2
        
        // The created user has to be deleted after the test
        resetAddressBook([user])
        
    }
    
    func testContactHandler() {
        createAddressBook([user])
        let res = ContactHandler().findContactBy(email: user.email)
        XCTAssertEqual(res.count, 1)
        resetAddressBook([user])
    }
}

