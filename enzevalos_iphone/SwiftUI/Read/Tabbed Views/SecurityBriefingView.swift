//
//  SecurityBriefingView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 09.04.20.
//  Modified by Chris Offner & Claire Bräuer in March 2021.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import SwiftUI

struct SecurityBriefingView<M: DisplayMail>: View {
    @EnvironmentObject var model: ReadModel<M>

    let padding: CGFloat = 10
    @State private var showWarning = true
    
    var body: some View {
        ScrollView {
            title
            if model.mail.warnings.count > 0 && showWarning {
                ForEach(0..<model.mail.warnings.count) {index in
                    // DialogView(option: self.mail.warnings[index], ctaAction: self.action, additionalAction: self.action, dismissAction: self.action, extend: false)
                    WarningView(warning: self.model.mail.warnings[index])
                   // WarningView(title: "Warning", description: "Description!", icon: Image(systemName: "xmark.octagon.fill"), color: Color(ThemeManager.troubleMessageColor()))
                }
                
            }
            
            confidentiality
            Divider()
            authenticity
            Divider()
            
            // if mail is encrypted or/and signed, display "PGP" or "S/MIME"
            if model.mail.encryptionType != nil || model.mail.signatureType != nil {
                encryptionType
                Divider()
            }
            actions
        }
    }
    
    func action () {
        showWarning.toggle()
    }
    
    var title: some View {
        Text("ReadView.SecurityBriefing.Title")
            .font(.largeTitle)
            .fixedSize(horizontal: false, vertical: true)
    }
    
    var encryptionType: some View {
        var type: String {
            get {
                if let type = model.mail.encryptionType {
                    return type.description
                }
                else if let type = model.mail.signatureType {
                    return type.description
                }
                return "no crypto."
            }
        }

        
        return HStack {
            VStack(alignment: .leading) {
                Text("EncryptionType")
                    .font(.headline)
                    .padding(padding)
                (Text("ReadView.SecurityBriefing.EncryptionType.Text")
                    + Text(type).fontWeight(.bold) + Text("."))
                    .padding(padding)
            }
            Spacer()
        }
    }
    
    var confidentiality: some View {
        var stateString: LocalizedStringKey
        switch (model.mail.encryptionState, model.mail.transportEnc) {
        case (.NoEncryption, true):     stateString = "ReadView.SecurityBriefing.Conf.State.TransportEnc"
        case (.NoEncryption, false):    stateString = "ReadView.SecurityBriefing.Conf.State.Plain"
        case (.UnableToDecrypt, _):     stateString = "ReadView.SecurityBriefing.Conf.State.UnableToDecrypt"
        case (.ValidEncryptedWithOldKey, _), (.ValidedEncryptedWithCurrentKey, _):  stateString = "ReadView.SecurityBriefing.Conf.State.Encrypted"
        }
        
        return InfoView(
            titleString: "ReadView.SecurityBriefing.Conf.Title",
            descriptionString: "ReadView.SecurityBriefing.Conf.Description",
            stateString: stateString,
            padding: padding, rating: model.mail.signatureState.rating)
    }
    
    var authenticity: some View {
        var stateString: LocalizedStringKey
        switch model.mail.signatureState {
        case .InvalidSignature:
            stateString = "ReadView.SecurityBriefing.Auth.State.InvalidSignature"
        case .NoSignature:
            stateString = "ReadView.SecurityBriefing.Auth.State.UnSigned"
        case .NoPublicKey:
            stateString = "ReadView.SecurityBriefing.Auth.State.MissingPK"
        case .ValidSignature:
            stateString = "ReadView.SecurityBriefing.Auth.State.Signed"
        }
        return InfoView(
            titleString: "ReadView.SecurityBriefing.Auth.Title",
            descriptionString: "ReadView.SecurityBriefing.Auth.Description",
            stateString: stateString,
            padding: padding, rating: model.mail.signatureState.rating)
    }
    
    var actions: some View {
        HStack {
            VStack (alignment: .leading){
                Text("ReadView.SecurityBriefing.Actions.Title")
                    .font(.headline)
                    .padding(padding)
                Text("ReadView.SecurityBriefing.Actions.Description")
                    .fontWeight(.light)
                    .padding(padding)
                    .fixedSize(horizontal: false, vertical: true)
                ForEach(0..<model.mail.buttonActions.count) {index in
                    Button(action: {
                        self.choseAction(action: self.model.mail.buttonActions[index])
                    }, label: {
                        Text(self.model.mail.buttonActions[index].title)
                            .frame(maxWidth: .infinity, minHeight: 50, maxHeight: 50)
                            .addBorder(self.model.mail.buttonActions[index].color, width: 1, cornerRadius: 30)
                            .padding(.vertical, 10)
                            .padding(.horizontal, 20)
                    })
                }
            }
            Spacer()
        }
    }
    
    func choseAction (action: ButtonAction) {
    }
}

struct WarningView: View {
    
    let title: String
    let description: String
    let icon: Image
    let color: Color
    var ctaButton: ButtonStruct? = nil
    let moreButtons: [ButtonStruct]
    
    init(warning: Dialog) {
        title = warning.title
        description = warning.body
        icon = Image(systemName: "xmark.octagon.fill")
        color = warning.dialogColor
        if let ctaTitle = warning.ctaButtonTitle, let ctaAction = warning.ctaButtonAction {
            ctaButton = ButtonStruct(titleKey: ctaTitle, action: ctaAction, type: .CTA)
        }
        moreButtons = warning.moreButtons
    }
    
    var body: some View {
        VStack {
            Text(title)
                .font(.headline)
                .padding(10)
            HStack {
                icon
                   .resizable()
                   .frame(width: 80, height: 80)
                   .foregroundColor(color)
                Text(description)
                    .fontWeight(.light)
                    .fixedSize(horizontal: false, vertical: true)
            }
            
            if ctaButton != nil {
                Button(action: {
                    self.choseAction(action: self.ctaButton!.action)
                }, label: {
                    DialogButtonLabel(button: ctaButton!)
                })
            }
            if moreButtons.count > 0 {
                ForEach(0..<moreButtons.count) {i in
                    Button(action: {
                        self.choseAction(action: self.moreButtons[i].action)
                    }, label: {
                        DialogButtonLabel(button: self.moreButtons[i])})
                }
           }
        }
        .padding(20)
        .addBorder(color, width: 3, cornerRadius: 30)
        .padding(10)
    }
    
    func choseAction (action: ButtonAction) {
    }
}

struct InfoView: View {
    let titleString: LocalizedStringKey
    let descriptionString: LocalizedStringKey
    var stateString: LocalizedStringKey
    var padding: CGFloat
    var rating: Rating
    
    var body: some View {
        HStack{
            VStack(alignment: .leading) {
                Text(titleString)
                    .font(.headline)
                    .padding(padding)
                HStack {
                    icon
                    Text(stateString)
                        .padding(padding)
                }
            }
            Spacer()
        }
    }
    
    var icon: some View {
        var image: Image
        var color: Color
        switch rating {
            case .Positiv:
                image = Image(systemName: "hand.thumbsup")
                color = Color(ThemeManager.encryptedMessageColor())
            case .Neutral:
                image = Image(systemName: "hand.thumbsdown")
                color = Color(ThemeManager.unencryptedMessageColor())
            case .Negativ:
                image = Image(systemName: "xmark.octagon.fill")
                color = Color(ThemeManager.troubleMessageColor())
        }
        image = image.resizable()
        let view = image
            .frame(width: 40, height: 40)
            .padding(.leading, padding)
            .foregroundColor(color)
        return view
    }
}

struct DialogButtonLabel: View {
    let data: ButtonStruct
    let color: Color
    
    init(button: ButtonStruct) {
        data = button
        switch data.type {
        case .CTA:      color = .blue
        case .DISMISS:  color = Color(ThemeManager.troubleMessageColor())
        case .MORE:     color = .white
        }
    }
    
    var body: some View {
        Text(data.title)
            .frame(maxWidth: .infinity, minHeight: 20, maxHeight: 20)
            .padding(10)
            .addBorder(color, width: 1, cornerRadius: 30)
            .padding(10)
    }
}

struct SecurityBriefingView_Previews: PreviewProvider {
    // 0: SecureMail, 1: PlainMail, 2: SignedOnlyMail,
    // 3: MissingPKMail, 4: NotDecryptedMail, 5: CorruptedMail
    static var model = ReadModel(mail: ProxyData.mails[0])
    static var previews: some View {
//        model.currentTab = ReadPart.Security.value
        let sim = Simulators<ReadMainView<ProxyMail>>()
        return Group {
            sim.previews(view: ReadMainView(model: ReadModel(mail: ProxyData.mails[0])))
            sim.previews(view: ReadMainView(model: ReadModel(mail: ProxyData.mails[1])))
            sim.previews(view: ReadMainView(model: ReadModel(mail: ProxyData.mails[2])))
            sim.previews(view: ReadMainView(model: ReadModel(mail: ProxyData.mails[3])))
            sim.previews(view: ReadMainView(model: ReadModel(mail: ProxyData.mails[4])))
            sim.previews(view: ReadMainView(model: ReadModel(mail: ProxyData.mails[5])))
        }
    }
}
