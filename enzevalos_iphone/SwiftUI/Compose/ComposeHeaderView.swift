//
//  ComposeHeaderView.swift
//  enzevalos_iphone
//
//  Created by Chris Offner & Claire Bräuer on 23.03.21.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import SwiftUI

/// A view that contains the Cancel and Send buttons for an email.
struct ComposeViewHeader: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var model: ComposeModel
    
    var body: some View {
        ZStack {
            HStack {
                // Cancel button
                Button("Cancel") {
                    presentationMode.wrappedValue.dismiss()
                }
                
                Spacer()
                
                // Send button, disabled if no recipients are set.
                Button("Send") {
                    // trigger an alert to attach files
                    // user can then decide in the alert if he wants to
                    // - resume sending
                    // - quit sending and edit again
                    if model.mentionsAttachments && model.attachments == [] {
                        model.showAttachmentAlert = true
                        model.resumeSend = false
                    }
                    if model.resumeSend {
                        model.sendMail()
                        presentationMode.wrappedValue.dismiss()
                    }
                    model.resumeSend = true
                }
                .disabled(model.recipientsModel.hasNoRecipients)
            }
            
            // Encryption button
            encryptionButton
        }
    }
    
    
    /// Encryption state button view
    private var encryptionButton: some View {
        Button {
            model.toogleEncryption()
        } label: {
            // Separate labels required for desired scaling transition between states
            if model.encryptionOn {
                encryptedButtonLabel
            } else {
                unencryptedButtonLabel
            }
        }
        .frame(width: 40, height: 40)
    }
    
    /// Label style for encryption button when encryption is activated.
    private var encryptedButtonLabel: some View {
        ZStack {
            Circle()
                .stroke(Color.blue, lineWidth: 2)
            
            Image(systemName: "lock.fill")
                .font(Font.system(size: 24, weight: Font.Weight.light))
                .foregroundColor(.blue)
        }
        .transition(AnyTransition.opacity.combined(with: .scale))
    }
    
    /// Label style for encryption button when encryption is deactivated.
    private var unencryptedButtonLabel: some View {
        ZStack {
            Circle()
                .fill(Color(UIColor.tertiaryLabel))
            
            Image(systemName: "lock.slash.fill")
                .font(Font.system(size: 24, weight: Font.Weight.light))
                .foregroundColor(.white)
        }
        .transition(AnyTransition.opacity.combined(with: .scale))
    }
}
