//
//  AuthenticationTests.swift
//  enzevalos_iphoneTests
//
//  Created by Cezary Pilaszewicz on 12.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Cuckoo
import XCTest
import Combine

@testable import enzevalos_iphone
class AuthenticationTests: XCTestCase {
    let login = "test_login"
    let password = "test_password"
    let username = "test_uesrname"
    let imapServer = "test.imap.server.de"
    let imapPort = 997
    let imapEncryption = MCOConnectionType.startTLS.rawValue
    let smtpServer = "test.smtp.server.com"
    let smtpPort = 1234
    let smtpEncryption = MCOConnectionType.startTLS.rawValue

    
    //Test whether correct arguments are passed to the model
    func testValidate() {
        let model = MockAuthenticationModel().withEnabledSuperclassSpy()
        let viewModel = MockAuthenticationViewModel(authenticationModel: model).withEnabledSuperclassSpy()
        let future: Future<AuthenticationModel.AuthenticationResult, Never> = Future {
            promise  in promise(.success(AuthenticationModel.AuthenticationResult.Success))
        }
        stub(model) { stub in
            when(stub.checkConfig(mailAccount: any(), extendedValidation: false)).thenReturn(future)
        }
        viewModel.validate(self.login, self.password)
        let argumentCaptor = ArgumentCaptor<MailAccount>()
        verify(model).checkConfig(mailAccount: argumentCaptor.capture(), extendedValidation: false)
        let passedMailAccount =  argumentCaptor.value
        XCTAssertEqual(passedMailAccount?.emailAddress, self.login)
        XCTAssertEqual(passedMailAccount?.password, self.password)
    }
    
    func testDetailValidate() {
        let model = MockAuthenticationModel().withEnabledSuperclassSpy()
        let viewModel = MockAuthenticationViewModel(authenticationModel: model).withEnabledSuperclassSpy()
        let future: Future<AuthenticationModel.AuthenticationResult, Never> = Future {
            promise  in promise(.success(AuthenticationModel.AuthenticationResult.Success))
        }
        stub(model) { stub in
            when(stub.checkConfig(mailAccount: any(), extendedValidation: true)).thenReturn(future)
        }
        viewModel.detailValidation(login, password, username, imapServer, String(imapPort), imapEncryption, smtpServer, String(smtpPort), smtpEncryption)
        let argumentCaptor = ArgumentCaptor<MailAccount>()
        verify(model).checkConfig(mailAccount: argumentCaptor.capture(), extendedValidation: true)
        let passedMailAccount =  argumentCaptor.value
        XCTAssertEqual(passedMailAccount?.emailAddress, self.login)
        XCTAssertEqual(passedMailAccount?.password, self.password)
        XCTAssertEqual(passedMailAccount?.username, self.username)
        XCTAssertEqual(passedMailAccount?.imapServer, self.imapServer)
        XCTAssertEqual(passedMailAccount?.imapPort, self.imapPort)
        XCTAssertEqual(passedMailAccount?.imapEncryption, self.imapEncryption)
        XCTAssertEqual(passedMailAccount?.smtpServer, self.smtpServer)
        XCTAssertEqual(passedMailAccount?.smtpPort, self.smtpPort)
        XCTAssertEqual(passedMailAccount?.smtpEncryption, self.smtpEncryption)
    }
    
    //Test authentification results
    func testAuthenticationSuccessful() {
        let model = MockAuthenticationModel().withEnabledSuperclassSpy()
        let viewModel = MockAuthenticationViewModel(authenticationModel: model).withEnabledSuperclassSpy()
        let future: Future<AuthenticationModel.AuthenticationResult, Never> = Future {
            promise  in promise(.success(AuthenticationModel.AuthenticationResult.Success))
        }
        stub(model) { stub in
            when(stub.checkConfig(mailAccount: any(), extendedValidation: false)).thenReturn(future)
        }
        viewModel.validate(self.login, self.password)
        verify(viewModel).authenticationSucceed()
    }
    
    func testAuthenticationFailed() {
        let model = MockAuthenticationModel().withEnabledSuperclassSpy()
        let viewModel = MockAuthenticationViewModel(authenticationModel: model).withEnabledSuperclassSpy()
        let future: Future<AuthenticationModel.AuthenticationResult, Never> = Future {
            promise  in promise(.success(AuthenticationModel.AuthenticationResult.Error(value: MailServerConnectionError.AuthenticationError)))
        }
        stub(model) { stub in
            when(stub.checkConfig(mailAccount: any(), extendedValidation: false)).thenReturn(future)
        }
        viewModel.validate(self.login, self.password)
        let argumentCaptor = ArgumentCaptor<MailServerConnectionError>()
        verify(viewModel).authenticationFailed(error: argumentCaptor.capture())
        XCTAssertEqual(argumentCaptor.value, MailServerConnectionError.AuthenticationError)
    }
    
    func testAuthenticationTimeout() {
        let model = MockAuthenticationModel().withEnabledSuperclassSpy()
        let viewModel = MockAuthenticationViewModel(authenticationModel: model).withEnabledSuperclassSpy()
        let future: Future<AuthenticationModel.AuthenticationResult, Never> = Future {
            promise  in promise(.success(AuthenticationModel.AuthenticationResult.Timeout))
        }
        stub(model) { stub in
            when(stub.checkConfig(mailAccount: any(), extendedValidation: false)).thenReturn(future)
        }
        viewModel.validate(self.login, self.password)
        verify(viewModel).timeoutNotification()
    }
}
