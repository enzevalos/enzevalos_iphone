//
//  FilesCategoryModel.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 22.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

class FilesCategoryVM: ObservableObject {
    
    init(_ categoryModel : CategoryViewModel) {
        categoryBaseModel = categoryModel
    }
    @ObservedObject var categoryBaseModel : CategoryViewModel
    
    @Published var  filteredFileTypes: [String] = []
    
    func filterByTypes(_ email : eMail)->Bool{
        filteredFileTypes.count == 0 ||
        email.displayAttachments.map{filteredFileTypes.contains($0.myName.components(separatedBy: ".").last ?? "¿")}.contains(true)
    }
    
    var mails : [eMail] {
        categoryBaseModel.allMails.filter(filterByTypes)
    }
    
    var mailsAttachments : [[DisplayAttachment]] {
        mails.map{
            $0.displayAttachments
                .filter{attachment in
                    filteredFileTypes.count == 0 ||
                    filteredFileTypes.contains(attachment.myName.components(separatedBy: ".").last ?? "¿")
                }
        }
    }
    
    
    var isFullscreen : Bool {categoryBaseModel.size == nil}
    func attachmentsize(_ attachmentCount:Int)->CGFloat {
        return isFullscreen ? 250 : ((categoryBaseModel.size! - 50) / CGFloat(attachmentCount))
    }
    
}
