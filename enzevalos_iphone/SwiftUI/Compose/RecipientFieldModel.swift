//
//  RecipientFieldModel.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 31.10.20.
//  Modified by Chris Offner & Claire Bräuer in March 2021.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import Combine
import SwiftUI

/// A model for a single recipient field.
class RecipientFieldModel: ObservableObject {
    @Published var suggestions = [AddressRecord]()
    @Published var selectedContacts = [AddressRecord]() 
    @Published var type: RecipientType
    private var dataprovider: PersistentDataProvider = LetterboxModel.instance.dataProvider
    var parentRecipientModel: RecipientsModel?
    
    init(type: RecipientType) {
        self.type = type
    }
    
    @Published var text = "" {
        didSet {
            // Split entered text at separator characters, add first part as address
            if text.contains(" ") || text.contains(",") || text.contains(";") {
                var firstWord = text.split(separator: "\n").first
                firstWord?.removeLast()
                if let newWord = firstWord {
                    addNewAddress(String(newWord))
                }
                text = ""
            }
            
            // Generate suggestions based on entered text.
            if !text.isEmpty {
                // Fetch addresses that match entered prefix.
                let frc = dataprovider.createFetchResultController(
                        fetchRequest: AddressRecord.lookForPrefix(prefix: text)
                    )
                
                // Set addresses as suggestions if any were found, otherwise empty suggestions
                suggestions = frc.fetchedObjects ?? []
            } else {
                suggestions = []
            }
        }
    }
    
    /// Gets array of contacts sorted by name or recency.
    ///
    /// - Parameter sortBy: Determines what key the contacts get sorted by.
    /// - Returns: Sorted array of contacts.
    func getContacts(sortBy: AddressRecord.SortBy) -> [AddressRecord] {
        if !suggestions.isEmpty {
            return suggestions
        }
        
        let frc = LetterboxModel.instance.dataProvider.createFetchResultController(
                fetchRequest: AddressRecord.all(sortBy: sortBy)
        )
        
        return frc.fetchedObjects ?? []
    }
    
    /// Adds AddressRecord of a contact to array of recipients.
    ///
    /// - Parameter _: AddressRecord that gets added to array of recipients.
    func selectContact(_ addressRecord: AddressRecord) {
        selectedContacts.append(addressRecord)
        if !addressRecord.hasPublicKey {
            self.parentRecipientModel?.parentComposeModel?.encryptionOn = false
        }
        text = ""
        suggestions = []
        
        // TODO: Find better way to manage state and UI updates!
        // This is a horrible hack to get UI to refresh for Send button enable/disable state.
        // Please fix wherever you find the following line of code. May require architecture
        // changes because state changes (currently ) don't propagate through nested models.
        // Read: https://rhonabwy.com/2021/02/13/nested-observable-objects-in-swiftui/
        parentRecipientModel?.parentComposeModel?.subject += ""
    }
    
    /// Removes contact at index from recipients.
    ///
    /// - Parameter at: Index of contact to be removed from array of recipients.
    func deselectContact(at index: Int) {
        let addr = selectedContacts[index]
        
        selectedContacts.remove(at: index)
        if !addr.hasPublicKey {
            parentRecipientModel?.checkEncryption()
        }
        // TODO: See TODO in selectContact.
        parentRecipientModel?.parentComposeModel?.subject += ""
    }
    
    /// Commits text to addresses.
    func commitText() {
        if !text.isEmpty {
            addNewAddress(text)
            text = ""
        }
        
        // TODO: See TODO in selectContact.
        parentRecipientModel?.parentComposeModel?.subject += ""
    }
    
    /// Adds new address to selected contacts.
    ///
    /// - Parameter _: Email address string to be added.
    func addNewAddress(_ address: String) {
        guard address.count > 0 && address.contains("@") else {
            // TODO: Add proper email validation + warning
            return
        }
        
        dataprovider.importNewData(from: [AddressProperties(email: address)]) { error in
            let frc = self
                .dataprovider
                .createFetchResultController(
                    fetchRequest: AddressRecord.lookForExisting(email: address)
                )
            
            if let addresses = frc.fetchedObjects, let addr = addresses.first {
                self.selectedContacts.append(addr)
                if !addr.hasPublicKey {
                    self.parentRecipientModel?.parentComposeModel?.encryptionOn = false
                }
            }
        }
    }
}
