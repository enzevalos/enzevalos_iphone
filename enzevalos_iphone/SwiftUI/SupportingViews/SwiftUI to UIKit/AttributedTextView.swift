//
//  AttributedTextView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 17.04.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct SelectableTextView: UIViewRepresentable {
    var attributedText: NSAttributedString

    init(_ attributedText: NSAttributedString) {
        self.attributedText = attributedText
    }

    func makeUIView(context: Context) -> UITextView {
        let view = UITextView()
        view.isSelectable = true
        view.allowsEditingTextAttributes = false
        view.isScrollEnabled = true
        return view
    }

    func updateUIView(_ label: UITextView, context: Context) {
        label.attributedText = attributedText
    }
}


struct AttributedTextView_Previews: PreviewProvider {
    static let text = "Hello World www.fu-berlin.de"
    static var previews: some View {
        SelectableTextView(NSAttributedString(string: text))
    }
}
