// MARK: - Mocks generated from file: enzevalos_iphone/AuthenticationModel.swift at 2021-04-10 20:34:30 +0000

//
//  AuthenticationModel.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 18.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Cuckoo
@testable import enzevalos_iphone

import Combine
import Foundation


 class MockAuthenticationModel: AuthenticationModel, Cuckoo.ClassMock {
    
     typealias MocksType = AuthenticationModel
    
     typealias Stubbing = __StubbingProxy_AuthenticationModel
     typealias Verification = __VerificationProxy_AuthenticationModel

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: AuthenticationModel?

     func enableDefaultImplementation(_ stub: AuthenticationModel) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     override var extendedValidation: Bool {
        get {
            return cuckoo_manager.getter("extendedValidation",
                superclassCall:
                    
                    super.extendedValidation
                    ,
                defaultCall: __defaultImplStub!.extendedValidation)
        }
        
        set {
            cuckoo_manager.setter("extendedValidation",
                value: newValue,
                superclassCall:
                    
                    super.extendedValidation = newValue
                    ,
                defaultCall: __defaultImplStub!.extendedValidation = newValue)
        }
        
    }
    
    
    
     override var imapCallback: AuthenticationCallback? {
        get {
            return cuckoo_manager.getter("imapCallback",
                superclassCall:
                    
                    super.imapCallback
                    ,
                defaultCall: __defaultImplStub!.imapCallback)
        }
        
        set {
            cuckoo_manager.setter("imapCallback",
                value: newValue,
                superclassCall:
                    
                    super.imapCallback = newValue
                    ,
                defaultCall: __defaultImplStub!.imapCallback = newValue)
        }
        
    }
    
    
    
     override var smtpCallback: AuthenticationCallback? {
        get {
            return cuckoo_manager.getter("smtpCallback",
                superclassCall:
                    
                    super.smtpCallback
                    ,
                defaultCall: __defaultImplStub!.smtpCallback)
        }
        
        set {
            cuckoo_manager.setter("smtpCallback",
                value: newValue,
                superclassCall:
                    
                    super.smtpCallback = newValue
                    ,
                defaultCall: __defaultImplStub!.smtpCallback = newValue)
        }
        
    }
    
    
    
     override var imapConfigurationSuccessful: Bool {
        get {
            return cuckoo_manager.getter("imapConfigurationSuccessful",
                superclassCall:
                    
                    super.imapConfigurationSuccessful
                    ,
                defaultCall: __defaultImplStub!.imapConfigurationSuccessful)
        }
        
        set {
            cuckoo_manager.setter("imapConfigurationSuccessful",
                value: newValue,
                superclassCall:
                    
                    super.imapConfigurationSuccessful = newValue
                    ,
                defaultCall: __defaultImplStub!.imapConfigurationSuccessful = newValue)
        }
        
    }
    
    
    
     override var smtpConfigurationSuccessful: Bool {
        get {
            return cuckoo_manager.getter("smtpConfigurationSuccessful",
                superclassCall:
                    
                    super.smtpConfigurationSuccessful
                    ,
                defaultCall: __defaultImplStub!.smtpConfigurationSuccessful)
        }
        
        set {
            cuckoo_manager.setter("smtpConfigurationSuccessful",
                value: newValue,
                superclassCall:
                    
                    super.smtpConfigurationSuccessful = newValue
                    ,
                defaultCall: __defaultImplStub!.smtpConfigurationSuccessful = newValue)
        }
        
    }
    
    
    
     override var dispatchGroup: DispatchGroup {
        get {
            return cuckoo_manager.getter("dispatchGroup",
                superclassCall:
                    
                    super.dispatchGroup
                    ,
                defaultCall: __defaultImplStub!.dispatchGroup)
        }
        
        set {
            cuckoo_manager.setter("dispatchGroup",
                value: newValue,
                superclassCall:
                    
                    super.dispatchGroup = newValue
                    ,
                defaultCall: __defaultImplStub!.dispatchGroup = newValue)
        }
        
    }
    

    

    
    
    
     override func checkConfig(mailAccount: MailAccount, extendedValidation: Bool) -> Future<AuthenticationResult, Never> {
        
    return cuckoo_manager.call("checkConfig(mailAccount: MailAccount, extendedValidation: Bool) -> Future<AuthenticationResult, Never>",
            parameters: (mailAccount, extendedValidation),
            escapingParameters: (mailAccount, extendedValidation),
            superclassCall:
                
                super.checkConfig(mailAccount: mailAccount, extendedValidation: extendedValidation)
                ,
            defaultCall: __defaultImplStub!.checkConfig(mailAccount: mailAccount, extendedValidation: extendedValidation))
        
    }
    
    
    
     override func checkIMAPConfig(mailAccount: MailAccount, _ callback: AuthenticationCallback)  {
        
    return cuckoo_manager.call("checkIMAPConfig(mailAccount: MailAccount, _: AuthenticationCallback)",
            parameters: (mailAccount, callback),
            escapingParameters: (mailAccount, callback),
            superclassCall:
                
                super.checkIMAPConfig(mailAccount: mailAccount, callback)
                ,
            defaultCall: __defaultImplStub!.checkIMAPConfig(mailAccount: mailAccount, callback))
        
    }
    
    
    
     override func checkSMTPConfig(mailAccount: MailAccount, _ callback: AuthenticationCallback)  {
        
    return cuckoo_manager.call("checkSMTPConfig(mailAccount: MailAccount, _: AuthenticationCallback)",
            parameters: (mailAccount, callback),
            escapingParameters: (mailAccount, callback),
            superclassCall:
                
                super.checkSMTPConfig(mailAccount: mailAccount, callback)
                ,
            defaultCall: __defaultImplStub!.checkSMTPConfig(mailAccount: mailAccount, callback))
        
    }
    
    
    
     override func onTimeout(timeoutResult: DispatchTimeoutResult) -> AuthenticationResult {
        
    return cuckoo_manager.call("onTimeout(timeoutResult: DispatchTimeoutResult) -> AuthenticationResult",
            parameters: (timeoutResult),
            escapingParameters: (timeoutResult),
            superclassCall:
                
                super.onTimeout(timeoutResult: timeoutResult)
                ,
            defaultCall: __defaultImplStub!.onTimeout(timeoutResult: timeoutResult))
        
    }
    
    
    
     override func onImapCompleted(imapWorks: Bool, _ login: String, _ password: String)  {
        
    return cuckoo_manager.call("onImapCompleted(imapWorks: Bool, _: String, _: String)",
            parameters: (imapWorks, login, password),
            escapingParameters: (imapWorks, login, password),
            superclassCall:
                
                super.onImapCompleted(imapWorks: imapWorks, login, password)
                ,
            defaultCall: __defaultImplStub!.onImapCompleted(imapWorks: imapWorks, login, password))
        
    }
    
    
    
     override func onSmtpCompleted(smtpWorks: Bool, _ login: String, _ password: String)  {
        
    return cuckoo_manager.call("onSmtpCompleted(smtpWorks: Bool, _: String, _: String)",
            parameters: (smtpWorks, login, password),
            escapingParameters: (smtpWorks, login, password),
            superclassCall:
                
                super.onSmtpCompleted(smtpWorks: smtpWorks, login, password)
                ,
            defaultCall: __defaultImplStub!.onSmtpCompleted(smtpWorks: smtpWorks, login, password))
        
    }
    

	 struct __StubbingProxy_AuthenticationModel: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var extendedValidation: Cuckoo.ClassToBeStubbedProperty<MockAuthenticationModel, Bool> {
	        return .init(manager: cuckoo_manager, name: "extendedValidation")
	    }
	    
	    
	    var imapCallback: Cuckoo.ClassToBeStubbedOptionalProperty<MockAuthenticationModel, AuthenticationCallback> {
	        return .init(manager: cuckoo_manager, name: "imapCallback")
	    }
	    
	    
	    var smtpCallback: Cuckoo.ClassToBeStubbedOptionalProperty<MockAuthenticationModel, AuthenticationCallback> {
	        return .init(manager: cuckoo_manager, name: "smtpCallback")
	    }
	    
	    
	    var imapConfigurationSuccessful: Cuckoo.ClassToBeStubbedProperty<MockAuthenticationModel, Bool> {
	        return .init(manager: cuckoo_manager, name: "imapConfigurationSuccessful")
	    }
	    
	    
	    var smtpConfigurationSuccessful: Cuckoo.ClassToBeStubbedProperty<MockAuthenticationModel, Bool> {
	        return .init(manager: cuckoo_manager, name: "smtpConfigurationSuccessful")
	    }
	    
	    
	    var dispatchGroup: Cuckoo.ClassToBeStubbedProperty<MockAuthenticationModel, DispatchGroup> {
	        return .init(manager: cuckoo_manager, name: "dispatchGroup")
	    }
	    
	    
	    func checkConfig<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(mailAccount: M1, extendedValidation: M2) -> Cuckoo.ClassStubFunction<(MailAccount, Bool), Future<AuthenticationResult, Never>> where M1.MatchedType == MailAccount, M2.MatchedType == Bool {
	        let matchers: [Cuckoo.ParameterMatcher<(MailAccount, Bool)>] = [wrap(matchable: mailAccount) { $0.0 }, wrap(matchable: extendedValidation) { $0.1 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationModel.self, method: "checkConfig(mailAccount: MailAccount, extendedValidation: Bool) -> Future<AuthenticationResult, Never>", parameterMatchers: matchers))
	    }
	    
	    func checkIMAPConfig<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(mailAccount: M1, _ callback: M2) -> Cuckoo.ClassStubNoReturnFunction<(MailAccount, AuthenticationCallback)> where M1.MatchedType == MailAccount, M2.MatchedType == AuthenticationCallback {
	        let matchers: [Cuckoo.ParameterMatcher<(MailAccount, AuthenticationCallback)>] = [wrap(matchable: mailAccount) { $0.0 }, wrap(matchable: callback) { $0.1 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationModel.self, method: "checkIMAPConfig(mailAccount: MailAccount, _: AuthenticationCallback)", parameterMatchers: matchers))
	    }
	    
	    func checkSMTPConfig<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(mailAccount: M1, _ callback: M2) -> Cuckoo.ClassStubNoReturnFunction<(MailAccount, AuthenticationCallback)> where M1.MatchedType == MailAccount, M2.MatchedType == AuthenticationCallback {
	        let matchers: [Cuckoo.ParameterMatcher<(MailAccount, AuthenticationCallback)>] = [wrap(matchable: mailAccount) { $0.0 }, wrap(matchable: callback) { $0.1 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationModel.self, method: "checkSMTPConfig(mailAccount: MailAccount, _: AuthenticationCallback)", parameterMatchers: matchers))
	    }
	    
	    func onTimeout<M1: Cuckoo.Matchable>(timeoutResult: M1) -> Cuckoo.ClassStubFunction<(DispatchTimeoutResult), AuthenticationResult> where M1.MatchedType == DispatchTimeoutResult {
	        let matchers: [Cuckoo.ParameterMatcher<(DispatchTimeoutResult)>] = [wrap(matchable: timeoutResult) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationModel.self, method: "onTimeout(timeoutResult: DispatchTimeoutResult) -> AuthenticationResult", parameterMatchers: matchers))
	    }
	    
	    func onImapCompleted<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable, M3: Cuckoo.Matchable>(imapWorks: M1, _ login: M2, _ password: M3) -> Cuckoo.ClassStubNoReturnFunction<(Bool, String, String)> where M1.MatchedType == Bool, M2.MatchedType == String, M3.MatchedType == String {
	        let matchers: [Cuckoo.ParameterMatcher<(Bool, String, String)>] = [wrap(matchable: imapWorks) { $0.0 }, wrap(matchable: login) { $0.1 }, wrap(matchable: password) { $0.2 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationModel.self, method: "onImapCompleted(imapWorks: Bool, _: String, _: String)", parameterMatchers: matchers))
	    }
	    
	    func onSmtpCompleted<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable, M3: Cuckoo.Matchable>(smtpWorks: M1, _ login: M2, _ password: M3) -> Cuckoo.ClassStubNoReturnFunction<(Bool, String, String)> where M1.MatchedType == Bool, M2.MatchedType == String, M3.MatchedType == String {
	        let matchers: [Cuckoo.ParameterMatcher<(Bool, String, String)>] = [wrap(matchable: smtpWorks) { $0.0 }, wrap(matchable: login) { $0.1 }, wrap(matchable: password) { $0.2 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationModel.self, method: "onSmtpCompleted(smtpWorks: Bool, _: String, _: String)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_AuthenticationModel: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var extendedValidation: Cuckoo.VerifyProperty<Bool> {
	        return .init(manager: cuckoo_manager, name: "extendedValidation", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var imapCallback: Cuckoo.VerifyOptionalProperty<AuthenticationCallback> {
	        return .init(manager: cuckoo_manager, name: "imapCallback", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var smtpCallback: Cuckoo.VerifyOptionalProperty<AuthenticationCallback> {
	        return .init(manager: cuckoo_manager, name: "smtpCallback", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var imapConfigurationSuccessful: Cuckoo.VerifyProperty<Bool> {
	        return .init(manager: cuckoo_manager, name: "imapConfigurationSuccessful", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var smtpConfigurationSuccessful: Cuckoo.VerifyProperty<Bool> {
	        return .init(manager: cuckoo_manager, name: "smtpConfigurationSuccessful", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var dispatchGroup: Cuckoo.VerifyProperty<DispatchGroup> {
	        return .init(manager: cuckoo_manager, name: "dispatchGroup", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func checkConfig<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(mailAccount: M1, extendedValidation: M2) -> Cuckoo.__DoNotUse<(MailAccount, Bool), Future<AuthenticationResult, Never>> where M1.MatchedType == MailAccount, M2.MatchedType == Bool {
	        let matchers: [Cuckoo.ParameterMatcher<(MailAccount, Bool)>] = [wrap(matchable: mailAccount) { $0.0 }, wrap(matchable: extendedValidation) { $0.1 }]
	        return cuckoo_manager.verify("checkConfig(mailAccount: MailAccount, extendedValidation: Bool) -> Future<AuthenticationResult, Never>", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func checkIMAPConfig<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(mailAccount: M1, _ callback: M2) -> Cuckoo.__DoNotUse<(MailAccount, AuthenticationCallback), Void> where M1.MatchedType == MailAccount, M2.MatchedType == AuthenticationCallback {
	        let matchers: [Cuckoo.ParameterMatcher<(MailAccount, AuthenticationCallback)>] = [wrap(matchable: mailAccount) { $0.0 }, wrap(matchable: callback) { $0.1 }]
	        return cuckoo_manager.verify("checkIMAPConfig(mailAccount: MailAccount, _: AuthenticationCallback)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func checkSMTPConfig<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(mailAccount: M1, _ callback: M2) -> Cuckoo.__DoNotUse<(MailAccount, AuthenticationCallback), Void> where M1.MatchedType == MailAccount, M2.MatchedType == AuthenticationCallback {
	        let matchers: [Cuckoo.ParameterMatcher<(MailAccount, AuthenticationCallback)>] = [wrap(matchable: mailAccount) { $0.0 }, wrap(matchable: callback) { $0.1 }]
	        return cuckoo_manager.verify("checkSMTPConfig(mailAccount: MailAccount, _: AuthenticationCallback)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func onTimeout<M1: Cuckoo.Matchable>(timeoutResult: M1) -> Cuckoo.__DoNotUse<(DispatchTimeoutResult), AuthenticationResult> where M1.MatchedType == DispatchTimeoutResult {
	        let matchers: [Cuckoo.ParameterMatcher<(DispatchTimeoutResult)>] = [wrap(matchable: timeoutResult) { $0 }]
	        return cuckoo_manager.verify("onTimeout(timeoutResult: DispatchTimeoutResult) -> AuthenticationResult", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func onImapCompleted<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable, M3: Cuckoo.Matchable>(imapWorks: M1, _ login: M2, _ password: M3) -> Cuckoo.__DoNotUse<(Bool, String, String), Void> where M1.MatchedType == Bool, M2.MatchedType == String, M3.MatchedType == String {
	        let matchers: [Cuckoo.ParameterMatcher<(Bool, String, String)>] = [wrap(matchable: imapWorks) { $0.0 }, wrap(matchable: login) { $0.1 }, wrap(matchable: password) { $0.2 }]
	        return cuckoo_manager.verify("onImapCompleted(imapWorks: Bool, _: String, _: String)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func onSmtpCompleted<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable, M3: Cuckoo.Matchable>(smtpWorks: M1, _ login: M2, _ password: M3) -> Cuckoo.__DoNotUse<(Bool, String, String), Void> where M1.MatchedType == Bool, M2.MatchedType == String, M3.MatchedType == String {
	        let matchers: [Cuckoo.ParameterMatcher<(Bool, String, String)>] = [wrap(matchable: smtpWorks) { $0.0 }, wrap(matchable: login) { $0.1 }, wrap(matchable: password) { $0.2 }]
	        return cuckoo_manager.verify("onSmtpCompleted(smtpWorks: Bool, _: String, _: String)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class AuthenticationModelStub: AuthenticationModel {
    
    
     override var extendedValidation: Bool {
        get {
            return DefaultValueRegistry.defaultValue(for: (Bool).self)
        }
        
        set { }
        
    }
    
    
     override var imapCallback: AuthenticationCallback? {
        get {
            return DefaultValueRegistry.defaultValue(for: (AuthenticationCallback?).self)
        }
        
        set { }
        
    }
    
    
     override var smtpCallback: AuthenticationCallback? {
        get {
            return DefaultValueRegistry.defaultValue(for: (AuthenticationCallback?).self)
        }
        
        set { }
        
    }
    
    
     override var imapConfigurationSuccessful: Bool {
        get {
            return DefaultValueRegistry.defaultValue(for: (Bool).self)
        }
        
        set { }
        
    }
    
    
     override var smtpConfigurationSuccessful: Bool {
        get {
            return DefaultValueRegistry.defaultValue(for: (Bool).self)
        }
        
        set { }
        
    }
    
    
     override var dispatchGroup: DispatchGroup {
        get {
            return DefaultValueRegistry.defaultValue(for: (DispatchGroup).self)
        }
        
        set { }
        
    }
    

    

    
     override func checkConfig(mailAccount: MailAccount, extendedValidation: Bool) -> Future<AuthenticationResult, Never>  {
        return DefaultValueRegistry.defaultValue(for: (Future<AuthenticationResult, Never>).self)
    }
    
     override func checkIMAPConfig(mailAccount: MailAccount, _ callback: AuthenticationCallback)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func checkSMTPConfig(mailAccount: MailAccount, _ callback: AuthenticationCallback)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func onTimeout(timeoutResult: DispatchTimeoutResult) -> AuthenticationResult  {
        return DefaultValueRegistry.defaultValue(for: (AuthenticationResult).self)
    }
    
     override func onImapCompleted(imapWorks: Bool, _ login: String, _ password: String)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func onSmtpCompleted(smtpWorks: Bool, _ login: String, _ password: String)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockAuthenticationCallback: AuthenticationCallback, Cuckoo.ClassMock {
    
     typealias MocksType = AuthenticationCallback
    
     typealias Stubbing = __StubbingProxy_AuthenticationCallback
     typealias Verification = __VerificationProxy_AuthenticationCallback

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: AuthenticationCallback?

     func enableDefaultImplementation(_ stub: AuthenticationCallback) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     override var callback: ((Bool, String, String) -> Void)? {
        get {
            return cuckoo_manager.getter("callback",
                superclassCall:
                    
                    super.callback
                    ,
                defaultCall: __defaultImplStub!.callback)
        }
        
        set {
            cuckoo_manager.setter("callback",
                value: newValue,
                superclassCall:
                    
                    super.callback = newValue
                    ,
                defaultCall: __defaultImplStub!.callback = newValue)
        }
        
    }
    

    

    
    
    
     override func onResult(worked: Bool, login: String, password: String)  {
        
    return cuckoo_manager.call("onResult(worked: Bool, login: String, password: String)",
            parameters: (worked, login, password),
            escapingParameters: (worked, login, password),
            superclassCall:
                
                super.onResult(worked: worked, login: login, password: password)
                ,
            defaultCall: __defaultImplStub!.onResult(worked: worked, login: login, password: password))
        
    }
    

	 struct __StubbingProxy_AuthenticationCallback: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var callback: Cuckoo.ClassToBeStubbedOptionalProperty<MockAuthenticationCallback, ((Bool, String, String) -> Void)> {
	        return .init(manager: cuckoo_manager, name: "callback")
	    }
	    
	    
	    func onResult<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable, M3: Cuckoo.Matchable>(worked: M1, login: M2, password: M3) -> Cuckoo.ClassStubNoReturnFunction<(Bool, String, String)> where M1.MatchedType == Bool, M2.MatchedType == String, M3.MatchedType == String {
	        let matchers: [Cuckoo.ParameterMatcher<(Bool, String, String)>] = [wrap(matchable: worked) { $0.0 }, wrap(matchable: login) { $0.1 }, wrap(matchable: password) { $0.2 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationCallback.self, method: "onResult(worked: Bool, login: String, password: String)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_AuthenticationCallback: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var callback: Cuckoo.VerifyOptionalProperty<((Bool, String, String) -> Void)> {
	        return .init(manager: cuckoo_manager, name: "callback", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func onResult<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable, M3: Cuckoo.Matchable>(worked: M1, login: M2, password: M3) -> Cuckoo.__DoNotUse<(Bool, String, String), Void> where M1.MatchedType == Bool, M2.MatchedType == String, M3.MatchedType == String {
	        let matchers: [Cuckoo.ParameterMatcher<(Bool, String, String)>] = [wrap(matchable: worked) { $0.0 }, wrap(matchable: login) { $0.1 }, wrap(matchable: password) { $0.2 }]
	        return cuckoo_manager.verify("onResult(worked: Bool, login: String, password: String)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class AuthenticationCallbackStub: AuthenticationCallback {
    
    
     override var callback: ((Bool, String, String) -> Void)? {
        get {
            return DefaultValueRegistry.defaultValue(for: (((Bool, String, String) -> Void)?).self)
        }
        
        set { }
        
    }
    

    

    
     override func onResult(worked: Bool, login: String, password: String)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}


// MARK: - Mocks generated from file: enzevalos_iphone/AuthenticationViewModel.swift at 2021-04-10 20:34:30 +0000

//
//  AuthenticationViewModel.swift
//  enzevalos_iphone
//
//  Created by Cezary Pilaszewicz on 02.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Cuckoo
@testable import enzevalos_iphone

import Combine
import Foundation


 class MockAuthenticationViewModel: AuthenticationViewModel, Cuckoo.ClassMock {
    
     typealias MocksType = AuthenticationViewModel
    
     typealias Stubbing = __StubbingProxy_AuthenticationViewModel
     typealias Verification = __VerificationProxy_AuthenticationViewModel

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: AuthenticationViewModel?

     func enableDefaultImplementation(_ stub: AuthenticationViewModel) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     override var errorMessage: String? {
        get {
            return cuckoo_manager.getter("errorMessage",
                superclassCall:
                    
                    super.errorMessage
                    ,
                defaultCall: __defaultImplStub!.errorMessage)
        }
        
        set {
            cuckoo_manager.setter("errorMessage",
                value: newValue,
                superclassCall:
                    
                    super.errorMessage = newValue
                    ,
                defaultCall: __defaultImplStub!.errorMessage = newValue)
        }
        
    }
    
    
    
     override var isDetailedAuthentication: Bool {
        get {
            return cuckoo_manager.getter("isDetailedAuthentication",
                superclassCall:
                    
                    super.isDetailedAuthentication
                    ,
                defaultCall: __defaultImplStub!.isDetailedAuthentication)
        }
        
        set {
            cuckoo_manager.setter("isDetailedAuthentication",
                value: newValue,
                superclassCall:
                    
                    super.isDetailedAuthentication = newValue
                    ,
                defaultCall: __defaultImplStub!.isDetailedAuthentication = newValue)
        }
        
    }
    
    
    
     override var showProgressSpinner: Bool {
        get {
            return cuckoo_manager.getter("showProgressSpinner",
                superclassCall:
                    
                    super.showProgressSpinner
                    ,
                defaultCall: __defaultImplStub!.showProgressSpinner)
        }
        
        set {
            cuckoo_manager.setter("showProgressSpinner",
                value: newValue,
                superclassCall:
                    
                    super.showProgressSpinner = newValue
                    ,
                defaultCall: __defaultImplStub!.showProgressSpinner = newValue)
        }
        
    }
    
    
    
     override var cancellable: AnyCancellable? {
        get {
            return cuckoo_manager.getter("cancellable",
                superclassCall:
                    
                    super.cancellable
                    ,
                defaultCall: __defaultImplStub!.cancellable)
        }
        
        set {
            cuckoo_manager.setter("cancellable",
                value: newValue,
                superclassCall:
                    
                    super.cancellable = newValue
                    ,
                defaultCall: __defaultImplStub!.cancellable = newValue)
        }
        
    }
    

    

    
    
    
     override func validate(_ login: String, _ password: String)  {
        
    return cuckoo_manager.call("validate(_: String, _: String)",
            parameters: (login, password),
            escapingParameters: (login, password),
            superclassCall:
                
                super.validate(login, password)
                ,
            defaultCall: __defaultImplStub!.validate(login, password))
        
    }
    
    
    
     override func detailValidation(_ login: String, _ password: String, _ username: String, _ imapServer: String, _ imapPort: String, _ imapEncryption: Int, _ smtpServer: String, _ smtpPort: String, _ smtpEncryption: Int)  {
        
    return cuckoo_manager.call("detailValidation(_: String, _: String, _: String, _: String, _: String, _: Int, _: String, _: String, _: Int)",
            parameters: (login, password, username, imapServer, imapPort, imapEncryption, smtpServer, smtpPort, smtpEncryption),
            escapingParameters: (login, password, username, imapServer, imapPort, imapEncryption, smtpServer, smtpPort, smtpEncryption),
            superclassCall:
                
                super.detailValidation(login, password, username, imapServer, imapPort, imapEncryption, smtpServer, smtpPort, smtpEncryption)
                ,
            defaultCall: __defaultImplStub!.detailValidation(login, password, username, imapServer, imapPort, imapEncryption, smtpServer, smtpPort, smtpEncryption))
        
    }
    
    
    
     override func startGoogleOauth()  {
        
    return cuckoo_manager.call("startGoogleOauth()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                super.startGoogleOauth()
                ,
            defaultCall: __defaultImplStub!.startGoogleOauth())
        
    }
    
    
    
     override func authenticationSucceed()  {
        
    return cuckoo_manager.call("authenticationSucceed()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                super.authenticationSucceed()
                ,
            defaultCall: __defaultImplStub!.authenticationSucceed())
        
    }
    
    
    
     override func authenticationFailed(error: MailServerConnectionError)  {
        
    return cuckoo_manager.call("authenticationFailed(error: MailServerConnectionError)",
            parameters: (error),
            escapingParameters: (error),
            superclassCall:
                
                super.authenticationFailed(error: error)
                ,
            defaultCall: __defaultImplStub!.authenticationFailed(error: error))
        
    }
    
    
    
     override func timeoutNotification()  {
        
    return cuckoo_manager.call("timeoutNotification()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                super.timeoutNotification()
                ,
            defaultCall: __defaultImplStub!.timeoutNotification())
        
    }
    

	 struct __StubbingProxy_AuthenticationViewModel: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var errorMessage: Cuckoo.ClassToBeStubbedOptionalProperty<MockAuthenticationViewModel, String> {
	        return .init(manager: cuckoo_manager, name: "errorMessage")
	    }
	    
	    
	    var isDetailedAuthentication: Cuckoo.ClassToBeStubbedProperty<MockAuthenticationViewModel, Bool> {
	        return .init(manager: cuckoo_manager, name: "isDetailedAuthentication")
	    }
	    
	    
	    var showProgressSpinner: Cuckoo.ClassToBeStubbedProperty<MockAuthenticationViewModel, Bool> {
	        return .init(manager: cuckoo_manager, name: "showProgressSpinner")
	    }
	    
	    
	    var cancellable: Cuckoo.ClassToBeStubbedOptionalProperty<MockAuthenticationViewModel, AnyCancellable> {
	        return .init(manager: cuckoo_manager, name: "cancellable")
	    }
	    
	    
	    func validate<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(_ login: M1, _ password: M2) -> Cuckoo.ClassStubNoReturnFunction<(String, String)> where M1.MatchedType == String, M2.MatchedType == String {
	        let matchers: [Cuckoo.ParameterMatcher<(String, String)>] = [wrap(matchable: login) { $0.0 }, wrap(matchable: password) { $0.1 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationViewModel.self, method: "validate(_: String, _: String)", parameterMatchers: matchers))
	    }
	    
	    func detailValidation<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable, M3: Cuckoo.Matchable, M4: Cuckoo.Matchable, M5: Cuckoo.Matchable, M6: Cuckoo.Matchable, M7: Cuckoo.Matchable, M8: Cuckoo.Matchable, M9: Cuckoo.Matchable>(_ login: M1, _ password: M2, _ username: M3, _ imapServer: M4, _ imapPort: M5, _ imapEncryption: M6, _ smtpServer: M7, _ smtpPort: M8, _ smtpEncryption: M9) -> Cuckoo.ClassStubNoReturnFunction<(String, String, String, String, String, Int, String, String, Int)> where M1.MatchedType == String, M2.MatchedType == String, M3.MatchedType == String, M4.MatchedType == String, M5.MatchedType == String, M6.MatchedType == Int, M7.MatchedType == String, M8.MatchedType == String, M9.MatchedType == Int {
	        let matchers: [Cuckoo.ParameterMatcher<(String, String, String, String, String, Int, String, String, Int)>] = [wrap(matchable: login) { $0.0 }, wrap(matchable: password) { $0.1 }, wrap(matchable: username) { $0.2 }, wrap(matchable: imapServer) { $0.3 }, wrap(matchable: imapPort) { $0.4 }, wrap(matchable: imapEncryption) { $0.5 }, wrap(matchable: smtpServer) { $0.6 }, wrap(matchable: smtpPort) { $0.7 }, wrap(matchable: smtpEncryption) { $0.8 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationViewModel.self, method: "detailValidation(_: String, _: String, _: String, _: String, _: String, _: Int, _: String, _: String, _: Int)", parameterMatchers: matchers))
	    }
	    
	    func startGoogleOauth() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationViewModel.self, method: "startGoogleOauth()", parameterMatchers: matchers))
	    }
	    
	    func authenticationSucceed() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationViewModel.self, method: "authenticationSucceed()", parameterMatchers: matchers))
	    }
	    
	    func authenticationFailed<M1: Cuckoo.Matchable>(error: M1) -> Cuckoo.ClassStubNoReturnFunction<(MailServerConnectionError)> where M1.MatchedType == MailServerConnectionError {
	        let matchers: [Cuckoo.ParameterMatcher<(MailServerConnectionError)>] = [wrap(matchable: error) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationViewModel.self, method: "authenticationFailed(error: MailServerConnectionError)", parameterMatchers: matchers))
	    }
	    
	    func timeoutNotification() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationViewModel.self, method: "timeoutNotification()", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_AuthenticationViewModel: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var errorMessage: Cuckoo.VerifyOptionalProperty<String> {
	        return .init(manager: cuckoo_manager, name: "errorMessage", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var isDetailedAuthentication: Cuckoo.VerifyProperty<Bool> {
	        return .init(manager: cuckoo_manager, name: "isDetailedAuthentication", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var showProgressSpinner: Cuckoo.VerifyProperty<Bool> {
	        return .init(manager: cuckoo_manager, name: "showProgressSpinner", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var cancellable: Cuckoo.VerifyOptionalProperty<AnyCancellable> {
	        return .init(manager: cuckoo_manager, name: "cancellable", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func validate<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(_ login: M1, _ password: M2) -> Cuckoo.__DoNotUse<(String, String), Void> where M1.MatchedType == String, M2.MatchedType == String {
	        let matchers: [Cuckoo.ParameterMatcher<(String, String)>] = [wrap(matchable: login) { $0.0 }, wrap(matchable: password) { $0.1 }]
	        return cuckoo_manager.verify("validate(_: String, _: String)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func detailValidation<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable, M3: Cuckoo.Matchable, M4: Cuckoo.Matchable, M5: Cuckoo.Matchable, M6: Cuckoo.Matchable, M7: Cuckoo.Matchable, M8: Cuckoo.Matchable, M9: Cuckoo.Matchable>(_ login: M1, _ password: M2, _ username: M3, _ imapServer: M4, _ imapPort: M5, _ imapEncryption: M6, _ smtpServer: M7, _ smtpPort: M8, _ smtpEncryption: M9) -> Cuckoo.__DoNotUse<(String, String, String, String, String, Int, String, String, Int), Void> where M1.MatchedType == String, M2.MatchedType == String, M3.MatchedType == String, M4.MatchedType == String, M5.MatchedType == String, M6.MatchedType == Int, M7.MatchedType == String, M8.MatchedType == String, M9.MatchedType == Int {
	        let matchers: [Cuckoo.ParameterMatcher<(String, String, String, String, String, Int, String, String, Int)>] = [wrap(matchable: login) { $0.0 }, wrap(matchable: password) { $0.1 }, wrap(matchable: username) { $0.2 }, wrap(matchable: imapServer) { $0.3 }, wrap(matchable: imapPort) { $0.4 }, wrap(matchable: imapEncryption) { $0.5 }, wrap(matchable: smtpServer) { $0.6 }, wrap(matchable: smtpPort) { $0.7 }, wrap(matchable: smtpEncryption) { $0.8 }]
	        return cuckoo_manager.verify("detailValidation(_: String, _: String, _: String, _: String, _: String, _: Int, _: String, _: String, _: Int)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func startGoogleOauth() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("startGoogleOauth()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func authenticationSucceed() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("authenticationSucceed()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func authenticationFailed<M1: Cuckoo.Matchable>(error: M1) -> Cuckoo.__DoNotUse<(MailServerConnectionError), Void> where M1.MatchedType == MailServerConnectionError {
	        let matchers: [Cuckoo.ParameterMatcher<(MailServerConnectionError)>] = [wrap(matchable: error) { $0 }]
	        return cuckoo_manager.verify("authenticationFailed(error: MailServerConnectionError)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func timeoutNotification() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("timeoutNotification()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class AuthenticationViewModelStub: AuthenticationViewModel {
    
    
     override var errorMessage: String? {
        get {
            return DefaultValueRegistry.defaultValue(for: (String?).self)
        }
        
        set { }
        
    }
    
    
     override var isDetailedAuthentication: Bool {
        get {
            return DefaultValueRegistry.defaultValue(for: (Bool).self)
        }
        
        set { }
        
    }
    
    
     override var showProgressSpinner: Bool {
        get {
            return DefaultValueRegistry.defaultValue(for: (Bool).self)
        }
        
        set { }
        
    }
    
    
     override var cancellable: AnyCancellable? {
        get {
            return DefaultValueRegistry.defaultValue(for: (AnyCancellable?).self)
        }
        
        set { }
        
    }
    

    

    
     override func validate(_ login: String, _ password: String)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func detailValidation(_ login: String, _ password: String, _ username: String, _ imapServer: String, _ imapPort: String, _ imapEncryption: Int, _ smtpServer: String, _ smtpPort: String, _ smtpEncryption: Int)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func startGoogleOauth()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func authenticationSucceed()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func authenticationFailed(error: MailServerConnectionError)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func timeoutNotification()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}

