//
//  RecipientRowView.swift
//  letterbox_prototyping
//
//  Created by Chris Offner & Claire Bräuer on 11.03.21.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import SwiftUI

/// A view that displays a single contact for recipient selection.
struct RecipientRowView: View {
    let contact: AddressRecord
    @EnvironmentObject var model: RecipientFieldModel
    
    var body: some View {
        Button {
            if let index = model.selectedContacts.firstIndex(where: { $0.email == contact.email }) {
                model.deselectContact(at: index)
            } else {
                model.selectContact(contact)
            }
        } label: {
            HStack {
                // Profile picture
                contact.avatar
                    .resizable()
                    .frame(width: 30, height: 30)
                    .aspectRatio(contentMode: .fit)
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                    .shadow(radius: 2)
                    .padding(EdgeInsets(top: 0, leading: 3, bottom: 0, trailing: 8))
                
                VStack(alignment: .leading) {
                    HStack {
                        // TODO: Show proper displayname.
                        // Currently for debugging purposes we show the email
                        // before "@" as name because displaynames are all nil.
                        Text(contact.displayname ?? contact.email.components(separatedBy: "@")[0])
                            .foregroundColor(.primary)
                        Spacer()
                        Text(contact.lastHeardFrom)
                            .font(.caption)
                            .padding(.trailing, 16)
                    }
                    
                    // Currently only shows first email address of contact.
                    // TODO: Decide which email address(es) to show.
                    Text(contact.email)
                        .font(.caption)
                }
                .foregroundColor(.secondary)
                
                Spacer()
                
                // TODO: Maybe use more robust identifier (Identifiable + UUID?)
                Image(systemName: model.selectedContacts.contains {
                        $0.email == contact.email
                } ? "checkmark.circle.fill" : "circle")
                    .foregroundColor(.accentColor)
            }
        }
    }
}
