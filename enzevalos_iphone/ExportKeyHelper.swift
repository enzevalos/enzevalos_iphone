//
//  ExportKeyHelper.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 09.04.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import UIKit


extension UIColor {
    
    struct KeyExport {
        
        static let publicKey = UIColor.green
        static let secretKey = UIColor.white
    }
}
