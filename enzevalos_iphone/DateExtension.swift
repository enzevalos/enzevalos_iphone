//
//  DateExtension.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 26.09.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation
extension Date {
    func timeAgo() -> DateComponents {
        let calender = Calendar.current
        let start = calender.startOfDay(for: self)
        let current = calender.startOfDay(for: Date())
        return calender.dateComponents([.day, .month, .year], from: start, to: current)
    }
    
     func isToday() -> Bool {
        let dateComponents = self.timeAgo()
        if let day = dateComponents.day,
           let month = dateComponents.month,
           let year = dateComponents.year,
           day == 0 && month == 0,
           year == 0 {
            return true
        }
        return false
    }
    
    func timeAgoText() -> String {
        let components = self.timeAgo()
        if let years = components.year, years > 0 {
            if years == 1 {
                return NSLocalizedString("Time.Year.One", comment: "one year")
            } else {
                return String(format: NSLocalizedString("Time.Year.Multiple", comment: "multiple years"), years)
            }
        } else if let months = components.month, months > 0 {
            if months == 1 {
                return NSLocalizedString("Time.Month.One", comment: "one month")
            } else {
                let s = NSLocalizedString("Time.Month.Multiple", comment: "multiple month")
                return String(format: s, months)
            }
        } else if let days = components.day {
            if days == 0 {
                return NSLocalizedString("Time.Day.Today", comment: "today")
            } else if days == 1 {
                return NSLocalizedString("Time.Day.One", comment: "one day")
            } else {
                let s = NSLocalizedString("Time.Day.Multiple", comment: "multiple days")
                return String(format: s, days)
            }
        }
        return self.description(with: .autoupdatingCurrent)
    }
    // TODO: Refactor -> Other name
    var dateToString: String {
        get {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale.current
            let mailTime = self
            let interval = Date().timeIntervalSince(mailTime as Date)
            switch interval {
            case -55..<55:
                return NSLocalizedString("Now", comment: "New email")
            case 55..<120:
                return NSLocalizedString("OneMinuteAgo", comment: "Email came one minute ago")
            case 120..<24 * 60 * 60:
                dateFormatter.timeStyle = .short
                return dateFormatter.string(from: mailTime as Date)
            case 86400..<172800: //24 * 60 * 60..<48 * 60 * 60:
                return NSLocalizedString("Yesterday", comment: "Email came yesterday")
            case 172800..<259200: //48 * 60 * 60..<72 * 60 * 60:
                return NSLocalizedString("TwoDaysAgo", comment: "Email came two days ago")
            default:
                dateFormatter.dateStyle = .short
                return dateFormatter.string(from: mailTime as Date)
            }
        }
    }
}
