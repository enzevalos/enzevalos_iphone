//
//  PersistentDataProvider.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 02.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import CoreData

/*
  TODO:
    * How to handle key rcord?
    * add fetch requests:
        + Find use cases
        + mails per Address/Key -> Contact View
        + Addresses per Key -> Contact View
        + all folders -> Folder Overview
        + find specific mail -> Detail view
 
  TODOs:
    SwiftUI and CoreData?
 
 Refactoring: Move importOneMail etc to Properties???
 
Reload mails for record loadMailsForRecord(contact!, folderPath: UserManager.backendInboxFolderPath, completionCallback: doneLoading)
 Update folder updateFolder(folder: Folder.inbox, completionCallback: {_ in
 Delete a Mail
 find public or secret keys
 find address by name
 find folder by name
 //TODO look for not verified signed mails.
 */
class PersistentDataProvider {
    static let dataProvider = PersistentDataProvider()
    static let FETCHLIMIT = 200
    
    static var prefKeyID: String {
        get {
            if let id = UserManager.loadUserValue(.prefSecretKeyID) as? String { return id }
            if let sk = dataProvider.fetchedSecretKeyResultsController.fetchedObjects?.first, let id = sk.fingerprint { return id }
            return ""
        }
    }

    private static var proxyAddresses: [AddressProperties] {
        var results = [AddressProperties]()
        let startValues = [("vic@example.com", "Vic"), ("alice@example.org", "Alice"), ("alex@mail.com", nil)]
        for i in 0...100 {
            for v in startValues {
                if let n = v.1 {
                    let addr = AddressProperties(email: "\(v.0)\(i)", name: "\(n)\(i)")
                    results.append(addr)
                } else {
                    let addr = AddressProperties(email: "\(v.0)\(i)")
                    results.append(addr)
                }                
            }
        }
        return results
    }
    
    private static var proxyMailProperties: [MailProperties] {
        let addr1 = AddressProperties(email: "vic@example.com")
        let addr2 = AddressProperties(email: "alex@example.com")
        let addrs = [addr1, addr2]
        let folder = FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: "Inbox", flags: 0)
        
        var msgs = [MailProperties]()
        for i in 1...10 {
            let a = addrs[i%addrs.count]
            let msg = MailProperties(messageID: "\(i)", uid: UInt64(i), subject: "MSG\(i)", date: Date(), flags: 0, from: a, folder: folder, signatureState: 0, encryptionState: 0)
            msgs.append(msg)
        }
        return msgs
    }
    
    private static var proxyFolders: [FolderProperties] {
        let f1 = FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: "Folder1", flags: 0)
        let f2 = FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: "Folder2", flags: 0)
        return [f1,f2]
    }
    
    private static var proxyPublicKeys: [PublicKeyProperties] {
        var keys = [PublicKeyProperties]()
        for i in 1...20 {
            let pk = PublicKeyProperties(fingerprint: "\(i)\(i)\(i)\(i)\(i)\(i)\(i)", cryptoProtocol: .PGP, origin: .KeyServer, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: nil, usedAddresses: [AddressProperties(email: "vic@example.com")])
            keys.append(pk)
        }
        
        return keys
    }
    
    static var proxyPersistentDataProvider: PersistentDataProvider = {
        let result = PersistentDataProvider(inMemory: true)
        
        for addr in proxyAddresses {
            result.importManyAddresses(addrs: proxyAddresses, taskContext: result.persistentContainer.viewContext, addTo: nil)
        }
        
        for m in proxyMailProperties {
            result.importOneMail(m: m, taskContext: result.persistentContainer.viewContext)
        }
        do {
            try result.save(taskContext: result.persistentContainer.viewContext)
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return result
    }()
    
    // MARK: - Core Data
    private let entitiesNames = [AddressRecord.entityName, MailRecord.entityName, FolderRecord.entityName, PublicKeyRecord.entityName, SecretKeyRecord.entityName, AttachmentRecord.entityName]
    
    let inMemory: Bool // for testing and previews...
    
    init(inMemory: Bool = false) {
        self.inMemory = inMemory
    }
    
    // A persistent container to set up the Core Data stack.
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "DataModel")
        
        
    // Enable remote notifications
    guard let description = container.persistentStoreDescriptions.first else {
        fatalError("Failed to retrieve a persistent store description.")
    }
        
    if inMemory {
        container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
    }
    description.setOption(true as NSNumber, forKey: NSPersistentStoreRemoteChangeNotificationPostOptionKey)

    container.loadPersistentStores { storeDesription, error in
        guard error == nil else {
            fatalError("Unresolved error \(error!)")
        }
    }

    // This sample refreshes UI by refetching data, so doesn't need to merge the changes.
    container.viewContext.automaticallyMergesChangesFromParent = true
    container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
    container.viewContext.undoManager = nil
    container.viewContext.shouldDeleteInaccessibleFaults = true
    
    return container
    }()


    // Creates and configures a private queue context.
    private func newTaskContext() -> NSManagedObjectContext {
        // Create a private queue context.
        let taskContext = persistentContainer.newBackgroundContext()
        taskContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        // Set unused undoManager to nil for macOS (it is nil by default on iOS)
        // to reduce resource requirements.
        taskContext.undoManager = nil
        return taskContext
    }
    
    func importNewData(from data: [DataPropertyProtocol], completionHandler: @escaping (Error?) -> Void) {
        importData(from: data, callMainThread: true, completionHandler: completionHandler)
    }
    
    
    private func importData(from data: [DataPropertyProtocol], callMainThread: Bool, completionHandler: @escaping (Error?) -> Void) {
        guard !data.isEmpty else {
            completionHandler(nil)
            return
        }
        var performError: Error? = nil
        let queue = DispatchQueue(label: "com.letterbox.persistant")
        queue.async {
            // Process records in batches to avoid a high memory footprint.
            
            let batchSize = 30
            let count = data.count
            
            if count < batchSize {
                do {
                    try self.importOneBatch(data)
                } catch {
                    performError = error
                }
            } else {
                // Determine the total number of batches.
                var numBatches = count / batchSize
                numBatches += count % batchSize > 0 ? 1 : 0
                
                for batchNumber in 0 ..< numBatches {
                    // Determine the range for this batch.
                    let batchStart = batchNumber * batchSize
                    let batchEnd = batchStart + min(batchSize, count - batchNumber * batchSize)
                    let range = batchStart..<batchEnd
                    
                    // Create a batch for this range from the decoded JSON.
                    // Stop importing if any batch is unsuccessful.
                    do {
                        try self.importOneBatch(Array(data[range]))
                    } catch {
                        performError = error
                    }
                }
           }
            if callMainThread {
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                    completionHandler(performError)
                })
            } else {
                completionHandler(performError)
            }
        }
    }
    
    
    private func importOneBatch(_ data: [DataPropertyProtocol]) throws {
        guard data.count > 0 else {
            throw PersistentDataError.creationError
        }

        let taskContext = newTaskContext()
        var performError: Error?
        
        taskContext.performAndWait {
            // Create a new record for each mail in the batch.
            for d in data {
                if let m = d as? MailProperties {
                    self.importOneMail(m: m, taskContext: taskContext)
                }
                else if let key = d as? PublicKeyProperties {
                    _ = self.importeOneKey(publicKey: key, taskContext: taskContext)
                }
                else {
                    // Create a managed object on the private queue context.
                    let item = NSEntityDescription.insertNewObject(forEntityName: d.entityName, into: taskContext)
                    if !d.update(m: item) {
                        performError = PersistentDataError.creationError
                        return
                    }
                }
            }
            do {
                try self.save(taskContext: taskContext)
            }
            catch {
                performError = error

            }
        }
        if let error = performError {
            throw error
        }
    }
    
   
    private func importeOneKey(publicKey: PublicKeyProperties, taskContext: NSManagedObjectContext) -> PublicKeyRecord {
        let publicKeyRecord: PublicKeyRecord
        if let pk = taskContext.findPublicKeys(fingerprint: publicKey.fingerprint)?.first {
            publicKeyRecord = pk
        }
        else {
            publicKeyRecord = NSEntityDescription.insertNewObject(forEntityName: PublicKeyRecord.entityName, into: taskContext) as! PublicKeyRecord
            publicKeyRecord.update(property: publicKey)
        }
        
        
        if let addresses = publicKey.usedAddresses {
            // First: If an address has no primaryKey -> the first associated public key is the primary key
            importManyAddresses(addrs: addresses, taskContext: taskContext, addTo: { record in
                if record.primaryKey == nil {
                    record.primaryKey = publicKeyRecord
                }
            })
            // Second: Add relationship between key and addresses
            importManyAddresses(addrs: addresses , taskContext: taskContext, addTo: publicKeyRecord.addToUsedAddress)
        }
        if let secretKey = publicKey.secretKeyProperty {
            let secretKeyRecord: SecretKeyRecord
            
            if let sk = taskContext.findSecretKeys(fingerprint: secretKey.fingerprint)?.first {
                secretKeyRecord = sk
            } else {
                secretKeyRecord = NSEntityDescription.insertNewObject(forEntityName: SecretKeyRecord.entityName, into: taskContext) as! SecretKeyRecord
                secretKeyRecord.update(property: secretKey)
            }
            secretKeyRecord.myPublicKeyRecord = publicKeyRecord
            if let addresses = secretKey.usedAddresses {
                importManyAddresses(addrs: addresses , taskContext: taskContext, addTo: secretKeyRecord.addToUsedAddress)
            }
        }
        return publicKeyRecord
    }
    
    private func importOneMail(m: MailProperties, taskContext: NSManagedObjectContext) {
        let mail: MailRecord
        if let myMail = try! lookUp(entityName: MailRecord.entityName, sorting: ["date": false], equalPredicates: ["messageID": m.messageID], differentPredicates: nil, inPredicates: nil, context: taskContext).fetchedObjects?.first as? MailRecord{
            mail = myMail
        } else {
            // Create a managed object on the private queue context.
            mail = NSEntityDescription.insertNewObject(forEntityName: MailRecord.entityName, into: taskContext) as! MailRecord
        }
        mail.update(with: m)
        // add relationships
        // From
        var sender = AddressProperties(email: "")
        if let from = m.from {
            sender = from
        }
        else {
            fatalError("MISSING SENDER!")
        }
        let from: AddressRecord
        if let addr = taskContext.findAddressRecords(email: sender.email)?.first {
            from = addr
        } else if let addr = try! lookUp(entityName: AddressRecord.entityName, sorting: ["email": true], equalPredicates: ["email": sender.email], differentPredicates: nil, inPredicates: nil, context: taskContext).fetchedObjects?.first as? AddressRecord {
            from = addr
        } else {
            from = NSEntityDescription.insertNewObject(forEntityName: AddressRecord.entityName, into: taskContext) as! AddressRecord
        }
        from.update(with: sender)
        mail.fromAddress = from
        
        // To, CC, BCC
        importManyAddresses(addrs: m.to, taskContext: taskContext, addTo: mail.addToToAddresses)
        importManyAddresses(addrs: m.cc, taskContext: taskContext, addTo: mail.addToCcAddresses)
        importManyAddresses(addrs: m.bcc, taskContext: taskContext, addTo: mail.addToBccAddresses)
        
        // Add to folder
        let folder: FolderRecord
        if let f = taskContext.findFolder(path: m.folder.path)?.first {
            folder = f
        }
        else if let f = try! lookUp(entityName: FolderRecord.entityName, sorting: ["path": true], equalPredicates: ["path": m.folder.path], differentPredicates: nil, inPredicates: nil, context: taskContext).fetchedObjects?.first as? FolderRecord{
              folder = f
        }
        else {
            folder = NSEntityDescription.insertNewObject(forEntityName: FolderRecord.entityName, into: taskContext) as! FolderRecord
        }
        folder.update(properties: m.folder)
        mail.inFolder = folder
        
        // Add attachments
        for att in m.attachments {
            let attRecord: AttachmentRecord
            if let at = taskContext.findAttachment(name: att.name, mimeType: att.mimeType, data: att.data) {
                attRecord = at
            // TODO add "data": att.data, to equal Predicates?
            } else if let at = try! lookUp(entityName: AttachmentRecord.entityName, sorting: ["name": true], equalPredicates: ["name":att.name, "mimeType": att.mimeType], differentPredicates: [String:String](), inPredicates: nil, context: taskContext).fetchedObjects?.first as? AttachmentRecord{
                attRecord = at
            } else {
                attRecord = NSEntityDescription.insertNewObject(forEntityName: AttachmentRecord.entityName, into: taskContext) as! AttachmentRecord
                attRecord.update(property: att)
            }
            mail.addToIncludedAttachments(attRecord)
        }
       
        
        // Handling keys
        if let signedKey = m.signatureKey {
            let key = importeOneKey(publicKey: signedKey, taskContext: taskContext)
            key.update(property: signedKey)
            mail.signatureKey = key
        }
        if let decKey = m.decryptionKey {
            let publicKeyProperties = PublicKeyProperties(fingerprint: decKey.fingerprint, cryptoProtocol: decKey.cryptoProtocol, origin: nil, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: decKey,usedAddresses: decKey.usedAddresses)
            let key = importeOneKey(publicKey: publicKeyProperties, taskContext: taskContext)
            if let secKey = key.mySecretKeyRecord {
                secKey.update(property: decKey)
                mail.decryptionKey = secKey
            }
        }
        if let autocryptKey = m.autocryptHeaderKey.first { // TODO: Fix multiple Autocrypt keys?
            let key = importeOneKey(publicKey: autocryptKey, taskContext: taskContext)
            key.update(property: autocryptKey)
            mail.attachedAutocryptPublicKey = key
        }
        for pk in m.attachedPublicKeys {
            let key = importeOneKey(publicKey: pk, taskContext: taskContext)
            key.update(property: pk)
            mail.addToAttachedPublicKeys(key)
        }
        for sk in m.attachedSecretKeys {
            let key = NSEntityDescription.insertNewObject(forEntityName: SecretKeyRecord.entityName, into: taskContext) as! SecretKeyRecord
            key.update(property: sk)
            mail.addToAttachedSecretKey(key)
        }
    }
    
    
    private func importManyAddresses(addrs: [AddressProperties], taskContext: NSManagedObjectContext, addTo: ((AddressRecord) -> ())? ) {
        guard addrs.count > 0 else {
            return
        }
        
        var knownAddresses = [AddressRecord]()
        knownAddresses.append(contentsOf: taskContext.findAddressRecords(emails: addrs.map({$0.email})))

        let controller = try! lookUp(entityName: AddressRecord.entityName, sorting: ["email":true], equalPredicates: nil, differentPredicates: nil, inPredicates: ["email": addrs.map{$0.email}], context: taskContext)
        if let addresses = controller.fetchedObjects as? [AddressRecord] {
            knownAddresses.append(contentsOf: addresses)
           
        }
        for record in knownAddresses {
            if let addTo = addTo {
                addTo(record)
            }
        }
        let knownEmails = knownAddresses.map{$0.email}
        for addr in addrs {
            if knownEmails.count == 0 || !knownEmails.contains(addr.email) {
                let record = NSEntityDescription.insertNewObject(forEntityName: AddressRecord.entityName, into: taskContext) as! AddressRecord
                record.update(with: addr)
                if let addTo = addTo {
                    addTo(record)
                }
            }
        }
    }
    
    
    func setCategoryOpenability(category:CategoryIDType,filter: @escaping MailFilter) throws{
        var performError: Error? = nil
        let queue = DispatchQueue(label: "AddCategoryEntriesToDatabase", qos: .userInitiated)
        queue.async {
            
            let predicate = NSPredicate(format:
                                            
                //filter for mails not categorized by this category (no entry in inCategory for this category)
                "SUBQUERY(inCategory, $category, $category.categoryName == %@ AND $category.openability > -1).@count == 0"
                                        
            ,category)
            
            let taskContext = self.newTaskContext()
            let sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            if let unclassifiedMailRecords = self.generateFetchResultController(
                entityName: MailRecord.entityName,
                sortDescriptors: sortDescriptors,
                predicate: predicate,
                propertiesToFetch: nil,
                fetchLimit: PersistentDataProvider.FETCHLIMIT,
                context: taskContext
            ).fetchedObjects as? [MailRecord]{
                taskContext.performAndWait {
                    
                    for mail in unclassifiedMailRecords {
                        let categoryRec = CategoryRecord(context: taskContext)
                        categoryRec.categoryName = category
                        categoryRec.openability = filter(mail).rawValue
                        mail.addToInCategory(categoryRec)
                    }
                    
                    //save
                    do {try self.save(taskContext: taskContext)}
                    catch {performError = error}
                }
            }else{
                performError = PersistentDataError.missingData
            }
            
        }
        if let error = performError {
            throw error
        }
    }
    
    func save(taskContext: NSManagedObjectContext) throws -> () {
        // Save all insertions and deletions from the context to the store.
        if taskContext.hasChanges {
            try taskContext.save()
            // Reset the taskContext to free the cache and lower the memory footprint.
            taskContext.reset()
        }
    }
    
    func updateFolder(folder: String, performingUpdates: @escaping((FolderRecord) -> Void)) throws {
        let queue = DispatchQueue(label: "com.letterbox.persistant")
        var performError: Error? = nil

        queue.async {
            let taskContext = self.newTaskContext()
            if let folderRecord = try! self.lookUp(entityName: FolderRecord.entityName, sorting: ["path": true], equalPredicates: ["path": folder], differentPredicates: nil, inPredicates: nil, context: taskContext).fetchedObjects?.first as? FolderRecord {
                taskContext.performAndWait {
                    performingUpdates(folderRecord)
                    do {
                        try self.save(taskContext: taskContext)
                    }
                    catch {
                        performError = error
                    }
                }
            }
            else {
                performError = PersistentDataError.missingData
            }
        }
        if let error = performError {
            throw error
        }
    }
    
    /**
     Deletes all the records in the Core Data store.
    */
    func deleteAll(completionHandler: @escaping (Error?) -> Void) {
        let taskContext = newTaskContext()
        taskContext.perform {
            for name in self.entitiesNames {
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: name)
                let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                batchDeleteRequest.resultType = .resultTypeCount
                
                guard let batchDeleteResult = try? taskContext.execute(batchDeleteRequest) as? NSBatchDeleteResult,
                    batchDeleteResult.result != nil else {
                    completionHandler(PersistentDataError.batchDeleteError)
                    return
                }
            }
            completionHandler(nil)
        }
    }

    func moveMails(with uids: [UInt64], from: String, to: String) {
        guard let rfc = try? lookUp(entityName: MailRecord.entityName, sorting: ["uID": true], equalPredicates: ["inFolder.path": from], differentPredicates: nil, inPredicates: ["uID": uids.map({"\($0)"})]) else {
            return
        }
        guard let toFolder = generateFetchedFolderResultsController(folderpath: to, moc: rfc.managedObjectContext).fetchedObjects?.first else {
            importNewData(from: [FolderProperties(delimiter: nil, uidValidity: nil, lastUpdate: nil, maxUID: nil, minUID: nil, path: to, parent: nil, children: nil, flags: 0)], completionHandler: {_ in self.moveMails(with: uids, from: from, to: to)})
            moveMails(with: uids, from: from, to: to)
            return
        }
        if let mails = rfc.fetchedObjects as? [MailRecord] {
            for mail in mails {
                mail.inFolder = toFolder
            }
        }
        try? save(taskContext: rfc.managedObjectContext)
        
    }
    
    func resetCategories(){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CategoryRecord.entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
          try self.newTaskContext().execute(deleteRequest)
            debugPrint("Deleted Entitie - ", CategoryRecord.entityName)
        } catch let error as NSError {
            debugPrint("Delete ERROR \(CategoryRecord.entityName)")
            debugPrint(error)
        }
    }
    
    func reset(){
        persistentContainer.viewContext.reset()
    }
    
    
    // MARK: - NSFetchedResultsController
    
    lazy var fetchedAddressResultController: NSFetchedResultsController<AddressRecord> = {
        let freq = NSFetchRequest<AddressRecord>(entityName: AddressRecord.entityName)
        freq.sortDescriptors = [NSSortDescriptor(key: "email", ascending: false)]

        let controller = NSFetchedResultsController(fetchRequest: freq, managedObjectContext: persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        let taskContext = persistentContainer.viewContext

        do {
            try controller.performFetch()
        } catch {
            fatalError("Unresolved error \(error)")
        }
        return controller
    }()
    
    lazy var fetchedFolderResultsController: NSFetchedResultsController<FolderRecord> = {
        let fetchRequest = NSFetchRequest<FolderRecord>(entityName: FolderRecord.entityName)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "path", ascending: false)]
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                    managedObjectContext: persistentContainer.viewContext,
                                                    sectionNameKeyPath: nil, cacheName: nil)
        // Perform the fetch.
        do {
            try controller.performFetch()
        } catch {
            fatalError("Unresolved error \(error)")
        }
        return controller
    } ()
    
    
    lazy var fetchedMailResultsController: NSFetchedResultsController<MailRecord> = {
        
        // Create a fetch request for the email entity sorted by time.
        let fetchRequest = NSFetchRequest<MailRecord>(entityName: MailRecord.entityName)
        // Create a fetched results controller and set its fetch request, context, and delegate.
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "messageID", ascending: false)]
        fetchRequest.propertiesToFetch = ["messageID", "subject"]
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                    managedObjectContext: persistentContainer.viewContext,
                                                    sectionNameKeyPath: nil, cacheName: nil)
        // Perform the fetch.
        do {
            try controller.performFetch()
        } catch {
            fatalError("Unresolved error \(error)")
        }
        return controller
    }()
    
    lazy var fetchedSecretKeyResultsController: NSFetchedResultsController<SecretKeyRecord> = {
        // Create a fetch request for the email entity sorted by time.
        let fetchRequest = NSFetchRequest<SecretKeyRecord>(entityName: SecretKeyRecord.entityName)
        // Create a fetched results controller and set its fetch request, context, and delegate.
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "fingerprint", ascending: false)]
        fetchRequest.propertiesToFetch = ["fingerprint"]
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                    managedObjectContext: persistentContainer.viewContext,
                                                    sectionNameKeyPath: nil, cacheName: nil)
        // Perform the fetch.
        do {
            try controller.performFetch()
        } catch {
            fatalError("Unresolved error \(error)")
        }
        
        return controller
    }()
    
    lazy var fetchedPublicKeyResultsController: NSFetchedResultsController<PublicKeyRecord> = {
        // Create a fetch request for the email entity sorted by time.
        return generateFetchResultController(entityName: PublicKeyRecord.entityName, sortDescriptors: [NSSortDescriptor(key: "fingerprint", ascending: false)], predicate: nil, propertiesToFetch: ["fingerprint"], fetchLimit: nil, context: nil)
    }()
    
    /**
     sorting: [String: Bool]:  -> NSSortDescriptor(key: key, ascending: value)
     */
    func lookUp<T: NSManagedObject>(
        entityName: String,
        sorting: [String: Bool],
        equalPredicates: [String: String]?,
        differentPredicates: [String: String]?,
        inPredicates: [String: [String]]?,
        propertiesToFetch: [String]? = nil,
        context: NSManagedObjectContext? = nil)
    throws -> NSFetchedResultsController<T> {
        let sortingDescriptor = sorting.map{(k,v) in NSSortDescriptor(key: k, ascending: v)}
        
        var predicates = [NSPredicate]()
        if let myPredicates = equalPredicates {
            for (k,v) in myPredicates {
                if v == "nil" {
                    predicates.append(NSPredicate(format: "\(k) == nil"))

                } else {
                    predicates.append(NSPredicate(format: "\(k) == %@", v)) // TODO REFACTOR: Predicates are created in Record files???
                }
            }
        }
        if let myPredicates = differentPredicates {
            for (k,v) in myPredicates {
                if v == "nil" {
                    predicates.append(NSPredicate(format: "\(k) != nil"))

                } else {
                    predicates.append(NSPredicate(format: "\(k) != %@", v)) // TODO REFACTOR: Predicates are created in Record files???
                }
            }
        }
        if let myPredicates = inPredicates {
            for (k,v) in myPredicates {
                if v.count == 1 {
                    predicates.append(NSPredicate(format: "\(k) == %@",v[0]))
                } else if v.count > 1 {
                    predicates.append(NSPredicate(format: "\(k) IN %@",v))
                }
                else {
                    throw PersistentDataError.wrongPredicateError
                }
            }
        }
        var predicate: NSPredicate?
        if predicates.count == 1, let myPredicate = predicates.first {
           predicate = myPredicate
        }
        else if predicates.count > 1 {
            predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        }
        return generateFetchResultController(entityName: entityName, sortDescriptors: sortingDescriptor, predicate: predicate, propertiesToFetch: propertiesToFetch, fetchLimit: PersistentDataProvider.FETCHLIMIT, context: context)
    }
    
    private func generateFetchResultController<T: NSManagedObject>(
        entityName: String,
        sortDescriptors: [NSSortDescriptor],
        predicate: NSPredicate?,
        propertiesToFetch: [String]?,
        fetchLimit: Int?,
        context: NSManagedObjectContext?)
    -> NSFetchedResultsController<T> {
        let freq = NSFetchRequest<T>(entityName: entityName)
        freq.sortDescriptors = sortDescriptors
        freq.predicate = predicate
        freq.propertiesToFetch = propertiesToFetch
        if let limit = fetchLimit {
            freq.fetchLimit = limit
        }
        
        var myContext: NSManagedObjectContext
        if let con = context {
            myContext = con
        } else if Thread.isMainThread {
            myContext = persistentContainer.viewContext
        } else {
            myContext = newTaskContext()
        }
        let controller = NSFetchedResultsController(fetchRequest: freq, managedObjectContext: myContext, sectionNameKeyPath: nil, cacheName: nil)
        // Perform the fetch.
        do {
            try controller.performFetch()
        } catch {
            fatalError("Unresolved error \(error)")
        }
        return controller
    }
    
    func createFetchResultController<T: NSManagedObject>(
        fetchRequest: NSFetchRequest<T>,
        inViewContext: Bool = true)
    -> NSFetchedResultsController<T> {
        let context = inViewContext ? persistentContainer.viewContext : persistentContainer.newBackgroundContext()
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        // Perform the fetch.
        do {
            try controller.performFetch()
        } catch {
            fatalError("Unresolved error \(error)")
        }
        return controller
    }
    
    
    
    func generateNoMailsController()->NSFetchedResultsController<MailRecord> {
        let sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        return generateFetchResultController(entityName: MailRecord.entityName, sortDescriptors: sortDescriptors, predicate: nil, propertiesToFetch: nil, fetchLimit: PersistentDataProvider.FETCHLIMIT, context: nil)
    }
    
    func generateFetchedMailsInFolderAndCategoryResultsController(folderpath: String, category:String, andPredicate:NSPredicate=NSPredicate(value: true), important:Bool = false) -> NSFetchedResultsController<MailRecord> {
        let sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        
        let mustOpenPredicate = NSPredicate(format:
                                                
            //filter for mails that must be opened by this category
            "SUBQUERY(inCategory, $category, $category.categoryName == %@ AND $category.openability == 3).@count > 0"
                                    
        ,category)
        
        let openablePredicate = NSPredicate(format:
                                        
            //filter for mails beeing openable by this category (at least .canOpen (= 1))
            "SUBQUERY(inCategory, $category, $category.categoryName == %@ AND $category.openability > \(important ? 1 : 0)).@count > 0 " +
            //where no other categories have a .mustOpen (= 3)
            "AND SUBQUERY(inCategory, $category, $category.categoryName != %@ AND $category.openability == 3).@count == 0"
                                    
        ,category)
        
        //an eMail is in this category if it must open it OR it can open it and no other kategory mustOPen
        let inCategoryPredicate = NSCompoundPredicate(type: .or, subpredicates: [mustOpenPredicate,openablePredicate])
        
        let inFolderPredicate = NSPredicate(format: "inFolder.path == %@", folderpath)
        
        //it also has to be in this folder (folders have higher rank than categories)
        let predicate = NSCompoundPredicate(type: .and, subpredicates: [inCategoryPredicate,inFolderPredicate,andPredicate])
        
        return generateFetchResultController(entityName: MailRecord.entityName, sortDescriptors: sortDescriptors, predicate: predicate, propertiesToFetch: nil, fetchLimit: PersistentDataProvider.FETCHLIMIT, context: nil)
    }
    
    
    
    func generateFetchedMailsInFolderResultsController(folderpath: String) -> NSFetchedResultsController<MailRecord> {
        let sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        let predicate = NSPredicate(format: "inFolder.path == %@", folderpath)
        return generateFetchResultController(entityName: MailRecord.entityName, sortDescriptors: sortDescriptors, predicate: predicate, propertiesToFetch: nil, fetchLimit: PersistentDataProvider.FETCHLIMIT, context: nil)
    }
    
    func generateFetchedFolderResultsController(folderpath: String, moc: NSManagedObjectContext? = nil) -> NSFetchedResultsController<FolderRecord> {
        var mymoc = persistentContainer.viewContext
        if let moc = moc {
            mymoc = moc
        }
        let freq = NSFetchRequest<FolderRecord>(entityName: FolderRecord.entityName)
        freq.sortDescriptors = [NSSortDescriptor(key: "path", ascending: true)]
        let predicate = NSPredicate(format: "path == %@", folderpath)
        freq.predicate = predicate
        let controller = NSFetchedResultsController(fetchRequest: freq,
                                                    managedObjectContext: mymoc ,
                                                    sectionNameKeyPath: nil, cacheName: nil)
        // Perform the fetch.
        do {
            try controller.performFetch()
        } catch {
            fatalError("Unresolved error \(error)")
        }
        return controller
    }
    
    
     func generateFetchedAddressResultsController(address: String) -> NSFetchedResultsController<AddressRecord> {
        return try! lookUp(entityName: AddressRecord.entityName, sorting: ["email": true], equalPredicates: ["email": address], differentPredicates: nil, inPredicates: nil)
    }
    
    func generateFetchedSecretKeysResultController(keyIds: [String]) -> NSFetchedResultsController<SecretKeyRecord>? {
        guard keyIds.count > 0 else {
            return nil
        }
        return try! lookUp(entityName: SecretKeyRecord.entityName, sorting: ["fingerprint": true], equalPredicates: nil, differentPredicates: nil, inPredicates: ["fingerprint": keyIds])
    }
    
    func generateFetchedPublicKeysResultController(keyIds: [String]) -> NSFetchedResultsController<PublicKeyRecord>? {
        guard keyIds.count > 0 else {
            return nil
        }
        return try! lookUp(entityName: PublicKeyRecord.entityName, sorting: ["fingerprint": true], equalPredicates: nil, differentPredicates: nil, inPredicates: ["fingerprint": keyIds])

    }
    
    func generateFetchedSecretKeyResultsController(address: String) -> NSFetchedResultsController<SecretKeyRecord> {
        return try! lookUp(entityName: SecretKeyRecord.entityName, sorting: ["fingerprint": true], equalPredicates: nil, differentPredicates: nil, inPredicates: ["email": [address]])
    }
    
    func generateFetchedAddresesWithKeyResultsController(addresses: [String]) -> NSFetchedResultsController<AddressRecord> {
        return try! lookUp(entityName: AddressRecord.entityName, sorting: ["email": true], equalPredicates: nil, differentPredicates: ["primaryKey":"nil"], inPredicates: ["email": addresses], context: nil)
    }
    
    func generateFetchedUnableToDecryptMailsResultsController() -> NSFetchedResultsController<MailRecord> {
        let sortingDescriptor = NSSortDescriptor(key: "date", ascending: true)
        let predicate = NSPredicate(format: "encryptionStateInt == \(EncryptionState.UnableToDecrypt.rawValue)")
       
        return generateFetchResultController(entityName: MailRecord.entityName, sortDescriptors: [sortingDescriptor], predicate: predicate, propertiesToFetch: nil, fetchLimit: PersistentDataProvider.FETCHLIMIT, context: nil)
    }
    
    func importSecretKeys(secretProperties: [SecretKeyProperties], origin: Origin, completionHandler: @escaping (Error?) -> Void) {
        guard secretProperties.count > 0 else {
            completionHandler(nil)
            return
        }
        // a secretKey should have a coresponding public key part...
        let publicKeyProperties = secretProperties.map{PublicKeyProperties(fingerprint: $0.fingerprint, cryptoProtocol: $0.cryptoProtocol, origin: origin, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: $0,usedAddresses: $0.usedAddresses)}
        importNewData(from: publicKeyProperties, completionHandler: completionHandler)
    }
    
        
    func createNewSecretKey(addr: String, completionHandler: @escaping (Error?) -> Void) -> String {
        // TODO: Find existing key?
        let pgp = SwiftPGP()
        let key = pgp.generateKey(adr: addr, new: true)
        let addrProperty = AddressProperties(email: addr)
        let sk = SecretKeyProperties(fingerprint: key, cryptoProtocol: .PGP,usedAddresses: [addrProperty])
        importSecretKeys(secretProperties: [sk], origin: .Generated, completionHandler: completionHandler)
        return key
    }
    
    func delete(object: NSManagedObject) {
        if let moc = object.managedObjectContext {
            moc.delete(object)
            try? save(taskContext: moc)
        }
    }
}


extension NSManagedObjectContext {
    func findMailRecord(msgID: String) -> [MailRecord]? {
        for set in [self.insertedObjects, self.updatedObjects, self.deletedObjects] {
            if let records = Array(set.filter({$0 is MailRecord})) as? [MailRecord] {
                let records = records.filter({$0.messageID == msgID})
                if records.count > 0 {
                    return records
                }
            }
        }
        return nil
    }
    
    func findAddressRecords(email: String) -> [AddressRecord]? {
        for set in [self.insertedObjects, self.updatedObjects, self.deletedObjects] {
            if let records = Array(set.filter({$0 is AddressRecord})) as? [AddressRecord] {
                let records = records.filter({$0.email == email})
                if records.count > 0 {
                    return records
                }
            }
        }
        return nil
    }
    
    func findAddressRecords(emails: [String]) -> [AddressRecord] {
        var result = [AddressRecord]()
        for set in [self.insertedObjects, self.updatedObjects, self.deletedObjects] {
            if let records = Array(set.filter({$0 is AddressRecord})) as? [AddressRecord] {
                let records = records.filter({emails.contains($0.email)})
                if records.count > 0 {
                    result.append(contentsOf: records)
                }
            }
        }
        return result
    }
    
    func findPublicKeys(fingerprint: String) -> [PublicKeyRecord]? {
        for set in [self.insertedObjects, self.updatedObjects, self.deletedObjects] {
            if let records = Array(set.filter({$0 is PublicKeyRecord})) as? [PublicKeyRecord] {
                let records = records.filter({$0.fingerprint == fingerprint})
                if records.count > 0 {
                    return records
                }
            }
        }
        return nil
    }
    
    func findSecretKeys(fingerprint: String) -> [SecretKeyRecord]? {
        for set in [self.insertedObjects, self.updatedObjects, self.deletedObjects] {
            if let records = Array(set.filter({$0 is SecretKeyRecord})) as? [SecretKeyRecord] {
                let records = records.filter({$0.fingerprint == fingerprint})
                if records.count > 0 {
                    return records
                }
            }
        }
        return nil
    }
    
    func findFolder(path: String) -> [FolderRecord]? {
        for set in [self.insertedObjects, self.updatedObjects, self.deletedObjects] {
            if let records = Array(set.filter({$0 is FolderRecord})) as? [FolderRecord] {
                let records = records.filter({$0.path == path})
                if records.count > 0 {
                    return records
                }
            }
        }
        return nil
    }
    
    func findAttachment(name: String, mimeType: String?, data: Data) -> AttachmentRecord? {
        for set in [self.insertedObjects, self.updatedObjects, self.deletedObjects] {
            if let records = Array(set.filter({$0 is AttachmentRecord})) as? [AttachmentRecord] {
                let records = records.filter({$0.name == name && $0.data == data && $0.mimeType == mimeType})
                if records.count > 0 {
                    return records.first
                }
            }
        }
        return nil
    }
}
