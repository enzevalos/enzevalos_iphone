//
//  DragResizableCard.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 08.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

/// A card that has a small handle to dynamically resize
struct DragResizableCard<Content: View> : View  {
    
    var shadowElevation:CGFloat = 30
    var cornerRadius:CGFloat = 35
    var fade:CGFloat = 0
    var effect3D: CGFloat = 4
    var padding: CGFloat?
    
    @Binding var height:CGFloat?
    
    var minHeight:CGFloat = 100
    
    var content: () -> Content
    
    var body: some View {
        Card(
            shadowElevation: shadowElevation,
            cornerRadius: cornerRadius,
            fade: fade,
            effect3D: effect3D,
            padding:padding
        ){
            ZStack{
                content()
                VStack{
                    Spacer()
                    Dragbar
                }
            }
        }
    }
    
    ///the handle on which the user drags to resize the card
    private var Dragbar:some View{
        ZStack{
            Rectangle().fill(Color.gray.opacity(0.0001)).frame(width: 100, height: 25, alignment: .center)
        Capsule()
            .fill(Color.gray)
            .frame(width: 100, height: 5)
            .padding(.bottom,5)
        }
            .gesture(
                DragGesture(minimumDistance: 0.0, coordinateSpace: .global)
                    .onChanged { gesture in
                        self.updateSize(gesture)
                    }
                    .onEnded { gesture in
                        self.endDrag(gesture)
                    }
            )
        
    }
    
    // this just buffers the latest location and calculates the dragoffset
    @State private var latestX : CGFloat?
    private func updateSize(_ dragGesture: DragGesture.Value){
        let updateBy : CGFloat = dragGesture.translation.height - (latestX ?? 0)
        let newHeight = (self.height ?? 0) + updateBy
        self.height  =  max(newHeight, minHeight)
        latestX = (latestX ?? 0) + updateBy
    }
    private func endDrag(_ dragGesture: DragGesture.Value){
        self.latestX = nil
    }
    

}


struct DragResizableCard_Previews: PreviewProvider {
    static var previews: some View {
        StatefulPreviewWrapper(200){size in
            DragResizableCard(height: size){
                Text("Sample Content")
            }.scaledToFit().padding()
        }
    }
}
