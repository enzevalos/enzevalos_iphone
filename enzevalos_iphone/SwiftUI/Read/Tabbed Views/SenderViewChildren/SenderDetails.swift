//
//  SenderDetails.swift
//  enzevalos_iphone
//
//  Created by Sabina on 22.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>./

import SwiftUI

/**
 
    SenderDetails contains information about the sender.
 
    They are wrapped in a VStack  that has also HStack for showing textes below
 
    Those more details are message information. They can be displayed in a drop down window.
 
    Click on the arrow to see all details.
 
 */

struct SenderDetails: View {
    @State private var moreDetails = false
    var body: some View {
        VStack (alignment: .leading, spacing: 20) {
                Text("Sender:")
                    .font(.subheadline)
                    .padding(.bottom, 5)
                HStack(alignment: .top) {
                    Text("More Details")
                        .foregroundColor(.blue)
                        .font(.system(size: 10))
                    Button (action: {
                        self.moreDetails.toggle()
                    }) {
                        Image(systemName: moreDetails ? "chevron.up" : "chevron.down")
                            .resizable()
                            .frame(width: 12.0, height: 7.0)
                    }
                }
                                
                    if moreDetails {
                        
                        HStack {
                            
                            Spacer()
                                .frame(width: 40)
                            
                            VStack(alignment: .leading){
                                Text("Last message received on:")
                                Text("Last message sent on:")
                                Text("Total number of messages sent:")
                                
                            } .font(.system(size: 10))
                            
                        }
                        
                    }
        }
  }
}

struct SenderDetails_Previews: PreviewProvider {
    static var previews: some View {
        SenderDetails()
    }
}
