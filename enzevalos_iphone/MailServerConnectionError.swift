//
//  MailServerConnectionError.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 21.01.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation

enum UserRecommandation: Int {
    case
    CheckConnectionInfo = 0,
    CheckPWAddr = 1,
    CheckIMAPServerConfig = 2,
    CheckSMTPServerConfig = 3,
    CheckSSLConfiguration = 4,
    CheckOAUTH = 5,
    
    Default = 100,
    Timeout = 200
}

// see: https://github.com/MailCore/mailcore2/blob/31e308a36108301e2f9cc5c4489bb81ff84e6d3a/src/objc/abstract/MCOConstants.h
enum MailServerConnectionError: Error {
    case NoError, ConnectionError, TLSNotAvailableError, ProtocolParseError, CertificateError, AuthenticationError,
    GmailIMAPNotEnabledError, GmailExceededBandwidthLimitError, GmailTooManySimultaneousConnectionsError, MobileMeMovedError,
    YahooUnavailableError, ImapIdleError, IdentityError, StartTLSNotAvailableError, AuthenticationRequiredError,
    SMTPInvalidAccountError, ServerDateError, UnspecifiedError, StorageLimitSMTPError, NoInternetconnection, NoData, SecurityIndicatorError,
    ImapSetupError, SmtpSetupError, TimeoutError
    
    var userRecommandations: UserRecommandation {
        get {
            switch self {
            case .NoError:
                return .Default
            case .AuthenticationError:
                return .CheckPWAddr
            case .ConnectionError, .NoData:
                return .CheckConnectionInfo
            case .TLSNotAvailableError, .CertificateError, .StartTLSNotAvailableError:
                return .CheckSSLConfiguration
            case .ProtocolParseError, .ImapIdleError, .GmailTooManySimultaneousConnectionsError, .GmailExceededBandwidthLimitError, .IdentityError, .ServerDateError, .NoInternetconnection:
                return .Default
            case .GmailIMAPNotEnabledError, .YahooUnavailableError:
                return .CheckOAUTH
            case .AuthenticationRequiredError, .SMTPInvalidAccountError:
                return .CheckPWAddr
            case .UnspecifiedError, .MobileMeMovedError, .StorageLimitSMTPError:
                return .Default
            case .SecurityIndicatorError :
                return .Default
            case .ImapSetupError :
                return .CheckIMAPServerConfig
            case .SmtpSetupError :
                return .CheckSMTPServerConfig
            case .TimeoutError :
                return .Timeout
            }
        }
    }
    
    var localizedUIBodyString: String {
        get{
            let recom : UserRecommandation = self.userRecommandations
            switch recom {
            case UserRecommandation.CheckPWAddr :
                return "MailServerError.Authentication.Body"
            case UserRecommandation.CheckOAUTH :
                return "MailServerError.OAUTH.Body"
            case UserRecommandation.CheckSSLConfiguration :
                return "MailServerError.Crypto.Body"
            case UserRecommandation.CheckIMAPServerConfig :
                return "MailServerError.IMAP.Body"
            case UserRecommandation.CheckSMTPServerConfig :
                return "MailServerError.SMTP.Body"
            case UserRecommandation.CheckConnectionInfo :
                return "MailServerError.Connection.Body"
            case UserRecommandation.Timeout :
                return "MailServerError.Timeout.Body"
            default:
                return "MailServerError.Default.Body"
            }
        }
    }
    
    static func findPrioError(errors: Set<MailServerConnectionError>) -> MailServerConnectionError? {
        if errors.contains(MailServerConnectionError.NoError) {
            return MailServerConnectionError.NoError
        }
        if errors.contains(MailServerConnectionError.NoInternetconnection) {
            return MailServerConnectionError.NoInternetconnection
        }
        if errors.contains(MailServerConnectionError.AuthenticationError) {
            return MailServerConnectionError.AuthenticationError
        }
        if errors.contains(MailServerConnectionError.NoData) {
            return MailServerConnectionError.NoData
        }
        return errors.first
    }
    
    static func wrongServerConfig(errors: Set<MailServerConnectionError>) -> Bool {
        if errors.isEmpty || errors.contains(MailServerConnectionError.NoError)  || errors.contains(MailServerConnectionError.AuthenticationError) ||  errors.contains(MailServerConnectionError.NoInternetconnection) {
            return false
        }
        return true
    }
    
    static func findErrorCode(error: Error) -> MailServerConnectionError {
        let text = error.localizedDescription.uppercased()
        let mscError: MailServerConnectionError
        switch text {
        case "A stable connection to the server could not be established.".uppercased():
            mscError = .ConnectionError
            break
        case "The server does not support TLS/SSL connections.".uppercased():
            mscError = .TLSNotAvailableError
            break
        case  "Unable to parse response from server.".uppercased():
            mscError = .ProtocolParseError
            break
        case "The certificate for this server is invalid.".uppercased():
            mscError = .CertificateError
            break
        case "Unable to authenticate with the current session's credentials.".uppercased():
            mscError = .AuthenticationError
            break
        case "IMAP is not enabled for this Gmail account.".uppercased():
            mscError = .GmailIMAPNotEnabledError
            break
        case "Yahoo!'s servers are currently unavailable.".uppercased():
            mscError = .YahooUnavailableError
            break
        case "The server does not support STARTTLS connections.".uppercased():
            mscError = .StartTLSNotAvailableError
            break
        case "The SMTP storage limit was hit while trying to send a large message.".uppercased():
            mscError = .StorageLimitSMTPError
            break
        case "Authentication is required for this SMTP server.".uppercased():
            mscError = .AuthenticationRequiredError
            break
        case "Account check failed because the account is invalid.".uppercased():
            mscError = .SMTPInvalidAccountError
            break
        case "An error occurred during an IDLE operation.".uppercased():
            mscError = .ImapIdleError
            break
        default:
            mscError = .UnspecifiedError
        }
        return mscError
    }
}






