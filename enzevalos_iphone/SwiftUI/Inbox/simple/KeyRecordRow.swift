//
//  KeyRecordRow.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 02.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct KeyRecordRow <M: DisplayMail, C: DisplayContact>: View {
    //TODO private let coord = AppDelegate.getAppDelegate().inboxCoordinator

    let keyrecord: C
    var first: M?
    var second: M?
    let name: String
    
   init(keyrecord: C) {
        self.keyrecord = keyrecord
       //TODO first = keyrecord.firstMail
        //TODO second = keyrecord.secondMail
        name = keyrecord.name
    }

    
    var body: some View {
        VStack {
            HStack(alignment: .lastTextBaseline) {
                icon
                Spacer()
                mailListView()
                .offset(y: -10)
            }
            HStack {
                nameView
                Spacer()
                moreMails
            }
        }
    }
    
    /*
     Displays the first (and second) mail
     */
    private func mailListView() -> AnyView {
        let spacing = CGFloat(10)
        
        if let first = self.first {
            if let second = self.second {
                return AnyView(VStack(alignment: .leading, spacing: spacing) {
                        MailView(mail: first)
                        Divider()
                        MailView(mail: second)
                        
                }
                .padding(.top, 10))
            } else {
                return AnyView(VStack(alignment: .leading,spacing: spacing) {
                    MailView(mail: first)
                    Divider()
                    Text(NSLocalizedString("NoFurtherMessages", comment: "No more Mails"))
                    
                }
                .padding(.top, 10))
            }
        } else {
            return AnyView(Text("NO MAILS..."))
        }
    }
    
    private var icon: some View{
        self.keyrecord.avatar
            .resizable()
            .frame(width: 60, height: 60)
            .shadow(radius: 5)
            .onTapGesture {
                /* TODO
                if let coord = KeyRecordRow.coord {
                    coord.pushRecordView(record: self.keyrecord)
                }*/
        }
    }
    
    private var nameView: some View {
        Text(name)
            .frame(maxWidth: 300, alignment: .leading)
            .lineLimit(1)
            .onTapGesture {
                /* TODO
                if let coord = KeyRecordRow.coord {
                    coord.pushRecordView(record: self.keyrecord)
                } */
        }
    }
    
    private var moreMails: some View {
        Button(action: {
            /* TODO
            if let coord = KeyRecordRow.coord {
                coord.pushMailListView(record: self.keyrecord)
            }*/
        }, label: {
            Text(NSLocalizedString("MailView.MoreMails", comment: "More mails"))
            .foregroundColor(.gray)
        })
    }
}
