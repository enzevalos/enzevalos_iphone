//
//  Logger.swift
//  enzevalos_iphone
//
//  Created by jakobsbode on 25.10.17.
//  Copyright © 2018 fu-berlin.
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

class LoggerDetail {

    static var logging = StudySettings.studyMode

    static let queue = DispatchQueue(label: "logging", qos: .background)

    static let defaultFileName = "log.json"
    static let loggingInterval = 21600 //60*60*6 seconds = 6 hours
    static let resendInterval = 5 * 60
    static let logReceiver = LOGGING_MAIL_ADR

    static var nextDeadline = (UserManager.loadUserValue(Attribute.nextDeadline) as? Date) ?? Date()

    static var studyID = StudySettings.studyID //identifies the participant in the study

    
    
    static fileprivate func sendCheck() {
       /*TODO if nextDeadline <= Date() && AppDelegate.getAppDelegate().currentReachabilityStatus != .notReachable && UserManager.loadUserValue(Attribute.accountname) != nil && UserDefaults.standard.bool(forKey: "launchedBefore"){
            //Do not send duplicate mails
            let tmpNextDeadline = Date(timeIntervalSinceNow: TimeInterval(resendInterval))
            nextDeadline = tmpNextDeadline
            UserManager.storeUserValue(nextDeadline as AnyObject?, attribute: Attribute.nextDeadline)
            sendLog()
        } */
    }

    static fileprivate func plainLogDict() -> [String: Any] {
        var fields: [String: Any] = [:]
        let now = Date()
        fields["timestamp"] = now.description
        fields["lang"] = Locale.current.languageCode
        return fields
    }

    static fileprivate func dictToJSON(fields: [String: Any]) -> String {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: fields)
            if let json = String(data: jsonData, encoding: String.Encoding.utf8) {
                return json
            }
            return ""
        } catch {
            return "{\"error\":\"json conversion failed\"}"
        }
    }

    static func log(setupStudy studypara: [StudyParameterProtocol.Type], alreadyRegistered: Bool) {
        if !logging {
            return
        }
        var event = plainLogDict()
        event["type"] = LoggingEventType.setupStudy.rawValue
        
        //event["language"] =
       
        for para in studypara{
            event[para.name] = para.load().value
        }
        event["alreadyRegistered"] = alreadyRegistered
        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }

    static func log(startApp onboarding: Bool) {
        if !logging {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.appStart.rawValue
        event["onboarding"] = onboarding
        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }

    static func log(terminateApp: Void) {
        if !logging {
            return
        }
        var event = plainLogDict()
        event["type"] = LoggingEventType.appTerminate.rawValue
        saveToDisk(json: dictToJSON(fields: event))
    }

    static func log(background goto: Bool) {
        if !logging {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.appBackground.rawValue
        event["going to"] = goto //true -> goto background; false -> comming from background
        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }

    static func log(onboardingState onboardingSection: String) {
        if !logging {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.onboardingState.rawValue
        event["onboardingSection"] = onboardingSection
        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }

    static func log(onboardingPageTransition from: Int, to: Int, onboardingSection: String) {
        if !logging {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.onboardingPageTransition.rawValue
        event["from"] = from
        event["to"] = to
        event["onboardingSection"] = onboardingSection
        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }
/*
    static func log(contactViewOpen keyRecord: KeyRecord?, otherRecords: [KeyRecord]?, isUser: Bool) {
        if !logging {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.contactViewOpen.rawValue

        if let keyRecord = keyRecord {
            if let keyID = keyRecord.keyID {
                event["keyID"] = resolve(keyID: keyID)
            } else {
                event["keyID"] = "nil"
            }
            event["mailaddresses"] = resolve(mailAddresses: keyRecord.addresses)
        }
        event["isUser"] = isUser
        if isUser {
            let (contact, mail) = GamificationData.sharedInstance.getSecureProgress()
            event["gamificationContact"] = contact
            event["gamificationMail"] = mail
        }
        event["numberOfOtherRecords"] = (otherRecords ?? []).count
        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }
 

  

    static func log(contactViewClose keyRecord: KeyRecord?, otherRecords: [KeyRecord]?, isUser: Bool) {
        if !logging {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.contactViewClose.rawValue

        if let keyRecord = keyRecord {
            if let keyID = keyRecord.keyID {
                event["keyID"] = resolve(keyID: keyID)
            } else {
                event["keyID"] = "nil"
            }
            event["mailaddresses"] = resolve(mailAddresses: keyRecord.addresses)
        }
        event["isUser"] = isUser
        if isUser {
            let (contact, mail) = GamificationData.sharedInstance.getSecureProgress()
            event["gamificationContact"] = contact
            event["gamificationMail"] = mail
        }
        event["numberOfOtherRecords"] = (otherRecords ?? []).count
        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }
 */

    static func log(keyViewOpen keyID: String) {
        if !logging {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.keyViewOpen.rawValue
        event["keyID"] = resolve(keyID: keyID)

        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }

    static func log(keyViewClose keyID: String, secondsOpened: Int) {
        if !logging {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.keyViewClose.rawValue
        event["keyID"] = resolve(keyID: keyID)
        event["opened for seconds"] = secondsOpened

        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }

    static func log(exportKeyViewOpen view: Int) {
        if !logging {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.exportKeyViewOpen.rawValue
        event["view"] = view

        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }

    static func log(exportKeyViewClose view: Int) {
        if !logging {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.exportKeyViewClose.rawValue
        event["view"] = view

        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }

    static func log(exportKeyViewButton send: Bool) {
        if !logging {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.exportKeyViewClose.rawValue
        if send {
            event["case"] = "sendMail"
        } else {
            event["case"] = "deletePasscode"
        }

        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }




    static func log(readViewClose message: String, draft: Bool = false) {
        if !logging {
            return
        }

        var event = plainLogDict()

        event["type"] = LoggingEventType.readViewClose.rawValue
        event["messagePresented"] = message
        event["draft"] = draft

        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }

  
    static func log(bitcoinMail gotIt: Bool) {
        if !logging {
            return
        }

        var event = plainLogDict()

        event["type"] = LoggingEventType.gotBitcoinMail.rawValue

        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }

    static func log(backgroundFetch newMails: UInt32, duration: Double) {
        if !logging {
            return
        }
        
        var event = plainLogDict()
        event["type"] = LoggingEventType.backgroundFetch.rawValue
        event["newMails"] = Int(newMails)
        event["duration"] = duration
        
        saveToDisk(json: dictToJSON(fields: event))
    }
    
    static func log(verify keyID: String, open: Bool, success: Bool? = nil) {
        if !logging {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.pubKeyVerification.rawValue
        event["keyID"] = LoggerDetail.resolve(keyID: keyID)
        var stateString = "open"
        if !open {
            stateString = "close"
        }
        event["state"] = stateString
        if let success = success {
            event["success"] = success
        }

        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }

    /**
     - parameters:
        - nrOfTrays: Number of search results
        - category: 0 is sender, 1 is subject, 2 is message, 3 is everything
        - opened:   "mail" User opened a message,
                    "mailList" User opened the mail list,
                    "contact" User opened contact view,
                    "searchedInMailList" User searched in the ListView.
        - keyRecordMailList: Array of MailAddresses that identify the KeyRecord in which the user searched. Nil when not in MailListView.
     */
    static func log(search nrOfTrays: Int, category: Int, opened: String, keyRecordMailList: [AddressRecord]? = nil) {
        guard logging else {
            return
        }

        var event = plainLogDict()
        event["type"] = LoggingEventType.search.rawValue
        event["category"] = category
        event["nrOfTrays"] = nrOfTrays
        event["opened"] = opened
        event["keyRecordMailList"] = resolve(mailAddresses: keyRecordMailList ?? [])

        saveToDisk(json: dictToJSON(fields: event))
        sendCheck()
    }

    static fileprivate func extract(from mail: MailRecord, event: [String: Any]) -> [String: Any] {
        var event = event
        if let from = mail.fromAddress {
            event["from"] = LoggerDetail.resolve(mailAddress: from)
        }
        event["to"] = LoggerDetail.resolve(mailAddresses: mail.tos)
        event["cc"] = LoggerDetail.resolve(mailAddresses: mail.ccs)
        event["bcc"] = LoggerDetail.resolve(mailAddresses: mail.bccs)
        event["communicationState"] = LoggerDetail.communicationState(subject: mail.subject)
        event["specialMail"] = LoggerDetail.specialMail(subject: mail.subject)
        event["timeInHeader"] = mail.date.description
        event["bodyLength"] = mail.body.count
        event["encState"] = mail.encryptionState.rawValue
        event["decryptedBodyLength"] = (mail.body).count
        event["signatureState"] = mail.signatureState.rawValue
        event["x-Mailer"] = mail.xMailer
        //TODO:
        //event["signingKeyID"] = Logger.resolve(keyID: signingKeyID)
        //event["myKeyID"] = Logger.resolve(keyID: myKeyID)



        //event["secureAddresses"] = secureAddresses //could mean the addresses, in this mail we have a key for
        //event["encryptedForKeyIDs"] = Logger.resolve(keyIDs: encryptedForKeyIDs)

        // event["trouble"] = mail.trouble
       // event["folder"] = LoggerDetail.resolve(folder: mail.folder)

        return event
    }

    static func communicationState(subject: String) -> String {
        if subject == "" {
            return ""
        }
        var oldSubject = subject
        var newSubject = ""
        var hasPrefix = true

        while hasPrefix {
            if oldSubject.hasPrefix("Re: ") || oldSubject.hasPrefix("RE: ") || oldSubject.hasPrefix("re: ") || oldSubject.hasPrefix("AW: ") || oldSubject.hasPrefix("Aw: ") || oldSubject.hasPrefix("aw: ") {
                newSubject += "Re: "
                oldSubject = String(oldSubject[oldSubject.index(oldSubject.startIndex, offsetBy: 4)...]) //damn swift3!
            } else if oldSubject.hasPrefix("Fwd: ") || oldSubject.hasPrefix("FWD: ") || oldSubject.hasPrefix("fwd: ") {
                newSubject += "Fwd: "
                oldSubject = String(oldSubject[oldSubject.index(oldSubject.startIndex, offsetBy: 5)...])
            } else if oldSubject.hasPrefix("WG: ") || oldSubject.hasPrefix("Wg: ") || oldSubject.hasPrefix("wg: ") {
                newSubject += "Fwd: "
                oldSubject = String(oldSubject[oldSubject.index(oldSubject.startIndex, offsetBy: 4)...])
            } else {
                hasPrefix = false
            }
        }

        return newSubject
    }

    static func specialMail(subject: String) -> String {
        if subject.contains(NSLocalizedString("inviteSubject", comment: "subject of invitation email")) {
            return "invitation"
        }
        return ""
    }

    //takes backendFolderPath
    static func resolve(folder: FolderRecord) -> String {
        let folderPath = folder.path
        if folderPath == UserManager.backendSentFolderPath {
            return "sent"
        }
        if folderPath == UserManager.backendDraftFolderPath {
            return "draft"
        }
        if folderPath == UserManager.backendInboxFolderPath {
            return "inbox"
        }
        if folderPath == UserManager.backendTrashFolderPath {
            return "trash"
        }
        if folderPath == UserManager.backendArchiveFolderPath {
            return "archive"
        }
        return folder.path ?? ""//TODO
    }

    //get an pseudonym for a mailAddress
    static func resolve(mailAddress: AddressRecord) -> String {
        if mailAddress.isUser {
                return "self"
        }
        return mailAddress.email//TODO .pseudonym
    }


    static func resolve(mailAddresses: NSSet) -> [String] {
        var result: [String] = []
        for addr in mailAddresses {
            if let addr = addr as? AddressRecord {
                result.append(resolve(mailAddress: addr))
            } else {
                result.append("unknownMailAddressType")
            }
        }
        return result
    }

    static func resolve(mailAddresses: [AddressRecord]) -> [String] {
        var result: [String] = []
        for addr in mailAddresses {
            result.append(resolve(mailAddress: addr))
        }
        return result
    }


    //get an pseudonym for a keyID
    static func resolve(keyID: String) -> String {
        /*if let key = DataHandler.handler.findKey(keyID: keyID) {
            return key.pseudonym
        }*/
        return "noKeyID"
    }

    static func resolve(key: PublicKeyRecord) -> String {
        return key.fingerprint ?? "" //TODO
    }

    static func resolve(keyIDs: [String]) -> [String] {
        var result: [String] = []
        for id in keyIDs {
            result.append(resolve(keyID: id))
        }
        return result
    }

    static func saveToDisk(json: String, fileName: String = defaultFileName) {

        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {

            let fileURL = dir.appendingPathComponent(fileName)

            if FileManager.default.fileExists(atPath: fileURL.path) {
                // append
                do {
                    let fileHandle = try FileHandle(forUpdating: fileURL)

                    fileHandle.seekToEndOfFile()
                    if let encoded = "\n\(json),".data(using: .utf8) {
                        fileHandle.write(encoded)
                    }
                    fileHandle.closeFile()
                }
                catch {
                    print("Error while appending to logfile: \(error.localizedDescription)")
                }
            } else {
                // write new
                do {
                    try json.write(to: fileURL, atomically: false, encoding: .utf8)
                }
                catch {
                    print("Error while writing logfile: \(error.localizedDescription)")
                }
            }

        }
    }

    static func sendLog(fileName: String = defaultFileName, logMailAddress: String = logReceiver) {

        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {

            let fileURL = dir.appendingPathComponent(fileName)

            // reading
            do {
                let currentContent = try String(contentsOf: fileURL, encoding: .utf8)
                if !currentContent.isEmpty {
                   //TODO  AppDelegate.getAppDelegate().mailHandler.send([logMailAddress], ccEntrys: [], bccEntrys: [], subject: "[Enzevalos] Log", message: "{\"studyID\":\"" + studyID + "\",\"data\":" + "[" + currentContent.dropLast() + "\n]" + "}", callback: sendCallback, loggingMail: true, uiState: .letter)
                }
            }
            catch {
                print("Error while reading logfile: \(error.localizedDescription)")
            }
        }
    }

    static func sendCallback(error: Error?) {
        guard error == nil else {
            print(error!)
            return
        }

        clearLog()
        let tmpNextDeadline = Date(timeIntervalSinceNow: TimeInterval(loggingInterval))
        nextDeadline = tmpNextDeadline
        UserManager.storeUserValue(nextDeadline as AnyObject?, attribute: Attribute.nextDeadline)
    }

    static func clearLog(fileName: String = defaultFileName) {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {

            let fileURL = dir.appendingPathComponent(fileName)

            do {
                try String().write(to: fileURL, atomically: false, encoding: .utf8)
            }
            catch {
                print("Error while clearing logfile: \(error.localizedDescription)")
            }
        }
    }
}
