//
//  AuthenticationView.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 17.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct AuthenticationView: View{
    
    @State private var loggingIn:Bool = false
    let imageSize : CGFloat = 35
    var onBack: ()->Void
    var onClick: ()->Void
    
    @State private var login: String = ""
    @State private var password: String = ""
    @State private var username: String = ""
    @State private var imapServer: String = ""
    @State private var imapPort: String = ""
    @State private var smtpServer: String = ""
    @State private var smtpPort: String = ""
    @State private var imapEncryption = 0
    @State private var smtpEncryption = 0
    @State private var smtpOrImap:Bool = false

    var encryptionOptions = ["Plaintext", "StartTLS", "TLS/SSL"]
    
    //@EnvironmentObject var viewModel:AuthenticationViewModel
    @ObservedObject private var viewModel = AuthenticationViewModel(authenticationModel: AuthenticationModel.instance)
    
    @ObservedObject private var keyboard = KeyboardResponder()
    
    //whether the next-button is enabled (correct email format and all necessary fields used)
    var buttonEnabled: Bool {
        let advancedCorrect = !imapServer.isEmpty && !imapPort.isEmpty && !smtpServer.isEmpty && !smtpPort.isEmpty
        let regex = try! NSRegularExpression(pattern: """
            (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])
            """)
        let emailCorrectFormat = regex.firstMatch(in: login, options: [], range: NSRange(location: 0, length: login.utf16.count)) != nil
        return (!login.isEmpty && !password.isEmpty && emailCorrectFormat)&&(!viewModel.isDetailedAuthentication || advancedCorrect)
    }
    
  
    
    var body: some View {
        VStack{
            if self.viewModel.errorMessage != nil && !self.viewModel.errorMessage!.isEmpty {
                VStack{
                Text(self.viewModel.errorMessage!)
                    .foregroundColor(Color.white)
                    
                }.padding(.all, 20)
                .clipShape(RoundedRectangle(cornerRadius: 8))
                .background(Color.red)
            }
            LoadingView(isShowing: self.$viewModel.showProgressSpinner) {
                VStack{
                    LoginCredentialSection(email: self.$login, password: self.$password)
                    LoginAdvancedSection(username: self.$username, imapServer: self.$imapServer, imapPort: self.$imapPort, smtpServer: self.$smtpServer, smtpPort: self.$smtpPort, imapEncryption: self.$imapEncryption, smtpEncryption: self.$smtpEncryption, isDetailed: self.$viewModel.isDetailedAuthentication, encryptionOptions: self.encryptionOptions).padding([ .leading, .trailing],20)
                    HStack{
                        Button(action: self.onBack){
                            HStack(alignment: .center){
                                Image(systemName: "arrow.down.circle")
                                .resizable()
                                    .frame(width: self.imageSize, height: self.imageSize)
                                Text("Back")
                            }
                        }
                        Spacer()
                        Button(action: {self.viewModel.isDetailedAuthentication ?
                            self.viewModel.detailValidation(self.login, self.password, self.username, self.imapServer, self.imapPort, self.imapEncryption, self.smtpServer, self.smtpPort, self.smtpEncryption) :
                            self.viewModel.validate(self.login, self.password)
                        }){
                            HStack(alignment: .center){
                                Text("Next")
                                Image(systemName: "arrow.right.circle")
                                .resizable()
                                    .frame(width: self.imageSize, height: self.imageSize)
                            }
                        }
                        .disabled(!self.buttonEnabled)
                        .opacity(self.buttonEnabled ? 1 : 0.5 )
                    }
                    .padding(20)
                    .foregroundColor(.white)
                    
                }.padding()
                    .padding(.bottom, self.keyboard.currentHeight)
                .edgesIgnoringSafeArea(.bottom)
                .animation(.easeOut(duration: 0.16))
            }
        }
    }
}



struct LoginCredentialSection: View{
@Binding var email: String
@Binding var password: String
@State private var secured: Bool = true

var bigspace:CGFloat = 25
var smallspace:CGFloat = 1

var body: some View {
        VStack {
            Text(NSLocalizedString("Onboarding.loginDescription", comment: ""))
                .fontWeight(.heavy)
                .multilineTextAlignment(.center)
                .padding(.bottom, bigspace)
                
            Text(NSLocalizedString("Onboarding.enterEmail", comment: ""))
                .padding(.bottom, smallspace)
        
            TextField("maxmustermann@icloud.com", text: $email)
                .padding(EdgeInsets(top: 8, leading:10, bottom: 8, trailing: 10))
                .background(Color.white)
                .foregroundColor(Color.black)
                .clipShape(RoundedRectangle(cornerRadius: 8))
                .shadow(radius: 8)
                .padding(.bottom, bigspace)
            
            Text(NSLocalizedString("Onboarding.enterPassword", comment: ""))
                .padding(.bottom, smallspace)
            // PW
            ZStack (alignment: .trailing) {
                if secured {
                    SecureField(NSLocalizedString("Your password", comment: ""), text: $password)
                        .padding(EdgeInsets(top: 8, leading:10, bottom: 8, trailing: 10))
                        .background(Color.white)
                        .foregroundColor(Color.black)
                        .clipShape(RoundedRectangle(cornerRadius: 8))
                        .shadow(radius: 8)
                        .padding(.bottom, bigspace)
                        
                } else {
                    TextField(NSLocalizedString("Your password", comment: ""), text: $password)
                    .padding(EdgeInsets(top: 8, leading:10, bottom: 8, trailing: 10))
                    .background(Color.white)
                    .foregroundColor(Color.black)
                    .clipShape(RoundedRectangle(cornerRadius: 8))
                    .shadow(radius: 8)
                    .padding(.bottom, bigspace)
                    
                }
            Button(action: {
                self.secured.toggle()
            }){
                if secured {
                    Image(systemName: "eye.slash")
                } else {
                    Image(systemName: "eye")
                }
            }.foregroundColor(Color.primary)
                .background(Color.white)
                .padding([.bottom, .trailing] , 25)
        }
    }
.padding()
.background(Color.blue)
    }
}



struct LoginAdvancedSection: View{
    @Binding  var username: String
    @Binding  var imapServer: String
    @Binding  var imapPort: String
    @Binding  var smtpServer: String
    @Binding  var smtpPort: String
    @Binding  var imapEncryption: Int
    @Binding  var smtpEncryption :Int
    
    @State var smtpOrImap:Bool = false
    @Binding var isDetailed: Bool
    
    var encryptionOptions:[String]
    
    var userAdvOpt: some View{
        HStack {
            Text(NSLocalizedString("Username", comment: ""))
            TextField(NSLocalizedString("Username", comment: ""), text: $username).foregroundColor(.black)
        }
    }
    
    func imapOrSmtpSection(isSmtp s :Bool)->some View{
        VStack{
            Text(s ? "SMTP" : "IMAP").font(.headline)
            HStack {
                Text("Server:")
                TextField(s ?
                    NSLocalizedString("exampleSMTPserver", comment: "e.g. smtp.web.de") :
                    NSLocalizedString("exampleIMAPserver", comment: "e.g. imap.web.de")
                    , text: s ? $smtpServer : $imapServer).foregroundColor(.accentColor)
            }
            HStack {
                Text("Port:")
                TextField(s ?
                    NSLocalizedString("exampleSMTPport", comment: "e.g. 559") :
                    NSLocalizedString("exampleIMAPport", comment: "e.g. 883")
                    , text: s ? $smtpPort : $imapPort).keyboardType(.numberPad).foregroundColor(.accentColor)
            }
            Text(NSLocalizedString("Transportencryption", comment: "")).padding(.top , 20)
            
            Picker(selection: s ? $smtpEncryption : $imapEncryption , label: Text(s ? "SMTP-Transportencryption" : "IMAP-Transportencryption")) {
                ForEach(0..<encryptionOptions.count) { i in
                    Text(self.encryptionOptions[i]).tag(i)
                        .onTapGesture {
                            if !s {
                                self.imapEncryption = i
                            } else {
                                self.smtpEncryption = i
                            }
                    }
                }
            }
            .pickerStyle(SegmentedPickerStyle())
            .accentColor(.accentColor)
        }
    }
    
    
    var imapSmtpSwitch : some View{
        Button(action: {self.smtpOrImap.toggle()}) {
            HStack{
                //either smtp or imap section is hidden; this button opens the other one
                if(self.smtpOrImap){
                    Text("IMAP")
                    Image(systemName: "chevron.up")
                }else{
                    Text("SMTP")
                    Image(systemName: "chevron.down")
                }
            }.foregroundColor(.accentColor)
            .font(.headline)
        }
        .padding(.all , 10)
    }
    
    var advancedSectionSwitch: some View{
        Button(action: {self.isDetailed.toggle()}) {
            HStack{
                if(!self.isDetailed){
                    Text(NSLocalizedString("Advanced options", comment: ""))
                    Image(systemName: "chevron.down")
                }else{
                    Text(NSLocalizedString("Hide advanced options", comment: ""))
                    Image(systemName: "chevron.up")
                }
            }.font(.headline)
        }
    }
    
    
    var body: some View {
        VStack{
            advancedSectionSwitch
            if self.isDetailed{
                VStack{
                    userAdvOpt
                    imapOrSmtpSection(isSmtp: false).rotation3DEffect(.degrees(self.smtpOrImap ? 90 : 0), axis: (x: 1, y: 0, z: 0), anchor: .bottom).frame(height: !self.smtpOrImap ? 200 : 0)
                    imapSmtpSwitch
                    imapOrSmtpSection(isSmtp: true).rotation3DEffect(.degrees(!self.smtpOrImap ? -90 : 0), axis: (x: 1, y: 0, z: 0), anchor: .top).frame(height: self.smtpOrImap ? 200 : 0)
                }
                .padding(20)
                .background(Color.white)
                .foregroundColor(.black)
                .clipShape(RoundedRectangle(cornerRadius: 8))
                .shadow(radius: 8)
            }
        }
    }
    
}


#if DEBUG
struct AuthenticationView_Previews: PreviewProvider {
   static var previews: some View {
      Group {
        AuthenticationView(onBack: {}, onClick: {})
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("iPhone SE")

         AuthenticationView(onBack: {}, onClick: {})
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("iPhone XS Max")
      }
   }
}
#endif
