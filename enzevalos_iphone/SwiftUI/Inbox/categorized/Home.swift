//
//  Home.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 17.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI
import TagNavVi

struct InboxHome: View {
    
    
    //Put the categories here
    fileprivate var categories: CategoryCollection {
        CategoryCollection(
            UnsortedCategory(showAllMails: controller.displayClassicalInbox), 
            ChatCategory,
            FilesCategory,
            "swang"                 //objekte, die keine CategoryViews sind, werden ignoriert
            //,SampleCategoryV2()     //Other Categories must conform to CategoryV
        )
    }
    
    
    
    
    
    //MARK: - HomeView
    init(folderpath:String=MailHandler.INBOX,controller:HomeModel) {
        controller.updateFolder(folderpath)
        self.controller=controller
        
        _ = categories //initialize all Categories
    }
    
    
    
    
    
    @ObservedObject var controller : HomeModel
    
    private let overviewBar_iconsPerRow:Int = 5
    
    
    
    @State var isNavigationBarHidden: Bool = true
    
    
    var body: some View {
        NavigationView{
            VStack{
                #if DEBUG && false
                Button(action: {PersistentDataProvider.dataProvider.resetCategories()}, label: {Text("reset")})
                #endif
                topSection
                if controller.isHome{ whenHome }else{ whenNotHome }
            }//.animation(.easeInOut)
            .navigationBarTitle(NSLocalizedString("Home", comment: ""))
            .navigationBarHidden(self.isNavigationBarHidden)
            .onAppear {
                self.isNavigationBarHidden = true
            }
        }
    }
    @ViewBuilder private var topSection : some View {
        VStack{
            HStack{
                backButton
                    .padding(.leading)
                CustomizableSearchView(search: $controller.search)
                
                    #if DEBUG && false
                    Button(action: {PersistentDataProvider.dataProvider.resetCategories()}, label: {Text("reset")})
                    #endif
                
            } .padding(6)
            HStack{
                if !controller.search.isExpanded{
                    CategoryOverviewBar(controller: controller)
                        //.padding(.top, 10)
                    if controller.isHome {settingsButton}
                }
            }
            if controller.isHome { settings.padding(.horizontal) }
        }
    }
    
    @ViewBuilder private var whenHome : some View {
        ScrollView(){
            ForEach( controller.allCatsSorted ) { cat in
                viewFromID(cat.id)
            }
        }
    }
    
    @ViewBuilder private var whenNotHome : some View {
        viewFromID(controller.activeCategory?.id ?? "¿")
    }
    
    @ViewBuilder private func viewFromID(_ id : CategoryIDType)-> some View{
        if let cate = categories[id] {cate}
    }
    
    @ViewBuilder private var backButton: some View{
        if !controller.isHome{
            Button(action: controller.goBack){
                Image(systemName: "house.fill")
                    .sizeTo()
            }
        }
    }
    
    @ViewBuilder private var settingsButton : some View {
        Button(action: {controller.settingsEnabled.toggle()}){
            Image(systemName: controller.settingsEnabled ? "gearshape" : "gearshape.fill")
        }.foregroundColor(.gray).padding(.horizontal).padding(.trailing)
    }
    
    @ViewBuilder private var settings : some View {
        if controller.settingsEnabled{
            VStack{
                
                HStack{
                    Text(NSLocalizedString("Sort By", comment: ""))
                    Spacer()
                    Picker( selection: $controller.sortBy, label: Text(NSLocalizedString("Sort By", comment: ""))){
                        ForEach(SortCategoriesBy.allCases) { sort in
                            return Text(sort.name).tag(sort)
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                
              
                Toggle(isOn: $controller.displayClassicalInbox, label: {
                    Text(NSLocalizedString("Classic Inbox", comment: ""))
                })
                
            }.padding()
        }
    }
}



// MARK: - helper

fileprivate class CategoryCollection : ObservableObject , ExpressibleByArrayLiteral , ExpressibleByDictionaryLiteral {
    typealias DType = [CategoryIDType:CategoryView<AnyView>]
    private var dict = DType()
    
    typealias Key = DType.Key
    typealias Value = DType.Value
    typealias ArrayLiteralElement = DType.Value
    
    required init(arrayLiteral elements: ArrayLiteralElement...) {
        for cat in elements {dict[cat.id]=cat}
    }
    
    required init(dictionaryLiteral elements: (Key, Value)...) {
        for (key, val) in elements {dict[key]=val}
    }
    
    convenience init(arrayLiteral elements: Any...) {
        self.init(array: elements)
    }
    convenience init(_ elements: Any...) {
        self.init(array: elements)
    }
    
    private init(array elems:[Any]){
        for elem in elems {
            if let cat = (elem as? CategoryViewToAnyView)?.toAnyView() {
                dict[cat.id]=cat
            }else if let cat = (elem as? CategoryV){
                dict[cat.name] =
                    CategoryView(
                        name: cat.name,
                        description: cat.description,
                        icon: cat.icon,
                        isBigger: cat.isBigger,
                        isResizable: cat.isResizable,
                        searchFields: cat.searchFields,
                        filter: cat.filter, andPredicate:
                        cat.andPredicate,
                    cat.body)
                
            }
        }
    }
    
    subscript(index: Key)->Value?{
        get {
            return dict[index]
        }
        set(val){
            dict[index]=val
        }
    }
    
}


extension CategoryCollection : Collection {
    typealias Index = DType.Index
    typealias Element = DType.Element
    var startIndex: Index { return dict.startIndex}
    var endIndex: Index { return dict.endIndex }
    subscript(index: Index) -> Iterator.Element {
        get { return dict[index] }
    }
    func index(after i: Index) -> Index {
        return dict.index(after: i)
    }
}

