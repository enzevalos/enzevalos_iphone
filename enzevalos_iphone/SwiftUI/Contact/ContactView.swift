//
//  ContactView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.10.20.
//  Modified by Amir Rajabi and Goekhan Guemues on 25.03.21
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

// TODO: Add contact book, related addresses, related keys, related mails.

/// A model of all published properties used in ContactView
class ContactProperties: ObservableObject {
    @Published var emailAddress: String = ""
    @Published var showKeysPopup = false
    @Published var composeMail = false
}

/// A contact view with all associated email addresses and also some emails received from or sent to  this  contact.
/// - Parameter contact: the contact that is being viewed
struct ContactView <C: DisplayContact>: View {
    public var contact: C
    @StateObject var properties = ContactProperties()
    
    /// Creates the view body
    var body: some View {
        ZStack {
            VStack {
                HStack {
                    avatar
                    VStack (alignment: .leading) {
                        contactName.padding(.bottom)
                        knownSince
                    }.padding(.leading)
                    Spacer()
                }.padding(.leading, 15)
                ScrollView {
                    
                    // A list of all email addresses associated to the contact
                    newHeader(header: NSLocalizedString("Contact.Email.Header",
                                                        comment: "Header for the section"))
                    listContactAddresses
                    
                    // Up to three emails received from this contact
                    newHeader(header: NSLocalizedString("Contact.Received.Header",
                                                        comment: "Header for the section"))
                    ContactMailListView(folderpath: MailHandler.INBOX,
                                        folderName: NSLocalizedString("Inbox", comment: "Inbox"),
                                        emailAddresses: contact.emails)
                    
                    // Up to three emails sent to this contact
                    newHeader(header: NSLocalizedString("Contact.Sent.Header",
                                                        comment: "Header for the section"))
                    ContactMailListView(folderpath: MailHandler.SENT,
                                        folderName: NSLocalizedString("Sent", comment: "Sent"),
                                        emailAddresses: contact.emails).padding(.bottom, 25)
                    
                    // TODO: Add a section here with all keys of this contact
                }
                .padding(.horizontal,5)
                
            }.padding(.top, 20)
            .blur(radius: properties.showKeysPopup ? 2 : 0)
            
            // Toggles the popup screen of all keys
            if properties.showKeysPopup {
                CryptoKeysPopup
            }
            
        }.background(Color(.systemGroupedBackground))
        .navigationBarItems(trailing: editButton)
        .sheet(isPresented: $properties.composeMail) {
            ComposeView(preData: PreMailData(body: "",
                                             subject: "",
                                             to: [properties.emailAddress],
                                             cc: [],
                                             bcc: []))
        }
    }
    
    
    // TODO: Add an action for the Edit button
    private var editButton: some View {
        Button {} label: {
            Text(NSLocalizedString("edit", comment: "edit")).font(.body)
        }
    }
    
    /// Styling the contact's avatar.
    private var avatar: some View {
        contact.avatar
            .resizable()
            .frame(width: 110, height: 110, alignment: .center)
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.white, lineWidth: 4))
            .shadow(radius: 10)
            .padding(.bottom)
    }
    
    /// Styling the contact's display name.
    private var contactName: some View {
        // TODO: Show proper name.
        // Currently for debugging purposes we show the email before "@" as name
        Text(contact.name.components(separatedBy: "@")[0])
            .font(.title)
            .fixedSize(horizontal: false, vertical: true)
    }
    
    /// Shows how long  the contact is known.
    /// If it is unknown, display an 'Add to contact book' button.
    var knownSince: some View {
        VStack (alignment: .leading){
            
            if let discovery = contact.primaryKey?.discovery {
                row(label: "ContactView.KnownSince", data: discovery.dateToString)
                // TODO Add discovery mail?
            } else if let discovery = contact.discovery {
                row(label: "ContactView.KnownSince", data: discovery.dateToString)
            } else {
                row(label: "ContactView.KnownSince.Unknown", data:nil)
                // TODO: 'Add to contact book' button here
            }
        }
    }
    
    /// Creates  a frame which contains all email addresses of the contact
    var listContactAddresses: some View {
        ZStack{
            newRoundedRectangle()
            VStack {
                ForEach(contact.emails, id: \.self){ eachAddress in
                    
                    // This row shows the email address and the label
                    HStack {
                        
                        // When an address is clicked, the ComposeView is called
                        Button {
                            properties.emailAddress = eachAddress
                            
                            properties.composeMail = true
                        } label: {
                            Text("\(eachAddress)")
                                .fontWeight(.light)
                                .font(.body)
                                .foregroundColor(.blue)
                                .truncationMode(.middle)
                                .frame(maxWidth: .infinity,
                                       maxHeight: .infinity,
                                       alignment: .leading)
                            emailAddressLabel
                        }
                    }
                    
                    // This row shows the security rating and a button do display the keys
                    HStack{
                        rating
                        if contact.primaryKey != nil {
                            showKeyBotton(addr: eachAddress)
                       }
                    }.padding(.vertical,3)

                    if eachAddress != contact.emails.last{
                        Divider().padding(.top,1)
                    }
                    
                }.padding(.horizontal,10)
                
            }.padding(5)
            
        }.padding(.horizontal,5)
    }
        
    private func showKeyBotton (addr: String) ->  some View {
            Button {
                withAnimation{
                    properties.emailAddress = addr
                    properties.showKeysPopup.toggle()
                }
                
            } label: {
                ZStack {
                    Capsule()
                        .frame(height: 25, alignment: .center)
                        .shadow(color:.gray, radius:2)
                        .foregroundColor(contact.addressRating(contact).color)
                    HStack{
                        Text(Image(systemName: "key.fill"))
                            .rotationEffect(Angle(degrees: 30))
                        
                        Text(NSLocalizedString("Contact.Key.Description", comment: "Contact.Key.Description"))
                            .fontWeight(.medium)
                        
                    }.foregroundColor(.white)
                    .font(.footnote)
                }
            }
        }
    
    /// Creates a frame for the address label.
    /// TODO: Add an addressLabel property  to the DisplayContact.
    var emailAddressLabel: some View {
        Text(NSLocalizedString("Email.Label", comment: "Email Label"))
            .font(.caption)
            .fontWeight(.light)
            .foregroundColor(Color(.systemGray))
            .frame(maxWidth: 50 ,
                   maxHeight: 20,
                   alignment: .bottomLeading)
    }
    
    /// Shows a list of all keys associated to the email address in a popup screen and blurs the background.
    var CryptoKeysPopup : some View {
        GeometryReader{ geometry in
            HStack{
                if keyID == nil {
                    Text(NSLocalizedString("Contact.Key.NotAvailable", comment: "Key not available message"))
                    
                } else {
                    // TODO: keyID should be an array of keys
                    VStack (alignment: .leading){
                        Text(NSLocalizedString("Contact.Key.Available", comment: "Key available message")).padding(.bottom, 3)
                        Text("   \u{2022} \(keyID!)")
                    }
                }
            }
            .padding()
            .background(Color(.tertiarySystemBackground))
            .cornerRadius(15)
            .frame(maxWidth: geometry.size.width,
                   maxHeight: geometry.size.height,
                   alignment: .center)
            
        }
        .padding(.horizontal,15)
        .background(
            Color.black.opacity(0.3)
                .edgesIgnoringSafeArea(.all)
                .onTapGesture {
                    properties.showKeysPopup.toggle()
                    properties.emailAddress = ""
                }
        )
        
    }
    
    // TODO: Change keyID to an array of keys
    private var keyID: String? {
        return contact.primaryKey?.keyID
    }
    
    /// Creates the security rating label.
    private var rating: some View {
        HStack{
            Text(Image(systemName: contact.addressRating(contact).shield))
                .font(.footnote)
            
            Text(contact.addressRating(contact).name)
                .fontWeight(.medium)
                .font(.footnote)
            
        }
        .foregroundColor(contact.addressRating(contact).color)
        .frame(maxWidth:.infinity,
               maxHeight: .infinity,
               alignment: .leading)
    }
    
    /// Creates the text for the 'Known since' property.
    private func row(label: LocalizedStringKey, data: String?) -> some View {
        HStack {
            Text(label)
            
            if let data = data{
                Text(data)
            }
            
        }
        .font(.custom("SF Pro", size: 12, relativeTo: .caption))
        .foregroundColor(.gray)
    }
}

/// Creates a header for each section.
struct newHeader: View {
    var header: String
    var body: some View {
        HStack {
            Text(header)
                .font(.footnote)
                .foregroundColor(.secondary)
                .padding(.horizontal)
                .padding(.top, 20)
                .textCase(.uppercase)
            Spacer()
        }
    }
}

/// Creates a rounded rectangle in which the emails are displayed
struct newRoundedRectangle: View {
    var body: some View {
        RoundedRectangle(cornerRadius: 20)
            .shadow(color:.gray, radius:2)
            .background(Color(.systemGroupedBackground))
            .foregroundColor(Color(.tertiarySystemBackground))
    }
}

struct ContactView_Previews: PreviewProvider {
    static var previews: some View {
        ContactView<ProxyContact>(contact: ProxyData.Alice)
        ContactView<ProxyContact>(contact: ProxyData.Alice)
            .colorScheme(.dark)
        ContactView<ProxyContact>(contact: ProxyData.Bob)
        ContactView<ProxyContact>(contact: ProxyData.Bob)
            .colorScheme(.dark)
        ContactView<ProxyContact>(contact: ProxyData.Charlie)
    }
}
