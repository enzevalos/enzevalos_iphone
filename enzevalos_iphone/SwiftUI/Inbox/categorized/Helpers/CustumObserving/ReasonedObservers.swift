//
//  ReasonedObservers.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 12.02.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import Foundation
import Combine

typealias ReasonedObservableObject = ReasonedObservableObject_ & ObservableObject

class ReasonedObservableObject_ : CustomObservableObject<PublishedReason> {}

enum PublishedReason : Equatable {
    case
        silent,
        normal,
        urgent,
        custom(_ reason : String)
}

//like @Published but with an additional reason (used to not update things not needing an update, while other subscribe to the same object need to update)
@propertyWrapper class PPublished<Wrapped> : CPublished<Wrapped,PublishedReason>{
    //why is the compiling not checking the Inheritance  :/
    override var wrappedValue: Wrapped {
        get     { super.wrappedValue }
        set(v)  { super.wrappedValue = v }
    }
}

@propertyWrapper class ReasonedObservedObject<Wrapped : ReasonedObservableObject> : CustomObservedObject<Wrapped> {
    //same here
    override var wrappedValue: Wrapped {
        get     { super.wrappedValue }
        set(v)  { super.wrappedValue = v }
    }
}
