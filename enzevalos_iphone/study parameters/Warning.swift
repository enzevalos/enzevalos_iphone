//
//  Warning.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 28.01.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation

enum Warning: Int, StudyParameterProtocol {
    case hide = 0, show = 1
    
    static var defaultValue: StudyParameterProtocol {
        get {
            return Warning.hide
        }
    }
    
    static func findParameter(rawvalue: Int) -> StudyParameterProtocol {
        switch rawvalue {
        case Warning.hide.rawValue:
            return Warning.hide
        case Warning.hide.rawValue:
            return Warning.show
        default:
            return defaultValue
        }
    }
    
    static var name: String {
        get {
            return "warning"
        }
    }
    
    static var keyName: String {
        get{
            return "warning"
        }
    }
    
    static var numberOfTreatments: UInt32 {
        get {
            return 2
        }
    }
    
    var value: Int {
        get {
            return self.rawValue
        }
    }
    
    func startStudy() {
        return
    }
    
    
}
