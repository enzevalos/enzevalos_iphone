//
//  HomeModel.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 17.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI
import Combine

class HomeModel : ObservableObject{
    
    
    
    ///how far its scrolled (currently not in use
    @Published var scrollAmount:CGFloat = 0
    
    ///whether the settings are opened
    @Published var settingsEnabled:Bool = false
    
    ///if the user whiches to show all mail in an old-fashioned inbox
    @Published var displayClassicalInbox : Bool = false
    
    /**
     Other API calls:
     - allCats : (better: allCatsSorted) all the categories used (their ViewModels to be precise)
     - activeCategory : the currently opened Category
     - search : the current search state including filters, searchtext etc
     - folderpath : the current folderpath (can be used to implement tags later on)
     - etc.
     */
    
    
    
    
    
    
    
    
    init() {
       _ = observe()
        let settings = persistent.getInboxSettings()
        self.displayClassicalInbox = settings.isClassicActive ?? false
        self.sortBy = settings.sortBy ?? .recent
    }
    
    
    //MARK: - persistent settings and publishing Coategory state
    //TODO this is not working yet
    
    
    deinit {saveSettings()}
    private var persistent = PersistentDataController()
    private func saveSettings(){
        persistent.inboxSettings = (self.displayClassicalInbox, self.sortBy)
    }
    
    private var cancellable:(AnyCancellable,AnyCancellable) = (AnyCancellable({}),AnyCancellable({}))
    func observe()->HomeModel{
        
        func update(_ reason : PublishedReason? ){
            if reason == .custom("For HomeModel") { self.objectWillChange.send() }
        }
        
        cancellable = ( vmProv.objectWillChangeR.sink{update($0)}  ,
                        vmProv.objectWillChange.sink{update(nil)} )
        return self
    }
    
    
    //MARK: - getting the categories

    @ObservedObject private var vmProv = CategoryViewModelProvider.s.observe()
    var allCategorieModels : ObservableArray<CategoryViewModel> {vmProv.viewModelList}
    
    var allCats : [CategoryViewModel] {
        return allCategorieModels.array
    }
    
    // MARK: - the Search state
    var search : Search{
        get{
            _search.fields=activeCategory?.constData.searchFields ?? SearchFilterCollections.Standarts
            return _search
        }
        set(newSearch){
            _search=newSearch
            updateSearch()
            objectWillChange.send()
        }
    }
    
    private var _search = Search(fields: SearchFilterCollections.Standarts)
    private func updateSearch(){
        allCats.forEach{category in
            category.updateSearch(self._search)
        }
    }
    
    // MARK: - current folder
    @Published var folderpath = MailHandler.INBOX
    func updateFolder(_ folderpath : String){
        self.folderpath=folderpath
        allCats.forEach{category in
            category.updateFolder(folderpath)
        }
    }
    
    //MARK: - the active Category
    
    ///nil if no category is maximized otherwise the maximized category
    var activeCategory : CategoryViewModel?{
        get{
            //okay this only takes <.2 milliseconds
            activeOne
        }
        set(newCat){
            setActiveCategory_(newCat)
            objectWillChange.send()
        }
    }
    func setActiveCategory(_ cat : CategoryViewModel){
        activeCategory = cat.isActive ? nil : cat
    }
    private func setActiveCategory_(_ newCat:CategoryViewModel?){
        allCats.forEach{category in
            category.isActive = false
        }
        newCat?.isActive = true
    }
    private var activeOne : CategoryViewModel?{
        for cat in allCats{
            if cat.isActive{
                return cat
            }
        }
        return nil
    }
    
    var isHome:Bool{activeCategory == nil}
    
    func goBack(){
        if isHome{
            scrollAmount = 0 //this sadly doesnt do anything
        }
        activeCategory = nil
    }
    
    
    //MARK: - Sorting the Categories
    
    @Published var sortBy : SortCategoriesBy = .recent
    
    var allCatsSorted : [CategoryViewModel] {
        return sortCategories(allCats)
    }
    func sortViews(_ views : [CategoryView<AnyView>])->[CategoryView<AnyView>]{
        views.sorted{catSorter($0.categoryVM, $1.categoryVM)}
    }
    func sortCategories(_ cats : [CategoryViewModel])->[CategoryViewModel]{
        cats.sorted(by: catSorter)
    }
    private func catSorter(_ lhs: CategoryViewModel, _ rhs: CategoryViewModel)->Bool{
        switch sortBy{
        case .non:
            return true
        case .recent:
            return (lhs.lastMailTimestamp ?? Date.distantPast) > (rhs.lastMailTimestamp ?? Date.distantPast)
            
        }
    }
    
}


enum SortCategoriesBy : Int,CaseIterable,Identifiable {
    
    case recent = 0, non = 1
    
    var name: String{
        get{
            switch self {
            case .recent:
                return NSLocalizedString("sort.recent", comment: "")
            case .non:
                return NSLocalizedString("sort.static", comment: "")
                
            }
        }
    }
    
    
    var id: Int {
        self.rawValue
    }
}
