//
//  AttachmentsViewMain.swift
//  enzevalos_iphone
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

/// a file that preview the attachments of incoming mails
// currently this view only previews a ProxyAttachment
import SwiftUI

struct AttachmentsViewMain: View {    
    
    // TODO: Refactor to model?

    //a list that contains all the attachment files
    @State var attachments: [DisplayAttachment]
    // Whether the Download all button was clicked
    @State var downloadAll = true;
    
    var body: some View {
        VStack{
            Spacer()
            // TODO: Add link section
            // default view if there are no attachments to be displayed
            if  attachments.count == 0{
                noAttachments
            }
            // otherwise preview the attachments
            else {
                attachmentsView
            }
        }
    }
    
    /// default view if there are no attachments to be displayed
    var noAttachments: some View {
        return Text(NSLocalizedString("ReadView.Attachments.No", comment: "")).font(.footnote).padding(30)
    }
    
    /// a view that previews all the attachment files
    var attachmentsView: some View {
        return VStack(alignment: .leading, spacing: 5){
            // headline
            HStack{
                Text(NSLocalizedString("ReadView.Attachments.Headline", comment: "attachments:"))
                    .font(.system(size:24)).fontWeight(.light)
                Spacer()
            }
            .padding(.horizontal , 15)
            .padding(3)
            // main attachment view
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 5) {
                    ForEach(0..<attachments.count) { i in
                        AttPrev(
                            attachment: self.attachments[i],
                            shouldBeDownloaded: self.downloadAll
                        )
                    }
                }
            }
        }
        .padding(.top, 30)
        .padding(.bottom,10)
    }
}

struct AttachmentsViewMain_Previews: PreviewProvider {
    static var previews: some View {
        // here we call the view with a single ProxyAttachment() for
        // demonstration purposes
        AttachmentsViewMain(attachments: [ProxyAttachment()])
    }
}


/// a view that previews one attachment file
struct AttPrev:View{
    
    // the attachment that is going to be previewed
    var attachment: DisplayAttachment
    // by default false
    var shouldBeDownloaded = false
    
    // whether this attachment is already downloaded to the documents directory
    @State var isDownloaded: Bool = false
    // used togehter with 'open' button to
    // open a fullScreenView of a file
    @State var isFullScreen: Bool = false
    
    /// a func to find the documents directory
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    /// a func that downloads the attachment into the documents directory
    func download() {
        let filename = getDocumentsDirectory().appendingPathComponent(self.attachment.myName)
        
        do {
            try self.attachment.myData.write(to: filename)
        } catch let error as NSError {
            print("Error: \(error)")
        }
        self.isDownloaded = true
    }
    
    /// a func that checks if an attachment is downloaded into the documents directory or not
    /// updates the 'isDownloaded' property accordingly
    func getDownloadedState(){
        let filename = getDocumentsDirectory().appendingPathComponent(self.attachment.myName)
        
        if FileManager.default.fileExists(atPath: filename.path){self.isDownloaded = true}
    }
    
    var body: some View{
        if shouldBeDownloaded {
            self.download()
        }
        
        // QuickLookView creates the preview of the file
        // it has to be defined here to update correctly
        let QLV = QuickLookView(name: self.attachment.myName, data: self.attachment.myData, shallDL: self.isDownloaded)
        
        return VStack{
            Group{
                    CardV(title: self.attachment.myName,
              
                          // these buttons are visable under the preview
                          // when users click on the preview
                          actions: self.isDownloaded||self.shouldBeDownloaded ?
                            // different combination of buttons
                            // according to downloading status
                            [
                                // anyViewDeleteButton
                                anyViewOpenButton,
                                anyViewShareButton
                            ]
                            :
                            [
                                anyViewDownloadButton,
                                // both lists need same length
                                // to make appearance the same
                                AnyView(EmptyView()),
                                AnyView(EmptyView())
                            ]
                            ){
                                QLV/*TODO:
                                    .allowsHitTesting(false).disabled(true)*/ //this should make the whole view scrollable averywhere not just on the title
                            }
    }
        // preview should resemble the look of A4 format
        .aspectRatio(100/141, contentMode: .fit)
        .frame(width: UIScreen.main.bounds.width*2/3)//, maxHeight: 500)
        
        //a hidden NavigationLink to open the preview in fullscreen
        // seems not to work currently
            NavigationLink(
            destination: QuickLookView(name: self.attachment.myName, data: self.attachment.myData)
                .navigationBarItems(trailing: anyViewShareButton), isActive: self.$isFullScreen) {Text("loi")}.hidden()}
            .onAppear(
                perform: {
                    self.getDownloadedState()
            
                    //download all functionality
                    if self.shouldBeDownloaded {self.isDownloaded=true; self.download()}
                }
            )
            // remove file from documents directory
            .onDisappear(
                perform: {
                    let filename = getDocumentsDirectory().appendingPathComponent(self.attachment.myName)
            
                        do {
                            try FileManager.default.removeItem(at: filename)
                        }
                        catch let error as NSError {print("Error: \(error)")}
                    self.isDownloaded = false
                    print("======== Attachment DISAPPEAR -> REMOVE FILES ========")
                }
            )
}

    // an open button converted into an AnyView
    // should open the view fullscreen
    var anyViewOpenButton: AnyView {
        AnyView(
            Button (
                action: {
                    self.isFullScreen.toggle()
                },
                label: {
                    VStack {
                        Image(systemName: "arrow.up.and.down")
                            .rotationEffect(.degrees(45))
                        Text("open").font(.system(size: 12))
                    }
                }
            )
        )
    }
    
    // a share button converted into an AnyView
    var anyViewShareButton: AnyView {
        AnyView(
            Button (
                action: {
                    // TODO
                    //popover the standard apple sharing stuff
                   /* if let coord = AppDelegate.getAppDelegate().readViewCoordinator {
                        coord.shareData(self.attachment.myData as NSData)
                    }*/
                },
                label: {
                    VStack {
                        Image(systemName: "square.and.arrow.up")
                        Text("share").font(.system(size: 12))
                    }
                }
            )
        )
    }
    
    // a delete button converted into an AnyView
    var anyViewDeleteButton: AnyView {
        AnyView(
            Button (
                action: {
                    // TODO
                    /*
                    let filename = getDocumentsDirectory().appendingPathComponent(self.attachment.myName)
                    
                    do {
                        try FileManager.default.removeItem(at: filename)
                    } catch let error as NSError {
                        print("Error: \(error)")
                    }
                    self.isDownloaded = false
                */
                },
                label: {
                    VStack {
                        Image(systemName: "trash")
                        Text("delete")
                            .font(.system(size: 12))
                    }
                }
            )
        )
    }
    
    // a download button converted into an AnyView
    // dowloads the file into the documents directory
    var anyViewDownloadButton: AnyView {
        AnyView(
            Button (
                action: {
                    self.download()
                },
                label: {
                    VStack {
                        Image(systemName: "arrow.down.circle")
                        Text("download")
                            .font(.system(size: 12))
                    }
                }
            )
        )
    }
}

