//
//  Certificate.swift
//  enzevalos_iphone
//
//  Created by lazarog98 on 18.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation

/**
 Contains information about a certificate
 */
class Certificate {
    
    static func isCertificateFileEnding(ending: String) -> Bool {
        // See: https://crypto.stackexchange.com/questions/43697/what-is-the-difference-between-pem-csr-key-and-crt-and-other-such-file-ext
        return ending == "p12" || ending == "cer" || ending == "pem"
    }
    
    let pem: String // raw PEM string
    let fingerPrint: String
    let eMails: [String]?
    let issuer: String?
    let hasSecretKey: Bool
    let subject: String?
    let startDate: String?
    let endDate: String?
    
    init(pem: String) throws {
        let cCertInfo = get_cert_data(pem)
        
        defer {
            deallocateCertInfo(certInfo: cCertInfo)
        }
        
        let certInfo = cCertInfo?.pointee
        
        self.hasSecretKey = pem.contains("PRIVATE KEY")
        
        let (fingerPrint, _, _) = getFingerprintFromPem(pem: pem)
        
        guard let fp = fingerPrint else {
            throw SMIMEError.init(message: "No fingerprint in file...", errorArray: [], type: SMIMEError.ErrorType.fingerPrint)
        }
        
        // if it crashes on this line, you can loop through the errors and see what's wrong
        self.fingerPrint = fp
        self.pem = pem
        
        self.eMails = certInfo?.extractField(field: CertInfoString.emails)
        self.endDate = certInfo?.extractField(field: CertInfoString.endDate)
        self.startDate = certInfo?.extractField(field: CertInfoString.startDate)
        self.subject = certInfo?.extractField(field: CertInfoString.subject)
        self.issuer = certInfo?.extractField(field: CertInfoString.issuer)
    }
}

/**
 An enum used in the extractString() method of cert_info
 */
enum CertInfoString {
    case issuer;
    case subject;
    case startDate;
    case endDate;
    case emails;
}

/**
 extensions to the C struct so that memory deallocation is automatic
 */
extension cert_info {
    /**
    Extracts an array of certs in PEM of the respective function from the result object
    Deallocates the "errors" part of the result object
    Only use in combination with_extractOutput and _extractErrors
    */
    private func extractEmails() -> [String]? {
        let arr = self.emails
        var strArr: [String]? = nil
        
        if arr != nil {
            strArr = []
            let size = self.num_emails
            //print("Size:",size)
            for i in 0..<size {
                let str = String(cString: arr![Int(i)]!)
                strArr?.append(str)
            }
        }
        
        return strArr
    }

    /**
    Extracts the output of the respective function from the result object
    Deallocates the "res" part of the result object
    Only use in combination with_extractErrors
     
     - parameters:
        - field: the result object
     */
    private func extractString(field: CertInfoString) -> String? {
        let cStr: UnsafeMutablePointer<Int8>?
        switch field {
        case .endDate:
            cStr = self.date_end
        case .startDate:
            cStr = self.date_start
        case .issuer:
            cStr = self.issuer
        case .subject:
            cStr = self.subject
        default:
            return nil
        }
         
         var swiftStr: String? = nil
         
         if cStr != nil {
             swiftStr = String(cString: cStr!)
         }
         
         return swiftStr
    }
    
    /**
     A generic function that reads a field and casts it to the type of the variable that stores the result. The variable must therefore have the correct type
     
     - parameters:
        - field: enum, specifiying the field to extract
     */
    func extractField<T>(field: CertInfoString) -> T? {
        switch field {
        case .emails:
            let emails = self.extractEmails()
            if emails is T? {
                return emails as! T?
            }
        default:
            let res = self.extractString(field: field)
            if res is T? {
                return res as! T?
            }
        }
        
        return nil
    }
}
