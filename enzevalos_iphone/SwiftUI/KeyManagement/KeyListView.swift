//
//  KeyListView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.12.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct PublicKeyListView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    let publicKey: Bool

    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct KeyListView_Previews: PreviewProvider {
    static var previews: some View {
        PublicKeyListView(publicKey: true)
    }
}
