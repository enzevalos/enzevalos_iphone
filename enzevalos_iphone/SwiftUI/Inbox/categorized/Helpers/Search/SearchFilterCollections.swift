//
//  SearchFilterCollections.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 09.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import Foundation


class SearchFilterCollections{
    


//MARK: - Standart Search filters : Sender, Subject, Body, All
static let Standarts:[SearchFilter] = [
        SearchFilter(
            text: NSLocalizedString("All", comment: ""),
            filter: SearchFilters.AllStandarts
        ),
        SearchFilter(
            text: NSLocalizedString("Subject", comment: ""),
            filter: SearchFilters.Subject
        ),
        SearchFilter(
            text: NSLocalizedString("Body", comment: ""),
            filter: SearchFilters.Body
        ),
        SearchFilter(
            text: NSLocalizedString("Sender", comment: ""),
            filter: SearchFilters.Sender
        )
    ]



//MARK: - Filters for Files
static let Files:[SearchFilter] = [
        SearchFilter(
            text: NSLocalizedString("Filename", comment: ""),
            filter: SearchFilters.Filename
        ),
        SearchFilter(
            text: NSLocalizedString("Subject", comment: ""),
            filter: SearchFilters.Subject
        ),
        SearchFilter(
            text: NSLocalizedString("Body", comment: ""),
            filter: SearchFilters.Body
        ),
        SearchFilter(
            text: NSLocalizedString("Sender", comment: ""),
            filter: SearchFilters.Sender
        )
    ]


}
