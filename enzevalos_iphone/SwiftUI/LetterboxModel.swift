//
//  LetterboxModel.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 16.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation
import SystemConfiguration
import Contacts

/**
 TODO:
 Import Secret keys
 Update mails in background
 var contactStore = CNContactStore()
 
 NotificationCenter.default.addObserver(
     self,
     selector: #selector(addressBookDidChange),
     name: NSNotification.Name.CNContactStoreDidChange,
     object: nil)
 
 @objc func addressBookDidChange(notification: NSNotification){
     AddressHandler.updateCNContacts()
 }

 */

enum ReachabilityStatus {
    case notReachable
    case reachableViaWWAN
    case reachableViaWiFi
}

enum UIState {
    case FIRSTLAUNCH
    case ACCOUNTSETTING
    case PERMISSIONS
    case GENERATEKEYS
    case LAUNCHEDBEFORE
}

class LetterboxModel: ObservableObject {
    static var instance: LetterboxModel {
        get {
            if let instance = currentInstance {
                return instance
            }
            return LetterboxModel()
        }
    }
    private static var currentInstance: LetterboxModel?

    let mailHandler = MailHandler()
    let dataProvider = PersistentDataProvider.dataProvider
    @Published var currentState: UIState = .FIRSTLAUNCH
    
    private var keysGenerated = false
    
    //TODO Refactor: Where to move this?
    static var currentReachabilityStatus: ReachabilityStatus {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }
        
        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        } else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaWWAN
        } else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        } else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        } else {
            return .notReachable
        }
    }
    
    init() {
        if UserDefaults.standard.bool(forKey: "launchedBefore") {
            currentState = .LAUNCHEDBEFORE
        }
        resetApp()
        StudySettings.setupStudy()
        if currentState == .LAUNCHEDBEFORE {
            ContactHandler.handler.findContacts(update: true)
            // TODO: Check Account data?
            do {
                try mailHandler.startIMAPIdleIfSupported()
            } catch {
                print("NO IMAP IDLE! \(error)")
            }
            mailHandler.loadMailsForInbox(completionCallback: { error in
                guard error == nil else {
                    print(error ?? "")
                    return
                }
            })
        } else {
            // TODO: ONLY FOR TESTING!!!!
            // OutgoingMail.LoadSMIMETEST()
            
            // Remove Google Auth token from keychain
            GTMKeychain.removePasswordFromKeychain(forName: "googleOAuthCodingKey")

            // Register defaults for signature handling
            //TODO FIX
            UserDefaults.standard.register(defaults: ["Signature.Switch": false])
            UserDefaults.standard.register(defaults: ["Signature.Text": "Verfasst mit Letterbox. Mehr Informationen: http://letterbox-app.org"])
        }
        LetterboxModel.currentInstance = self
    }
    
    
    func resetApp() {
        if UserDefaults.standard.bool(forKey: "reset") {
            PersistentDataProvider.dataProvider.deleteAll(completionHandler: { error in
                print("Delete All data")
                
                // Reset Keychain!
                UserManager.resetUserValues()
                UserDefaults.standard.set(false, forKey: "launchedBefore")
                UserDefaults.standard.set(false, forKey: "reset")
                self.currentState = .FIRSTLAUNCH
            })
        }
    }
    
    func afterPermissions() {
        self.currentState = .LAUNCHEDBEFORE
    }
    
    func afterOnboarding() {
        UserDefaults.standard.set(true, forKey: "launchedBefore")
        currentState = .PERMISSIONS
        setupKeys()
    }
    
    
    private func setupKeys() {
        let provider = PersistentDataProvider.dataProvider
        guard let userAddr = UserManager.loadUserValue(Attribute.userAddr) as? String else {
            fatalError("Missing user address")
        }
        _ = provider.createNewSecretKey(addr: userAddr, completionHandler: { (error) in
            // Done -> LaunchedBefore or Permissions?!
            print("Key created.")
        })
        StudySettings.setupStudyKeys()
        Mailbot.firstMail()
        mailHandler.allFolders({(error) in
            
            try? provider.fetchedFolderResultsController.performFetch()
            if let folders = provider.fetchedFolderResultsController.fetchedObjects {
                for f in folders {
                    let flags = MCOIMAPFolderFlag(rawValue: Int(f.flags))
                    if flags.contains(MCOIMAPFolderFlag.drafts) {
                        UserManager.storeUserValue(f.path as AnyObject?, attribute: Attribute.draftFolderPath)
                    }
                    if flags.contains(MCOIMAPFolderFlag.sentMail) {
                        UserManager.storeUserValue(f.path as AnyObject?, attribute: Attribute.sentFolderPath)
                    }
                    if flags.contains(MCOIMAPFolderFlag.trash) {
                        UserManager.storeUserValue(f.path as AnyObject?, attribute: Attribute.trashFolderPath)
                    }
                    if flags.contains(MCOIMAPFolderFlag.archive) {
                        UserManager.storeUserValue(f.path as AnyObject?, attribute: Attribute.archiveFolderPath)
                    }
                    if flags.contains(MCOIMAPFolderFlag.inbox) {
                        UserManager.storeUserValue(f.path as AnyObject?, attribute: Attribute.inboxFolderPath)
                    }
                }
            }
        })
        mailHandler.loadMailsForInbox(completionCallback: {_ in })
    }
}
