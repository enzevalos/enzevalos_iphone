//
//  TypePicker.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 22.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

/// a row of possible file extensions the user can choose and we can bind to
/// currently mostly used by the filescategory
struct TypePicker : View{
    @Binding var selectedTypes : [String]
    
    var selectableTypes : [String] = ["jpg","png","pdf","txt","mp4"]
    
    var body: some View{
        ScrollView(.horizontal){HStack{
            ForEach(0..<selectableTypes.count){i -> ToggleableTag<Text> in
                let boolToArrayBinder = Binding<Bool>(
                    get: {selectedTypes.contains(selectableTypes[i])},
                    set: {
                        if $0 {
                            selectedTypes.append(selectableTypes[i])
                        }else{
                            selectedTypes.removeAll(where: {$0==selectableTypes[i]})
                        }
                    }
                )
                return ToggleableTag(isActive: boolToArrayBinder){
                    Text(selectableTypes[i])
                }
            }
        }.padding(.horizontal,20)}
    }
}

struct TypePicker_Previews: PreviewProvider {
    static var previews: some View {
        StatefulPreviewWrapper(["Tag 1", "Tag 2"]){
            TypePicker(selectedTypes: $0)
        }
    }
}
