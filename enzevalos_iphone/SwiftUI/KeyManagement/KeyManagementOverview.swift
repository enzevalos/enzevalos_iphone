//
//  KeyManagementOverview.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 01.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

struct KeyManagementOverview: View {
    var body: some View {
        VStack (alignment: .center, spacing: 10){
            Text("Management.Crypto.Name.Info")
                .padding()
            Divider()
            // Other keys
            NavigationLink("Management.Name.Crypto.Other", destination: PublicKeyListView()
                    .environment(\.managedObjectContext, PersistentDataProvider.dataProvider.persistentContainer.viewContext))
            Divider()
            // My keys
            NavigationLink("Management.Crypto.Name.You", destination:
                           SecretKeyListView()
                            .environment(\.managedObjectContext, PersistentDataProvider.dataProvider.persistentContainer.viewContext))
            Divider()
            // Import keys
            NavigationLink("Management.Name.Crypto.Import", destination: MixedKeyListView())
            Spacer()
        }
        .navigationTitle("Management.Crypto.Title")
    }
}

struct KeyManagementOverview_Previews: PreviewProvider {
    static var previews: some View {
        KeyManagementOverview()
    }
}
