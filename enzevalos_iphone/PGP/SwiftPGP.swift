//
//  SwiftPGP.swift
//
//  Created by Oliver Wiese on 25.09.17.
//

import Foundation
import Security
import KeychainAccess

class SwiftPGP: Encryption {
    
    let cryptoScheme = CryptoScheme.PGP
    
    let PasscodeSize = 36
    
    public func resetKeychains(){
        do{
            try keychain.removeAll()
            try pwKeyChain.removeAll()
            try exportPwKeyChain.removeAll()
        }catch {
            print("Can not reset keychains.")
        }
        
    }
    
    private func generatePW(size: Int, splitInBlocks: Bool) -> String{
        let file = open("/dev/urandom", O_RDONLY)
        if file >= 0{
            var pw = ""
            while pw.count < size{
                var bits: UInt64 = 0
                read(file, &bits, MemoryLayout<UInt64>.size)
                pw = pw + String(bits)
            }
            let subpw = pw.prefix(size)
            if splitInBlocks{
                pw = ""
                var i = 0
                for c in subpw{
                    pw.append(c)
                    if i % 4 == 3 && i < size - 4{
                        pw.append("-")
                    }
                    i = i + 1
                }
            }
            else{
                pw = String(subpw)
            }
            return pw
        }
        else{
            return generatePW(arc4: size)
        }
    }
    
    private func generatePW(arc4 size: Int)-> String{
        var pw = ""
        for i in 0..<size{
            // digit from 0...9
            let p = Int(arc4random_uniform(10))
            pw.append(String(p))
            if i % 4 == 3 && i < size - 4{
                pw.append("-")
            }
        }
        return pw
    }
    
    private var keychain: Keychain{
        get{
            return Keychain(service: "Enzevalos/PGP")
        }
    }
    
    private var pwKeyChain: Keychain{
        get{
            return Keychain(service: "Enzevalos/PGP/Password")
        }
    }
    
    private var exportPwKeyChain: Keychain{
        get{
            return Keychain(service: "Enzevalos/PGP/ExportPassword")
        }
    }
    
    //TODO: @jabo oldSecretKeys
    private var allSecretKeys: [Key]{
        get{
            var myKeys = Set<Key>()
            if let keys = ((try? keychain.getString("secretKeys")) as String??){
                if let keyIDs = keys{
                    for id in keyIDs.split(separator: ";"){
                        if let key = loadKey(id: String(id)){
                            if key.isSecret{
                                myKeys.insert(key)
                            }
                        }
                    }
                }
            }
            return Array(myKeys)
        }
    }

    private func storeKey(key: Key) -> String{
        let keyring = Keyring()
        keyring.import(keys: [key])
        let id = key.keyID.longIdentifier
        if let testData = ((try? keychain.getData(id)) as Data??), testData != nil{
            // merge keys. i.e. secret key stored and key is public key.
            if let keys = try? ObjectivePGP.readKeys(from: testData!){
                keyring.import(keys: keys)
            }
        }
        if let k = keyring.findKey(id){
            if let data = try? k.export(){
                keychain[data: id] = data
            }
        }
        if key.isSecret{
            if let keys = ((try? keychain.getString("secretKeys")) as String??){
                if var ids = keys{
                    ids = ids + ";"+id
                    keychain["secretKeys"] = ids
                }
                else{
                    keychain["secretKeys"] = id
                }
            }
            else{
                keychain["secretKeys"] = id
            }
        }
        return id
    }
    
    func loadKey(id: String) -> Key?{
        do{
            if let data = try keychain.getData(id){
                if let keys = try? ObjectivePGP.readKeys(from: data), keys.count > 0{
                    if let key = keys.first{
                        return key
                    }
                }
            }
        } catch{
            return nil
        }
        return nil
    }
    
    func loadExportPasscode(id: String) -> String?{
        do{
            if let pw = try exportPwKeyChain.getString(id){
                return pw
            }
        } catch{
            return nil
        }
        return nil
    }
    
    func deleteExportPasscodes() throws {
        try exportPwKeyChain.removeAll()
    }
    
    private func loadPassword(id: String) -> String?{
        do{
            if let pw = try pwKeyChain.getString(id) {
                return pw
            }
        } catch{
            return nil
        }
        return nil
    }
    
    private func loadPassword(key: Key?) -> String?{
        if let k = key{
            return loadPassword(id: k.keyID.longIdentifier)
        }
        return nil
    }
    
    func generateKey(adr: String, new: Bool = false) -> String{
        if allSecretKeys.count > 0 && !new{
            var primkey: Key?
            for key in allSecretKeys{
                if vaildAddress(key: key).contains(adr){
                    _ = storeKey(key: key)
                    primkey = key
                }
            }
            if let key = primkey{
                return key.keyID.longIdentifier
            }
        }
        let gen = KeyGenerator()
        let pw: String? = nil
        let key = gen.generate(for: "\(adr) <\(adr)>", passphrase: pw)
        if pw != nil{
            pwKeyChain[key.keyID.longIdentifier] = pw
        }
        return storeKey(key: key)
    }
    
    private func delete(_ key: Key) {
        do {
            try keychain.remove(key.keyID.longIdentifier)
        } catch {
            NSLog("Error while removing key from keychain. KeyID: %@, isSecretKey: %@ ", key.keyID, key.isSecret)
        }
    }
    
    func deleteSecretKeys() {
        let secretKeys = allSecretKeys
        for key in secretKeys {
            //a Key-object may contain both public and secret key
            var pubKey: Key? = nil
            if key.isPublic, let partialKey = key.publicKey {
                pubKey = Key(secretKey: nil, publicKey: partialKey)
            }
            delete(key)
            if let pubKey = pubKey {
                _ = storeKey(key: pubKey)
            }
        }
        do {
            try keychain.remove("secretKeys")
        } catch {
            NSLog("Eror while removing secretKeys keychain")
        }
    }
    
    func importKeys(keys: [String]) -> [String] {
        var keyIds = [String]()
        for key in keys {
            if let newIds = try? importKeys(key: key, pw: nil, isSecretKey: false, autocrypt: false) {
                keyIds.append(contentsOf: newIds)
            }
        }
        return keyIds
    }
    
    func importKeys (key: String,  pw: String?, isSecretKey: Bool, autocrypt: Bool) throws -> [String]{
        var keys = [Key]()
        var keyData: [Data] = []
        if autocrypt{
            if let keyD = try? Armor.readArmored(key) {
                keyData.append(keyD)
            } else if let keyD = key.toBase64() {
                keyData.append(keyD)
            }
        }
        else {
            var keyStrings = PGPPart.PUBLICKEY.findPGPPartInString(content: key)
            keyStrings.append(contentsOf: PGPPart.SECRETKEY.findPGPPartInString(content: key))
            for s in keyStrings {
                if let data = try? Armor.readArmored(s) {
                    keyData.append(data)
                }
            }
        }
        for data in keyData {
            do {
                let readKeys = try ObjectivePGP.readKeys(from: data)
                keys.append(contentsOf: readKeys)
            }
            catch {
               //TODO Error handling
            }
        }
        for key in keys{
            if key.isSecret{
                //test key
                try ObjectivePGP.sign("1234".data(using: .utf8)!, detached: false, using: [key], passphraseForKey: {(key) -> String? in return pw})
            }
        }
        return storeMultipleKeys(keys: keys, pw: pw, secret: isSecretKey)
    }
    
    func importKeys(data: Data,  pw: String?, secret: Bool) throws -> [String]{
        if let keys = try? ObjectivePGP.readKeys(from: data){
            return storeMultipleKeys(keys: keys, pw: pw, secret: secret)
        }
        return [String]()
    }
    
    
    func importKeysFromFile(file: String,  pw: String?) throws -> [String]{
        if let keys = try? ObjectivePGP.readKeys(fromPath: file){
            return storeMultipleKeys(keys: keys, pw: pw, secret: false)
        }
        return [String]()
    }
    
    func readKeys(data: Data, importDate: Date = Date()) -> [TempKey] {
        if let keys = try? ObjectivePGP.readKeys(from: data) {
            return keys.map{
                let key = TempKey(pgpKey: $0, mailAddresses: vaildAddress(key: $0), importDate: importDate)
                return key
            }
        }
        return []
    }
    
    func readKeys(text: String, importDate: Date = Date()) -> [TempKey] {
        guard let data = try? Armor.readArmored(text) else {
            return []
        }
        return readKeys(data: data, importDate: importDate)
    }
    
    private func storeMultipleKeys(keys: [Key], pw: String?, secret: Bool)-> [String] {
        var ids = [String]()
        let keyring = Keyring()
        // merge secret and public keys
        keyring.import(keys: keys)
        let newKeys = keyring.keys
        for k in newKeys {
            if k.isSecret && secret || !k.isSecret && !secret {
                let id = storeKey(key: k)
                ids.append(id)
                if k.isSecret && secret {
                    findMailForSecretKey(keyID: id)
                }
                if let password = pw {
                    pwKeyChain[k.keyID.longIdentifier] = password
                }
            }
        }
        return ids
    }
    
    func store(tempKeys: [TempKey]) -> [String] {
        var ids: [String] = []
        tempKeys.forEach {
            if let key = $0.pgpKey {
                ids.append(contentsOf: storeMultipleKeys(keys: [key], pw: $0.password, secret: $0.isSecret))
            }
        }
        return ids
    }
    
    func exportKey(id: String, isSecretkey isSecretKey: Bool, autocrypt: Bool, newPasscode: Bool = false) -> String?{
        if let key = exportKeyData(id: id, isSecretkey: isSecretKey){
            if !isSecretKey && autocrypt{
                return key.base64EncodedString()
            }
            else{
                var armoredKey : String
                if isSecretKey{
                    armoredKey = Armor.armored(key, as: PGPArmorType.secretKey)
                }
                else{
                    armoredKey = Armor.armored(key, as: PGPArmorType.publicKey)
                }
                if isSecretKey && autocrypt{
                    // Create Autocrypt Setup-message
                    // See: https://autocrypt.readthedocs.io/en/latest/level1.html#autocrypt-setup-message
                    var passcode = loadExportPasscode(id: id)
                    if passcode == nil || newPasscode {
                        passcode = generatePW(size: PasscodeSize, splitInBlocks: true)
                    }
                    exportPwKeyChain[id] = passcode
//                    if let message = armoredKey.data(using: .utf8) {
//
//                        if let cipher = try? ObjectivePGP.symmetricEncrypt(message, signWith: nil, encryptionKey: passcode, passphrase: passcode, armored: false){
//                            let armorMessage =  Armor.armored(cipher, as: PGPArmorType.message)
//                            return armorMessage
//                        }
//
//                    }
                    return nil
                }
                return armoredKey
            }
        }
        return nil
    }
    
    func importKey(_ passcode: String, key: String) -> [String] {
//        if let keyData = try? Armor.readArmored(key) {
//            if let plaintext = try? ObjectivePGP
//                .symmetricDecrypt(keyData,
//                                  key: passcode,
//                                  verifyWith: nil,
//                                  signed: nil,
//                                  valid: nil,
//                                  integrityProtected: nil),
//               let ids = try? importKeys(data: plaintext,
//                                         pw: nil,
//                                         secret: true) {
//                return ids
//            }
//        }
        return []
    }
    
    private func exportKeyData(id: String, isSecretkey: Bool) -> Data?{
        if let key = loadKey(id: id){
            if key.isSecret && isSecretkey{
                if let keyData = try? key.export(keyType: PGPKeyType.secret){
                    return keyData
                }
            }
            if key.isPublic && !isSecretkey{
                if let keyData = try? key.export(keyType: PGPKeyType.public){
                    return keyData
                }
            }
        }
        return nil
    }
    
    func encrypt(plaintext: String, ids: [String], myId: String, encryptForMyID: Bool = true) -> CryptoObject {
        if let data = plaintext.data(using: String.Encoding.utf8){
            return encrypt(plaindata: data, ids: ids, myId: myId, encryptForMyID: encryptForMyID)
        }
        return CryptoObject(chiphertext: nil, plaintext: plaintext, decryptedData: nil, sigState: .NoSignature, encState: .NoEncryption, signKey: nil, encType: .PGP, signedAdrs: [])
    }
    
    func encrypt(plaindata: Data, ids: [String], myId: String, encryptForMyID: Bool = true) -> CryptoObject {
        var keys: [Key] = []
        let signKey = loadKey(id: myId)
        if let signKey = signKey{
            if encryptForMyID {
                keys.append(signKey)
            }
        }
        let signedAdr = vaildAddress(key: signKey)
        for id in ids{
            if let key = loadKey(id: id){
                keys.append(key)
            }
        }
        do {
            let chipher = try ObjectivePGP.encrypt(plaindata, addSignature: encryptForMyID && signKey != nil, using: keys, passphraseForKey: loadPassword)
            let armorChipherString = Armor.armored(chipher, as: .message)
            let armorChipherData = armorChipherString.data(using: .utf8)
            let plainString = String(data: plaindata, encoding: .utf8)
            if signKey != nil{
                return CryptoObject(chiphertext: armorChipherData, plaintext: plainString, decryptedData: plaindata, sigState: SignatureState.ValidSignature, encState: EncryptionState.ValidedEncryptedWithCurrentKey, signKey: myId, encType: CryptoScheme.PGP, signedAdrs: signedAdr)
            }
            return CryptoObject(chiphertext: armorChipherData, plaintext: plainString, decryptedData: plaindata, sigState: SignatureState.NoSignature, encState: EncryptionState.ValidedEncryptedWithCurrentKey, signKey: nil, encType: CryptoScheme.PGP, signedAdrs: [])
            
        } catch {
            NSLog("Encryption error. Ids: %@, myID: %@, encryptedForMyID: %@", ids, myId, encryptForMyID)
        }
        
        return CryptoObject(chiphertext: nil, plaintext: nil,decryptedData: nil, sigState: SignatureState.InvalidSignature, encState: EncryptionState.UnableToDecrypt, signKey: nil, encType: cryptoScheme, signedAdrs: signedAdr)
    }
    
    func verify(data: Data, attachedSignature: Data?, verifyId: String, fromAdr: String) -> CryptoObject {
        var sigKeyID: String? = nil
        var signedAdr = [String]()
        var sigState = SignatureState.NoSignature
        let encState = EncryptionState.NoEncryption
        let keyring = Keyring()
        var toverify = data
        
        if let text = String(data: data, encoding: .utf8) {
            if let unarmored = try? Armor.readArmored(text){
                toverify = unarmored
            }
        }
        
        if let key = loadKey(id: verifyId){
            keyring.import(keys: [key])
        }
        
        do{
            let keys = keyring.keys
            try ObjectivePGP.verify(toverify, withSignature: attachedSignature, using: keys, passphraseForKey: loadPassword)
            sigState = SignatureState.ValidSignature
            sigKeyID = verifyId
            signedAdr = vaildAddress(key: keys.first)
        } catch {
            let nsError = error as NSError
            print(nsError)
            switch nsError.code {
            case 7: // no public key
                sigState = SignatureState.NoPublicKey
            case 8: // no signature
                sigState = SignatureState.NoSignature
            case 9: // unable to decrypt
                sigState = SignatureState.InvalidSignature
            default:
                sigState = SignatureState.InvalidSignature
            }
        }
        if !signedAdr.contains(fromAdr) && sigState == SignatureState.ValidSignature {
            //sigState = .MissingFromAddr
            // TODO: Have we 
        }
        return CryptoObject(chiphertext: data, plaintext: nil, decryptedData: nil, sigState: sigState, encState: encState, signKey: sigKeyID, encType: .PGP, signedAdrs: signedAdr)
    }
    
    func decrypt(mail: IncomingMail) -> CryptoObject {
        let decIds = mail.decryptionKeyIDs
        let data = mail.cryptoData
        let sigIDs = mail.signatureKeyIDs
        if let addr = mail.signatureAddr {
            return decrypt(data: data, attachedSignature: nil, decKeyIDs: decIds, signatureIDs: sigIDs, fromAddr: addr)
        }
        return CryptoObject(chiphertext: data, plaintext: nil, decryptedData: nil, sigState: .InvalidSignature, encState: .UnableToDecrypt, signKey: nil, encType: .PGP, signedAdrs: [])
    }
    
    func decrypt(data: Data, attachedSignature: Data? = nil, decKeyIDs: [String], signatureIDs: [String], fromAddr: String) -> CryptoObject{
        
        var plaindata: Data? = nil
        var plaintext: String? = nil
        var sigState = SignatureState.NoSignature
        var encState = EncryptionState.UnableToDecrypt
        var sigKeyID: String? = nil
        var signedAdr = [String]()
        let prefID = PersistentDataProvider.prefKeyID
        var keyring = Keyring()
        var validDecryptionIDs: [String] = []
        
        /*
         DECRYPTION
         */
        // TODO: Maybe consider: try ObjectivePGP.recipientsKeyID(forMessage: ...) but currently not working...
        if decKeyIDs.contains(prefID), let decKey = loadKey(id: prefID) {
            let (currentPlain, currentEncState) = decryptMessage(data: data, keys: [decKey], encForCurrentSK: true)
            if currentEncState == EncryptionState.ValidedEncryptedWithCurrentKey {
                plaindata = currentPlain
                encState = currentEncState
                validDecryptionIDs.append(prefID)
            }
        }
        if encState != .ValidedEncryptedWithCurrentKey {
            for decID in decKeyIDs {
                if let decKey = loadKey(id: decID) {
                    keyring = Keyring()
                    let (currentPlain, currentEncState) = decryptMessage(data: data, keys: [decKey], encForCurrentSK: false)
                    if currentEncState != .UnableToDecrypt && currentEncState != .NoEncryption {
                        validDecryptionIDs.append(decID)
                        if encState != .ValidedEncryptedWithCurrentKey && encState != .ValidEncryptedWithOldKey {
                            plaindata = currentPlain
                            encState = currentEncState
                        }
                    }
                }
            }
        }
        
        if !(encState == EncryptionState.ValidedEncryptedWithCurrentKey || encState == .ValidEncryptedWithOldKey) {
            (plaindata, encState) = decryptMessage(data: data, keys: keyring.keys, encForCurrentSK: false)
        }
        /*
         VERIFICATION
         */
       // test if message is signed
        sigState = verifySignature(data: data, attachedSignature: attachedSignature, keys: [])
        for id in validDecryptionIDs {
            if let key = loadKey(id: id) {
                keyring.import(keys: [key])
                let currentState = verifySignature(data: data, attachedSignature: attachedSignature, keys: [key])
                if currentState == SignatureState.ValidSignature{
                    sigState = currentState
                    sigKeyID = id
                    signedAdr = vaildAddress(key: key)
                    break
                }
                sigState = currentState
            }
        }
        if sigState != .ValidSignature {
            for id in signatureIDs {
                if let key = loadKey(id: id) {
                    keyring.import(keys: [key])
                    let currentState = verifySignature(data: data, attachedSignature: attachedSignature, keys: keyring.keys)
                    if currentState == SignatureState.ValidSignature {
                        sigState = currentState
                        sigKeyID = id
                        signedAdr = vaildAddress(key: key)
                        break
                    }
                    sigState = currentState
                }
            }
        }
        
        if encState == EncryptionState.UnableToDecrypt{
            sigState = SignatureState.NoSignature
        }
        if let data = plaindata{
            plaintext =  String(data: data, encoding: String.Encoding.utf8)
        }
        else if encState == .NoEncryption{
            plaintext =  String(data: data, encoding: String.Encoding.utf8)
        }
        
        if sigState == .ValidSignature && !signedAdr.contains(fromAddr.lowercased()) {
            sigState = .InvalidSignature
        }
        return CryptoObject(chiphertext: data, plaintext: plaintext, decryptedData: plaindata, sigState: sigState, encState: encState, signKey: sigKeyID, encType: CryptoScheme.PGP, signedAdrs: signedAdr)
    }
    
    /**
     A help function for findMailForSecrectKey and findNotSignedMailForPublicKey
     Receives a keyID as String and returns the corresponding key as a [Key] List.
     */
    func keyAsKeyList(keyID: String) -> [Key] {
        var keyList = [Key]()
        if let key: Key = loadKey(id: keyID) {
            keyList.append(key)
        }
        return keyList
    }
    
    /**
     Receives a keyID as String and searches for undecrypted mails in persistent mails and and tries to decrypt them with the incoming secret key.
     */
    func findMailForSecretKey(keyID: String) {
        var encState = EncryptionState.UnableToDecrypt
        var plaindata: Data? = nil
        let key: [Key] = keyAsKeyList(keyID: keyID)
        let controller = PersistentDataProvider.dataProvider.generateFetchedUnableToDecryptMailsResultsController()
        
        
        if ((try? controller.performFetch()) != nil), let mailList = controller.fetchedObjects {
            for mail in mailList {
                // Change data
                guard let data =  mail.body.data(using: .utf8) else {
                    return
                }
                // Try to decrypt mail
                (plaindata, encState) = decryptMessage(data: data, keys: key, encForCurrentSK: true)
                if let plaindata = plaindata, encState == EncryptionState.ValidedEncryptedWithCurrentKey || encState == EncryptionState.ValidEncryptedWithOldKey {
                    // Update database
                    mail.encryptionStateInt = encState.rawValue
                    mail.body = String.init(data: plaindata, encoding: .utf8) ?? ""
                    do {
                        try PersistentDataProvider.dataProvider.save(taskContext: controller.managedObjectContext)
                    } catch {
                        print("Could not store decrypted mails!")
                    }
                }
            }
        }
    }
    
    private func decryptMessage(data: Data, keys: [Key], encForCurrentSK: Bool) -> (Data?, EncryptionState){
        if let dataString = String(data: data, encoding: .utf8) {
            do {
                let unarmored = try Armor.readArmored(dataString)
                let plain = try ObjectivePGP.decrypt(unarmored, andVerifySignature: true, using: keys, passphraseForKey: loadPassword)
                if encForCurrentSK{
                    return (plain, EncryptionState.ValidedEncryptedWithCurrentKey)
                }
                else{
                    return(plain, EncryptionState.ValidEncryptedWithOldKey)
                }
            } catch {
                let nsError = error as NSError
                // print(nsError)
                switch nsError.localizedDescription {
                case "Invalid header":
                    return (nil, EncryptionState.NoEncryption)
                default:
                    return (nil, EncryptionState.UnableToDecrypt)
                }
            }
            
        }
        return (nil, EncryptionState.NoEncryption)
    }
    
    /**
    Receives a keyID as String and searches for unsigned mails in persistent mails and and tries to verify them with the incoming public key.
    */
    func findNotSignedMailForPublicKey(keyID: String) {
        var sigState = SignatureState.NoPublicKey
        let key: [Key] = keyAsKeyList(keyID: keyID)
        let datahandler = PersistentDataProvider.dataProvider
        if let mailList = datahandler.fetchedMailResultsController.fetchedObjects { //TODO look for not verified signed mails.
            for mail in mailList {
                // Change data
                guard let data =  mail.body.data(using: .utf8) else {
                        return
                }
                // Try to verify mail signature
                sigState = verifySignature(data: data, attachedSignature:mail.body.toBase64(), keys: key) // TODO  mail.attachedSignature
                if sigState == SignatureState.ValidSignature {
                    // TODO Update database
                    // mail.isSigned = true
                    //mail.isCorrectlySigned = true
                    //datahandler.save(during: "verifying of older mails")
                }
            }
        }
    }
    
    private func verifySignature(sigString: String, attachedSignature: Data?, keys: [Key]) -> SignatureState {
        if let unarmored = try? Armor.readArmored(sigString){
            return verifySignature(data: unarmored, attachedSignature: attachedSignature, keys: keys)
        }
        if let data = sigString.data(using: .utf8) {
            return verifySignature(data: data, attachedSignature: attachedSignature, keys: keys)
        }
        return SignatureState.NoSignature
    }
    
    private func verifySignature(data: Data, attachedSignature: Data?, keys: [Key]) -> SignatureState {
        guard !(keys.isEmpty && attachedSignature != nil) else {
            return SignatureState.NoPublicKey
        }
        var sigData = data
        var sigState = SignatureState.NoSignature
        if let dataString = String(data: data, encoding: .utf8), let unarmored = try? Armor.readArmored(dataString){
            sigData = unarmored
        }
       
        var pubKeys = keys.filter{$0.isPublic}
        for key in pubKeys {
            if key.isSecret, let pk = key.publicKey{                
                pubKeys.append(Key(secretKey: nil, publicKey: pk) )
            }
        }
        
        do{
            try ObjectivePGP.verify(sigData, withSignature: attachedSignature, using: pubKeys, passphraseForKey: loadPassword)
            sigState = SignatureState.ValidSignature
        } catch {
            let nsError = error as NSError
           // print(nsError)
            switch nsError.code {
            case 7: // no public key
                sigState = SignatureState.NoPublicKey
            case 8: // no signature
                sigState = SignatureState.NoSignature
            case 9: // unable to decrypt
                sigState = SignatureState.InvalidSignature
            default:
                sigState = SignatureState.InvalidSignature
            }
        }
        return sigState
    }
    
    private func extractAddr(from user: User) -> String? {
        if let start = user.userID.range(of: "<"),
            let end = user.userID.range(of: ">"){
            let s = start.lowerBound
            let e = end.upperBound
            var adr = String(user.userID[s..<e])
            if adr.count > 2 {
                adr = String(user.userID[user.userID.index(s, offsetBy: 1)..<user.userID.index(e, offsetBy: -1)])
            }
            adr = adr.lowercased()
            return adr
        }
        return nil
    }
    
    func vaildAddress(key: Key?) -> [String]{
        var adrs = [String]()
        if let k = key{
            if let pk = k.publicKey {
                let users = pk.users
                for user in users{
                    if let adr = extractAddr(from: user) {
                        adrs.append(String(adr))
                    }
                }
            }
            if let sk = k.secretKey {
                for user in sk.users {
                    if let adr = extractAddr(from: user), !adrs.contains(adr) {
                        adrs.append(adr)
                    }
                }
            }
        }
        return adrs
    }
    
    func vaildAddress(keyId: String?) -> [String]{
        if let id = keyId{
            if let key = loadKey(id: id){
                return vaildAddress(key: key)
            }
        }
        return []
    }
    
    func checkPasswordFor(secretKeyID: String?, password: String? = nil, secretkey: Key? = nil) -> Bool {
        var key: Key
        guard let id = secretKeyID else {
            return false
        }
        if let sk = secretkey, secretKeyID == sk.keyID.longIdentifier {
            key = sk
        }
        else if let newKey = loadKey(id: id) {
            key = newKey
        }
        else {
            return false
        }
        //TODO: Problem overwrite password?
        if let pw = password {
            pwKeyChain[key.keyID.longIdentifier] = pw
        }
        if let data = String("Test").data(using: .utf8) {
            if let pw = ((try? pwKeyChain.get(key.keyID.longIdentifier)) as String??), let _ = try? ObjectivePGP.sign(data, detached: false, using: [key], passphraseForKey: {(key) -> String? in return pw}) {
                 return true
            }
        }
        return false
    }
    /*
     encrypt a array of strings with one password. Returns encrypted strings and the password for decryption
     */
    func symmetricEncrypt(textToEncrypt: [String], armored: Bool, password: String?) -> (chiphers: [String], password: String){
        var pw = generatePW(size: 8, splitInBlocks: true)
        if let p = password{
            pw = p
        }
        let chiphers = [String]()
        
//        for text in textToEncrypt {
//            if let data = text.data(using: .utf8) {
//                if let chipher = try? ObjectivePGP
//                    .symmetricEncrypt(data,
//                                      signWith: nil,
//                                      encryptionKey: password,
//                                      passphrase: pw,
//                                      armored: false) {
//                    if armored {
//                        chiphers.append(Armor.armored(chipher, as: PGPArmorType.message))
//                    } else {
//                        chiphers.append(chipher.base64EncodedString(options: .init(arrayLiteral: .lineLength76Characters, .endLineWithLineFeed)))
//                    }
//                }
//            }
//        }
        return (chiphers, pw)
    }
    
    func symmetricDecrypt(chipherTexts: [String], password: String) -> [String]{
        let plaintexts = [String]()
        
//        for chipher in chipherTexts {
//            if let data = chipher.data(using: .utf8) {
//                if let plainData = try? ObjectivePGP
//                    .symmetricDecrypt(data,
//                                      key: password,
//                                      verifyWith: nil,
//                                      signed: nil,
//                                      valid: nil,
//                                      integrityProtected: nil) {
//                    if let plainText = String(data: plainData, encoding: .utf8){
//                        plaintexts.append(plainText)
//                    }
//                }
//            }
//        }
        return plaintexts
    }
}
