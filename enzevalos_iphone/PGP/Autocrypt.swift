
//
//  Autocrypt.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 27.10.18.
//  Copyright © 2018 fu-berlin. All rights reserved.
//

import Foundation

// See: https://autocrypt.org/level1.html

public enum AutocryptState {
    case MUTUAL
    case GOSSIP
    case NOPREFERENCE
    case RESET
    case NOAUTOCRYPT
    
    var name: String{
        get{
            switch self {
            case .MUTUAL:
                return "mutual"
            case .NOPREFERENCE:
                return "nopreference"
            default:
                return ""
            }
        }
    }


    static func find(i: Int) -> AutocryptState {
        switch i {
        case 0:
            return AutocryptState.MUTUAL
        case 1:
            return AutocryptState.GOSSIP
        case 2:
            return AutocryptState.NOPREFERENCE
        case 3:
            return AutocryptState.RESET
        case 4:
            return AutocryptState.NOAUTOCRYPT
        default:
            return AutocryptState.NOAUTOCRYPT
        }
    }

    func canEnc() -> Bool {

        switch self {
        case AutocryptState.MUTUAL:
            return true
        case AutocryptState.GOSSIP:
            return true
        case AutocryptState.RESET:
            return true
        default:
            return false
        }
    }
    func asInt() -> Int16 {
        switch self {
        case AutocryptState.MUTUAL:
            return 0
        case AutocryptState.GOSSIP:
            return 1
        case AutocryptState.NOPREFERENCE:
            return 2
        case AutocryptState.RESET:
            return 3
        case AutocryptState.NOAUTOCRYPT:
            return 4
        }
    }

}


class Autocrypt {
    static let ENFORCEENCRYPTION = true
    static let AUTOCRYPTHEADER = "Autocrypt"
    static let SETUPMESSAGE = "Autocrypt-Setup-Message"
    static let ADDR = "addr"
    static let TYPE = "type"
    static let ENCRYPTION = "prefer-encrypt"
    static let KEY = "keydata"
    static let EXTRAHEADERS = [Autocrypt.AUTOCRYPTHEADER, Autocrypt.SETUPMESSAGE]
    
    var addr: String = ""
    var type: CryptoScheme = .PGP
    var prefer_encryption: AutocryptState = AutocryptState.NOAUTOCRYPT
    var key: String = ""
    
    init(addr: String, type: String, prefer_encryption: String, key: String) {
        self.addr = addr
        self.key = key
        setPrefer_encryption(prefer_encryption)
    }
    
    convenience init(header: MCOMessageHeader) {
        var autocrypt = header.extraHeaderValue(forName: Autocrypt.AUTOCRYPTHEADER)
        var field: [String]
        var addr = ""
        var type = "1"
        var pref = AutocryptState.MUTUAL.name
        var key = ""
        
        if autocrypt != nil {
            autocrypt = autocrypt?.trimmingCharacters(in: .whitespacesAndNewlines)
            let autocrypt_fields = autocrypt?.components(separatedBy: ";")
            for f in autocrypt_fields! {
                field = f.components(separatedBy: "=")
                if field.count > 1 {
                    let flag = field[0].trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                    var value = field[1]
                    if field.count > 2 {
                        for i in 2...(field.count - 1) {
                            value = value + "="
                            value = value + field[i]
                        }
                    }
                    switch flag {
                    case Autocrypt.ADDR:
                        addr = value.trimmingCharacters(in: .whitespacesAndNewlines)
                        addr = addr.lowercased()
                        break
                    case Autocrypt.TYPE:
                        type = value.trimmingCharacters(in: .whitespacesAndNewlines)
                        break
                    case Autocrypt.ENCRYPTION:
                        pref = value.trimmingCharacters(in: .whitespacesAndNewlines)
                        break
                    case Autocrypt.KEY:
                        if value.count > 0 {
                            key = value
                        }
                        break
                    default:
                        break
                    }
                }
            }
        }
        self.init(addr: addr, type: type, prefer_encryption: pref, key: key)
    }
    
    private func addArmor() -> Bool{
        let header = "-----BEGIN PGP PUBLIC KEY BLOCK-----"
        guard !key.isEmpty && !key.starts(with: header), let data = key.toBase64() else {
            return false
        }
        key = Armor.armored(data, as: .publicKey)
        return true
    }

    func setPrefer_encryption(_ input: String){
        let pref = input.lowercased()
        if pref == "yes" || pref == "mutual" || pref == AutocryptState.MUTUAL.name {
            self.prefer_encryption = AutocryptState.MUTUAL
        } else if pref == "no" || pref == AutocryptState.NOPREFERENCE.name {
            self.prefer_encryption = AutocryptState.NOPREFERENCE
        }
        else {
            self.prefer_encryption = AutocryptState.NOPREFERENCE
        }
    }
    
    func toString() -> String {
        return "Addr: \(addr) | type: \(type) | encryption? \(prefer_encryption) key size: \(key.count)"
    }
    
    static var autocryptHeader: String? {
        get {
            let adr = (UserManager.loadUserValue(Attribute.userAddr) as! String).lowercased()
            let encPref = AutocryptState.MUTUAL
            
            let pgp = SwiftPGP()
            if let key = pgp.exportKey(id: PersistentDataProvider.prefKeyID, isSecretkey: false, autocrypt: true) {
                var string = "\(ADDR)=" + adr
                string = string + "; \(ENCRYPTION)=\(encPref.name)"
                string = string + "; \(KEY)=" + key
                return string
            }
            return nil
        }
    }
    
    /// Use this function to create an autocrypt header for manuel header creation.
    /// - returns: Autcrypt: Addr = ...; key=...
    static var customAutocryptHeader: String? {
        guard let autocrypt = autocryptHeader else {
            return nil
        }
        return Autocrypt.AUTOCRYPTHEADER + ": " + autocrypt + "\r\n"
    }
    
    static func addAutocryptHeader(_ builder: MCOMessageBuilder) {
        if let autocrypt = Autocrypt.autocryptHeader {
            builder.header.setExtraHeaderValue(autocrypt, forName: AUTOCRYPTHEADER)
        }
    }
    
    static func recommandateEncryption (receiver: AddressRecord) -> (hasAutocrypt: Bool, recommandEnc: Bool){
        if receiver.hasPublicKey, let key = receiver.primaryKey {
            if key.preferEncryption == AutocryptState.NOAUTOCRYPT.asInt() {
                return (false, ENFORCEENCRYPTION)
            }
            else if key.preferEncryption == AutocryptState.MUTUAL.asInt() {
                return (true, true)
            }
            return (true, false)
        }
        else {
            return (false, false)
        }
    }
    
    static func createAutocryptKeyExport(builder: MCOMessageBuilder, keyID: String, key: String) {
        builder.header.setExtraHeaderValue("v1", forName: SETUPMESSAGE)
        
        builder.addAttachment(MCOAttachment.init(text: "This message contains a secret for reading secure mails on other devices. \n 1) Input the passcode from your smartphone to unlock the message on your other device. \n 2) Import the secret key into your pgp program on the device.  \n\n For more information visit: https://https://autocrypt.org/ \n\n"))
        
        if let keyAttachment = MCOAttachment.init(text: key){
            builder.addAttachment(keyAttachment)
        }
        
        // See: https://autocrypt.org/level1.html#autocrypt-setup-message
        let filename = keyID+".asc.asc"
        if let keyAttachment = MCOAttachment.init(contentsOfFile: filename){
            keyAttachment.mimeType = "application/autocrypt-setup"
            keyAttachment.setContentTypeParameterValue("UTF-8", forName: "charset")
            keyAttachment.setContentTypeParameterValue(filename, forName: "name")
            keyAttachment.filename = filename
            keyAttachment.data = key.data(using: .utf8)
            
            builder.addAttachment(keyAttachment)
            
        }
    }
}
