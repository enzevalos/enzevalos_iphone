//
//  SMIME.swift
//  enzevalos_iphone
//
//  Created by lazarog98 on 02.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation
import Security
import KeychainAccess

/**
 The class is a swift wrapper around the C implementation for SMIME. It implements S/MIME ver 3.2 (RFC5751). S/MIME 4 isn't supported yet because OpenSSL currently does not support GCM mode for S/MIME encryption
 
 TODO: Multiple certifcates?
    * Change mapping email <-> key -> fingerprint <-> key One email could have multiple keys and one key can have multiple emails
 */
class SMIME {
    // patterns used for extraction of keys and certs from PEMs
    private let privateKeyPattern = "[-]*BEGIN[a-zA-Z ]*PRIVATE KEY[-]*[a-zA-Z+/\n\r=0-9]*[-]*END[a-zA-Z ]*PRIVATE KEY[-]*"
    private let certificatePattern = "[-]*BEGIN[a-zA-Z ]* CERTIFICATE[-]*[a-zA-Z+/\n\r=0-9]*[-]*END[a-zA-Z ]* CERTIFICATE[-]*"
    
    private var privateKeyKeychain: Keychain {
        get {
            return Keychain(service: "Enzevalos/SMIME/privateKeys")
        }
    }

    
    private var certsKeychain: Keychain {
        get {
            return Keychain(service: "Enzevalos/SMIME/certs")
        }
    }

    private var CAKeychain: Keychain {
        get {
            return Keychain(service: "Enzevalos/SMIME/CAs")
        }
    }
    
    func getOwnCert() -> String? {
        if let fp = getOwnKeyFP() {
            return certsKeychain[fp]
        }
        return nil
    }
    
    func getOwnKey() -> String? {
        if let fp = getOwnKeyFP() {
            return privateKeyKeychain[fp]
        }
        return nil
    }
    
    func getKey(fp: String) -> String?{
        return privateKeyKeychain[fp]
    }
    
    func getCAs() -> [String] {
        var CAs : [String] = []
        for k in CAKeychain.allKeys()
        {
            if let ca = CAKeychain[k] {
                CAs.append(ca)
            }
        }
        
        return CAs
    }
   
    private func importInKeychain(certs: [String], keychain: Keychain) -> [String] {
        var fingerprints: [String] = []
        
        for cert in certs {
            let (fp, _, _) = getFingerprintFromPem(pem: cert)
            if let fp = fp {
                keychain[fp] = cert
                fingerprints.append(fp)
            }
        }
        
        return fingerprints
    }
    
    func importCerts(certs: [String]) -> [String] {
        return importInKeychain(certs: certs.map({ (cert: String) -> String in
            return cert.extractPattern(pattern: certificatePattern).joined(separator: "\r\n")
        }), keychain: certsKeychain)
    }
    
    func importCertForAddress(cert: String, addr: String) -> String {
        let fp = importCerts(certs: [cert])[0]
        certsKeychain[addr] = fp
        return fp
    }
    
    func importCA(certs: [String]) -> [String] {
        return importInKeychain(certs: certs.map({ (cert: String) -> String in
            return cert.extractPattern(pattern: certificatePattern).joined(separator: "\r\n")
        }), keychain: CAKeychain)
    }
    
    func importKeys(certsAndKeys: [(String, String)]) -> [String] {
        var fingerprints: [String] = []
        
        for (cert, key) in certsAndKeys {
            let (certFP, _, _) = getFingerprintFromPem(pem: cert)
            if let certFP = certFP {
                certsKeychain[certFP] = cert
                privateKeyKeychain[certFP] = key
                fingerprints.append(certFP)
            }
        }
        
        return fingerprints
    }
    
    func resetKeychain () {
        do{
            try privateKeyKeychain.removeAll()
            try certsKeychain.removeAll()
            try CAKeychain.removeAll()
        }catch {
            print("Can not reset keychains.")
        }
    }
    
    func importKeys(certsAndKeys: [String]) -> [String] {
        var certsAndKeysSplit: [(String, String)] = []
        
        for str in certsAndKeys {
            let keys = str.extractPattern(pattern: privateKeyPattern)
            let certs = str.extractPattern(pattern: certificatePattern)
            if keys.count != 1 {
                print("Incorrect amount of keys: ", keys.count)
                continue
            }
            if certs.count == 0
            {
                print("No certs")
                continue
            }
            certsAndKeysSplit.append((certs.joined(separator: "\r\n"), keys[0]))
        }
        
        return importKeys(certsAndKeys: certsAndKeysSplit)
    }
    
    
    func addPrivateKey(keyPlusCertPEM: String) -> String?
    {
        let fp = importKeys(certsAndKeys: [keyPlusCertPEM])
        if fp.count > 0 {
            privateKeyKeychain["ownkey"] = fp[0]
            return fp[0]
        }
        return nil
    }
    
    func addCertForContant(email: String, cert:String) -> String
    {
        let fp = importCerts(certs: [cert])
        certsKeychain[email]=fp[0]
        return fp[0]
    }
    
    func getOwnKeyFP() -> String? {
        return privateKeyKeychain["ownkey"]
    }
        
    /**
     Decrypts a message and verifies its signature.
     
     - returns: a `CryptoObject`, the `decryptedData` attribute contains the decrypted message
     
     - throws: `SMIMEError` decryption fails for whatever reason, the `errorArray` field can be iterated to print out the errors. See `getErrorReasonString()` and `getErrorString()`
     
     - parameters:
        - data: the message to decrypt and verify as `Data`
        - fromAddr: address of the sender
        - isMailNew: used for the verification, see `SMIME.verify()`
     */
    func decrypt(data: Data, fromAddr: String, isMailNew: Bool) throws -> CryptoObject {
        var outputData: Data = data
        guard var fp = getOwnKeyFP() , let cert = certsKeychain[fp], let key = privateKeyKeychain[fp], let text = String(data: data, encoding: .utf8) else {
            return CryptoObject(chiphertext: data, plaintext: nil, decryptedData: nil, sigState: .NoPublicKey, encState: .UnableToDecrypt, signKey: nil, encType: .SMIME, signedAdrs: [])
        }
                
        var (decStr, errArr) = decryptWithPem(message: text, certAsPem: cert, keyAsPem: key)
        var encState = EncryptionState.UnableToDecrypt
        if let errArr = errArr,  errArr.count == 0 && decStr != nil {
            encState = EncryptionState.ValidedEncryptedWithCurrentKey
        }
        else
        {
            let fps = privateKeyKeychain.allKeys()
            for f in fps {
                if f != fp, let certAsPem = certsKeychain[f], let keyAsPem = privateKeyKeychain[f] {
                    (decStr, errArr) = decryptWithPem(message: text, certAsPem: certAsPem, keyAsPem: keyAsPem)
                    if decStr != nil{
                        fp = f
                        encState = EncryptionState.ValidEncryptedWithOldKey
                        break
                    }
                }
            }
        }
        
        if let decStr = decStr, let decData = decStr.data(using: .utf8) {
            outputData = decData
        }
        
        /**
         we have tried to decrypt with our current key and with our older keys and if all of that failed then:
            * the email wasn't meant for us and we shouldn't be able to decrypt it anyway
            * there is something wrong with the e-mail
        */
        if let errArr = errArr, errArr.count > 0 {
            throw SMIMEError(message: "Decryption failed!", errorArray: errArr, type: SMIMEError.ErrorType.decryption)
        }
        
        let verifyCryptoObj = verify(data: outputData, email: fromAddr, isMailNew: isMailNew)
        
        let signedKeys = verifyCryptoObj.signedKeys
        let signKey = verifyCryptoObj.signKey
        let sigState = verifyCryptoObj.signatureState
        let addresses = verifyCryptoObj.signedAdrs
        let plainText = verifyCryptoObj.plaintext
        let decryptedData = plainText?.data(using: .utf8)
       
        
        return CryptoObject(chiphertext: data, plaintext: plainText, decryptedData: decryptedData, sigState: sigState, encState: encState, signKey: signKey, encType: CryptoScheme.SMIME, signedAdrs: addresses, signedKeys: signedKeys)
    }
    
    /**
     Signs a message.
     
     - returns: a `CryptoObject`, the `ciphertext` attribute contains the signed message
     
     - throws: `SMIMEError` signing fails for whatever reason, the `errorArray` field can be iterated to print out the errors. See `getErrorReasonString()` and `getErrorString()`
     
     - parameters:
        - plainData: the message to sign as `Data`
        - myEmail: the current user's e-mail
        - detached: if true, the signature is detached (`multipart/signed`); else not (`application/pkcs7-mime`)
     */
    func sign(plainData: Data, myEmail: String, detached: Bool = true) throws -> CryptoObject {
        // TODO: FIX what about older keys?
        guard let fp = getOwnKeyFP() else {
            throw SMIMEError(message: "No signing key....", errorArray: nil, type: SMIMEError.ErrorType.missingKey)
        }
        guard let cert = certsKeychain[fp], let key = privateKeyKeychain[fp] else {
            throw SMIMEError(message: "No signing key....", errorArray: nil, type: SMIMEError.ErrorType.missingKey)
        }
        guard let text = String(data: plainData, encoding: .utf8) else {
            throw SMIMEError(message: "No data...", errorArray: nil, type: SMIMEError.ErrorType.noData)

        }
        
        let (sigText, errArr) = signWithPem(message: text, certAsPem: cert, keyAsPem: key, detached: detached)
        
        if let errArr = errArr, errArr.count > 0 {
            throw SMIMEError(message: "Signing failed!", errorArray: errArr, type: SMIMEError.ErrorType.signing)
        }
        if let sigText = sigText {
            return CryptoObject(chiphertext: sigText.data(using: .utf8), plaintext: text, decryptedData: plainData, sigState: SignatureState.ValidSignature, encState: EncryptionState.NoEncryption, signKey: fp, encType: CryptoScheme.SMIME, signedAdrs: [myEmail])
        } else {
            throw SMIMEError(message: "Signing failed! No signed data...", errorArray: errArr, type: SMIMEError.ErrorType.signing)
        }
                
        
    }
    
    /**
     Verifies the signature of a signed message. If multiple signatures are present, ALL need to be valid AND have a matching mail address, else the signature is invalid.
     Previously unknown (not present in the keychain) but valid certificates are stored in the keychain.
    
     - returns: a `CryptoObject` with all the information about the verification process
     - parameters:
        - data: the (signed) message to verify
        - email: the e-mail of the sender
        - isMailNew: if true and the certificate is unknown, the fingerprint of the certificate is mapped to the e-mail in the keychain so it can be used to encrypt future messages to the sender
     */
    func verify(data: Data?, string: String? = nil, email: String, isMailNew: Bool) -> CryptoObject {
        var CAs : [String] = []
        CAKeychain.allKeys().forEach({ (key) in
            if let ca = CAKeychain[key] {
                CAs.append(ca)
            }
        })
        guard data != nil || string != nil  else {
            return CryptoObject(chiphertext: nil, plaintext: nil, decryptedData: nil, sigState: .NoSignature, encState: .UnableToDecrypt, signKey: nil, encType: .UNKNOWN, signedAdrs: [])
        }
        var text = ""
        
        if let data = data, let t = String(data: data, encoding: .utf8) {
            text = t
        } 
        if let s = string {
            text = s
        }
        let (verStr, certsFPArrWithNil, errArr) = verifyWithCApem(message: text, pemCAArr: CAs)
        
        guard let certsFPArr = certsFPArrWithNil, certsFPArr.count > 0 else {
            if let errors = errArr {
                for error in errors {
                    let reason = getErrorReasonString(errCode: error)
                    // check reasons to identify different error causes
                    // string comaprison necessary because doesn't have fixed error codes...
                    if reason == "no content type" {
                        return CryptoObject(chiphertext: data, plaintext: text, decryptedData: nil, sigState: .NoSignature, encState: EncryptionState.NoEncryption, signKey: nil, encType: .UNKNOWN, signedAdrs: [])
                    }
                }
            }
            return CryptoObject(chiphertext: data, plaintext: text, decryptedData: nil, sigState: .InvalidSignature, encState: EncryptionState.NoEncryption, signKey: nil, encType: .UNKNOWN, signedAdrs: [])
        }
        
        var newCerts: [String] = [] // all certs that came from the email and weren't available before
        var mailMatch = true
        
        for item in certsFPArr {
            var mailFound = false
            let (fp, cert) = item
            guard let certObj = try? Certificate(pem: cert) else {
                continue
            }
            // check if the email from the parameters matches with one of the emails in each of the certs used to sign the message
            for addr in certObj.eMails ?? [] {
                if addr == email {
                    mailFound = true
                    break
                }
            }
            mailMatch = mailFound && mailMatch
            // check if cert is already in the keychain
            if let _ = try? certsKeychain.get(fp) {
                continue
            }
            newCerts.append(cert)
        }
        
        let fps = importCerts(certs: newCerts)
        // check if the email has no certificate or the certificate is newer than the present one and update accordingly
        for fp in fps{
            guard let pem = certsKeychain[fp], let cert = try? Certificate(pem: pem) else {
                continue
            }
            for email in cert.eMails ?? []
            {
                if isMailNew || certsKeychain[email] == nil
                {
                    certsKeychain[email] = fp
                }
            }
        }
        
        let sigState = mailMatch ? SignatureState.ValidSignature : SignatureState.InvalidSignature
        
        let signedAddresses = getAllEmailsFromPEMs(certs: certsFPArr.map( { (arg) -> String in
            let (_, pem) = arg
            return pem
        }))
        
        let signKeyFps = certsFPArr.map( { (arg) -> String in
            let (fp, _) = arg
            return fp
        })
        if let verStr = verStr {
            return CryptoObject(chiphertext: data, plaintext: verStr, decryptedData: nil, sigState: sigState, encState: EncryptionState.NoEncryption, signKey: nil, encType: .SMIME, signedAdrs: signedAddresses, signedKeys: signKeyFps)
        } else {
            return CryptoObject(chiphertext: data, plaintext: text, decryptedData: nil, sigState: .NoSignature, encState: EncryptionState.NoEncryption, signKey: nil, encType: .UNKNOWN, signedAdrs: [])
        }
    }
    
    /**
     Encrypts and signs a message. Signing isn't optional.
     
     - parameters:
        - plainData: a `Data` object that contains the string to encrypt
        - ids: an array with the e-mails of all users that need to be able to decrypt this. They need to have their certificates imported with `importCertForAddress(cert, addr)` beforehand
        - ownAddr: the e-mail of the user of the app, only needed for the CryptoObject
        - encryptForMyId: if true, message is also encrypted for the sender
     
     - returns: a `CryptoObject` with all needed information
    
     -  throws: `SMIMEError` if:
        * any of the e-mails in `ids` doesn't have a certificate registered in the keychain (see `importCertForAddress(cert, addr)`)
        * can't sign because the user doesn't have a private key
        * if encryption or signing failsfor any other reason
        the `errorArray` field can be iterated to print out the errors. See `getErrorReasonString()` and `getErrorString()`
     */
    func encrypt(plainData: Data, ids: [String], ownAddr: String, encryptForMyId: Bool = true) throws -> CryptoObject {
        let plainText = String(data: plainData, encoding: .utf8)
        var pems: [String] = []
        let ownFp: String? = getOwnKeyFP()
        var sigText: String? = nil
        
        // retrieve the certs as pems for each ID (email), certsKeychain stores for each email a fingerprint of the cert of that user and under a fingerprint a cert is stored
        for id in ids {
            if let fp = certsKeychain[id], let pem = certsKeychain[fp] {
                pems.append(pem)
            }
            else{
                throw SMIMEError(message: "No cert for email " + id + "!", errorArray: nil, type: SMIMEError.ErrorType.other)
            }
        }
        
        // if we want to encrypt with the user's own key, retrieve the key and handle errors
        if encryptForMyId {
            if let ownFp = ownFp, let cert = certsKeychain[ownFp] {
                pems.append(cert)
            }
            else {
                throw SMIMEError(message: "Tried to encrypt email with the user's key but no cert for own key present!", errorArray: nil, type: SMIMEError.ErrorType.other)
            }
        }
        
        // check if the user has a certificate
        if ownFp != nil {
            var signObj: CryptoObject? = nil
            do {
                signObj = try sign(plainData: plainData, myEmail: ownAddr, detached: false)
            } catch let error {
                throw error
            }

            // reminder: signObj can't be nil here b/c we always get it unless we get an exception (and exceptions get instantly re-thrown)
            sigText = String(data: signObj!.chiphertext!, encoding: .utf8)
        } else {
            throw SMIMEError(message: "Tried to sign with user's certificate but none was present!", errorArray: nil, type: SMIMEError.ErrorType.other)
        }
        
        // do the actual encryption
        // NOTE: sigText can't be nil b/c we force signing so we either sign successfully or we throw an exception and never reach this code block
        let (encStr, errArr) = encryptWithPem(message: sigText!, certPems: pems)
        if let errArr = errArr, errArr.count > 0 {
            throw SMIMEError(message: "Encryption failed!", errorArray: errArr, type: SMIMEError.ErrorType.encryption)
        }
        if let encStr = encStr {
            return CryptoObject(chiphertext: encStr.data(using: .utf8), plaintext: plainText, decryptedData: plainData, sigState: SignatureState.ValidSignature, encState: EncryptionState.ValidedEncryptedWithCurrentKey, signKey: ownFp, encType: CryptoScheme.SMIME, signedAdrs: [ownAddr])
        }
        throw SMIMEError(message: "Encryption failed! No data...", errorArray: errArr, type: SMIMEError.ErrorType.encryption)

    }

    /**
     A wrapper for the C function for encryption. Takes care of memory managment. Only meant tobe used as a helper in the SMIME class.
     */
    private func encryptWithPem(message: String,certPems: [String]) -> (String?, [UInt]?) {
        let cCertsArray = createCStrArr(sarr: certPems)
        let enc = OpenSSL_encrypt(message, cCertsArray, Int32(certPems.count))
        defer {
            deallocateResult(res: enc)
        }
        
        let result = enc?.pointee;
        
        let encStr: String? = result?.extractField(field: ResultAttribute.output)
        let errArr: [UInt]? = result?.extractField(field: ResultAttribute.errors)
        
        return (encStr, errArr)
    }
    
    /**
     A wrapper for the C function for decryption. Takes care of memory managment. Only meant tobe used as a helper in the SMIME class.
     */
    private func decryptWithPem(message:String, certAsPem: String, keyAsPem:String) -> (String?, [UInt]?) {
        let dec = OpenSSL_decrypt(message, certAsPem, keyAsPem)
        defer {
            deallocateResult(res: dec)
        }
        let result = dec?.pointee;
        
        let decStr: String? = result?.extractField(field: ResultAttribute.output)
        let errArr: [UInt]? = result?.extractField(field: ResultAttribute.errors)
        
        return (decStr, errArr)
    }
    
    /**
     A wrapper for the C function for signing. Takes care of memory managment. Only meant tobe used as a helper in the SMIME class.
     */
    private func signWithPem(message:String, certAsPem: String, keyAsPem:String, detached:Bool) -> (String?, [UInt]?) {
        var detFlag : Int32 = 0
        if detached {
            detFlag = 1
        }
        
        let sig = OpenSSL_sign(message, certAsPem, keyAsPem, detFlag)
        defer {
            deallocateResult(res: sig)
        }
        
        let result = sig?.pointee
        
        let sigStr: String? = result?.extractField(field: ResultAttribute.output)
        let errArr: [UInt]? = result?.extractField(field: ResultAttribute.errors)
        
        return (sigStr, errArr)
    }

    /**
     A wrapper for the C function for verification. Takes care of memory managment. Only meant to be used as a helper in the SMIME class.
    */
    private func verifyWithCApem (message:String, pemCAArr: [String]) -> (String?, [(String, String)]?, [UInt]?) {
        let pemCAArrC = createCStrArr(sarr: pemCAArr)
        let ver = OpenSSL_verify(message, pemCAArrC, Int32(pemCAArr.count))
        
        defer {
            deallocateResult(res: ver)
        }
        
        deallocateCStrArr(arr: pemCAArrC, len:pemCAArr.count)
        
        let result = ver?.pointee;
        
        let verStr: String? = result?.extractField(field: ResultAttribute.output)
        let errArr: [UInt]? = result?.extractField(field: ResultAttribute.errors)
        let certArr: [String]? = result?.extractField(field: ResultAttribute.certificates)
        let fpArr: [String]? = result?.extractField(field: ResultAttribute.fingerprints)
        
        
        var numCerts = 0
        if let n = result?.num_certs {
            numCerts = Int(n)
        }
        var certFPArr: [(String, String)] = []
        
        if (numCerts > 0) {
            for i in 0...(numCerts-1) {
                if let fp = fpArr?[i], let cert = certArr?[i] {
                    certFPArr.append((fp, cert))
                }
            }
        }
        
        return (verStr, certFPArr, errArr)
    }
}
