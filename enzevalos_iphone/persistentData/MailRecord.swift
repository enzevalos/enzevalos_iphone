//
//  CoreMail.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.09.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

/**
 Managed object subclass extension for the mail entity.
 
 Update with a batch request?
 
 TODO:
    Encrypted before?
    uidvalidity
    received
    showMessage -> Set true after ack the warning and user action. Add?
    Different types of content? Text vs HTML?!
 
 self.keyDiscoveryDate = mail?.from.contact?.firstSecureMailReceived
 isNewPubKey -> First mail with a new public key
 add attached Signature?
 */

import CoreData

extension MailRecord {
    static let entityName = "MailRecord"
    
    func update(with mailProperties: MailProperties) {
        // Header
        messageID = mailProperties.messageID
        date = mailProperties.date
        flag = mailProperties.flags
        subject = mailProperties.subject
        xMailer = mailProperties.xMailer
        uID = Int64(mailProperties.uid)

        
        // Content
        body = mailProperties.body
        
        // Security
        signatureStateInt = mailProperties.signatureState
        encryptionStateInt = mailProperties.encryptionState
    }
    
    var signatureState: SignatureState {
        return SignatureState.init(rawValue: signatureStateInt) ?? .NoSignature
    }
    
    var encryptionState: EncryptionState {
        return EncryptionState.init(rawValue: encryptionStateInt) ?? .NoEncryption
    }
    
    var addresses: [AddressRecord] {
        get {
            var addrs = [AddressRecord]()
            addrs.append(contentsOf: addressesFromSet(set: self.toAddresses))
            addrs.append(contentsOf: addressesFromSet(set: self.ccAddresses))
            addrs.append(contentsOf: addressesFromSet(set: self.bccAddresses))
            if let from = self.fromAddress {
                addrs.append(from)
            }
            return addrs
        }
    }
    
    private func addressesFromSet(set: NSSet?) -> [AddressRecord] {
        if let set = set, let addrs = set.allObjects as? [AddressRecord] {
            return addrs
        }
        return []
    }

    var messageFlags: MCOMessageFlag {
        get {
            return MCOMessageFlag.init(rawValue: Int(flag))
        }
    }
    
    /**
     Just a short part of the body. Can be used for a preview.
     In case of trouble there is no preview.
     */
    var shortBodyString: String? {
        guard self.signatureState == .InvalidSignature || self.encryptionState == .UnableToDecrypt else {
            return nil
        }
       
        body = body.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if body.count > 50 {
            body = String(body[..<body.index(body.startIndex, offsetBy: 50)])
        }
        let messageArray = body.components(separatedBy: "\n")
        return messageArray.joined(separator: " ")
    }
}

extension MailRecord: DisplayMail {
    var encryptionType: CryptoScheme? {
        // TODO: Consider unable to decrypt case. Extend mail record?
        if let key = decryptionKey {
            return key.type
        }
        return nil
    }
    
    var signatureKeyID: String? {
        if let key = signatureKey {
            return key.keyID
        }
        return nil
    }
   
    var subjectWithFlag: String {
        var returnString: String = ""
        if !self.isRead {
            returnString.append("🔵 ")
        }
        if MCOMessageFlag.answered.isSubset(of: messageFlags) {
            returnString.append("↩️ ")
        }
        if MCOMessageFlag.forwarded.isSubset(of: messageFlags) {
            returnString.append("➡️ ")
        }
        if MCOMessageFlag.flagged.isSubset(of: messageFlags) {
            returnString.append("⭐️ ")
        }
        return "\(returnString)\(subject)"
    }
    
    var isRead: Bool {
        get {
            let mcoflag = MCOMessageFlag.init(rawValue: Int(flag))
            let value = mcoflag.contains(MCOMessageFlag.seen)
            return value
        }
        
        set {
            var mcoflag = MCOMessageFlag.init(rawValue: Int(flag))
            if newValue {
                mcoflag = mcoflag.update(with: .seen) ?? mcoflag
            } else {
                mcoflag.subtract(.seen)
            }
            
            self.flag = Int16(mcoflag.rawValue)
            
            if let moc = self.managedObjectContext {
                try? PersistentDataProvider.dataProvider.save(taskContext: moc)
            }
            if let folder = self.inFolder?.path {
                MailHandler.init().setFlag(UInt64(self.uID), flags: mcoflag, folder: folder)
            }
        }
    }
    
    typealias C = AddressRecord
    
    var sender: C {
        if let from = self.fromAddress {
            return from
        }
        fatalError("MISSING SENDER!")
    }
    
    var tos: [C] {
        if let array = toAddresses?.allObjects as? [AddressRecord]{
            return array
        }
        return []
    }
    
    var ccs: [C] {
        if let array = ccAddresses?.allObjects as? [AddressRecord]{
            return array
        }
        return []
    }
    
    var bccs: [C] {
        if let array = bccAddresses?.allObjects as? [AddressRecord]{
            return array
        }
        return []
    }
    
    var routingStops: [Landmark] {
        return [] // TODO
    }
    
    var persistentMail: MailRecord? {
        return self
    }
    
    var displayAttachments: [DisplayAttachment] {
        var attachments = [DisplayAttachment]()
        if let coreDataAttachment = self.includedAttachments {
               for att in coreDataAttachment {
                   if let att = att as? AttachmentRecord {
                    attachments.append(att)
                }
            }
        }
        return attachments
    }
    
    var folderType: FolderType {
        if let folder = self.inFolder {
            return folder.type
        }
        return .Other
    }
    
    func markAsRead(isRead: Bool) {
        self.isRead = isRead
        return
    }
    
    var transportEnc: Bool {
        get {
            return true
        }
    }
}

extension MailRecord: Identifiable {}

import SwiftUI
extension MailRecord {
    static var sorting = [NSSortDescriptor(key: "date", ascending: false)]
    
    static func inFolders(folder: String) -> NSPredicate {
        return NSPredicate(format: "inFolder.path == %@", folder)
    }
    
    static func mailsInFolderFetchRequest(folderpath: String) -> FetchRequest<MailRecord> {
        return FetchRequest(entity: MailRecord.entity(), sortDescriptors: sorting , predicate: inFolders(folder: folderpath), animation: nil)
    }
    
    static func allMailsFetchRequest(in folderpath: String) -> NSFetchRequest<MailRecord> {
        let request: NSFetchRequest<MailRecord> = NSFetchRequest<MailRecord>(entityName: MailRecord.entityName)
        request.predicate = inFolders(folder: folderpath)
        request.sortDescriptors = sorting
        return request
    }
    
    static func findMailFetchRequest(msgID: String) -> NSFetchRequest<MailRecord> {
        let request: NSFetchRequest<MailRecord> = NSFetchRequest<MailRecord>(entityName: MailRecord.entityName)
        request.predicate = NSPredicate(format: "messageID == %@", msgID)
        request.sortDescriptors = sorting
        return request
    }
}
/// Functions to manipulate mails
extension MailRecord {
    
    static func delete(with ids: [UInt64], folderPath: String) {
        MailHandler.init().move(mails: ids, fromPath: folderPath, toPath: UserManager.backendTrashFolderPath)
    }
    
    func archive() {
        guard let folder = self.inFolder?.path else {
            return
        }
        MailHandler.init().move(mails: [UInt64(self.uID)], fromPath: folder, toPath: UserManager.backendArchiveFolderPath)
    }
    
    func delete() {
        guard let folder = self.inFolder?.path else {
            return
        }
        MailHandler.init().move(mails: [UInt64(self.uID)], fromPath: folder, toPath: UserManager.backendTrashFolderPath)
    }
}


extension MailRecord {
    var isSingle : Bool { addresses.count <= 2 }
}
