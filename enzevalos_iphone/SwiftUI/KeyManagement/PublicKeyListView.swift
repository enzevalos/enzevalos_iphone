//
//  KeyListView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.12.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct PublicKeyListView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(fetchRequest: PublicKeyRecord.FetchRequest)
    var keys: FetchedResults<PublicKeyRecord>
    
    var body: some View {
        List(keys, id: \.self){ key in
            NavigationLink(
                destination: KeyView(key: key),
                label: {
                    KeyRowView(key: key)
                })
        }
        .navigationBarTitle("Keys", displayMode: .large)
        .navigationBarItems(trailing: AddButton (action: {print("Add!")}))
    }
}

struct KeyListView_Previews: PreviewProvider {
    static let context = PersistentDataProvider.proxyPersistentDataProvider.persistentContainer.viewContext
    
    static var previews: some View {
        PublicKeyListView()
            .environment(\.managedObjectContext, context)
    }
}
