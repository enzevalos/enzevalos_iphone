//
//  FilesCategory.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 21.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

let FilesCategory = CategoryView(
    name    : NSLocalizedString("Category.Files.name", comment: "Dateien"),
    icon    : Image(systemName: "archivebox.fill"),
    searchFields : SearchFilterCollections.Files,
    filter  : filterForWithAttachment
){model in
    FilesCategoryView(model:model)
        
}



fileprivate func filterForWithAttachment(_ email : eMail)->MailCategoryOpenability{
    guard email.displayAttachments.count > 0 else {
        return .cannotOpen
    }
    //TODO: filter for actually displayable attachments
    return .canOpen
}
