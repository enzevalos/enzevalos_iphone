//
//  SecurityIndicator.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 22.01.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation
import SwiftUI



enum SecurityIndicator: Int, StudyParameterProtocol {
    case letter = 0, padlock = 1
    
    static var defaultValue: StudyParameterProtocol {
        get {
            return SecurityIndicator.letter
        }
    }
    
    static func findParameter(rawvalue: Int) -> StudyParameterProtocol {
        switch rawvalue {
        case SecurityIndicator.letter.rawValue:
            return SecurityIndicator.letter
        case SecurityIndicator.padlock.rawValue:
            return SecurityIndicator.padlock
        default:
            return defaultValue
        }
    }
    
    static var name: String {
        get {
            return "indicator"
        }
    }
    
    static var keyName: String {
        get {
            return "indicator mode"
        }
    }
    
    static var numberOfTreatments: UInt32 {
        get {
            return 2
        }
    }
    
    
    var value: Int {
        get {
            return self.rawValue
        }
    }

    func startStudy() {
        return
    }
    
    func imageOfSecureIndicator(background: Bool = false, open: Bool = false, button: Bool = false) -> UIImage {
        switch self {
        case .letter:
            if button {
                return IconsStyleKit.imageOfLetterButton
            }
            if background {
                return IconsStyleKit.imageOfLetterBG
            }
            return IconsStyleKit.imageOfLetter
        case .padlock:
            if button {
                return IconsStyleKit.imageOfPadlockSecureButton
            }
            if background {
                return IconsStyleKit.imageOfPadlockSecureBG
            }
            return IconsStyleKit.imageOfPadlockSecure
        }
    }
    
    func imageOfSecureIndicatorSwiftUI(background: Bool = false, open: Bool = false, button: Bool = false) -> Image {
        return Image(uiImage: imageOfSecureIndicator(background: background, open: open, button: button))
    }
    
    func imageOfCorruptedIndicator(background: Bool = false) -> UIImage {
        switch self {
        case .letter:
            return IconsStyleKit.imageOfLetterCorrupted
        case .padlock:
            return IconsStyleKit.imageOfPadlockError
        }
    }
    
    func imageOfCorruptedIndicatorSwiftUI(background: Bool = false) -> Image{
        return Image(uiImage: imageOfCorruptedIndicator(background: background))
    }
    
    func imageOfInsecureIndicator(background: Bool = false, button: Bool = false) -> UIImage {
        switch self {
        case .letter:
            if button {
                return IconsStyleKit.imageOfPostcardButton
            }
            if background {
                return IconsStyleKit.imageOfPostcardBG
            }
            return IconsStyleKit.imageOfPostcard
        case .padlock:
            if button {
                return IconsStyleKit.imageOfPadlockInsecureButton
            }
            if background {
                return IconsStyleKit.imageOfPadlockInsecureBG
            }
            return IconsStyleKit.imageOfPadlockInsecure
        }
    }
    
    func imageOfInsecureIndicatorSwiftUI(background: Bool = false, button: Bool = false) -> Image {
        return Image(uiImage: imageOfInsecureIndicator(background: background, button: button))
    }
    
    
    func drawOfSecureIndictor(frame: CGRect?, rezising: IconsStyleKit.ResizingBehavior = .aspectFit, color: UIColor = IconsStyleKit.strokeColor, fillbackground: Bool = false, open: Bool) {
        switch self {
        case .letter:
            if open {
                var f = CGRect(x: 0, y: 0, width: 47, height: 43)
                if let frame = frame {
                    f = frame
                }
                IconsStyleKit.drawLetterOpen(frame: f, resizing: rezising, color: color)
            }
            else {
                var f = CGRect(x: 0, y: 0, width: 50, height: 35)
                if let frame = frame {
                    f = frame
                }
                IconsStyleKit.drawLetter(frame: f, resizing: .aspectFit, color: color, fillBackground: fillbackground)
            }
            break
        case .padlock:
            var f = CGRect(x: 0, y: 0, width: 35, height: 50)
            if let frame = frame {
                f = frame
            }
            IconsStyleKit.drawPadlockSecure(frame: f, resizing: rezising, color: color, fillBackground: fillbackground)
        }
    }
    
     func drawOfInSecureIndictor(frame: CGRect?, rezising: IconsStyleKit.ResizingBehavior = .aspectFit, color: UIColor = IconsStyleKit.strokeColor, fillbackground: Bool = false, open: Bool) {
        switch self {
        case .letter:
            var f = CGRect(x: 0, y: 0, width: 49, height: 34)
            if let frame = frame {
                f = frame
            }
            IconsStyleKit.drawPostcard(frame: f, resizing: rezising, color: color, fillBackground: fillbackground)
            break
        case .padlock:
            var f = CGRect(x: 0, y: 0, width: 35, height: 50)
            if let frame = frame {
                f = frame
            }
            IconsStyleKit.drawPadlockInsecure(frame: f, resizing: rezising, color: color, fillBackground: fillbackground)
        }
    }
    
     func drawOfCorruptedIndictor(frame: CGRect?, rezising: IconsStyleKit.ResizingBehavior = .aspectFit, color: UIColor = IconsStyleKit.strokeColor, fillbackground: Bool = false, open: Bool) {
        switch self {
        case .letter:
            var f = CGRect(x: 0, y: 0, width: 49, height: 34)
            if let frame = frame {
                f = frame
            }
            IconsStyleKit.drawLetterCorrupted(frame: f, resizing: rezising, color: color)
            break
        case .padlock:
            var f = CGRect(x: 0, y: 0, width: 35, height: 50)
            if let frame = frame {
                f = frame
            }
            IconsStyleKit.drawPadlockError(frame: f, resizing: rezising, color: color, fillBackground: fillbackground)
        }
    }
    
}
