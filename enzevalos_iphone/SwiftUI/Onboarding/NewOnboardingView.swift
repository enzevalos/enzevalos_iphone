//
//  NewOnboardingView.swift
//  enzevalos_iphone
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

struct NewOnboardingView: View {
    @ObservedObject var press=loginManager()
    var body: some View {
        if press.loggedIn{
            return AnyView(
                VCSwiftUIView(storyboard: "Main", vcID: "RootViewController")
                //AuthenticationScreen()
            )
        }else{
            return AnyView(
                OnboardingIntro(onClick: press.login)
            )
        }
    }
}

struct NewOnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        NewOnboardingView()
    }
}

//this class checks whether we are logged in or nah
class loginManager: ObservableObject {
    @Published var loggedIn=false
    func login(){
        loggedIn=true
    }

    @Published var show = false
}
