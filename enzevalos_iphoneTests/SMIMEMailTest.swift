//
//  SMIMEMailTest.swift
//  enzevalos_iphoneTests
//
//  Created by Oliver Wiese on 12.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import XCTest

@testable import enzevalos_iphone
class SMIMEMailTest: XCTestCase {

    
    let test_key = """
Bag Attributes
    friendlyName: myKey
    localKeyID: B9 C7 A6 B7 A0 03 91 C3 89 79 AA D7 A3 72 C9 E9 BE 00 45 30
Key Attributes: <No Attributes>
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCUFDosMJHlsuIb
ot/2FE4qXg388BqaEtkuJ8Ea18nzMXTT6dYH53wAfIt33OwqxEFpcrN3GndhRJTk
g68wmTvSFP23tVKWNk1SJOZAlEJJH8Q2wKTnt4Bh6y3XEXxZ3csgNM+BL3jItD+l
FZviSkqQyGreXZ+XnQPHRYH0IpzQ8cq7obJnzKMFsVsVltSJDLijUWR8K54lciCo
smz+3bKonCy1hYjuQB/3JEQlWb8Ij4ZiMtUn9Iju/8XnIuspkay83ktUyz7hYAld
K3yvn+tsrOepr98n+aU6bGJSYrbv76R+xpBp2ryRoFbbuzy7wdL8afQJDemKcMQI
87ra30mxAgMBAAECggEAAKj11iqggNixyS1KJcyIVBLOSZ0Cmlfoica7CwEF7SMr
RefxjeBsUSEnpZDu6Mp42eo9O76lhEcyvxwUOsweYWjPFXDjLt4Wt8MIN3FtuZ9A
5TH+6NU8nFBzeFVIrmAb+ASXku1krVIqg98Uo6RgqxQ9791/a594mTHBgwvTMqYf
1024bF9ImI/GhIbzPeX2lrbzHq+HZ6jXg/Jjx0QWfYhyTPqvvCUhyiY0WBvelsQH
orlh7IhnSyFQ6FqgPXzoYWOmHqXODUmmCS6nMct5hRq3Fagvr8aNp2wD6FoYuJGd
FSvIwX+ZMNgzObNA4flY7TfplOaTl6PV6H+KGJ14wQKBgQDEV1hRuKGPB27BH40x
pu0lRmgE/5pdz2Vbg6AuRelZEVCKju33tvXhNrzhPj85jRCPNMPFWQJPlJsQ7lB8
wEANc1PnLffxfwBFs7ke1h3qs6WBkKVtVxXDed1SsMkZ3CmVotSIp3fLzbl9SMYv
Pih3arKI0wwTV8i4Ort5QNyOwQKBgQDBEr7x684we+rISodiDZATmX7fAw490M2u
KQHC3hmAB0Y6htx4E2ti7jNtzV95XkBoW3vShC75xDIZJwXM+yVxYZRQ3dcEMmE7
yR+npAI6pFZGBTsAs9mXUP+JIQVwKbNpTH8x2klb6ylXGJvFFGAhwq4ni4lf1xls
s7x93Vxm8QKBgQCVgCSh5UIvVGuC5fFu8znzfg7Kb44tDDSTYEBhu54Y0dRQDNEq
I8hJepKLuAgQXFdVk8nVxRa7Xd5NIAltVD3xf/VNPHVlD103tcepsUQKaEwHwgoU
nZDWzT43LqGR7VBEvj2y6EGRj85DlVxCsMPDWh0jjf/N4rPVg/MoOrk2gQKBgQCS
HRquQLNJE+5pZ6WvOe5oNsjCAzD67RNLEzOHAsgvkNHAJyGgKaoiM3xuQ3dWhVKe
52T8uxZF5Qm46URFjWcXuC4jSM+ZGHtnYFz5ZHBfatDzqq+tZ25rSlworfnMmXJy
ZPb5hmXwDza4+CGiczoRMqDTwpDTHBjcn+UtJ6HvMQKBgACDZQgWdYXLrGl6qB6A
X0Xb6hoEuz7BeP+ZW3tspyjySLe8SXDf8QXQXvyfivo1N3VxbWuTSf/e4U2SqXXH
+/kFUS+KgKMKWefR+MJ3+Do8AGf4lrqf9AsSnrTtLskuS9aab9lZ1Mm/Q4P79K2J
eforhMXYki+DqiwhRf6Q3rxF
-----END PRIVATE KEY-----
Bag Attributes
    friendlyName: myKey
    localKeyID: B9 C7 A6 B7 A0 03 91 C3 89 79 AA D7 A3 72 C9 E9 BE 00 45 30
subject=C = DE, O = A MAILTEST, CN = myKey

issuer=C = DE, O = A MAILTEST, CN = myTestCA

-----BEGIN CERTIFICATE-----
MIIDpzCCAo+gAwIBAgIIkMopuuUM9N4wDQYJKoZIhvcNAQELBQAwNTELMAkGA1UE
BhMCREUxEzARBgNVBAoMCkEgTUFJTFRFU1QxETAPBgNVBAMMCG15VGVzdENBMB4X
DTIwMDMwNTEzMzUxNVoXDTQwMDIyOTEzMzUxMVowMjELMAkGA1UEBhMCREUxEzAR
BgNVBAoMCkEgTUFJTFRFU1QxDjAMBgNVBAMMBW15S2V5MIIBIjANBgkqhkiG9w0B
AQEFAAOCAQ8AMIIBCgKCAQEAlBQ6LDCR5bLiG6Lf9hROKl4N/PAamhLZLifBGtfJ
8zF00+nWB+d8AHyLd9zsKsRBaXKzdxp3YUSU5IOvMJk70hT9t7VSljZNUiTmQJRC
SR/ENsCk57eAYest1xF8Wd3LIDTPgS94yLQ/pRWb4kpKkMhq3l2fl50Dx0WB9CKc
0PHKu6GyZ8yjBbFbFZbUiQy4o1FkfCueJXIgqLJs/t2yqJwstYWI7kAf9yREJVm/
CI+GYjLVJ/SI7v/F5yLrKZGsvN5LVMs+4WAJXSt8r5/rbKznqa/fJ/mlOmxiUmK2
7++kfsaQadq8kaBW27s8u8HS/Gn0CQ3pinDECPO62t9JsQIDAQABo4G9MIG6MBsG
A1UdEQQUMBKBEHVsbGltdWVsbEB3ZWIuZGUwKQYDVR0OBCIEIP0eILP4r0WN95za
SSmuP0yzs/Vdh33rAaa0B3tiGF7SMHAGA1UdIwRpMGeAIPaFmApv+CilhWeSrOpY
FZxKcDjfKwXv8OmoFbvkA8nooTmkNzA1MQswCQYDVQQGEwJERTETMBEGA1UECgwK
QSBNQUlMVEVTVDERMA8GA1UEAwwIbXlUZXN0Q0GCCH6WoapleQq4MA0GCSqGSIb3
DQEBCwUAA4IBAQAvIknw5G8YV85/5LR90tCocane3jse5gE9AEoqSWDUemfnhmJd
MOOMjZvhQNVA7lV4X4jv1wbqLPKOpKAag5UHjXls3MfgTGyehhKvNYWpvoXdeEWU
5sIU7QYAUlzVgZ+KJZ4ImmnF2wEiiQU4GsitRlUzGYd5R/Zux+xZZcSbmHYZT5nn
xq9K9BRmWeM6eeJ99m/ZKiLH1Vmc5oC5gPQDXj0RNJTfBXiSp0HB2DMEUITvCkxh
cNFLTtHfTLPn1WsSS/EvUqc2BszXXI3df7Axftp3Dd2VtOC04Nb48AGJbxY6ryu7
XFB7TFYvphnuQZKw5mG0FNyADnP0CgGPobTI
-----END CERTIFICATE-----
Bag Attributes: <Empty Attributes>
subject=C = DE, O = A MAILTEST, CN = myTestCA

issuer=C = DE, O = A MAILTEST, CN = myTestCA

-----BEGIN CERTIFICATE-----
MIIC5TCCAc0CCH6WoapleQq4MA0GCSqGSIb3DQEBCwUAMDUxCzAJBgNVBAYTAkRF
MRMwEQYDVQQKDApBIE1BSUxURVNUMREwDwYDVQQDDAhteVRlc3RDQTAeFw0yMDAz
MDUxMzM1MTFaFw00MDAyMjkxMzM0NTlaMDUxCzAJBgNVBAYTAkRFMRMwEQYDVQQK
DApBIE1BSUxURVNUMREwDwYDVQQDDAhteVRlc3RDQTCCASIwDQYJKoZIhvcNAQEB
BQADggEPADCCAQoCggEBAKI5WXFBwGEnvAaFCFf9LhTO+w9uD09bt40rG1x2vTdF
xIO6aHsLuHbXEAfFgXzcF5ifdKlFwkIs8YV3R1Eqe7Ehoi393js9idqx5zefgHw4
Q/e6XbvAFWc0BwHNCL9EzwJzKZoQi2HxxWg/wMpu7urWQRxtYpxsp/Hr/oJaADBI
j/H/brx5Hybwy38T0IhD2FFRHFjmlJmnvawrzKk76/bMLg2TNBUIj/dNPdnfTMEk
RiMJJQ4tP4FPH0WoOoPrQNA2ijKN+nf/QyRzx8dEcOaTtsjhXDxsZmRooyuCL08x
GO/mXF9b6Mz5sCUjYetAfyhdytO2VzrMe5/oQYrjSJUCAwEAATANBgkqhkiG9w0B
AQsFAAOCAQEAmtn5vDCC3PCx9xBCA4R2YaQ34wjoCkVAwLzAmHDoXIo9kbEaARhu
/ikNDCtNd394HRjWZlVunWmyzKba7cZ53OY2q6lLwAyFFRC3+RF7MaA4OIiGF5Cx
t+NcrmzKm4cHJvi+DUxOqskl16fwycspIqokru3+dv3tqfZ8kxIOe3/hILLWYCbR
jRqChUmvLzwDtP8bFJ5COad91Cfx9DmHSnoPkhdg0f+2x+0eajjEx8fRCppHOSPK
5O2AvjjS4akSRqVwlqFUqgDvO1PRhLw31i14EbkO3q1OC/4t00HJnszqzbwGWTrf
n1O3czuVl7rPXrJn0A/MVI2ReKOQeIAYMg==
-----END CERTIFICATE-----
"""
    
    var testCA = """
-----BEGIN CERTIFICATE-----
MIIC5TCCAc0CCH6WoapleQq4MA0GCSqGSIb3DQEBCwUAMDUxCzAJBgNVBAYTAkRF
MRMwEQYDVQQKDApBIE1BSUxURVNUMREwDwYDVQQDDAhteVRlc3RDQTAeFw0yMDAz
MDUxMzM1MTFaFw00MDAyMjkxMzM0NTlaMDUxCzAJBgNVBAYTAkRFMRMwEQYDVQQK
DApBIE1BSUxURVNUMREwDwYDVQQDDAhteVRlc3RDQTCCASIwDQYJKoZIhvcNAQEB
BQADggEPADCCAQoCggEBAKI5WXFBwGEnvAaFCFf9LhTO+w9uD09bt40rG1x2vTdF
xIO6aHsLuHbXEAfFgXzcF5ifdKlFwkIs8YV3R1Eqe7Ehoi393js9idqx5zefgHw4
Q/e6XbvAFWc0BwHNCL9EzwJzKZoQi2HxxWg/wMpu7urWQRxtYpxsp/Hr/oJaADBI
j/H/brx5Hybwy38T0IhD2FFRHFjmlJmnvawrzKk76/bMLg2TNBUIj/dNPdnfTMEk
RiMJJQ4tP4FPH0WoOoPrQNA2ijKN+nf/QyRzx8dEcOaTtsjhXDxsZmRooyuCL08x
GO/mXF9b6Mz5sCUjYetAfyhdytO2VzrMe5/oQYrjSJUCAwEAATANBgkqhkiG9w0B
AQsFAAOCAQEAmtn5vDCC3PCx9xBCA4R2YaQ34wjoCkVAwLzAmHDoXIo9kbEaARhu
/ikNDCtNd394HRjWZlVunWmyzKba7cZ53OY2q6lLwAyFFRC3+RF7MaA4OIiGF5Cx
t+NcrmzKm4cHJvi+DUxOqskl16fwycspIqokru3+dv3tqfZ8kxIOe3/hILLWYCbR
jRqChUmvLzwDtP8bFJ5COad91Cfx9DmHSnoPkhdg0f+2x+0eajjEx8fRCppHOSPK
5O2AvjjS4akSRqVwlqFUqgDvO1PRhLw31i14EbkO3q1OC/4t00HJnszqzbwGWTrf
n1O3czuVl7rPXrJn0A/MVI2ReKOQeIAYMg==
-----END CERTIFICATE-----
"""
    
    var testEvilCA = """
-----BEGIN CERTIFICATE-----
MIIC4TCCAckCCDWaS3dso6D3MA0GCSqGSIb3DQEBCwUAMDMxCzAJBgNVBAYTAkRF
MRMwEQYDVQQKDApBIE1BSUxURVNUMQ8wDQYDVQQDDAZldmlsQ0EwHhcNMTkwNzI5
MTUzODA1WhcNMTkwODA1MTUzODA1WjAzMQswCQYDVQQGEwJERTETMBEGA1UECgwK
QSBNQUlMVEVTVDEPMA0GA1UEAwwGZXZpbENBMIIBIjANBgkqhkiG9w0BAQEFAAOC
AQ8AMIIBCgKCAQEAv0W8s3Dav5brYO+MZQ+DiNPSf0Mgd6g6zR1vdk+sPTuJbAZ2
owxR376WKEayBp3jF3omitej4ltuAoepQ7xgsFLVJDijAxeLeoAzPc+J8qDcY1NF
4up9+DpN7L0L3rgfU0/I8Az4jpR8pHJTHmu0L074Er5+Vk2cBvxSY3srp2NnEga1
Fudun9YUYfOp432Ac7xv/6KYz99ocbI+F/egnHQm49GnyFs1zxCuh9qfTeCFO644
dUlkMXfQF7sdZmxdxwKIF4D8AroAecFXWei4PtNIJpPvr/UdCBIyEmZroL2fMnPg
zMtChlEG5Ryw7UuwKrQGs903n3nxvopr4mOumwIDAQABMA0GCSqGSIb3DQEBCwUA
A4IBAQCFnuNL7hhJjhvPpeMPFahMdadA1OWRLIe0XdAJI8Pvlx3f8XR5udcGS2Mi
r5znDhWHT5fFyYTj0JZQUf5GaYrucZDh2M2lXzuazhh5J+PSgvMez1fqfC0pp3Iy
IIqIxZCzGaZp9A7CkAO1qyDqM3fAtkJ0f6JoIrUN9Q4PphDpi6vlRDIoHROmK/Xr
QVzf2Y0lnKGQisw048XPLWqGagu8ZO0n6GMmyldnwVDEsQomndWDrW0EHEV2s/fq
bgvyhVRenp1O5IH3nOyXm8vR2FWOEHwR686l8Mxy24APzzn5K7nPeyBx0+ZpyqYh
aqQKdT1pty4gKWz3zSNTupVsyA/t
-----END CERTIFICATE-----
"""

    var test_key_other = """
-----BEGIN CERTIFICATE-----
MIIDsDCCApigAwIBAgIIAUuththJ3rkwDQYJKoZIhvcNAQELBQAwNTELMAkGA1UE
BhMCREUxEzARBgNVBAoMCkEgTUFJTFRFU1QxETAPBgNVBAMMCG15VGVzdENBMB4X
DTE5MDczMDE0MTM0MloXDTE5MDgwNjE0MTM0MlowNjELMAkGA1UEBhMCREUxEzAR
BgNVBAoMCkEgTUFJTFRFU1QxEjAQBgNVBAMMCW90aGVya2V5MTCCASIwDQYJKoZI
hvcNAQEBBQADggEPADCCAQoCggEBAOaXgg4cD8V2Fg/n6v/YuHsOTa7vlrSragOa
eVwcYzAATRIQAP6suakRkr5D2a3u2N+EBOawCvZPeQZwZMtKpnEdIKdkmUi/OYMS
y6D7xRL/MizwsNP/YvRfwByPmYRo30YCwLeJEEIlPDw5zwfIiqyl31mOrx+5eebv
t83TCJzU7ORZv/OqFEwGt4FcWk8FydmOJxW3My3A2GUWPOmnrvOGamRoKkN/O7xO
9qaQSkRWhuyb6R3EIDo0WfsM263aEhbVHOsCk7PAtzgW18+3qvK5ZrENk3B+F3+2
fh1Qola266kX6/ZbB+/eOPJlWBh2NZHhBZ+Z/wr69tCSTJ80au0CAwEAAaOBwjCB
vzAgBgNVHREEGTAXgRVvdGhlcmFjY0BkcmVuZ2Vscy5uZXQwKQYDVR0OBCIEIGAb
47YtbmauxZ5hYka1/wYF9xh8+pJ+Zc0eRqkrgbeNMHAGA1UdIwRpMGeAILLLgvdd
qVj9AJE8CrP9PYeShvHigM+CczpLs9EYh+bkoTmkNzA1MQswCQYDVQQGEwJERTET
MBEGA1UECgwKQSBNQUlMVEVTVDERMA8GA1UEAwwIbXlUZXN0Q0GCCKtx6aDujfcT
MA0GCSqGSIb3DQEBCwUAA4IBAQCaaBwEyegcUwTsHFiZGq64tvhRBNdiaXIGZk64
+Jw+FyfgcMp80VIhAdKuPtodtMWRQFl6F7xI3Gkvhosv73jWSwl05iGvxFWN0wNr
N2nZ8JPCHNqQQkuGaHEM3B591EifrswGLhIaoSihHIBFlBx6e0fgBmSTHYbx9usI
XyjDyGWkq0z72KGjifqow6zZKDFeX2Br3K1hP2V2ObnVkL+Arj+lBConkD+Ja9fa
vfbCKFRMmniHBSKcaTdHu/yzFaS8zGdWaMXIVzidPrIpqiv4KC2iVWqizZX3u6ej
nVDuRRggGA08TMPaJa3ziQo+zcjFkeHyd+7vw1w7ogSPlwx8
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIC5TCCAc0CCKtx6aDujfcTMA0GCSqGSIb3DQEBCwUAMDUxCzAJBgNVBAYTAkRF
MRMwEQYDVQQKDApBIE1BSUxURVNUMREwDwYDVQQDDAhteVRlc3RDQTAeFw0xOTA3
MjkxNTM4MDRaFw0xOTA4MDUxNTM4MDRaMDUxCzAJBgNVBAYTAkRFMRMwEQYDVQQK
DApBIE1BSUxURVNUMREwDwYDVQQDDAhteVRlc3RDQTCCASIwDQYJKoZIhvcNAQEB
BQADggEPADCCAQoCggEBAKJOatbPDL3lw/7tqeugaRHxqMgx7/DOLdSnDnK66Eys
GrFcp43N3tQL+0rm77tz/jcyVkHAZF9Sv1c/tIZay/J975cbpHPMraJVbM+/mL6f
14wrwU4rAJLILDkZ3HqkKKnrGmfoKrB1RZ9LA+NLtQlfiuP4C8Et7bOZOkuGdq0T
aygotW0ELeQPbVWXV/GcP6Xd0FnKQF7m9ioRn9MKxu4VDm3hSKDtN1zyXQ1HXhQV
/UGODSaFyUgoCTnz7kFcF3hY9Dw7M55CCM3U0PnfxuxUdn8qMH1lym/G5K5xaVLV
fCjsly8GUUfHtls3Y0F7c6NE5CPkjub2HNNdCW+JD0ECAwEAATANBgkqhkiG9w0B
AQsFAAOCAQEAAAVXNPtXVaUP9k2wNZAw4hOKp5n0EdeOkmtMdedvuQF39d+cHJkL
HjyTp3zd22S1EbgFgNB6RPlRPXie4aivcDGSAGc88NF2xEOezbLZr30x/KAc94OG
ssKjin+X8aMoZSppUxVHI1j6gzydvEWhVBIRUjgd4F5uiV+TBOAOj39xGkpCXIBg
bLRdalYjzxWYb5cR5qniCVbKrRg2oQWghStEuuRTww9SQIeSerb0lqHF3gaL82pG
F4K1I0HbAXgq/D+xIj6lbqZlchI8a4qQq2Ic75yTP/3Nt/Ls6Dt17eT6O+tA9W4e
tag/vFWjhTwGBIjjvyrjTM3fwvUN3HIZeg==
-----END CERTIFICATE-----
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEA5peCDhwPxXYWD+fq/9i4ew5Nru+WtKtqA5p5XBxjMABNEhAA
/qy5qRGSvkPZre7Y34QE5rAK9k95BnBky0qmcR0gp2SZSL85gxLLoPvFEv8yLPCw
0/9i9F/AHI+ZhGjfRgLAt4kQQiU8PDnPB8iKrKXfWY6vH7l55u+3zdMInNTs5Fm/
86oUTAa3gVxaTwXJ2Y4nFbczLcDYZRY86aeu84ZqZGgqQ387vE72ppBKRFaG7Jvp
HcQgOjRZ+wzbrdoSFtUc6wKTs8C3OBbXz7eq8rlmsQ2TcH4Xf7Z+HVCiVrbrqRfr
9lsH79448mVYGHY1keEFn5n/Cvr20JJMnzRq7QIDAQABAoIBADUXMwFuoo7zFjdJ
TUjkq+zhKS+ayBcG1FzKByEXBBs8JoJ/HsuEFyb+/ctCse6i7Fnh+oLmi0+UkqfR
3tMTd+jZLPh9Z/Fg3av4+tswvHoYOYQxUnUs+qqLFR8+TLk4sBddcOOQ8iZYjlrL
cIg5OvxvIFJtUJK8UH3EOYxxBy2cRHw44Fg54E0CpI3LDympYUnkT6YBB+MddqF5
mvo85XO2WoN6m+JbVvS/jDBEI707ggAjKqHoomyzEQB0lrvksPtYfUuhGqfFRrdY
nHAfH4Nxrv4FB61GldyNBphQFpMYEtOwSGLSXEaMzHPrmaAegQminJBuQaODKCeh
IVht6d0CgYEA/6CyWfkHRRmAV/hm1fndZLcZ3oN2JIJjznZbB0R1IuaXTl7kbXvA
mdEeLD9OJayhbSIkpBSs7g8NsHPqcew4gR1lm6WhSvNg6qCg5ahLj3siyrsL3k/P
jHAb6qa68L2kc5HhI29nZ3Iqt4hL/IsWD9UoRS3zRu13A4V68MWRbMMCgYEA5u16
OamANO1sXyeDTXPOX9HSVXXKZiBsQvkpT6x+tnSIHzsgBaZaDX+myk4B5IF19191
X5YFzNeIw8PykNZ9+UfchIJvEp11l5z8cGcNYBXCTmW+j62y4llFqKGMS+aP1Q0r
E1QXhnvRE8SYRWHoDzQ4HLRQT2exuvPum1yCDo8CgYEAsveTxnAWCee3tl8/yBE+
UjPrFaEeRqNPMw4j3uurwrqENiczb/9TMZ15rWrI0JGTFu/vjTy4cZbyc5ZEDmxe
hu7I43bLfMj2Em5RF1iDRjnkeuEHhyaDXYRj1x3SBZgQGaWJZSCq2X7Rr1rqMC5E
TONkZUKtbvvz9fAuyYDYtrcCgYEAsed4Bx4/SAc1OyVcsXmrHwStjusdD4qg/QOp
QNk6EWQ7ThwlslSTAd066JYqPRlDgV0INqiENWhC09aMc+rayAYFrAw0d9nYswHd
NNcfZcu9ktotcdE2g8yUrrIuPHcWjeMEMXLSYOfYjesiCCnCNpxbMjnzuQjA0q+E
PiqJg58CgYAFvkmTfr8Wp3SqmQmw5TuNaRLr1IVk/cMcFjXE3AOFcPuPnYIlJ+L6
iwxRK85BnWBVkQIMvxrbb2Ro04buiizELRoKsDdkSdYXFSmSOvw1pvT8IpNJjNRU
AYIHvW6qRLTsSR6BZZS3pqGXYue7fE0vj4HJ2IEpj05qQ5RXrD57Wg==
-----END RSA PRIVATE KEY-----
"""
     
    let att_sig_invalid = """
MIME-Version: 1.0
Content-Disposition: attachment; filename="smime.p7m"
Content-Type: application/pkcs7-mime; smime-type=signed-data; name="smime.p7m"
Content-Transfer-Encoding: base64

MIAGCSqGSIb3DQEHAqCAMIACAQExDTALBglghkgBZQMEAgEwgAYJKoZIhvcNAQcB
oIAkgAQdSGVsbG8gd29ybGQgd2hhdGV2ZXIgdGhlIGZ1Y2sAAAAAAACgggOrMIID
pzCCAo+gAwIBAgIIkMopuuUM9N4wDQYJKoZIhvcNAQELBQAwNTELMAkGA1UEBhMC
REUxEzARBgNVBAoMCkEgTUFJTFRFU1QxETAPBgNVBAMMCG15VGVzdENBMB4XDTIw
MDMwNTEzMzUxNVoXDTQwMDIyABCzMzUxMVowMjELMAkGA1UEBhMCREUxEzARBgNV
BAoMCkEgTUFJTFRFU1QxDjAMBgNVBAMMBW15S2V5MIIBIjANBgkqhkiG9w0BAQEF
AAOCAQ8AMIIBCgKCAQEAlBQ6LDCR5bLiG6Lf9hROKl4N/PAamhLZLifBGtfJ8zF0
0+nWB+d8AHyLd9zsKsRBaXKzdxp3YUSU5IOvMJk70hT9t7VSljZNUiTmQJRCSR/E
NsCk57eAYest1xF8Wd3LIDTPgS94yLQ/pRWb4kpKkMhq3l2fl50Dx0WB9CKc0PHK
u6GyZ8yjBbFbFZbUiQy4o1FkfCueJXIgqLJs/t2yqJwstYWI7kAf9yREJVm/CI+G
YjLVJ/SI7v/F5yLrKZGsvN5LVMs+4WAJXSt8r5/rbKznqa/fJ/mlOmxiUmK27++k
fsaQadq8kaBW27s8u8HS/Gn0CQ3pinDECPO62t9JsQIDAQABo4G9MIG6MBsGA1Ud
EQQUMBKBEHVsbGltdWVsbEB3ZWIuZGUwKQYDVR0OBCIEIP0eILP4r0WN95zaSSmu
P0yzs/Vdh33rAaa0B3tiGF7SMHAGA1UdIwRpMGeAIPaFmApv+CilhWeSrOpYFZxK
cDjfKwXv8OmoFbvkA8nooTmkNzA1MQswCQYDVQQGEwJERTETMBEGA1UECgwKQSBN
QUlMVEVTVDERMA8GA1UEAwwIbXlUZXN0Q0GCCH6WoapleQq4MA0GCSqGSIb3DQEB
CwUAA4IBAQAvIknw5G8YV85/5LR90tCocainejse5gE9AEoqSWDUemfnhmJdMOOM
jZvhQNVA7lV4X4jv1wbqLPKOpKAag5UHjXls3MfgTGyehhKvNYWpvoXdeEWU5sIU
7QYAUlzVgZ+KJZ4ImmnF2wEiiQU4GsitRlUzGYd5R/Zux+xZZcSbmHYZT5nnxq9K
9BRmWeM6eeJ99m/ZKiLH1Vmc5oC5gPQDXj0RNJTfBXiSp0HB2DMEUITvCkxhcNFL
TtHfTLPn1WsSS/EvUqc2BszXXI3df7Axftp3Dd2VtOC04Nb48AGJbxY6ryu7XFB7
TFYvphnuQZKw5mG0FNyADnP0CgGPobTIMYICUTCCAk0CAQEwQTA1MQswCQYDVQQG
EwJERTETMBEGA1UECgwKQSBNQUlMVEVTVDERMA8GA1UEAwwIbXlUZXN0Q0ECCJDK
KbrlDPTeMAsGCWCGSAFlAwQCAaCB5DAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcB
MBwGCSqGSIb3DQEJBTEPFw0yMDAzMzExMTI2MDlaMC8GCSqGSIb3DQEJBDEiBCCE
OK7BZ1gJUVMov9b6uJu/b4WfYqzmzgJ0a2czduMPvTB5BgkqhkiG9w0BCQ8xbDBq
MAsGCWCGSAFlAwQBKjALBglghkgBZQMEARYwCwYJYIZIAWUDBAECMAoGCCqGSIb3
DQMHMA4GCCqGSIb3DQMCAgIAgDANBggqhkiG9w0DAgIBQDAHBgUrDgMCBzANBggq
hkiG9w0DAgIBKDANBgkqhkiG9w0BAQEFAASCAQApVxIN94BLteGD40M/GgT21C29
7+rtEInq+dkB6XPEHC1GowgMZA9VPFrj0xenkj1qpZvqHcwNSjukz6Y8U4we9EzG
dZY6sEVhuoEct5TXznciLNarhikhqmqxmQvVy+G5w7AiQBrd7zePDI0PJ8AKoinR
klsTYRIi2JPBgcsoCRtxQqTLmFQNKtF5vJDTrv8Y42Gue+elTYTHhJi6bj4Yp9+T
qFj7X3hz0XayCj/CPiyGdJcaQvjIbDVI2cE0pY0jaIIJrIRVkPyLxieKBQYfUp/b
KzZCr1wZb2W1JA+vTk7/wHp33Jll0EZ4DV0BgmeSmnXknJM3wZVAa1z7e5o0AAAA
AAAA
"""
    
    let det_sig_invalid = """
------56F3BC21B2CE83EB9E02E24139D860BC
Hello world whatever the fuckaaDadaDAd
------56F3BC21B2CE83EB9E02E24139D860BC
Content-Type: application/pkcs7-signature; name="smime.p7s"
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename="smime.p7s"

MIIGNgYJKoZIhvcNAQcCoIIGJzCCBiMCAQExDTALBglghkgBZQMEAgEwCwYJKoZI
hvcNAQcBoIIDqzCCA6cwggKPoAMCAQICCJDKKbrlDPTeMA0GCSqGSIb3DQEBCwUA
MDUxCzAJBgNVBAYTAkRFMRMwEQYDVQQKDApBIE1BSUxURVNUMREwDwYDVQQDDAht
eVRlc3RDQTAeFw0yMDAzMDUxMzM1MTVaFw00MDAyMjkxMzM1MTFaMDIxCzAJBgNV
BAYTAkRFMRMwEQYDVQQKDApBIE1BSUxURVNUMQ4wDAYDVQQDDAVteUtleTCCASIw
DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJQUOiwwkeWy4hui3/YUTipeDfzw
GpoS2S4nwRrXyfMxdNPp1gfnfAB8i3fc7CrEQWlys3cad2FElOSDrzCZO9IU/be1
UpY2TVIk5kCUQkkfxDbApOe3gGHrLdcRfFndyyA0z4EveMi0P6UVm+JKSpDIat5d
n5edA8dFgfQinNDxyruhsmfMowWxWxWW1IkMuKNRZHwrniVyIKiybP7dsqicLLWF
iO5AH/ckRCVZvwiPhmIy1Sf0iO7/xeci6ymRrLzeS1TLPuFgCV0rfK+f62ys56mv
3yf5pTpsYlJitu/vpH7GkGnavJGgVtu7PLvB0vxp9AkN6YpwxAjzutrfSbECAwEA
AaOBvTCBujAbBgNVHREEFDASgRB1bGxpbXVlbGxAd2ViLmRlMCkGA1UdDgQiBCD9
HiCz+K9Fjfec2kkprj9Ms7P1XYd96wGmtAd7Yhhe0jBwBgNVHSMEaTBngCD2hZgK
b/gopYVnkqzqWBWcSnA43ysF7/DpqBW75APJ6KE5pDcwNTELMAkGA1UEBhMCREUx
EzARBgNVBAoMCkEgTUFJTFRFU1QxETAPBgNVBAMMCG15VGVzdENBggh+lqGqZXkK
uDANBgkqhkiG9w0BAQsFAAOCAQEALyJJ8ORvGFfOf+S0fdLQqHGp3t47HuYBPQBK
Kklg1Hpn54ZiXTDjjI2b4UDVQO5VeF+I79cG6izyjqSgGoOVB415bNzH4ExsnoYS
rzWFqb6F3XhFlObCFO0GAFJc1YGfiiWeCJppxdsBIokFOBrIrUZVMxmHeUf2bsfs
WWXEm5h2GU+Z58avSvQUZlnjOnniffZv2Soix9VZnOaAuYD0A149ETSU3wV4kqdB
wdgzBFCE7wpMYXDRS07R30yz59VrEkvxL1KnNgbM11yN3X+wMX7adw3dlbTgtODW
+PABiW8WOq8ru1xQe0xWL6YZ7kGSsOZhtBTcgA5z9AoBj6G0yDGCAlEwggJNAgEB
MEEwNTELMAkGA1UEBhMCREUxEzARBgNVBAoMCkEgTUFJTFRFU1QxETAPBgNVBAMM
CG15VGVzdENBAgiQyim65Qz03jALBglghkgBZQMEAgGggeQwGAYJKoZIhvcNAQkD
MQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMjAwMzMxMTEyNjA5WjAvBgkq
hkiG9w0BCQQxIgQghDiuwWdYCVFTKL/W+ribv2+Fn2Ks5s4CdGtnM3bjD70weQYJ
KoZIhvcNAQkPMWwwajALBglghkgBZQMEASowCwYJYIZIAWUDBAEWMAsGCWCGSAFl
AwQBAjAKBggqhkiG9w0DBzAOBggqhkiG9w0DAgICAIAwDQYIKoZIhvcNAwICAUAw
BwYFKw4DAgcwDQYIKoZIhvcNAwICASgwDQYJKoZIhvcNAQEBBQAEggEAKVcSDfeA
S7Xhg+NDPxoE9tQtve/q7RCJ6vnZAelzxBwtRqMIDGQPVTxa49MXp5I9aqWb6h3M
DUo7pM+mPFOMHvRMxnWWOrBFYbqBHLeU1853IizWq4YpIapqsZkL1cvhucOwIkAa
3e83jwyNDyfACqIp0ZJbE2ESItiTwYHLKAkbcUKky5hUDSrRebyQ067/GONhrnvn
pU2Ex4SYum4+GKffk6hY+194c9F2sgo/wj4shnSXGkL4yGw1SNnBNKWNI2iCCayE
VZD8i8YnigUGH1Kf2ys2Qq9cGW9ltSQPr05O/8B6d9yZZdBGeA1dAYJnkpp15JyT
N8GVQGtc+3uaNA==

------56F3BC21B2CE83EB9E02E24139D860BC--
"""
    
    let enc_test_key_pwd = "testpwd"
    let enc_test_key =
"""
-----BEGIN ENCRYPTED PRIVATE KEY-----
MIIFLTBXBgkqhkiG9w0BBQ0wSjApBgkqhkiG9w0BBQwwHAQIjRzqyJoIb6YCAggA
MAwGCCqGSIb3DQIJBQAwHQYJYIZIAWUDBAEqBBA0jxeh6SHCELOVVrAOw8wYBIIE
0NJtVjgMrMblxLPoF4N5Wn44qJb6v/DlISrXM0OiFL+E35BfUaQk7Tb/vWNuT7Ak
sfYer3XvL4DxvoEJNqUaxpKbyAWNmcROdfvZEu2gwX9rY6/5MNm+NJPn5ICpv06O
FrJn60RtHGG6TYtN7Yqy+8+2IHumQ8jBjt4nDoBcIriD7tyTZJarzrnlytMIfDP0
kyiNQpJLEcBNazpn/3GLIdlJvjdup0hYoGoaMcKtTuc1JGQMwehu/P9KxaSvkyqN
4DStqB5Ua0IOzZdk+BJhodvwp3FTU0+bxbEVIy6NSgHCSfroyZ6fXZzazKTvqbg+
gIB0dEGpELEjaksltbKsgaBIYahs9iT1RSEKyp6DcReIJM+bkQa6RHQvFBmqLKP2
sZgyDzrrNUyYWXsxEGmcbJ6Irsa2WQf6G2HhzOTs3rWGpST0Nk6DwFWZTy0U3uBZ
OgVP9VEFc7yL0RQPV2Ppq54k9WBZgNYky/pteb1hq2RrZ/y/ez9zMHMlmkpRNwIQ
2EX/dcp9Y2QNZAumZt+Zh7DKLgm464n0srWn1mZOWsGy/7oajqPakay42EvVTCvB
iDjhm6OKFZChe6mietj30tNRbBC1LbpwGq21uhGihQjNp7CSWcf7osniN0UCJJ43
BXy2g71EeVFLiBwl2qzoDNwrsRygUV0yomGvS6ic8jESm2D8J5fb1on0VKr/CBC8
4vBHTHTW2JMOy8RyVZTBZiVeH7CiA54WwJBmARCxrzhvCLG14IE5gvfmbSm7+N+8
ZNUcqEAt8vrdBkaLcU0knRv3r+B2/oM+9vHLqebijibHXt26fTJ4NLpjNutIbNxh
uDVUjMeH/A0zVg6jevbsANlB4H2TV/swVVk+qkRQfvDsgcCS89jr7rlU7kgmn0v8
Ofs8JLmR7PEA3i/QLyOLNnu73qszI0o/Ag1Q2HWY2/uXxxFUTRXgiGl5Oi5wY2TZ
Rj8Ouitm0Jf9YQu4tL0zpvulgVM6jkbJKo9uYysGdS0jQVaHl3AlRnE8EYSyjJwW
QGkwcaLetcCp0kX2td/kOAXGGS5bvVHcn1vnYih1ic/Ibu1oeCOoypHouVD0jsex
gg5gNifL+63BElhpSTdKO6u5UZWzJDMlmeI62Bgp7vPlV936IgtGzCx9zkSarIZn
4u0kbWHjscVA+L3wC91IRG6Klaa27f4o2gdSGwiMo+rd37mo1Cs2u7y4/K1TvpKh
6jOlB65mIvPzzEJH1wTLkuhrfITnr2M0ddRW1am+BVOiBOjE8O+UQ1HLIeSHCmXh
6Gm+rJVvVV4UEb/gqRneAaIvS0X851aVgkyb4X3S76Tab1nNzwD/gJUb3ODPQ9Tc
ew4asU75zH6zg3frxHmdtw27Q+R0H0hxVOf0iF1znS2PBpHCg9jvtMMxRcHktpOC
dGuNDcuAtwqQN48XAgP9EkLHwRkJyN+KCSwzLzbhUKsTK3aUa9ON+YJhwyDbuNZT
tCS6rxHDe77j4fzb9jXyfM99GK9YN36/spUfl5UijDoou5rM4Tt92VBMn8iMNXx4
OxeGuSvQoBUdP50ZspBBrf2+simuJLKMQ8kVXx8TuJqXe1FQPvFWDX5lBAf57BET
PkfA6mR7rtcyIbHi34tfkCv/qolV3QivMHov0IJpRyNO
-----END ENCRYPTED PRIVATE KEY-----
"""
    let provider = PersistentDataProvider()

    
    var userAdr = "bob@enzevalos.de"
    let userName = "bob"
    var user: MCOAddress =  MCOAddress.init(mailbox: "bob@enzevalos.de")
    var userKeyID: String = ""
    
    static let body = """
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dapibus id diam ac volutpat. Sed quis cursus ante. Vestibulum eget gravida felis. Nullam accumsan diam quis sem ornare lacinia. Aenean risus risus, maximus quis faucibus et, maximus at nunc. Duis pharetra augue libero, et congue diam varius eget. Nullam efficitur ex purus, non accumsan tellus laoreet hendrerit. Suspendisse gravida interdum eros, eu venenatis ante suscipit nec. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent pellentesque cursus sem, non ornare nunc commodo vel. Praesent sed magna at ligula ultricies sagittis malesuada non est. Nam maximus varius mauris. Etiam dignissim congue ligula eu porta. Nunc rutrum nisl id mauris efficitur ultrices. Maecenas sit amet velit ac mauris consequat sagittis at et lorem.
    """
    let smimeObj = SMIME()
    

    var certi: Certificate?
    var othercerti: Certificate?
    var otherAddr = "alice@enzevalos.de"
    var ourPrivateKeyFP: String?
            
    override func setUp() {
        super.setUp()
        guard let certi = try? Certificate(pem: test_key) else {
            XCTFail("No key!")
            return
        }
        userAdr = certi.eMails![0]
        _ = owner()
        ourPrivateKeyFP = smimeObj.addPrivateKey(keyPlusCertPEM: test_key)
        let _ = smimeObj.importCA(certs: [testCA])
        let _ = smimeObj.importCertForAddress(cert: test_key, addr: userAdr)
        print(createSMIMEUser(name: "Other"))
    }
    
    func createUser(adr: String = String.random().lowercased(), name: String = String.random()) -> MCOAddress {
        return MCOAddress.init(displayName: name, mailbox: adr.lowercased())
    }
    
    func owner() -> (MCOAddress, String) {
        let createExpectation = expectation(description: "Create key!")
        Logger.logging = false
        let user = createUser(adr: userAdr, name: userName)
        let userid = provider.createNewSecretKey(addr: userAdr, completionHandler: {error in
            if let error = error {
                XCTFail("Error while creating a new key: \(error)")
            }
            createExpectation.fulfill()
        })
        wait(for: [createExpectation], timeout: TimeInterval(30))
        UserManager.storeUserValue(userAdr as AnyObject, attribute: Attribute.userAddr)
        UserManager.storeUserValue(userName as AnyObject, attribute: Attribute.userDisplayName)
        UserManager.storeUserValue(userid as AnyObject, attribute: Attribute.prefSecretKeyID)
        return (user, userid)
    }


    override func tearDown() {
        super.tearDown()
        smimeObj.resetKeychain()
    }
    
    
    func createSMIMEUser(name: String = String.random()) -> (MCOAddress, String) {
        othercerti = try? Certificate(pem: test_key_other)
        otherAddr = othercerti!.eMails![0]
        let fp = smimeObj.importCertForAddress(cert: test_key_other, addr: otherAddr)
        let createExpectation = expectation(description: "Create public key record!")
        let property = PublicKeyProperties(fingerprint: fp, cryptoProtocol: .SMIME, origin: .FileTransfer, preferEncryption: nil, lastSeenInAutocryptHeader: nil, lastSeenSignedMail: nil, secretKeyProperty: nil, usedAddresses: [AddressProperties(email: otherAddr)])
        PersistentDataProvider.dataProvider.importNewData(from: [property], completionHandler: {error in
            if let error = error {
                XCTFail("Error while creating a new key: \(error)")
            }
            createExpectation.fulfill()
        })
        wait(for: [createExpectation], timeout: TimeInterval(30))
        return (user, fp)
    }
    
    func testCreateEncSMIMEMail() {
        let mail = OutgoingMail(toAddresses: [userAdr], ccAddresses: [], bccAddresses: [], subject: "encrypted smime mail", textContent: "Hello world", htmlContent: nil)
        guard let data = mail.createEncSMIMEData() else {
            XCTFail("No signed mail...")
            return
        }
        let inc = IncomingMail(rawData: data, uID: 0, folderPath: "Test", flags: .seen)
        guard let result = inc.cryptoObj else {
            XCTFail("No crypto object!")
            return
        }
        XCTAssertEqual(result.signatureState, .ValidSignature)
        XCTAssertEqual(result.encryptionState, .ValidedEncryptedWithCurrentKey)

    }

    
    func testCreateSignedSMIMEMail() {
        let mail = OutgoingMail(toAddresses: [otherAddr], ccAddresses: [], bccAddresses: [], subject: "signed smime mail", textContent: "Hello world", htmlContent: nil)
        guard let data = mail.createSignedSMIMEData() else {
            XCTFail("No signed mail...")
            return
        }
        let inc = IncomingMail(rawData: data, uID: 0, folderPath: "test", flags: .seen)
        guard let cryptoObj = inc.extractSignedSMIMEState() else {
            XCTFail("No crypto Object...")
            return
        }
        XCTAssertEqual(cryptoObj.signatureState, .ValidSignature)
    }
    
    
}
