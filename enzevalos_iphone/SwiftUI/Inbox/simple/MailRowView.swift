//
//  MailRowView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 27.10.20.
//  Modified by Chris Offner & Claire Bräuer in March 2021.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import SwiftUI

struct MailRowView <M: DisplayMail>: View {
    let mail: M
    @State var isLinkActive = false
    // ContactMailListView  toggles this to disable the onTapGesture modifier for the avatar
    var activateOnTap: Bool = true
    
    var body: some View {
       body_v3
    }
    
    private var content: String {
        get {
            let max = 500
            if mail.subject.count < max {
                return mail.subject + "\n" + mail.body.prefix(max - mail.subject.count).replacingOccurrences(of: "\n", with: " ")
            }
            return mail.subject
        }
    }
    
    private var body_v3: some View {
        HStack(alignment:.center){
            avatar
                .padding(StudySettings.useAvatars ? -15 : 0)
                .frame(width: 60, height: 60, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            VStack (alignment: .leading){
                HStack{
                    // Sender name
                   sender
                    
                    Spacer()
                    if !mail.displayAttachments.isEmpty {
                        Image(systemName: "paperclip")
                            .font(.caption)
                            .foregroundColor(.secondary)
                    }
                    // Arrival time
                    Text(mail.date.timeAgoText())
                        .font(.caption)
                }
                // Subject
                Text(content)
                    .font(.caption)
                    .lineLimit(3)
            }
        }
        .foregroundColor(mail.isRead ? .secondary : .primary)
    }
    
    private var body_v2: some View {
        VStack(alignment: .leading) {
            HStack{
                // Sender name
                Text(mail.sender.name)
                    .fontWeight(mail.isRead ? .regular : .light)
                    .lineLimit(2)
                
                Spacer()
                
                if !mail.displayAttachments.isEmpty {
                    Image(systemName: "paperclip")
                        .font(.caption)
                        .foregroundColor(.secondary)
                }
                
                // Arrival time
                Text(mail.date.timeAgoText())
                    .font(.caption)
                    .padding(.trailing, -20)
            }
            HStack {
                avatar
                    .padding(StudySettings.useAvatars ? -15 : 0)
                // Subject
                Text(content)
                    .font(.caption)
                    .lineLimit(3)
            }
            .padding(.top, -7)
            .frame(height: 45)
        }
        .foregroundColor(mail.isRead ? .secondary : .primary)
    }
    
    private var body_v1: some View {
        HStack {
            avatar
            
            VStack(alignment: .leading) {
                HStack {
                    // Sender name
                    Text(mail.sender.name)
                        .fontWeight(mail.isRead ? .regular : .medium)
                        .lineLimit(1)
                    
                    Spacer()
                    
                    // Arrival time
                    Text(mail.date.timeAgoText())
                        .font(.caption)
                }
                
                HStack {
                    // Subject
                    Text(mail.subject)
                        .font(.caption)
                        .lineLimit(2)
                    
                    Spacer()
                    
                    // Attachment indicator if relevant
                    if !mail.displayAttachments.isEmpty {
                        Image(systemName: "paperclip")
                            .font(.caption)
                            .foregroundColor(.secondary)
                    }
                }
            }
            .foregroundColor(mail.isRead ? .secondary : .primary)
        }
        .padding(4)
        .frame(height: 65)
    }
    

    private var avatar: some View {
        mail.sender.avatar
            .resizable()
            .aspectRatio(contentMode: .fit)
            .shadow(radius: 2)
            .opacity(mail.isRead ? 0.45 : 1)
            .onTapGesture(count: 1, perform: {isLinkActive = true})
            .allowsHitTesting(activateOnTap)
            .background(
                NavigationLink(destination: ContactView(contact: mail.sender), isActive: $isLinkActive) {
                    EmptyView()
                }
                .hidden()
            )
    }
    
    private var sender: some View {
        Text(mail.sender.name)
            .font(mail.sender.name.contains("@") ? .callout : .body)
            .fontWeight(mail.isRead ? .regular : .light)
            .lineLimit(1)
            .truncationMode(.head)
            .minimumScaleFactor(0.5)
    }
}

struct MailRowView_Previews: PreviewProvider {
    static var previews: some View {
        MailRowView(mail: ProxyData.PlainMail)
    }
}
