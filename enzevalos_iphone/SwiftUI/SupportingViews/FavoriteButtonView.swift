//
//  FavoriteButtonView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.12.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI


struct FavoriteButton: View {
    @Binding var isFav: Bool

    var body: some View {
        Button(action: {
            isFav.toggle()
        }) {
            Image(systemName: isFav ? "star.fill" : "star")
                .foregroundColor(isFav ? Color.yellow : Color.gray)

        }

    }

}


struct FavoriteButton_Previews: PreviewProvider {
    static var previews: some View {
        FavoriteButton(isFav: .constant(true))
        FavoriteButton(isFav: .constant(false))
    }
}

