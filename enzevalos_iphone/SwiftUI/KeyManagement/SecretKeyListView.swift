//
//  SecretKeyListView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 31.12.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct SecretKeyListView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(fetchRequest: SecretKeyRecord.FetchRequest)
    var keys: FetchedResults<SecretKeyRecord>
    
    var body: some View {
        List(keys, id: \.self){ key in
            NavigationLink(
                destination: KeyView(key: key),
                label: {
                    KeyRowView(key: key)
                })
        }
        .navigationBarTitle("Keys", displayMode: .large)
        .navigationBarItems(trailing: AddButton (action: {print("Add!")})) // TODO FIX -> How to input a password?
    }
}

struct SecretKeyListView_Previews: PreviewProvider {
    static var previews: some View {
        SecretKeyListView()
    }
}
