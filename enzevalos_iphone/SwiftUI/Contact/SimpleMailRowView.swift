//
//  SimpleMailRowView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 10.04.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

struct SimpleMailRowView <M: DisplayMail>: View {
    let mail: M
    
    private var content: String {
        get {
            let max = 500
            if mail.subject.count < max {
                return mail.subject + "\n" + mail.body.prefix(max - mail.subject.count)
            }
            return mail.subject
        }
    }
    
    var body: some View {
        HStack {            
            VStack(alignment: .leading) {
                HStack {
                    // Attachment indicator if relevant
                    if !mail.displayAttachments.isEmpty {
                        Image(systemName: "paperclip")
                            .font(.caption)
                            .foregroundColor(.secondary)
                    }
                    
                    Spacer()
                    
                    // Arrival time
                    Text(mail.date.timeAgoText())
                        .font(.caption)
                }
                
                HStack {
                    // Subject
                    Text(content)
                        .font(.caption)
                        .lineLimit(3)
                }
            }
            .foregroundColor(mail.isRead ? .secondary : .primary)
        }
        .padding(4)
        .frame(height: 65)
    }
}

/*
struct SimpleMailRow_Previews: PreviewProvider {
    static var previews: some View {
        SimpleMailRowView(mail: ProxyData.PlainMail)
    }
}*/
