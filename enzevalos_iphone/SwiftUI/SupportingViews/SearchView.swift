//
//  SearchView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 05.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI
import Combine

/**
 A SearchView for mails with a search segmented Picker to choose the search area.
 Open Problems: Deplay the search, s.t. we do not start a search after each input.
 */
struct SearchView: View {
    @Binding var searchText: String
    @Binding var searchType: SearchType
    @State private var editingSearchField: Bool = false
    
    var body: some View {
        VStack {
            HStack {
                searchFieldView
                if editingSearchField {
                    Button("Cancel") {
                        UIApplication.shared.endEditing(true)
                        searchText = ""
                    }
                    .foregroundColor(.accentColor)
                }
            }
            
            if editingSearchField {
                searchTypePicker
            }
        }
    }
    
    var searchFieldView: some View {
        HStack {
            Image(systemName: "magnifyingglass")
            
            TextField("Searchbar.Title", text: $searchText) { isEditing in
                withAnimation {
                    editingSearchField = isEditing
                }
            }
            .foregroundColor(.primary)
            
            Button {
                searchText = ""
            } label: {
                Image(systemName: "xmark.circle.fill")
                    .opacity(searchText == "" ? 0 : 1)
            }
        }
        .padding(8)
        .foregroundColor(.secondary)
        .background(Color(.secondarySystemBackground))
        .cornerRadius(10.0)
    }
    
    var searchTypePicker: some View {
        Picker("Search Filter", selection: $searchType) {
            ForEach(SearchType.allCases, id: \.self) { searchType in
                Text(searchType.rawValue)
            }
        }
        .pickerStyle(SegmentedPickerStyle())
    }
}

/**
 Helper about remove the keyboard when scrolling
 */

extension UIApplication {
    func endEditing(_ force: Bool) {
        self.windows
            .filter { $0.isKeyWindow }
            .first?
            .endEditing(force)
    }
}

struct ResignKeyboardOnDragGesture: ViewModifier {
    var gesture = DragGesture().onChanged{_ in
        UIApplication.shared.endEditing(true)
    }
    func body(content: Content) -> some View {
        content.gesture(gesture)
    }
}

extension View {
    func resignKeyboardOnDragGesture() -> some View {
        return modifier(ResignKeyboardOnDragGesture())
    }
}

/**
 Where are we looking for? Used in the search footer.
 */
enum SearchType: LocalizedStringKey, CaseIterable {
    case All = "All"
    case Sender = "Sender"
    case Subject = "Subject"
    case Body = "Body"
}
