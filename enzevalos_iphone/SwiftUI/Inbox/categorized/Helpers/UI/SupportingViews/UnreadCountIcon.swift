//
//  UnreadCountIcon.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 09.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

/// can be used by categories to display the amount of unread emails
struct UnreadCountIcon: View {
    
    var unreadCount = 0
    
    ///whether it only shows the amound of unread mails or also the total amount of mails in this category
    var totalCount:Int?
    
    var size:CGFloat = 10
    
    var withPlus = false
    
    var onPress = {}
    
    var body: some View {
        FloatingActionButton(
            radius:5,
            //elevation:0,//now depricated
            onShortPress: onPress
        ){
            VStack{
                Text(withPlus ? "+" : "" + "\(unreadCount)").font(.system(size: 18))
                if let tot=totalCount{
                    Text("/ \(tot)").font(.system(size: 10)).opacity(0.8)
                }
            }.frame(width: size, height: size)
        }
    }
}

struct UnreadCountIcon_Previews: PreviewProvider {
    static var previews: some View {
        VStack{
            Spacer()
            UnreadCountIcon(unreadCount: 10, totalCount: 15, size: 29)
            Spacer()
            Rectangle().frame(width: 100, height: 100, alignment: .center)
                .withUnreadCounter(5)
            Spacer()
        }
    }
}


///applying the unreadcounter to a view
extension View {
    func withUnreadCounter(
        _ unreadCount : Int,
        totalCount:Int?  = nil,
        size:CGFloat = 10,
        offset:CGSize = CGSize(width:10,height:10),
        withPlus:Bool = false,
        
        onPress:@escaping ()->Void = {}
    ) -> some View {
        self.modifier(WithUnreadCounter(
            unreadCount: unreadCount, totalCount: totalCount, size: size, withPlus: withPlus, offset: offset, onPress: onPress
        ))
    }
    func withUnreadCounter(
        _ amount : (Int,Int),
        size:CGFloat = 35,
        offset:CGSize = CGSize(width:5,height:8),
        withPlus:Bool = false,
        withoutTotal:Bool = false,
        
        onPress:@escaping ()->Void = {}
    ) -> some View {
        self.withUnreadCounter(
            amount.0, totalCount: withoutTotal ? nil : amount.1, size: size, offset: offset, withPlus: withPlus, onPress: onPress
        )
    }
}


///this is kinda of an 'umweg' , but i wanted to try the ViewModifier
fileprivate struct WithUnreadCounter: ViewModifier {
    
    var unreadCount = 0
    var totalCount:Int?
    
    var size:CGFloat = 10
    
    var withPlus = false
    
    var offset:CGSize = CGSize(width:10,height:10)
    
    var onPress = {}
    
    func body(content: Content) -> some View {
        ZStack(alignment: .bottomTrailing){
            content
            UnreadCountIcon(
                unreadCount: unreadCount, totalCount: totalCount, size: size, withPlus: withPlus, onPress: onPress
            )
                .offset(offset)
        }
            
    }
}
