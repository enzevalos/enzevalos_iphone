//
//  ScreenerCategory.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 31.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

/// The classic category with nothing special UI-wise , but it shows all the mails the other categories couldnt show (and depending on the user preferences just all mails)
struct UnsortedCategory : CategoryV {
    
    var showAllMails : Bool = false
    
    var name: String = NSLocalizedString("Category.Screener.name", comment: "No other cateogy is able to open this")
    
    var icon: Image = Image(systemName: "questionmark.folder.fill")
    
    //open all mails , and stay on top
    var filter: MailFilter = {_ in .shouldOpen}
    
    var andPredicate: NSPredicate {
        showAllMails
            ? NSPredicate(value: true)
            //query just the emails the other categories couldn't open (openability <= 0)
            : NSPredicate(format: "SUBQUERY(inCategory, $category, $category.categoryName != %@ AND $category.openability > 0).@count == 0",name)
    }
    
    func body(model: CategoryViewModel) -> AnyView {AnyView(
        ScrollView{ForEach(model.allMails){
            OptionsMailRow(mail: $0)
        }}
    )}

}
