//
//  Mailbot.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 28.01.19.
//  Copyright © 2019 fu-berlin. All rights reserved.
//

import Foundation

class Mailbot {
    
    static let raffleURL = ""
    //TODO FIX HERE OLIVER PRE RELEASE!
    
    static var entrySurveyURL: String {
        get {
            return "https://userpage.fu-berlin.de/~wieseoli/umfrage/limesurvey/index.php/952145?lang=en&ID=\(StudySettings.studyID)"
        }
    }
    
    
    
    static let welcomeBodyDE =
        """
    Liebe Nutzerin, lieber Nutzer von Letterbox,
    
    Ich bin Lette, der Letterbox-Mailbot. Wir freuen uns, dass du dich für unsere App entschieden hast. Magst du mir gleich auf die E-Mail antworten? Dann kannst du gleich erfahren, wie einfach es ist sichere Mails zu schreiben. Du und ich wissen dann das es bei dir auch klappt.

    Wir freuen uns auch jederzeit über Fragen oder einem Feedback von dir. Wir freuen uns auf deine Antwort!

    Vielen Dank und viele Grüße
    Lette
    
    PS: Diese Nachricht wurde automatisch auf deinem Gerät bei der Installation von Letterbox erzeugt und ist nur hier gespeichert.
    """
    
    
    static let welcomeBodyEn =
        """
    Hi,
    
    Thank you for using and testing our app! This is an automatically generated email and you can reply and test how to write a confidential mail. Our bot will reply and you can get an idea about confidential communication. We look forward to receiving your feedback!
    
    We are interested in your opinion, experience and impression of Letterbox and confidential mail communication in general. Maybe you can spend a few minutes (less than 10 minutes) filling out our short online survey?
    You can find our survey here:
        \(Mailbot.entrySurveyURL)
    
    By participating in our survey, you support research on confidential mail communication. We will share our findings in order to improve the usability of confidential mail communication.
    
    We welcome any question, comment or story about confidential mail or Letterbox. Just write us a (confidential) mail!
    
    Thanks and best
    Your Letterbox-Mailbot Vic
    
    """
    
    
    
    static func firstMail() {
        if !StudySettings.studyMode || !StudySettings.presentFirstQuestionaireMail {
            return
        }
        let subject = "Welcome to Letterbox"
        let body = welcomeBodyEn
        
        mailToParticipat(subject: subject, body: body)
    }
    
    
    private static func mailToParticipat(subject: String, body: String) {
        let senderAdr = SUPPORT_MAIL_ADR
        let sender = AddressProperties(email: "Letterbox-Team", name: senderAdr)
        var email = ""
        if let user = UserManager.loadUserValue(.userAddr) as? String {
            email = user
        }
        let user = AddressProperties(email: email)
        var pkproperty: PublicKeyProperties? = nil
        if let addrRecord = PersistentDataProvider.dataProvider.generateFetchedAddresesWithKeyResultsController(
            addresses: [senderAdr]).fetchedObjects?.first,
           let key = addrRecord.primaryKey {
            pkproperty = PublicKeyProperties(
                fingerprint: key.keyID,
                cryptoProtocol: key.type,
                origin: Origin(rawValue: key.origin) ?? Origin.FileTransfer,
                preferEncryption: AutocryptState.find(i: Int(key.preferEncryption)),
                lastSeenInAutocryptHeader: key.lastSeenInAutocryptHeader,
                lastSeenSignedMail: key.lastSeenSignedMail,
                secretKeyProperty: nil,
                usedAddresses: [sender])
        }
        
        let mail = MailProperties(
            messageID: UUID().uuidString,
            uid: 0,
            subject: subject, date: Date(),
            flags: 0,
            from: sender,
            to: [user],
            cc: [],
            bcc: [],
            folder: FolderProperties(
                delimiter: nil,
                uidValidity: nil,
                lastUpdate: nil,
                maxUID: nil,
                minUID: nil,
                path: UserManager.backendInboxFolderPath,
                parent: nil, children: nil, flags: 0),
            body: body,
            attachments: [],
            signatureState: SignatureState.ValidSignature.rawValue,
            encryptionState: EncryptionState.ValidedEncryptedWithCurrentKey.rawValue,
            signatureKey: pkproperty,
            decryptionKey: nil,
            autocryptHeaderKey: [],
            attachedPublicKeys: [],
            attachedSecretKeys: [])
        PersistentDataProvider.dataProvider.importNewData(from: [mail], completionHandler: {error in })
    }
}
