//
//  ReadModel.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 26.02.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import Foundation
import SwiftUI

enum UserAction {
    case Flag, Delete, Move, Reply, ReplyAll, Forward, Archive, Unread
}

enum ReadPart {
    case Security, Header, Body, Attachments
    
    var value: Int {
        get {
            switch self {
            case .Security:
                return 0
            case .Header:
                return 1
            case .Body:
                return 2
            case .Attachments:
                return 3
            }
        }
    }
    
}


class ReadModel <M: DisplayMail>: ObservableObject {
    @Published var dismissView = false
    @Published var currentTab = ReadPart.Body.value
    
    private var userAction:  UserAction?
    var mail: M
    
    init(mail: M) {
        self.mail = mail
        if mail.encryptionState == .UnableToDecrypt ||
            mail.warnings.count > 0 {
            currentTab = ReadPart.Security.value
        }
    }
    
    func onDisappear() {
        if let action = userAction {
            switch action {
            case .Delete:
                if let mail = mail as? MailRecord {
                    mail.delete()
                }
            case .Archive:
                if let mail = mail as? MailRecord {
                    mail.archive()
                }
            case .Unread:
                mail.markAsRead(isRead: false)
            default:
                mail.markAsRead(isRead: true)
            }
        } else {
            mail.markAsRead(isRead: true)
        }
    }
    
    func newUserAction(action: UserAction) -> PreMailData? {
        userAction = action
        switch action {
        case .Flag:
            // TODO
            return nil
        case .Delete:
            dismissView = true
            return nil
        case .Move:
            dismissView = true
            return nil
        case .Reply:
            return PreMailData(body: mail.preparePreviousMailBody(), subject: ResponseType.Reply.addPrefix(subject: mail.subject), to: [mail.sender.email], cc: [], bcc: [])
        case .ReplyAll:
            return PreMailData(body: mail.preparePreviousMailBody(), subject: ResponseType.Reply.addPrefix(subject: mail.subject), to: [mail.sender.email], cc: mail.tos.map({$0.email}) + mail.ccs.map({$0.email}), bcc: mail.bccs.map({$0.email}))
        case .Forward:
            return PreMailData(body: mail.preparePreviousMailBody(), subject: ResponseType.Forward.addPrefix(subject: mail.subject), to: [], cc: [], bcc: [])
        case .Archive:
            dismissView = true
            return nil
        case .Unread:
            dismissView = true
            return nil
        }
    }
    
    
}

struct PreMailData: Identifiable {
    var id = UUID()
    var body: String = ""
    var subject: String = ""
    var to: [String] = []
    var cc: [String] = []
    var bcc: [String] = []
    // TODO: Consider message ids for response mail
    
    static func invitePerson<M: DisplayMail> (mail: M) -> PreMailData? {
        let body = String(format: NSLocalizedString("inviteText", comment: "Body for the invitation mail"),StudySettings.studyID)
        let subject =  NSLocalizedString("inviteSubject", comment: "Subject for the invitation mail")
        var to = [String]()
        if mail.sender.primaryKey == nil {
            to.append(mail.sender.email)
        }
        to.append(contentsOf: mail.tos.filter({$0.primaryKey == nil}).map({$0.email}))
        to.append(contentsOf: mail.ccs.filter({$0.primaryKey == nil}).map({$0.email}))
        return PreMailData(body: body, subject: subject, to: to, cc: [], bcc: [])
    }
    
    static func askSenderToResendForPK <M: DisplayMail> (mail: M) -> PreMailData? {
        var body = NSLocalizedString("ReadView.PrefilledMail.AskForResend.Body", comment: "")
        body += mail.preparePreviousMailBody()
        // TODO add own PK?
        return PreMailData(body: body, subject: ResponseType.Reply.addPrefix(subject: mail.subject), to: [mail.sender.email], cc: [], bcc: [])
    }
    
    static func askSenderToSendPK <M: DisplayMail>(mail: M) -> PreMailData? {
        let body = NSLocalizedString("ReadView.PrefilledMail.AskForPK.Body", comment: "")
        let subject = NSLocalizedString("ReadView.PrefilledMail.AskForPK.Subject", comment: "")
        var to = [String]()
        if mail.sender.primaryKey == nil {
            to.append(mail.sender.email)
        }
        to.append(contentsOf: mail.tos.filter({$0.primaryKey == nil}).map({$0.email}))
        to.append(contentsOf: mail.ccs.filter({$0.primaryKey == nil}).map({$0.email}))
        return PreMailData(body: body, subject: subject, to: to, cc: [], bcc: [])
    }
    
    func askSenderToConfirm <M: DisplayMail>(mail: M) -> PreMailData? {
        let body = NSLocalizedString("didYouSendThis", comment: "Did you sent this mail?") + "\n" + mail.preparePreviousMailBody()
        return PreMailData(body: body, subject: ResponseType.Reply.addPrefix(subject: mail.subject), to: [mail.sender.email], cc: [], bcc: [])
    }
}
