//
//  MailRecord+CoreDataProperties.swift
//  
//
//  Created by Oliver Wiese on 27.10.20.
//
//

import Foundation
import CoreData


extension MailRecord {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MailRecord> {
        return NSFetchRequest<MailRecord>(entityName: "MailRecord")
    }

    @NSManaged public var body: String
    @NSManaged public var date: Date
    @NSManaged public var encryptionStateInt: Int16
    @NSManaged public var flag: Int16
    @NSManaged public var messageID: String?
    @NSManaged public var signatureStateInt: Int16
    @NSManaged public var subject: String
    @NSManaged public var uID: Int64
    @NSManaged public var xMailer: String?
    @NSManaged public var attachedAutocryptPublicKey: PublicKeyRecord?
    @NSManaged public var attachedPublicKeys: NSSet?
    @NSManaged public var attachedSecretKey: NSSet?
    @NSManaged public var bccAddresses: NSSet?
    @NSManaged public var ccAddresses: NSSet?
    @NSManaged public var decryptionKey: SecretKeyRecord?
    @NSManaged public var fromAddress: AddressRecord?
    @NSManaged public var includedAttachments: NSSet?
    @NSManaged public var inFolder: FolderRecord?
    @NSManaged public var signatureKey: PublicKeyRecord?
    @NSManaged public var toAddresses: NSSet?
}

// MARK: Generated accessors for attachedPublicKeys
extension MailRecord {

    @objc(addAttachedPublicKeysObject:)
    @NSManaged public func addToAttachedPublicKeys(_ value: PublicKeyRecord)

    @objc(removeAttachedPublicKeysObject:)
    @NSManaged public func removeFromAttachedPublicKeys(_ value: PublicKeyRecord)

    @objc(addAttachedPublicKeys:)
    @NSManaged public func addToAttachedPublicKeys(_ values: NSSet)

    @objc(removeAttachedPublicKeys:)
    @NSManaged public func removeFromAttachedPublicKeys(_ values: NSSet)

}

// MARK: Generated accessors for attachedSecretKey
extension MailRecord {

    @objc(addAttachedSecretKeyObject:)
    @NSManaged public func addToAttachedSecretKey(_ value: SecretKeyRecord)

    @objc(removeAttachedSecretKeyObject:)
    @NSManaged public func removeFromAttachedSecretKey(_ value: SecretKeyRecord)

    @objc(addAttachedSecretKey:)
    @NSManaged public func addToAttachedSecretKey(_ values: NSSet)

    @objc(removeAttachedSecretKey:)
    @NSManaged public func removeFromAttachedSecretKey(_ values: NSSet)

}

// MARK: Generated accessors for bccAddresses
extension MailRecord {

    @objc(addBccAddressesObject:)
    @NSManaged public func addToBccAddresses(_ value: AddressRecord)

    @objc(removeBccAddressesObject:)
    @NSManaged public func removeFromBccAddresses(_ value: AddressRecord)

    @objc(addBccAddresses:)
    @NSManaged public func addToBccAddresses(_ values: NSSet)

    @objc(removeBccAddresses:)
    @NSManaged public func removeFromBccAddresses(_ values: NSSet)

}

// MARK: Generated accessors for ccAddresses
extension MailRecord {

    @objc(addCcAddressesObject:)
    @NSManaged public func addToCcAddresses(_ value: AddressRecord)

    @objc(removeCcAddressesObject:)
    @NSManaged public func removeFromCcAddresses(_ value: AddressRecord)

    @objc(addCcAddresses:)
    @NSManaged public func addToCcAddresses(_ values: NSSet)

    @objc(removeCcAddresses:)
    @NSManaged public func removeFromCcAddresses(_ values: NSSet)

}

// MARK: Generated accessors for includedAttachments
extension MailRecord {

    @objc(addIncludedAttachmentsObject:)
    @NSManaged public func addToIncludedAttachments(_ value: AttachmentRecord)

    @objc(removeIncludedAttachmentsObject:)
    @NSManaged public func removeFromIncludedAttachments(_ value: AttachmentRecord)

    @objc(addIncludedAttachments:)
    @NSManaged public func addToIncludedAttachments(_ values: NSSet)

    @objc(removeIncludedAttachments:)
    @NSManaged public func removeFromIncludedAttachments(_ values: NSSet)

}

// MARK: Generated accessors for toAddresses
extension MailRecord {

    @objc(addToAddressesObject:)
    @NSManaged public func addToToAddresses(_ value: AddressRecord)

    @objc(removeToAddressesObject:)
    @NSManaged public func removeFromToAddresses(_ value: AddressRecord)

    @objc(addToAddresses:)
    @NSManaged public func addToToAddresses(_ values: NSSet)

    @objc(removeToAddresses:)
    @NSManaged public func removeFromToAddresses(_ values: NSSet)

}


// MARK: Generated accessors for inCategory
extension MailRecord {

    @objc(addInCategoryObject:)
    @NSManaged public func addToInCategory(_ value: CategoryRecord)

    @objc(removeInCategoryObject:)
    @NSManaged public func removeFromInCategory(_ value: CategoryRecord)

    @objc(addInCategory:)
    @NSManaged public func addToInCategory(_ values: NSSet)

    @objc(removeInCategory:)
    @NSManaged public func removeFromInCategory(_ values: NSSet)

}

