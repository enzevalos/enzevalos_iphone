//
//  FolderOverView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.10.20.
//  Modified by Chris Offner & Claire Bräuer in March 2021.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import SwiftUI

/// A view that lists all email folders and lets the user navigate to them.
struct FolderListView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(fetchRequest: FolderRecord.FetchRequest)
    private var folders: FetchedResults<FolderRecord>
    @State private var nameOfChosenFolder: String?
    
    var body: some View {
        NavigationView {
            List(folders, id: \.self) { f in
                let name = formatName(f.name)
                
                NavigationLink(destination: InboxView(folderPath: f.path ?? name, folderName: name)
                                .environment(\.managedObjectContext,
                                             PersistentDataProvider
                                                .dataProvider
                                                .persistentContainer
                                                .viewContext),
                               tag: f.name,
                               selection: $nameOfChosenFolder,
                               label: { FolderRowView(folder: f) })
            }
            .navigationBarTitle("Folders", displayMode: .large)
        }
        .onAppear { nameOfChosenFolder = "INBOX" }
    }
    
    /// Capitalizes first letter, lowercases all subsequent letters of string.
    ///
    /// - Parameter name: String to be formatted
    /// - Returns: Formatted string
    private func formatName(_ name: String) -> String {
        name.prefix(1).capitalized + name.dropFirst().lowercased()
    }
}

// Preview
struct FolderOverView_Previews: PreviewProvider {
    static var previews: some View {
        FolderListView()
    }
}
