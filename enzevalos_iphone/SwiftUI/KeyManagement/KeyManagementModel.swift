//
//  KeyManagementModel.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.12.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import Foundation

class KeyManagementModel: ObservableObject {
    private let fileManager = FileManager.default
    
    private var pgpKeyURL: [URL] {
        get {
            var keys: [URL] = []
            let dirs = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
            for url in dirs {
                if let files = try? fileManager.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: .skipsHiddenFiles) {
                    let keyFiles = files.filter{ $0.pathExtension == "asc"}
                    keys.append(contentsOf: keyFiles)
                }
            }
            return keys
        }
    }
    
    
    private var smimeCertificateURL: [URL] {
        get {
            var keys = [URL] ()
            let dirs = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
            for url in dirs {
                if let files = try? fileManager.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: .skipsHiddenFiles) {
                    let keyFiles = files.filter{Certificate.isCertificateFileEnding(ending: $0.pathExtension)}
                    keys.append(contentsOf: keyFiles)
                }
            }
            return keys
        }
    }
        
    var storedKeys: [TempKey] {
        get {
            var keys: [TempKey] = []
            // PGP
            let pgp = SwiftPGP()
            for url in pgpKeyURL {
                if let file = try? FileHandle(forReadingFrom: url) {
                    let data = file.readDataToEndOfFile()
                    var creationDate = Date()
                    if let attrs = try? fileManager.attributesOfItem(atPath: url.path) as NSDictionary, let date = attrs.fileCreationDate() {
                        creationDate = date
                    }
                    keys.append(contentsOf: pgp.readKeys(data: data,importDate: creationDate))
                }
            }
            // S/MIME
            for url in smimeCertificateURL {
                if (try? FileHandle(forReadingFrom: url)) != nil {
                    var creationDate = Date()
                    if let attrs = try? fileManager.attributesOfItem(atPath: url.path) as NSDictionary, let date = attrs.fileCreationDate() {
                        creationDate = date
                    }
                    var certString: String?
                    if url.pathExtension == "pem", let string = try? String(contentsOf: url, encoding: .ascii) {
                        certString = string
                    }
                    // TODO: Add p12 case...
                    if let certString = certString {
                        if let cert = try? Certificate(pem: certString) {
                            print("My fingerprint! \(cert.fingerPrint)")
                            keys.append(TempKey(smimeCert: cert, mailAddresses: [], importDate: creationDate))
                        }
                        // let cert = SMIME().addPrivateKey(keyPlusCertPEM: certString)//.importCerts(certs: [certString])
                        // let cert = getFingerprintFromPem(pem: certString)
                        // let cert = Certificate(pem: certString)
                        
                    }
                    
                }
            }
            return keys
        }
    }
    
    func importKey(key: TempKey, password: String?) -> Bool {
        if key.hasPassword, let password = password {
            let correctPW = key.testPassword(guess: password)
            if !correctPW {
                return false
            }
            let res = key.save()
            return res
        }
        if !key.hasPassword {
            return key.save()
        }
        
        return false
    }
    
    
    
    
    
    
}
