<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<model type="com.apple.IDECoreDataModeler.DataModel" documentVersion="1.0" lastSavedToolsVersion="17709" systemVersion="19H524" minimumToolsVersion="Automatic" sourceLanguage="Swift" userDefinedModelVersionIdentifier="">
    <entity name="AbstractKeyRecord" representedClassName="AbstractKeyRecord" isAbstract="YES" syncable="YES" codeGenerationType="class">
        <attribute name="cryptoProtocol" attributeType="Integer 16" defaultValueString="0" usesScalarValueType="YES"/>
        <attribute name="fingerprint" attributeType="String"/>
        <attribute name="isSecret" attributeType="Boolean" defaultValueString="NO" usesScalarValueType="YES"/>
        <uniquenessConstraints>
            <uniquenessConstraint>
                <constraint value="fingerprint"/>
                <constraint value="isSecret"/>
            </uniquenessConstraint>
        </uniquenessConstraints>
    </entity>
    <entity name="AddressRecord" representedClassName="AddressRecord" syncable="YES">
        <attribute name="displayname" optional="YES" attributeType="String"/>
        <attribute name="email" attributeType="String"/>
        <attribute name="phoneBookID" optional="YES" attributeType="String"/>
        <relationship name="inBCCField" toMany="YES" deletionRule="Nullify" destinationEntity="MailRecord" inverseName="bccAddresses" inverseEntity="MailRecord"/>
        <relationship name="inCCField" toMany="YES" deletionRule="Nullify" destinationEntity="MailRecord" inverseName="ccAddresses" inverseEntity="MailRecord"/>
        <relationship name="inFromField" toMany="YES" deletionRule="Nullify" destinationEntity="MailRecord" inverseName="fromAddress" inverseEntity="MailRecord"/>
        <relationship name="inToField" toMany="YES" deletionRule="Nullify" destinationEntity="MailRecord" inverseName="toAddresses" inverseEntity="MailRecord"/>
        <relationship name="primaryKey" optional="YES" maxCount="1" deletionRule="Nullify" destinationEntity="PublicKeyRecord" inverseName="isPrimaryKeyOf" inverseEntity="PublicKeyRecord"/>
        <relationship name="usedPublicKeys" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="PublicKeyRecord" inverseName="usedAddress" inverseEntity="PublicKeyRecord"/>
        <relationship name="usedSecretKeys" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="SecretKeyRecord" inverseName="usedAddress" inverseEntity="SecretKeyRecord"/>
        <uniquenessConstraints>
            <uniquenessConstraint>
                <constraint value="email"/>
            </uniquenessConstraint>
        </uniquenessConstraints>
    </entity>
    <entity name="AttachmentRecord" representedClassName="AttachmentRecord" syncable="YES" codeGenerationType="class">
        <attribute name="contentID" optional="YES" attributeType="String"/>
        <attribute name="data" attributeType="Binary"/>
        <attribute name="mimeType" optional="YES" attributeType="String"/>
        <attribute name="name" attributeType="String"/>
        <attribute name="type" optional="YES" attributeType="Integer 16" defaultValueString="0" usesScalarValueType="YES"/>
        <relationship name="isPartOfMail" optional="YES" maxCount="1" deletionRule="Nullify" destinationEntity="MailRecord" inverseName="includedAttachments" inverseEntity="MailRecord"/>
    </entity>
    <entity name="FolderRecord" representedClassName="FolderRecord" syncable="YES" codeGenerationType="class">
        <attribute name="delimiter" optional="YES" attributeType="String"/>
        <attribute name="flags" optional="YES" attributeType="Integer 16" defaultValueString="0" usesScalarValueType="YES"/>
        <attribute name="lastUpdate" optional="YES" attributeType="Date" usesScalarValueType="NO"/>
        <attribute name="maxUID" optional="YES" attributeType="Decimal" defaultValueString="0.0"/>
        <attribute name="minUID" optional="YES" attributeType="Decimal" defaultValueString="0.0"/>
        <attribute name="path" attributeType="String"/>
        <attribute name="uidValidity" optional="YES" attributeType="Decimal" defaultValueString="0.0"/>
        <relationship name="childrenFolder" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="FolderRecord" inverseName="partentFolder" inverseEntity="FolderRecord"/>
        <relationship name="mailsInFolder" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="MailRecord" inverseName="inFolder" inverseEntity="MailRecord"/>
        <relationship name="partentFolder" optional="YES" maxCount="1" deletionRule="Nullify" destinationEntity="FolderRecord" inverseName="childrenFolder" inverseEntity="FolderRecord"/>
        <uniquenessConstraints>
            <uniquenessConstraint>
                <constraint value="path"/>
            </uniquenessConstraint>
        </uniquenessConstraints>
    </entity>
    <entity name="MailRecord" representedClassName="MailRecord" syncable="YES">
        <attribute name="body" attributeType="String"/>
        <attribute name="date" attributeType="Date" usesScalarValueType="NO"/>
        <attribute name="encryptionStateInt" attributeType="Integer 16" minValueString="0" maxValueString="3" defaultValueString="0" usesScalarValueType="YES"/>
        <attribute name="flag" attributeType="Integer 16" defaultValueString="0" usesScalarValueType="YES"/>
        <attribute name="messageID" attributeType="String"/>
        <attribute name="signatureStateInt" attributeType="Integer 16" minValueString="-1" maxValueString="2" defaultValueString="0" usesScalarValueType="YES"/>
        <attribute name="subject" attributeType="String"/>
        <attribute name="uID" optional="YES" attributeType="Integer 64" defaultValueString="0" usesScalarValueType="YES"/>
        <attribute name="xMailer" optional="YES" attributeType="String"/>
        <relationship name="attachedAutocryptPublicKey" optional="YES" maxCount="1" deletionRule="Nullify" destinationEntity="PublicKeyRecord" inverseName="sendWithAutocryptHeader" inverseEntity="PublicKeyRecord"/>
        <relationship name="attachedPublicKeys" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="PublicKeyRecord" inverseName="sendWithMail" inverseEntity="PublicKeyRecord"/>
        <relationship name="attachedSecretKey" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="SecretKeyRecord" inverseName="sendWithMail" inverseEntity="SecretKeyRecord"/>
        <relationship name="bccAddresses" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="AddressRecord" inverseName="inBCCField" inverseEntity="AddressRecord"/>
        <relationship name="ccAddresses" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="AddressRecord" inverseName="inCCField" inverseEntity="AddressRecord"/>
        <relationship name="decryptionKey" optional="YES" maxCount="1" deletionRule="Nullify" destinationEntity="SecretKeyRecord" inverseName="decryptedMails" inverseEntity="SecretKeyRecord"/>
        <relationship name="fromAddress" optional="YES" maxCount="1" deletionRule="Nullify" destinationEntity="AddressRecord" inverseName="inFromField" inverseEntity="AddressRecord"/>
        <relationship name="includedAttachments" optional="YES" toMany="YES" deletionRule="Cascade" destinationEntity="AttachmentRecord" inverseName="isPartOfMail" inverseEntity="AttachmentRecord"/>
        <relationship name="inFolder" optional="YES" maxCount="1" deletionRule="Nullify" destinationEntity="FolderRecord" inverseName="mailsInFolder" inverseEntity="FolderRecord"/>
        <relationship name="signatureKey" optional="YES" maxCount="1" deletionRule="Nullify" destinationEntity="PublicKeyRecord" inverseName="signedMails" inverseEntity="PublicKeyRecord"/>
        <relationship name="toAddresses" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="AddressRecord" inverseName="inToField" inverseEntity="AddressRecord"/>
        <uniquenessConstraints>
            <uniquenessConstraint>
                <constraint value="messageID"/>
            </uniquenessConstraint>
        </uniquenessConstraints>
    </entity>
    <entity name="PublicKeyRecord" representedClassName="PublicKeyRecord" parentEntity="AbstractKeyRecord" syncable="YES" codeGenerationType="class">
        <attribute name="discovery" attributeType="Date" usesScalarValueType="NO"/>
        <attribute name="expire" optional="YES" attributeType="Date" usesScalarValueType="NO"/>
        <attribute name="lastSeenInAutocryptHeader" optional="YES" attributeType="Date" usesScalarValueType="NO"/>
        <attribute name="lastSeenSignedMail" optional="YES" attributeType="Date" usesScalarValueType="NO"/>
        <attribute name="origin" attributeType="Integer 16" defaultValueString="0" usesScalarValueType="YES"/>
        <attribute name="preferEncryption" optional="YES" attributeType="Integer 16" usesScalarValueType="YES"/>
        <attribute name="receivedMyPublicKey" attributeType="Boolean" defaultValueString="NO" usesScalarValueType="YES"/>
        <attribute name="verificationDate" optional="YES" attributeType="Date" usesScalarValueType="NO"/>
        <relationship name="isPrimaryKeyOf" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="AddressRecord" inverseName="primaryKey" inverseEntity="AddressRecord"/>
        <relationship name="mySecretKeyRecord" optional="YES" maxCount="1" deletionRule="Cascade" destinationEntity="SecretKeyRecord" inverseName="myPublicKeyRecord" inverseEntity="SecretKeyRecord"/>
        <relationship name="sendWithAutocryptHeader" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="MailRecord" inverseName="attachedAutocryptPublicKey" inverseEntity="MailRecord"/>
        <relationship name="sendWithMail" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="MailRecord" inverseName="attachedPublicKeys" inverseEntity="MailRecord"/>
        <relationship name="signedMails" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="MailRecord" inverseName="signatureKey" inverseEntity="MailRecord"/>
        <relationship name="usedAddress" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="AddressRecord" inverseName="usedPublicKeys" inverseEntity="AddressRecord"/>
    </entity>
    <entity name="SecretKeyRecord" representedClassName="SecretKeyRecord" parentEntity="AbstractKeyRecord" syncable="YES" codeGenerationType="class">
        <attribute name="exported" attributeType="Boolean" defaultValueString="NO" usesScalarValueType="YES"/>
        <attribute name="importedDate" optional="YES" attributeType="Date" usesScalarValueType="NO"/>
        <relationship name="decryptedMails" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="MailRecord" inverseName="decryptionKey" inverseEntity="MailRecord"/>
        <relationship name="myPublicKeyRecord" optional="YES" maxCount="1" deletionRule="Cascade" destinationEntity="PublicKeyRecord" inverseName="mySecretKeyRecord" inverseEntity="PublicKeyRecord"/>
        <relationship name="sendWithMail" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="MailRecord" inverseName="attachedSecretKey" inverseEntity="MailRecord"/>
        <relationship name="usedAddress" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="AddressRecord" inverseName="usedSecretKeys" inverseEntity="AddressRecord"/>
    </entity>
    <fetchRequest name="FetchAllPublicKeys" entity="PublicKeyRecord"/>
    <fetchRequest name="FetchAllSecretKeys" entity="SecretKeyRecord"/>
    <elements>
        <element name="AbstractKeyRecord" positionX="35.984375" positionY="-344.375" width="128" height="88"/>
        <element name="AddressRecord" positionX="91.82421875" positionY="34.6484375" width="128" height="193"/>
        <element name="AttachmentRecord" positionX="-602.1484375" positionY="-73.89453125" width="128" height="133"/>
        <element name="FolderRecord" positionX="-498.28125" positionY="219.9140625" width="128" height="193"/>
        <element name="MailRecord" positionX="-291.875" positionY="-121.84375" width="128" height="343"/>
        <element name="PublicKeyRecord" positionX="141.7421875" positionY="-257.57421875" width="128" height="28"/>
        <element name="SecretKeyRecord" positionX="-107.44140625" positionY="-231.81640625" width="128" height="133"/>
    </elements>
</model>