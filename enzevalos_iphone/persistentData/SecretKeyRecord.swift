//
//  SecretKeyRecord.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 10.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import CoreData

extension SecretKeyRecord {
    static var FetchRequest: NSFetchRequest<SecretKeyRecord> {
        let request: NSFetchRequest<SecretKeyRecord> = NSFetchRequest<SecretKeyRecord>(entityName: SecretKeyRecord.entityName)
        request.sortDescriptors = [NSSortDescriptor(key: "importedDate", ascending: true)]
        return request
    }
}

extension SecretKeyRecord {
    static let entityName = "SecretKeyRecord"

    func update(property: SecretKeyProperties) {
        self.fingerprint = property.fingerprint
        self.cryptoProtocol = property.cryptoProtocol.asInt()
        self.isSecret = true
        
        // TODO? Fix -> knownDate? Import as Int?
        if self.importedDate == nil {
            self.importedDate = Date()
        }
    }
}
// TODO: FIX Some stuff
extension SecretKeyRecord: DisplayKey {
    var keyID: String {
        return fingerprint ?? ""
    }
    
    var discovery: Date? {
        return self.importedDate
    }
    
    var lastSeen: Date? {
        return nil
    }
    
    var type: CryptoScheme {
        return CryptoScheme.find(i: Int(self.cryptoProtocol))
    }
    
    var isPreferedKey: Bool {
        return PersistentDataProvider.prefKeyID == self.fingerprint
    }
    
    var signedMailsCounter: Int {
        return 0
    }
    
    var sentMailsCounter: Int {
        return 0
    }
}
