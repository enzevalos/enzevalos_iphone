//
//  CustomObservers.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 11.02.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import Foundation
import Combine

protocol CustomObservableObjectP {
    associatedtype SendValue
    var objectWillChangeR : PassthroughSubject<SendValue, Never> { get set }
}

class CustomObservableObject<T> : CustomObservableObjectP{
    typealias SendValue = T
    var objectWillChangeR = PassthroughSubject<SendValue, Never>()
    init() {
        //this is how we make all the CPublished vars send updates
        Mirror(reflecting: self).children.forEach { child in
            if let observedProperty = child.value as? CustomObservableObject<T> {
                observedProperty.objectWillChangeR = self.objectWillChangeR
            }
        }
    }
}

@propertyWrapper class CPublished<Wrapped,Send> : CustomObservableObject<Send> {
    
    var wrappedValue: Wrapped {
        didSet {
            objectWillChangeR.send(reason)
        }
    }

    let reason : SendValue
    init(wrappedValue: Wrapped, _ reason: SendValue) {
        self.reason = reason
        self.wrappedValue = wrappedValue
    }
}







//TODO: make this rebuild a View
//otherwise it is useless
@propertyWrapper class CustomObservedObject<Wrapped : CustomObservableObjectP> {
    
    typealias SendValue = Wrapped.SendValue
    
    var wrappedValue: Wrapped {
        didSet {
            wrappedValue.objectWillChangeR.send(reason)
        }
    }
    
    let reason : SendValue
    init(wrappedValue: Wrapped, _ reason: SendValue) {
        self.reason = reason
        self.wrappedValue = wrappedValue
    }
    
}
