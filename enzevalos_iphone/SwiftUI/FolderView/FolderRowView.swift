//
//  FolderRow.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct FolderRowView: View {
    // TODO: increase padding for each delimiter? -> Subfolder level?
    public var folder: DisplayFolder
    var body: some View {
        HStack {
            folder.icon.foregroundColor(.accentColor)
            Text(folder.name)
            Spacer()
            Text("\(folder.mails)")
        }
        .padding()
    }
}

struct FolderRow_Previews: PreviewProvider {
    static var previews: some View {
        FolderRowView(folder: ProxyFolder(name: "Inbox"))
    }
}
