//
//  RecipientListView.swift
//  letterbox_prototyping
//
//  Created by Chris Offner & Claire Bräuer on 11.03.21.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see https://www.gnu.org/licenses/.
//

import SwiftUI

/// A view that lists contacts for selection as recipients.
struct RecipientListView: View {
    @EnvironmentObject var model: RecipientFieldModel
    @State private var sortByName = true
    
    var body: some View {
        VStack(alignment: .leading) {
            // Sorting options
            HStack {
                Text("SortBy")
                    .foregroundColor(.secondary)
                Button(sortByName ? "Name" : "LastContacted") {
                    sortByName.toggle()
                }
            }
            .font(.caption)
            .transition(.opacity)
            .id(sortByName ? "Name" : "LastContacted")
            
            // Contact list
            ScrollView {
                ForEach(model.getContacts(sortBy: sortByName ? .name : .recency)) { contact in
                    RecipientRowView(contact: contact)
                    Divider()
                }
            }
        }
    }
}
