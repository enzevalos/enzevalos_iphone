//
//  KeyView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 31.12.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI


// TODO: Favorite button does not work -> Add a model?

struct KeyView: View {
    public var key: DisplayKey
    @State var isPrefed = false
    
    init(key: DisplayKey) {
        self.key = key
        isPrefed = key.isPreferedKey
    }
    var body: some View {
        VStack (alignment: .leading){
            // TODO: Add an icon (PGP vs SMIME or a fingerprint or avatar ...
            HStack {
                Text(key.keyID)
                Spacer()
                FavoriteButton(isFav: $isPrefed)
            }
            backgroundInfo
            Spacer()
        }
        .padding()
    }
    
    var backgroundInfo: some View {
        VStack (alignment: .leading, spacing: 3){
            row(label: "ContactView.KnownSince", data: key.discovery?.dateToString, action: nil)
            row(label: "ContactView.LastMail.Name", data: key.lastSeen?.dateToString ?? "", action: nil)
           
        }
    }

    private func row(label: LocalizedStringKey, data: String?, action: ((()->Void)?)) -> some View {
        HStack {
            Text(label)
                .foregroundColor(.gray)
            if let data = data, let action = action {
                Button(data, action: action)
            } else if let data = data{
                Text(data)
                    .foregroundColor(.gray)
            }
        }
    }
}

struct KeyView_Previews: PreviewProvider {
    static var previews: some View {
        KeyView(key: ProxyPublicKey(keyID: "232312323", discovery: nil, lastSeen: nil, type: .PGP, signedMailsCounter: 0, sentMailsCounter: 0))
    }
}
