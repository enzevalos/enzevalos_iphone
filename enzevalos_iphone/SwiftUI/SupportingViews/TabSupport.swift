//
//  TabSupport.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.10.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//
import SwiftUI

struct Tab {
    let id = UUID()
    
    var tab : Int
    var image       : Image         = Image(systemName: "tag.fill")
    var description : LocalizedStringKey        = "---"
    var content     : AnyView       = AnyView(Text("Still in development"))
}
