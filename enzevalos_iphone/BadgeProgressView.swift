//
//  BadgeProgressView.swift
//  enzevalos_iphone
//
//  Created by Sezzart on 23.03.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct BadgeProgressView: View {
    
    let badge: Badge
    
    var body: some View {
        VStack {
            titleBar
            Divider()
            List {
                /*ForEach(GamificationData.sharedInstance.subBadgesforBadge(badge: badge.type).map { Badge(type: $0.type, description: $0.displayName, imageOn: $0.onName, imageOff: $0.offName, isActive: $0.isAchieved()) }) { subBadge in
                    Quest(isQuestCompleted: subBadge.isActive, description: subBadge.description, imageOn: subBadge.imageOn, imageOff: subBadge.imageOff)
                }*/
            }
        }
    }
    
    private var titleBar: some View {
        HStack{
            Image(badge.imageOn)
                .resizable()
                .scaledToFit()
                .frame(width: 50,height: 50)
                .padding(5)
            
            Text(badge.description)
            
            Spacer()
        }
    }
}


struct Quest: View {
    let isQuestCompleted: Bool
    let description: String
    let imageOn: String
    let imageOff: String
    
    init(isQuestCompleted: Bool, description: String, imageOn: String, imageOff: String) {
        self.isQuestCompleted = isQuestCompleted
        self.description = description
        self.imageOn = imageOn
        self.imageOff = imageOff
    }
    
    var body: some View {
        HStack {
            Image(isQuestCompleted ? imageOn : imageOff)
                .resizable()
                .scaledToFit()
                .frame(width: 40,height: 40)
                .padding(10)
            Text(description)
        }
    }
}

struct BadgeProgressView_Previews: PreviewProvider {
    static var previews: some View {
        BadgeProgressView(badge: Badge(description: NSLocalizedString("Mailmaster", comment:"Mailmaster badge" ), imageOn: "verschluesseltOn", imageOff: "verschluesseltOff", isActive: true))
    }
}
