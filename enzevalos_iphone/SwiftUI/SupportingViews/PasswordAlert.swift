//
//  PasswordAlert.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 01.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI

struct PasswortAlert<Presenting>: View where Presenting: View {

    @Binding var isShowing: Bool
    @Binding var password: String
    let presenting: Presenting

    var body: some View {
        GeometryReader { (deviceSize: GeometryProxy) in
            ZStack {
                self.presenting
                    .disabled(isShowing)
                VStack{
                    Text("Dialog.Password.Title")
                        .font(.title)
                        .padding(.vertical)
                    Text("Dialog.Password.Body.First")
                    PasswordView(password: $password)
                    Button(action: {
                        print("Import!")
                    }, label: {
                        Text("Dialog.Password.Button.Ok")
                            .frame(maxWidth: .infinity, minHeight: 20, maxHeight: 40)
                            .overlay(RoundedRectangle(cornerRadius: 20)
                                        .stroke(Color.blue, lineWidth: 2)
                                        )
                            .padding(.vertical, 10)
                            .padding(.horizontal, 20)
                    })
                    Button(action: {
                        print("No Import!")
                    }, label: {
                        Text("Dialog.Password.Button.No")
                            .foregroundColor(Color.red)
                            .frame(maxWidth: .infinity, minHeight: 20, maxHeight: 40)
                            .overlay(RoundedRectangle(cornerRadius: 20)
                                        .stroke(Color.red, lineWidth: 2)
                                        )
                            .padding(.vertical, 10)
                            .padding(.horizontal, 20)
                    })
                    Spacer()
                }
                .padding()
                .background(Color.white)
                .frame(
                    width: deviceSize.size.width*0.7,
                    height: deviceSize.size.height*0.7
                )
                .shadow(radius: 1)
                .opacity(self.isShowing ? 1 : 0)
            }
        }
    }
}

extension View {

    func passwortAlert(isShowing: Binding<Bool>,
                       password: Binding<String>) -> some View {
        PasswortAlert(isShowing: isShowing,
                      password: password,
                       presenting: self)
    }

}
