//
//  SmallContactListView.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 06.04.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

import SwiftUI

struct SmallContactListView <C: DisplayContact>: View {
    let contacts: [C]
    var title: String
    @State var showList = true
    
    
    var body: some View {
        VStack (alignment: .leading, spacing: 10){
            HStack {
                Text(title)
                    .font(.headline)
                Button (action: {
                    self.showList.toggle()
                }) {
                    
                    Image(systemName: showList ? "chevron.up" : "chevron.down")
                        .resizable()
                        .frame(width: 12.0, height: 7.0)
                }
                Spacer()
            }
            
            if showList {
                ForEach(contacts, id: \.email) {contact in
                    NavigationLink(destination: ContactView<C>(contact: contact)) {
                        HStack {
                            CircleImage(image: contact.avatar, radius: 40)
                            VStack (alignment: .leading, spacing: 2){
                                Text(contact.name)
                                    .font(.headline)
                                Text(contact.email)
                                    .foregroundColor(.secondary)
                            }
                            Spacer()
                            Image(systemName: "chevron.right")
                        }
                    }
                }
            }
        }
        .padding(10)
    }
    
    private func goToContact(contact: C) {
        /* TODOguard let con = contact.keyRecord else {
         return
         } */
        return
        // AppDelegate.getAppDelegate().readViewCoordinator?.pushContactView(contact: con)
    }
}


struct SmallContactListView_Previews: PreviewProvider {
    static let alice = ProxyData.Alice
    static let bob = ProxyData.Bob
    static let charlie = ProxyData.Charlie
    
    static var previews: some View {
        VStack{
            SmallContactListView(contacts: [alice,bob,charlie], title: "To")
            
            SmallContactListView(contacts: [alice,bob,charlie], title: "To", showList: false)
        }
    }
}
