//
//  CoreAddress.swift
//  enzevalos_iphone
//
//  Created by Oliver Wiese on 30.09.20.
//  Copyright © 2020 fu-berlin. All rights reserved.
//

/* TODO: usedKeys -> associted?
 */
import Contacts
import SwiftUI
import CoreData

extension AddressRecord {
    /// Enum defining which key to to sort contacts by (name, recency).
    enum SortBy: String {
        case name = "displayname"
        case recency = "email" // TODO: change to "last" once that data is implemented
    }
    
    private static var sorting: [NSSortDescriptor] {
        return [NSSortDescriptor(key: "email", ascending: true)]
    }
    
    var mcoAddress: MCOAddress {
        get {
            let name = displayname ?? email
            return MCOAddress.init(displayName: name, mailbox: email)
        }
    }
    
    static func lookForPrefix(prefix: String) -> NSFetchRequest<AddressRecord> {
        let freq = NSFetchRequest<AddressRecord>(entityName: AddressRecord.entityName)
        freq.sortDescriptors = sorting
        freq.fetchLimit = 30
        freq.propertiesToFetch = ["email", "displayname"]
        let predicate = NSPredicate(format: "email BEGINSWITH %@", prefix.lowercased())
        freq.predicate = predicate
        return freq
    }
    
    static func lookForExisting(email: String) -> NSFetchRequest<AddressRecord> {
        let freq = NSFetchRequest<AddressRecord>(entityName: AddressRecord.entityName)
        freq.sortDescriptors = sorting
        freq.fetchLimit = 30
        freq.propertiesToFetch = ["email", "displayname"]
        let predicate = NSPredicate(format: "email == %@", email.lowercased())
        freq.predicate = predicate
        return freq
    }
    
    static func contains(_ text: String) -> NSFetchRequest<AddressRecord> {
        let freq = NSFetchRequest<AddressRecord>(entityName: AddressRecord.entityName)
        freq.sortDescriptors = sorting
        freq.fetchLimit = 30
        freq.propertiesToFetch = ["email", "displayname"]
        let predicate = NSPredicate(format: "email CONTAINS %@", text.lowercased())
        freq.predicate = predicate
        return freq
    }
    
    static func all(sortBy: SortBy) -> NSFetchRequest<AddressRecord> {
        let freq = NSFetchRequest<AddressRecord>(entityName: AddressRecord.entityName)
        freq.sortDescriptors = [NSSortDescriptor(key: sortBy.rawValue, ascending: true)]
        freq.fetchLimit = 200
        freq.propertiesToFetch = ["email", "displayname"]
        return freq
    }
}

extension AddressRecord {
    static var entityName = "AddressRecord"
    
    /// Updates AddressRecord with information from AddressProperties.
    func update(with addressProperties: AddressProperties) {
        email = addressProperties.email.lowercased()
        if let name = addressProperties.name {
            self.displayname = name
        }
    }
    
    public var name: String {
        get {
            // TODO: Look up CN contact name!
//            if let contact = self.phoneBookID {}
            if let displayname = self.displayname {
                return displayname
            }
            return self.email
        }
    }
    
    var mails: [MailRecord] {
        get {
            var mails = [MailRecord]()
            mails.append(contentsOf: mailsFromSet(set: self.inToField))
            mails.append(contentsOf: mailsFromSet(set: self.inCCField))
            mails.append(contentsOf: mailsFromSet(set: self.inBCCField))
            mails.append(contentsOf: mailsFromSet(set: self.inFromField))
            return mails
        }
    }
    
    private func mailsFromSet(set: NSSet?) -> [MailRecord] {
        if let set = set, let mails = set.allObjects as? [MailRecord] {
            return mails
        }
        return []
    }
    
    public var hasPublicKey: Bool {
        get {
            if let keys = self.usedPublicKeys {
                return keys.count > 0
            }
            return false
        }
    }
    
    public var isUser: Bool {
        if let user = UserManager.loadUserValue(.userAddr) as? String {
            return user == email
        }
        return false
    }
}

extension AddressRecord: DisplayContact {
    var emails: [String] {
        get{
            var addresses = [String]()
            addresses.append(self.email)
            if let keys = self.usedPublicKeys as? Set<PublicKeyRecord>  {
                for key in keys {
                    if let set = key.usedAddress as? Set<String> {
                        addresses.append(contentsOf: set)
                    }
                }
            }
            return addresses
        }
    }
    
    
    
    typealias C = AddressRecord
    
    var avatar: Image {
        return ContactHandler.getImageOrDefault(addr: self)
    }
    
    var isInContactBook: Bool {
        return self.phoneBookID != nil
    }
    
    var otherAddresses: [C] {
        get{
            var addresses = [AddressRecord] ()
            addresses.append(self)
            if let keys = self.usedPublicKeys as? Set<PublicKeyRecord>  {
                for key in keys {
                    if let set = key.usedAddress as? Set<AddressRecord> {
                        addresses.append(contentsOf: set)
                    }
                }
            }
            return addresses
        }
    }
    
    var keyIDs: [String] {
        if let keys = usedPublicKeys?.allObjects as? [PublicKeyRecord] {
            return keys.map{($0.fingerprint ?? "")}.filter{$0 != ""}
        }
        return []
    }
    
    var previousMails: Int {
        return self.inFromField?.count ?? 0
    }
    
    var previousResponses: Int {
        var responses = 0
        responses = self.inToField?.count ?? 0 + responses
        responses = self.inBCCField?.count ?? 0 + responses
        responses = self.inCCField?.count ?? 0 + responses
        return responses
    }
    
    var hasSimilarContacts: Bool {
        return false // TODO
    }
    
    var similarContacts: [String] {
        return []
    }
    
    var discovery: Date? {
        return nil //TODO
    }
    
    /// Date of last correspondence.
    var last: Date? {
        return nil // TODO
    }
    
    /// Casual string for recency of correspondence.
    var lastHeardFrom: LocalizedStringKey {
        if let lastHeard = last {
            let now = Date()
            switch now.timeIntervalSince(lastHeard) {
            case 0..<259200:
                return "AddressRecord.lastHeardFrom.recently"
            case 259200..<604800:
                return "AddressRecord.lastHeardFrom.lastWeek"
            case 259200..<2592000:
                return "AddressRecord.lastHeardFrom.lastMonth"
            default:
                return "AddressRecord.lastHeardFrom.LongTimeAgo"
            }
        } else {
            return "AddressRecord.lastHeardFrom.Never"
        }
    }
}
