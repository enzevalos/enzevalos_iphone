//
//  Chat_PeopleRow.swift
//  enzevalos_iphone
//
//  Created by hanneh00 on 25.01.21.
//  Copyright © 2021 fu-berlin. All rights reserved.
//

import SwiftUI
import TagNavVi

struct Chat_PeopleRow: View {
    
    let chat:SingleChat
    
    @State var opens:Bool = false
    
    var body : some View {
        HStack{
            profile
            VStack {
                nameView
                Spacer().frame(height: 5)
                HStack{
                    Text(chat.messages[0].body)
                        .font(.caption)
                        .lineLimit(2)
                    Spacer()
                    Text(chat.timeStamp.timeAgoText())
                        .font(.caption)
                }
            }.onTapGesture {
                opens.toggle()
            }
            TagNavigationLink(parentTag: 1, destination: ChatView(chat:chat), isActive: $opens) {
                EmptyView()
            }
        }
    }
    
    private var profile : some View {
        chat.partner.avatar
            .resizable()
            .frame(width: 50, height: 50)
    }
    private var nameView : some View {
        Text(chat.partner.name.split(separator: "@")[0])
            .frame(maxWidth: 300, alignment: .leading)
            .lineLimit(1)
            .font(.subheadline)
    }
}

struct Chat_PeopleRow_Previews: PreviewProvider {
    static var previews: some View {
        EmptyView()//Chat_PeopleRow()
    }
}
